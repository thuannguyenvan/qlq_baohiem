﻿using System.Collections.Generic;
using System.Linq;
using FX.Data;
using QLQ.Core.Domain;
using QLQ.Core.IService;
namespace QLQ.Core.ServiceImp
{
    public class DM_HOPDONGService : BaseService<QLQ_DMHOPDONG, long?>, IDM_HOPDONGService
    {
        public DM_HOPDONGService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {
        }

        public QLQ_DMHOPDONG GetById(long? id)
        {
            var result = Query.FirstOrDefault(x => x.hopdong_id == id);
            return result;
        }

        public QLQ_DMHOPDONG GetByMa(string ma)
        {
            var result = Query.FirstOrDefault(x => x.ma_hopdong == ma);
            return result;
        
        }

        public List<QLQ_DMHOPDONG> Search(string timkiem)
        {
            if (timkiem == null)
                timkiem = "";
            return Query.Where(x => x.ma_hopdong.ToUpper().Contains(timkiem.ToUpper()) || x.ten_hopdong.ToUpper().Contains(timkiem.ToUpper())).ToList();
        }
    }
}
