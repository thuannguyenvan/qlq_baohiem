﻿using FX.Data;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using System.Linq;

namespace QLQ.Core.ServiceImp
{
    public class HOSOService : BaseService<HOSO, long>, IHOSOService
    {
        public HOSOService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {
        }

        public HOSO GetById(long? hosoId)
        {
            return Query.FirstOrDefault(x => x.HOSO_ID == hosoId);
        }

        /// <summary>
        /// Check id ngân hàng xem có tồn tại trong bảng hồ sơ không?
        /// </summary>
        /// <param name="id">Ngân hàng Id</param>
        /// <returns></returns>
        public bool CheckExistNganHangIdInHoSo(long id)
        {
            var result = Query.FirstOrDefault(x => x.NGANHANG_ID == id);
            if (result != null)
            {
                return true;
            }
            return false;
        }
    }
}
