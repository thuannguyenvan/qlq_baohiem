﻿using FX.Data;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using System.Collections.Generic;
using System.Linq;

namespace QLQ.Core.ServiceImp
{
    public class DM_CHUCDANHService : BaseService<QLQ_DMCHUCDANH, long?>, IDM_CHUCDANHService
    {
        public DM_CHUCDANHService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {
        }

        public QLQ_DMCHUCDANH GetById(long? id)
        {
            var result = Query.FirstOrDefault(x => x.chucdanh_id == id);
            return result;
        }
        public QLQ_DMCHUCDANH GetByMa(string ma)
        {
            var result = Query.FirstOrDefault(x => x.ma_chucdanh == ma);
            return result;
        }

        public List<QLQ_DMCHUCDANH> Search(string timkiem)
        {
            if (timkiem == null)
                timkiem = "";
            return Query.Where(x => x.ma_chucdanh.ToUpper().Contains(timkiem.ToUpper()) || x.ten_chucdanh.ToUpper().Contains(timkiem.ToUpper())).ToList();
        }
    }
}
