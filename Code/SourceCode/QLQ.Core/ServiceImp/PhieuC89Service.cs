﻿using FX.Data;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using QLQ.Core.Common;
using QLQ.Core.CustomView;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using System.Collections.Generic;
using System.Data;
using System.Xml.Serialization;

namespace QLQ.Core.ServiceImp
{
    public class PhieuC89Service : BaseService<PhieuLaiPhaiThuTrongNamC89, long>, IPhieuC89Service
    {
        public PhieuC89Service(string sessionFactoryConfigPath) : base(sessionFactoryConfigPath)
        {
        }

        public List<PhieuC89Model> Search(PhieuC89SearchModel request)
        {
            using (var transaction = NHibernateSession.BeginTransaction())
            {
                using (var command = NHibernateSession.Connection.CreateCommand())
                {
                    command.CommandText = "QLQ_PHIEUC89_PKG.SP_GETDANHSACHPHIEUC89";
                    command.CommandType = CommandType.StoredProcedure;
                    var paramOut = Helpers.GetParam(command, OracleDbType.XmlType, "p_out", direction: ParameterDirection.Output);
                    command.Parameters.Add(paramOut);
                    command.Parameters.Add(Helpers.GetParam(command, OracleDbType.Decimal, "p_hinhthucid", request.HinhThucDauTuId));
                    command.Parameters.Add(Helpers.GetParam(command, OracleDbType.Decimal, "p_dautuid", request.DauTuId));
                    command.Parameters.Add(Helpers.GetParam(command, OracleDbType.Decimal, "p_danhmuchopdongid", request.DanhMucHopDongId));
                    command.Parameters.Add(Helpers.GetParam(command, OracleDbType.Decimal, "p_hopdongid", request.HopDongId));
                    command.Parameters.Add(Helpers.GetParam(command, OracleDbType.Date, "p_thoigian", request.ThoiGianDate));
                    transaction.Enlist(command);
                    command.ExecuteNonQuery();
                    var reader = ((OracleXmlType)paramOut.Value).GetXmlReader();
                    var serializer = new XmlSerializer(typeof(PhieuC89ListModel));
                    var model = (PhieuC89ListModel)serializer.Deserialize(reader);
                    if (model != null && model.Items != null)
                    {
                        return model.Items;
                    }
                    return new List<PhieuC89Model>();
                }
            }
        }
    }
}
