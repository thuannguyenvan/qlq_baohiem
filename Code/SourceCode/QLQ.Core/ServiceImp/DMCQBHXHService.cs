﻿using FX.Data;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using System.Linq;

namespace QLQ.Core.ServiceImp
{
    public class DMCQBHXHService : BaseService<QLQ_DMCQBHXH,long>,IDMCQBHXHService
    {
        public DMCQBHXHService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {
        }
        public string TimKiem
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["DMCQBHXH_TimKiem"] != null)
                {
                    return (string)System.Web.HttpContext.Current.Session["DMCQBHXH_TimKiem"];
                }
                return null;
            }
            set
            {
                System.Web.HttpContext.Current.Session["DMCQBHXH_TimKiem"] = value;
                //  this.TimCTu_Filter = value;

            }
        }
        public IQueryable<QLQ_DMCQBHXH> GetAllData()
        {
            var list = Query;
            if(TimKiem!=null)
            {
                list = list.Where(t => t.MA_CQBHXH.Contains(TimKiem) || t.TEN_CQBHXH.Contains(TimKiem));
            }
            return list;
        }
        public QLQ_DMCQBHXH GetById(long? id)
        {
            QLQ_DMCQBHXH dvql = new QLQ_DMCQBHXH();
            if(Query!=null)
            {
                dvql = Query.FirstOrDefault(t=>t.CQBHXH_ID==id);
            }
            return dvql;
        }
        
    }
}
