﻿using FX.Data;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using System.Collections.Generic;
using System.Linq;

namespace QLQ.Core.ServiceImp
{
    public class QLQ_PERMISSIONService : BaseService<QLQ_PERMISSION, long?>, IQLQ_PERMISSIONService
    {
        public QLQ_PERMISSIONService(string sessionFactoryConfigPath) : base(sessionFactoryConfigPath)
        {
        }

        public QLQ_PERMISSION GetById(long? id)
        {
            return Query.FirstOrDefault(x => x.permission_id == id);
        }

        public List<QLQ_PERMISSION> GetByObjId(int? id)
        {
            var data = Query.Where(x=>x.object_id==id).ToList();
            return data;
        }
    }
}
