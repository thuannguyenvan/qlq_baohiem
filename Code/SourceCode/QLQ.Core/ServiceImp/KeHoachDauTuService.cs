﻿using FX.Data;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QLQ.Core.ServiceImp
{
    public class KeHoachDauTuService : BaseService<KeHoachDauTu, long>, IKeHoachDauTuService
    {
        public KeHoachDauTuService(string sessionFactoryConfigPath) : base(sessionFactoryConfigPath)
        {
        }
    }
}
