﻿using FX.Data;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using System.Collections.Generic;
using System.Linq;

namespace QLQ.Core.ServiceImp
{
    public class ACCOUNT_ROLEService : BaseService<QLQ_ACCOUNTROLE, long?>, IACCOUNT_ROLEService
    {
        public ACCOUNT_ROLEService(string sessionFactoryConfigPath) : base(sessionFactoryConfigPath)
        {
        }

        public List<QLQ_ACCOUNTROLE> GetByAccount(long accountId)
        {
            return Query.Where(x => x.ACCOUNT_ID == accountId && x.ACTIVE==1).ToList();
        }

        public QLQ_ACCOUNTROLE GetById(long? id)
        {
            return Query.FirstOrDefault(x => x.ACCOUNTROLE_ID == id && x.ACTIVE==1);
        }

        public QLQ_ACCOUNTROLE GetId(long accountId, long roleId, int active)
        {
            return Query.FirstOrDefault(x => x.ACCOUNT_ID == accountId && x.ROLE_ID == roleId && x.ACTIVE== active);
        }
    }
}
