﻿using FX.Data;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using System;
using System.Collections.Generic;
using System.Linq;

namespace QLQ.Core.ServiceImp
{
    public class ACCOUNTService : BaseService<QLQ_ACCOUNT, long?>, IACCOUNTService
    {
        public ACCOUNTService(string sessionFactoryConfigPath) : base(sessionFactoryConfigPath)
        {
        }

        public QLQ_ACCOUNT GetByUser(string username)
        {
            try
            {
                var result = Query.FirstOrDefault(x => x.USERNAME == username);
                return result;
            }
            catch (Exception)
            {
                return null;
            }            
        }
        public QLQ_ACCOUNT GetById(int id)
        {
            try
            {
                var result = Query.FirstOrDefault(x => x.ACCOUNT_ID == id);
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<QLQ_ACCOUNT> Search(string timkiem)
        {
            try
            {
                if (timkiem == null) timkiem = "";
                var result = Query.Where(x => x.USERNAME.ToUpper().Contains(timkiem.ToUpper())).ToList();
                return result;
            }
            catch (Exception)
            {
                return new List<QLQ_ACCOUNT>();
            }
        }

        //public List<QLQ_ACCOUNT> GetAllAccount()
        //{
        //    return Query.ToList();
        //}
    }
}
