﻿using FX.Data;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using System.Collections.Generic;
using System.Linq;

namespace QLQ.Core.ServiceImp
{
    public class DM_TOCHUCDAUTHAUService : BaseService<QLQ_DMTOCHUCDAUTHAU, long?>, IDM_TOCHUCDAUTHAUService
    {
        public DM_TOCHUCDAUTHAUService(string sessionFactoryConfigPath) : base(sessionFactoryConfigPath)
        {
        }


        public QLQ_DMTOCHUCDAUTHAU GetById(long? id)
        {
            var result = Query.FirstOrDefault(x => x.tochucdauthau_id == id);
            return result;
        }

        public QLQ_DMTOCHUCDAUTHAU GetByMa(string ma)
        {
            var result = Query.FirstOrDefault(x => x.ma_tochucdauthau == ma);
            return result;
        }

        public List<QLQ_DMTOCHUCDAUTHAU> Search(string timkiem)
        {
            if (timkiem == null)
                timkiem = "";
            return Query.Where(x => x.ma_tochucdauthau.ToUpper().Contains(timkiem.ToUpper()) || x.ten_tochucdauthau.ToUpper().Contains(timkiem.ToUpper())).ToList();
        }
    }
}
