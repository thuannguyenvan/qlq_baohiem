﻿using FX.Data;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using System.Linq;

namespace QLQ.Core.ServiceImp
{
    public class ObjectService : BaseService<QLQ_OBJECT, long?>, IOBJECTService
    {
        public ObjectService(string sessionFactoryConfigPath) : base(sessionFactoryConfigPath)
        {
        }
        public QLQ_OBJECT GetbyId(long? id)
        {
            var result = Query.FirstOrDefault(x => x.object_id == id);
            return result;
        }

    }
}
