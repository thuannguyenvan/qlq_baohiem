﻿using System;
using System.Collections.Generic;
using System.Linq;
using FX.Data;
using QLQ.Core.Domain;
using QLQ.Core.IService;
namespace QLQ.Core.ServiceImp
{
    public class DM_HOPDONGDIEUCHINHService : BaseService<QLQ_HOPDONGLAISUATDC, long?>, IHopDongDieuChinhLaiSuatService
    {
        public DM_HOPDONGDIEUCHINHService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {
        }

        public QLQ_HOPDONGLAISUATDC GetById(long? id)
        {
            var result = Query.FirstOrDefault(x => x.laisuatdc_id == id);
            return result;
        }

        public IQueryable<QLQ_HOPDONGLAISUATDC> Search(string timKiem, long? id)
        {
            var list = Query;
            if (id.HasValue)
            {
                list = list.Where(x => x.hopdong_id == id);
            }
            if (!string.IsNullOrEmpty(timKiem))
            {
                string[] tutimkiem = timKiem.Split(',');
                foreach(var tu in tutimkiem)
                    list = list.Where(t => t.ghichu.Contains(tu));
            }
            return list;
        }
    }
}
