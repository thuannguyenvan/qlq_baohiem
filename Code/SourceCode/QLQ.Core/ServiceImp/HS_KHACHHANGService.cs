﻿using FX.Data;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using System.Linq;

namespace QLQ.Core.ServiceImp
{
    public class HS_KHACHHANGService : BaseService<HS_KHACHHANG, long>, IHS_KHACHHANGService
    {
        public HS_KHACHHANGService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {
        }

        public IQueryable<HS_KHACHHANG> Search(string text)
        {
            if (!string.IsNullOrEmpty(text))
	        {
                text = text.Trim().ToUpper();
                return Query.Where(x => x.TEN_KHACHHANG.ToUpper().Contains(text) 
                    || x.DIA_CHI.ToUpper().Contains(text) 
                    || x.FAX.ToUpper().Contains(text) 
                    || x.TAIKHOAN.ToUpper().Contains(text) 
                    || x.DAIDIEN.ToUpper().Contains(text) 
                    || x.CHUCVU.ToUpper().Contains(text) 
                    || x.SO_GPKD.ToUpper().Contains(text));
	        }
            return Query;
        }
        public bool CheckExistDM_Dautu(long id)
        {
            return Query.FirstOrDefault(x => x.DAUTU_ID == id) != null;
        }
    }
}
