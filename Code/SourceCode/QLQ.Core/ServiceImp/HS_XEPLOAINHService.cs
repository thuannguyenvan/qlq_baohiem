﻿using FX.Data;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;


namespace QLQ.Core.ServiceImp
{
    public class HS_XEPLOAINHService : BaseService<HS_XEPLOAINH, long>, IHS_XEPLOAINHService
    {
        public HS_XEPLOAINHService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {
        }

        public HS_XEPLOAINH GetById(long? xepLoaiId)
        {
            return Query.FirstOrDefault(x => x.XEPLOAINH_ID == xepLoaiId);
        }

        public List<HS_XEPLOAINH> Search(int? timkiem)
        {
            return Query.Where(x => x.NAM == timkiem ).ToList();
        }
    }
}
