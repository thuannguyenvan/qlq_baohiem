﻿using FX.Data;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using System.Collections.Generic;
using System.Linq;

namespace QLQ.Core.ServiceImp
{
    public class DM_DAUTUService : BaseService<QLQ_DMDAUTU, long?>, IDM_DAUTUService
    {
        public DM_DAUTUService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {
        }
        //public IQueryable<QLQ_DMDAUTU> GetAllDmDauTu()
        //{
        //    var data = Query;
        //    return data;
        //}

        public QLQ_DMDAUTU GetById(long? id)
        {
            var result = Query.FirstOrDefault(x => x.dautu_id == id);
            return result;
        }

        public QLQ_DMDAUTU GetByMa(string ma)
        {
            var result = Query.FirstOrDefault(x => x.ma_dautu == ma);
            return result;
        }

        public List<QLQ_DMDAUTU> Search(string timkiem)
        {
            if (timkiem == null)
                timkiem = "";
            return Query.Where(x => x.ma_dautu.ToUpper().Contains(timkiem.ToUpper()) || x.ten_dautu.ToUpper().Contains(timkiem.ToUpper())).ToList();
        }

        /// <summary>
        /// Check hình thức đầu tư có được sử dụng trong dm đầu tư không
        /// </summary>
        /// <param name="id">hình thức đầu tư id</param>
        /// <returns></returns>
        public bool CheckExistDmDauTu(long id)
        {
            var result = Query.FirstOrDefault(x => x.hinhthuc_id == id);
            if (result!= null)
            {
                return true; //Tồn tại
            }
            return false; //Không tồn tại
        }
    }
}
