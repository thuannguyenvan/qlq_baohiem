﻿using System.Collections.Generic;
using System.Linq;
using QLQ.Core.IService;
using QLQ.Core.Domain;
using FX.Data;

namespace QLQ.Core.ServiceImp
{
    public class DM_BAOCAOService : BaseService<QLQ_DMBAOCAO, long?>, IDM_BAOCAOService
    {
        public DM_BAOCAOService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {
        }
        public QLQ_DMBAOCAO GetById(long? id)
        {
            var result = Query.FirstOrDefault(x => x.baocao_id == id);
            return result;
        }
        public QLQ_DMBAOCAO GetByMa(string ma)
        {
            var result = Query.FirstOrDefault(x => x.ma_baocao == ma);
            return result;
        }

        public List<QLQ_DMBAOCAO> Search(string timkiem)
        {
            if (timkiem == null)
                timkiem = "";
            return Query.Where(x => x.ma_baocao.ToUpper().Contains(timkiem.ToUpper()) || x.ten_baocao.ToUpper().Contains(timkiem.ToUpper())).ToList();
        }
    }
}
