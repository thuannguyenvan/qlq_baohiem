﻿using System.Collections.Generic;
using System.Linq;
using FX.Data;
using QLQ.Core.Domain;
using QLQ.Core.IService;

namespace QLQ.Core.ServiceImp
{
    public class DMDVQLService : BaseService<DM_DVQL, string>,IDMDVQLService
    {
        public DMDVQLService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {
 
        }
        public string TimkiemS
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["DM_DVQL_Filter"] != null)
                {
                    return (string)System.Web.HttpContext.Current.Session["DM_DVQL_Filter"];
                }
                return null;
            }
            set
            {
                System.Web.HttpContext.Current.Session["DM_DVQL_Filter"] = value;
            }
        }

        public List<DM_DVQL> GetAllid()
        {
            var result = (from t in Query where t.MA_CAPTREN == "0000" select t).ToList();
            return result;
        }
        //public List<DM_DVQL> GetAllDmdvql()
        //{
        //    return Query.ToList();
        //}

        public DM_DVQL GetDmDvqlId(string id)
        {
            return Query.FirstOrDefault(m => m.MA_DVQL == id);
        }

        public DM_DVQL GetDvqlPrintReport(string maDvql)
        {

            var dvql =  Query.FirstOrDefault(m => m.MA_DVQL == maDvql);
            if (dvql != null && dvql.GetCapTheoDoi() == 2 && dvql.MA_DVQL.Substring(2, 2) == "00")
            {
                return Query.FirstOrDefault(m => m.MA_DVQL == dvql.MA_CAPTREN);
            }
            return dvql;
        }

        public IQueryable<DM_DVQL> SearchDvql(string maCapTren)
        {
            var result = Query.Where(m => m.MA_CAPTREN == maCapTren);
            if (string.IsNullOrWhiteSpace(TimkiemS))
            {
                return result;
            }
            result = result.Where(m => m.MA_DVQL.ToLower().Contains(TimkiemS) || m.TEN_DVQL.ToLower().Contains(TimkiemS) && m.MA_CAPTREN == maCapTren);
            return result;
        }
        public IQueryable<DM_DVQL> GetDmDvqlsByMaCapTren(string maCapTren)
        {
            return Query.Where(m => m.MA_CAPTREN == maCapTren).OrderBy(m=>m.MA_DVQL);
        }

        public IQueryable<DM_DVQL> GetDmDvqlsByMadvql(string madvql)
        {
            return Query.Where(m => m.MA_DVQL == madvql).OrderBy(m => m.MA_DVQL);
        }

        public IQueryable<DM_DVQL> GetDmDvqlsByCap(int cap)
        {
            return Query.Where(m => m.CAP == cap);
        }
        
        public List<string> GetNoiDung(string macaptren)
        {
            var m = (from t in Query where t.MA_CAPTREN == macaptren select t.TEN_DVQL );
            return m.ToList();
        }
        public IQueryable<DM_DVQL> Getbymacaptren(string macaptren)
        {
            var m = (from t in Query
                     where t.MA_CAPTREN == macaptren
                     orderby t.MA_DVQL
                     select t);
            return m;
        }

        public IQueryable<DM_DVQL> GetByPhamVi(string maDvql,string phamvi)
        {
            if (maDvql == "0000")
            {
                return Query.Where(m => m.MA_DVQL == maDvql || (m.CAP == 3 && m.MA_DVQL.Substring(3,2) =="00")).OrderBy( m=>m.MA_DVQL);
            }
            if (string.IsNullOrEmpty(phamvi))
            {
                return Query.Where(m => m.MA_DVQL == maDvql);

            }
            var phamviarr = phamvi.Split(',');
            return Query.Where(m => m.MA_DVQL == maDvql || phamviarr.Contains(m.MA_DVQL));
        }
        public IQueryable<DM_DVQL> GetDonViCon(string maDvql)
        {
            // kiểm tra nếu đơn vị quản lý là văn phòng tỉnh thì coi như tỉnh
            if (maDvql != null && maDvql.Substring(2, 2) == "00")
            {
                var vanphongTinh = Query.FirstOrDefault(m => m.MA_DVQL == maDvql);if (vanphongTinh != null)
                    maDvql = vanphongTinh.MA_CAPTREN;
                return Query.Where(m => m.MA_CAPTREN == maDvql).OrderBy(m => m.MA_DVQL);
            }
            return Query.Where(m => m.MA_CAPTREN == maDvql || m.MA_DVQL == maDvql).OrderBy(m=>m.MA_DVQL);
        }
    }
}
