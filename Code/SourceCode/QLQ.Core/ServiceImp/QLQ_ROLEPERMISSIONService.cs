﻿using FX.Data;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using System.Collections.Generic;
using System.Linq;

namespace QLQ.Core.ServiceImp
{
    public class QLQ_ROLEPERMISSIONService : BaseService<QLQ_ROLEPERMISSION, long?>, IQLQ_ROLEPERMISSIONService
    {
        public QLQ_ROLEPERMISSIONService(string sessionFactoryConfigPath) : base(sessionFactoryConfigPath)
        {
        }
        
        public QLQ_ROLEPERMISSION GetById(long? id)
        {
            return Query.FirstOrDefault(x => x.rolepermission_id == id);
        }

        public List<QLQ_ROLEPERMISSION> GetByRoleId(long? id)
        {
            var data = Query.Where(x=>x.role_id==id).ToList();
            return data;
        }

        public QLQ_ROLEPERMISSION GetObj(long? roleId, long? perId)
        {
            return Query.FirstOrDefault(x => x.permission_id == perId && x.role_id == roleId);
        }
    }
}
