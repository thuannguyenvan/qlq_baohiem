﻿using FX.Data;
using QLQ.Core.CustomView;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using QLQ.Core.StoreProcedure;
using System;
using System.Collections.Generic;
using System.Linq;

namespace QLQ.Core.ServiceImp
{
    public class LAISUATTBService : BaseService<QLQ_LAISUATTB, decimal?>, ILAISUATTBService
    {
        public LAISUATTBService(string sessionFactoryConfigPath) : base(sessionFactoryConfigPath)
        {
        }

        public List<QLQ_LAISUATTB> GetByDay(DateTime day)
        {
            var daunam = new DateTime(day.Year,1,1);// DateTime.Parse(day.Year.ToString());
            var data = Query.Where(x => x.NGAY_THAYDOI > daunam && x.NGAY_THAYDOI < day).ToList();
            return data;
        }

        public IEnumerable<LaiSuatHuyDongNHModel> LayLaiSuatBinhQuan(DateTime day)
        {
            QLQStoreProcedure _spQlq = new QLQStoreProcedure();
            var laiSuatBq = _spQlq.sp_LaiSuatBinhQuan(day);
            var ngayHieuLuc = laiSuatBq.Select(t => t.NGAY_HIEULUC).Distinct().ToList();
            List<LaiSuatHuyDongNHModel> lstResult = new List<LaiSuatHuyDongNHModel>();
            foreach (var ngay in ngayHieuLuc)
            {
                var laiSuat = from ls in laiSuatBq
                              where ls.NGAY_HIEULUC <= ngay
                              group ls by new { ls.NGANHANG_ID, ls.KYHAN_ID } into nganhang
                              select nganhang.OrderByDescending(p => p.NGAY_HIEULUC).First();

                // calculate
                var query = from ls in laiSuat
                            group ls by ls.KYHAN_ID into kyhan
                            select new LaiSuatHuyDongNHModel()
                            {   NGAY_HIEULUC = ngay,
                                KYHAN_ID = kyhan.Key,
                                CANHAN_CUOIKY = kyhan.Average(t => t.CANHAN_CUOIKY),
                                CANHAN_HANGTHANG = kyhan.Average(t => t.CANHAN_HANGTHANG),
                                TOCHUC_CUOIKY = kyhan.Average(t => t.TOCHUC_CUOIKY),
                                TOCHUC_HANGTHANG = kyhan.Average(t => t.TOCHUC_HANGTHANG)
                            };
                lstResult.AddRange(query);
            }
            return lstResult;
        }
    }
}
