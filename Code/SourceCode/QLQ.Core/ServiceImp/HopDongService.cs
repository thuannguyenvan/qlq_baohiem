﻿using FX.Data;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using QLQ.Core.CustomView;
using System.Data;
using QLQ.Core.Common;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Xml.Serialization;

using System.Linq.Expressions;
using System.Globalization;
using QLQ.Core.StoreProcedure;

namespace QLQ.Core.ServiceImp
{
    public class HopDongService : BaseService<QLQ_HOPDONG, long>, IHopDongService
    {
        public HopDongService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {
        }
        public QLQ_HOPDONG getHopDongBySo_HD(string soh)
        {
            QLQ_HOPDONG hopdong = new QLQ_HOPDONG();
            if (Query.Count() != 0)
            {
                hopdong = Query.FirstOrDefault(t => t.so_hopdong == soh);
            }
            return hopdong;
        }
        public List<QLQ_HOPDONG> Search(QLQ_HOPDONG hd)
        {
            var list = Query;
            if (hd.hopdong_id != 0)
            {
                list = list.Where(t => t.hopdong_id == hd.hopdong_id);
            }
            if (hd.hinhthuc_id != 0)
            {
                list = list.Where(t => t.hinhthuc_id == hd.hinhthuc_id);
            }
            if (hd.DAUTU_ID != 0)
            {
                list = list.Where(t => t.DAUTU_ID == hd.DAUTU_ID);
            }
            list = list.Where(s => s.IsDeleted == 0);
            return list.ToList();
        }

        public bool CheckDmHopDongId(long id)
        {
            var result = Query.FirstOrDefault(x => x.hopdong_id == id);
            if (result != null)
            {
                return true;
            }
            return false;
        }
        public bool CheckDmKhachHangId(long id)
        {
            var result = Query.FirstOrDefault(x => x.khachhang_id == id);
            if (result != null)
            {
                return true;
            }
            return false;
        }

        public bool SoftDelete(long id)
        {
            try
            {
                var dbObj = Get(s => s.qlhopdong_id == id);
                dbObj.IsDeleted = 1;
                Update(dbObj);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool CheckExistDM_Dautu(long id)
        {
            return Query.FirstOrDefault(x => x.DAUTU_ID == id) != null;
        }

        public List<SoDuDauTuCommonModel> GetSoDuDauTuChiTiet(GetSoDuDauTuRequest request)
        {
            var query = Query.Where(i => i.IsDeleted == 0);
            if (request.DauTuId.HasValue)
            {
                query = query.Where(i => i.DAUTU_ID == request.DauTuId.Value);
            }
            if (request.KhachHangId.HasValue)
            {
                query = query.Where(i => i.khachhang_id == request.KhachHangId.Value);
            }
            if (request.FromDate.HasValue)
            {
                query = query.Where(i => i.ngay_hieuluc == request.FromDate.Value);
            }
            if (request.ToDate.HasValue)
                query = query.Where(i => i.han_hopdong == request.ToDate.Value);

            return query
                .OrderBy(i => i.so_hopdong)
                .ToList()
                .Select(i => new SoDuDauTuCommonModel
                {
                    DenNgay = i.han_hopdong ?? new DateTime(),
                    HopDongId = i.hopdong_id,
                    LaiSuat = (decimal)i.laisuat,
                    NgayKy = i.creation_date ?? new DateTime(),
                    SoHopDong = i.so_hopdong,
                    SoTien = i.sotien,
                    TuNgay = i.ngay_hieuluc ?? new DateTime(),
                }).ToList();
        }

        public List<SoDuDauTuCommonModel> GetSoDuDauTuDenHan(GetSoDuDauTuRequest request)
        {
            var query = Query.Where(i => i.IsDeleted == 0);
            if (request.DauTuId.HasValue)
            {
                query = query.Where(i => i.DAUTU_ID == request.DauTuId.Value);
            }
            if (request.KhachHangId.HasValue)
            {
                query = query.Where(i => i.khachhang_id == request.KhachHangId.Value);
            }
            if (request.FromDate.HasValue)
            {
                query = query.Where(i => i.han_hopdong >= request.FromDate.Value);
            }
            if (request.ToDate.HasValue)
            {
                query = query.Where(i => i.han_hopdong <= request.ToDate.Value);
            }
            return query
                .OrderBy(i => i.so_hopdong)
                .ToList()
                .Select(i => new SoDuDauTuCommonModel
                {
                    DenNgay = i.han_hopdong ?? new DateTime(),
                    HopDongId = i.hopdong_id,
                    LaiSuat = (decimal)i.laisuat,
                    NgayKy = i.creation_date ?? new DateTime(),
                    SoHopDong = i.so_hopdong,
                    SoTien = i.sotien,
                    TuNgay = i.ngay_hieuluc ?? new DateTime(),
                }).ToList();
        }

        public List<SoDuDauTuCommonModel> GetSoDuDauTuMoi(GetSoDuDauTuRequest request)
        {
            var query = Query.Where(i => i.IsDeleted == 0);
            if (request.DauTuId.HasValue)
            {
                query = query.Where(i => i.DAUTU_ID == request.DauTuId.Value);
            }
            if (request.KhachHangId.HasValue)
            {
                query = query.Where(i => i.khachhang_id == request.KhachHangId.Value);
            }
            if (request.FromDate.HasValue)
            {
                query = query.Where(i => i.creation_date >= request.FromDate.Value);
            }
            if (request.ToDate.HasValue)
            {
                query = query.Where(i => i.creation_date <= request.ToDate.Value);
            }
            return query
                .OrderBy(i => i.so_hopdong)
                .ToList()
                .Select(i => new SoDuDauTuCommonModel
                {
                    DenNgay = i.han_hopdong ?? new DateTime(),
                    HopDongId = i.hopdong_id,
                    LaiSuat = (decimal)i.laisuat,
                    NgayKy = i.creation_date ?? new DateTime(),
                    SoHopDong = i.so_hopdong,
                    SoTien = i.sotien,
                    TuNgay = i.ngay_hieuluc ?? new DateTime(),
                }).ToList();
        }
        public List<QLQ_HOPDONG> GetSoDuDauTu(DateTime? thoigian)
        {
            if (thoigian.HasValue)
                return (Query.Where(t => t.ngay_hieuluc.Value == thoigian.Value).ToList());
            return Query.ToList();
        }
        public LaiSuatDauTuModel GetLaiSuatDauTu(long hopDongId)
        {
            if (hopDongId > 0)
            {
                using (var transaction = NHibernateSession.BeginTransaction())
                {
                    using (var command = NHibernateSession.Connection.CreateCommand())
                    {
                        command.CommandText = "PKG_TEST.SP_LAISUATDAUTU";
                        command.CommandType = CommandType.StoredProcedure;
                        var paramOut = Helpers.GetParam(command, OracleDbType.XmlType, "p_out", direction: ParameterDirection.Output);
                        command.Parameters.Add(paramOut);
                        command.Parameters.Add(Helpers.GetParam(command, OracleDbType.Decimal, "p_hopdongid", hopDongId));
                        transaction.Enlist(command);
                        command.ExecuteNonQuery();
                        var reader = ((OracleXmlType)paramOut.Value).GetXmlReader();
                        var serializer = new XmlSerializer(typeof(LaiSuatDauTuModel));
                        return (LaiSuatDauTuModel)serializer.Deserialize(reader);
                    }
                }
            }
            else
            {
                return new LaiSuatDauTuModel();
            }
        }

        public PhieuLaiPhaiThuTrongNamC89 GetPhieuC89(long hopDongId, int nam)
        {
            var hd = Query.FirstOrDefault(s => s.qlhopdong_id == hopDongId);
            if (hd != null)
            {
                //Get neu chua tao
                Expression<Func<QLQ_HOPDONGLAISUATDC, bool>> predicate = s => s.ngayhieuluc > hd.ngay_hieuluc;
                var laiDieuChinh = NHibernateSession.QueryOver<QLQ_HOPDONGLAISUATDC>().Where(predicate).List().FirstOrDefault();
                var lai = laiDieuChinh != null ? laiDieuChinh.laisuatdieuchinh : hd.laisuat;
                var from = hd.ngay_hieuluc.HasValue && hd.ngay_hieuluc > new DateTime(nam, 1, 1) ? hd.ngay_hieuluc.Value : new DateTime(nam, 1, 1);
                var to = hd.han_hopdong.HasValue && hd.han_hopdong < new DateTime(nam + 1, 1, 1) ? hd.han_hopdong.Value : new DateTime(nam, 12, 31);
                var soTienLai = (to - from).TotalDays * (double)hd.sotien * lai;
                var toHanThanhToan = hd.NGAY_TRALAI_L2.HasValue ? new DateTime(nam, hd.NGAY_TRALAI_L2.Value.Month, hd.NGAY_TRALAI_L2.Value.Day) : new DateTime(nam, hd.ngay_tralai.Value.Month, hd.ngay_tralai.Value.Day);
                if (toHanThanhToan > to)
                {
                    toHanThanhToan = to;
                }
                var soTienDenHan = (toHanThanhToan - from).TotalDays * (double)hd.sotien * lai;
                return new PhieuLaiPhaiThuTrongNamC89
                {
                    Id = 132,
                    Nam = nam,
                    HopDong = hd,
                    HopDongId = hopDongId,
                    TuNgay = from,
                    DenNgay = to,
                    LaiSuatDieuChinh = laiDieuChinh != null ? laiDieuChinh.laisuatdieuchinh : (float?)null,
                    TongSoLaiPhaiThu = (Decimal)soTienLai,
                    LaiPhaiThuTinhDenHan = (Decimal)soTienDenHan,
                    LaiPhaiThuChuaDenHan = (Decimal)(soTienLai - soTienDenHan)
                };
            }
            return null;
        }

        public LaiSuatDauTuModel GetLaiSuatDauTu(long hopDongId, DateTime ngayxem)
        {
            if (hopDongId != 0)
            {
                using (var transaction = NHibernateSession.BeginTransaction())
                {
                    using (var command = NHibernateSession.Connection.CreateCommand())
                    {
                        try
                        {
                            command.CommandText = "PKG_TEST.SP_LAISUATDAUTU_HDHT";
                            command.CommandType = CommandType.StoredProcedure;
                            var paramOut = Helpers.GetParam(command, OracleDbType.XmlType, "p_out", direction: ParameterDirection.Output);
                            command.Parameters.Add(paramOut);
                            command.Parameters.Add(Helpers.GetParam(command, OracleDbType.Decimal, "p_hopdongid", hopDongId));
                            command.Parameters.Add(Helpers.GetParam(command, OracleDbType.Date, "p_ngayxem", ngayxem));
                            transaction.Enlist(command);
                            command.ExecuteNonQuery();
                            var reader = ((OracleXmlType)paramOut.Value).GetXmlReader();
                            var serializer = new XmlSerializer(typeof(LaiSuatDauTuModel));
                            return (LaiSuatDauTuModel)serializer.Deserialize(reader);
                        }
                        catch (Exception) { return new LaiSuatDauTuModel(); }
                    }
                }
            }
            else
            {
                return new LaiSuatDauTuModel();
            }
        }

        public GocVaLaiModel GetGocVaLai(long hinhthucId, DateTime ngayxem)
        {
            using (var transaction = NHibernateSession.BeginTransaction())
            {
                using (var command = NHibernateSession.Connection.CreateCommand())
                {
                    try
                    {
                        command.CommandText = "PKG_TEST.SP_GOCVALAISUATDAUTU_HDHT";
                        command.CommandType = CommandType.StoredProcedure;
                        var paramOut = Helpers.GetParam(command, OracleDbType.XmlType, "p_out", direction: ParameterDirection.Output);
                        command.Parameters.Add(paramOut);
                        command.Parameters.Add(Helpers.GetParam(command, OracleDbType.Decimal, "p_hinhthucid", hinhthucId));
                        command.Parameters.Add(Helpers.GetParam(command, OracleDbType.Date, "p_ngayxem", ngayxem));
                        transaction.Enlist(command);
                        command.ExecuteNonQuery();
                        var reader = ((OracleXmlType)paramOut.Value).GetXmlReader();
                        var serializer = new XmlSerializer(typeof(GocVaLaiModel));
                        return (GocVaLaiModel)serializer.Deserialize(reader);
                    }
                    catch (Exception)
                    {
                        return new GocVaLaiModel();
                    }
                }
            }
        }

        public C90Model GetPhieuC90(long khachhangid, int nam)
        {
            using (var transaction = NHibernateSession.BeginTransaction())
            {
                using (var command = NHibernateSession.Connection.CreateCommand())
                {
                    try
                    {
                        command.CommandText = "PKG_TEST.SP_LAISUATDAUTUTHEOKHACHHANG";
                        command.CommandType = CommandType.StoredProcedure;
                        var paramOut = Helpers.GetParam(command, OracleDbType.XmlType, "p_out", direction: ParameterDirection.Output);
                        command.Parameters.Add(paramOut);
                        command.Parameters.Add(Helpers.GetParam(command, OracleDbType.Decimal, "p_khachhangid", khachhangid));
                        command.Parameters.Add(Helpers.GetParam(command, OracleDbType.Decimal, "p_nam", nam));
                        transaction.Enlist(command);
                        command.ExecuteNonQuery();
                        var reader = ((OracleXmlType)paramOut.Value).GetXmlReader();
                        var serializer = new XmlSerializer(typeof(GocVaLaiModel));
                        var gocvalai = (GocVaLaiModel)serializer.Deserialize(reader);
                        return C90Model.CreateFromGocVaLaiModel(gocvalai);
                    }
                    catch (Exception)
                    {
                        return new C90Model();
                    }
                }
            }
        }

        public HopDongQuaHanListModel GetHopDongQuaHanList(SearchHopDongQuaHanRequest request)
        {
            using (var transaction = NHibernateSession.BeginTransaction())
            {
                using (var command = NHibernateSession.Connection.CreateCommand())
                {
                    try
                    {
                        command.CommandText = "QLQ_HOPDONG_PKG.SP_GETHOPDONGQUAHAN";
                        command.CommandType = CommandType.StoredProcedure;
                        var paramOut = Helpers.GetParam(command, OracleDbType.XmlType, "p_out", direction: ParameterDirection.Output);
                        command.Parameters.Add(paramOut);
                        command.Parameters.Add(Helpers.GetParam(command, OracleDbType.Char, "p_isgoc", request.LoaiGocLai));
                        command.Parameters.Add(Helpers.GetParam(command, OracleDbType.Decimal, "p_dautuid", request.DauTuId));
                        command.Parameters.Add(Helpers.GetParam(command, OracleDbType.Decimal, "p_dmhopdongid", request.DanhMucHopDongId));
                        command.Parameters.Add(Helpers.GetParam(command, OracleDbType.Decimal, "p_hopdongid", request.HopDongId));
                        command.Parameters.Add(Helpers.GetParam(command, OracleDbType.Date, "p_hopdongid", request.DenNgay));
                        transaction.Enlist(command);
                        command.ExecuteNonQuery();
                        var reader = ((OracleXmlType)paramOut.Value).GetXmlReader();
                        var serializer = new XmlSerializer(typeof(HopDongQuaHanListModel));
                        return (HopDongQuaHanListModel)serializer.Deserialize(reader);
                    }
                    catch (Exception)
                    {
                        return new HopDongQuaHanListModel();
                    }
                }
            }
        }

        public List<HopDongDenHanModel> GetHopDongDenHanByDate(DateTime date)
        {
            return new QLQStoreProcedure().SP_GetHopDongDenHan(date);
        }
    }
}
