﻿using QLQ.Core.Domain;
using QLQ.Core.IService;
using FX.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace QLQ.Core.ServiceImp
{
    public class DMDAILYService : BaseService<DM_DAILY, dmDaiLyPK>, IDMDAILYService
    {
        public DMDAILYService(string sessionFactoryConfigPath)
          : base(sessionFactoryConfigPath)
        {

        }
        public string TimKiem
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["DM_DAILY_Filter"] != null)
                {
                    return (string)System.Web.HttpContext.Current.Session["DM_DAILY_Filter"];
                }
                return null;
            }
            set
            {
                System.Web.HttpContext.Current.Session["DM_DAILY_Filter"] = value;
            }
        }

        public IQueryable<DM_DAILY> Filter(string madvql)
        {
            var res = Query.Where(m => m.DM_DVQL.MA_DVQL == madvql);
            if (string.IsNullOrWhiteSpace(TimKiem))
            {
                return res;
            }
            else
            {

                res = res.Where(p => p.DmDaiLyPk.MA.ToLower().Contains(TimKiem) || p.DmDaiLyPk.MA_DVQL.ToLower().Contains(TimKiem) 
                    || p.NOIDUNG.ToLower().Contains(TimKiem) || p.DM_DVQL.TEN_DVQL.ToLower().Contains(TimKiem) && p.DM_DVQL.MA_DVQL == madvql);
            }
            return res;
        }
        public string MaDvql { get; set; }
        public IQueryable<DM_DAILY> GetAllDmDaiLy1()
        {
            return Query.Where(m => m.DM_DVQL.MA_DVQL == MaDvql && m.NGAY_BD <= DateTime.Now && m.NGAY_KT >= DateTime.Now).OrderByDescending(p => p.NGAY_BD);
        }

        public IQueryable<DM_DAILY> GetAllDmDaiLy(string madvql)
        {
            return Query.Where(m => m.DM_DVQL.MA_DVQL == madvql && m.NGAY_BD <= DateTime.Now && m.NGAY_KT >= DateTime.Now);
        }
        public IQueryable<DM_DAILY> GetAlLforC73Hd()
        {
            return Query.Where(m => m.MA_TINH != null && m.DM_DVQL.MA_DVQL == MaDvql && m.NGAY_BD <= DateTime.Now && m.NGAY_KT >= DateTime.Now).OrderBy(m => m.DmDaiLyPk.MA);
        }
        public DM_DAILY getByIdDm_DaiLy(string ma, string madv)
        {
            var res = Query.FirstOrDefault(p => p.DmDaiLyPk.MA == ma && p.DmDaiLyPk.MA_DVQL == madv && p.NGAY_BD <= DateTime.Now && p.NGAY_KT >= DateTime.Now);
            return res;
        }

        public List<DM_DAILY> GetDmDaiLy(string madvql)
        {
            //Cache = new DefaultCacheProvider();
            //var DmDaiLy = Cache.Get("DM_DAILY") as List<DM_DAILY>;
            //if (DmDaiLy != null) return DmDaiLy;
            //DmDaiLy = Query.ToList();
            //if (DmDaiLy.Any())
            //{
            //    Cache.Set("DM_DAILY", DmDaiLy, 30);

            //}
            var c = (from p in Query where p.DM_DVQL.MA_DVQL == madvql && p.NGAY_BD <= DateTime.Now && p.NGAY_KT >= DateTime.Now select p).ToList();
            return c;
        }

        public List<string> GetNoiDung(string madvql)
        {
            var m = (from t in Query where t.DM_DVQL.MA_DVQL == madvql && t.NGAY_BD <= DateTime.Now && t.NGAY_KT >= DateTime.Now select t.NOIDUNG);
            return m.ToList();
        }

        public DM_DAILY GetDaily(string maTk)
        {
            var c = (from p in Query where p.DmDaiLyPk.MA == maTk && p.DM_DVQL.MA_DVQL == MaDvql && p.NGAY_BD <= DateTime.Now && p.NGAY_KT >= DateTime.Now select p).FirstOrDefault();
            return c;
        }

        //public void ClearCache()
        //{
        //   // Cache.Invalidate("DM_DAILY");
        //}
    }
}
