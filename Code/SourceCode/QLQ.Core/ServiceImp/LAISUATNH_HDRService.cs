﻿using FX.Data;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using System;
using System.Linq;
using System.Collections.Generic;

namespace QLQ.Core.ServiceImp
{
    public class LAISUATNH_HDRService : BaseService<QLQ_LAISUATNH_HDR, long?>, ILAISUATNH_HDRService
    {
        public LAISUATNH_HDRService(string sessionFactoryConfigPath) : base(sessionFactoryConfigPath)
        {
        }

        public QLQ_LAISUATNH_HDR GetId(long idNganhang, DateTime? dateHieuLuc, string txtCvDen, DateTime? dateCvDen, string txtCv, DateTime? dateCv, string txtNh, DateTime? dateNh)
        {
            var data = Query.Where(x => x.NGANHANG_ID == idNganhang).ToList();
            if(dateHieuLuc!=null)
            {
                data = data.Where(x => x.NGAY_HIEULUC == dateHieuLuc).ToList();
            }
            if (!string.IsNullOrEmpty(txtCvDen))
            {
                data = data.Where(x => x.SO_CVDEN.ToUpper().Contains(txtCvDen.ToUpper())).ToList();
            }
            if (dateCvDen != null)
            {
                data = data.Where(x => x.NGAY_HIEULUC == dateHieuLuc).ToList();
            }
            if (!string.IsNullOrEmpty(txtCv))
            {
                data = data.Where(x => x.SO_CVNH.ToUpper().Contains(txtCv.ToUpper())).ToList();
            }
            if (dateCv != null)
            {
                data = data.Where(x => x.NGAY_CVNH == dateCv).ToList();
            }
            if (!string.IsNullOrEmpty(txtNh))
            {
                data = data.Where(x => x.THONGBAO_NH.ToUpper().Contains(txtNh.ToUpper())).ToList();
            }
            if (dateNh != null)
            {
                data = data.Where(x => x.NGAY_THONGBAO == dateNh).ToList();
            }
            return data.FirstOrDefault();
        }

        /// <summary>
        /// Check ngân hàng id xem có tồn tại trong bảng lãi suất không?
        /// </summary>
        /// <param name="id">Ngân hàng Id</param>
        /// <returns></returns>
        public bool CheckIdNganHangInLaiSuat(long id)
        {
            var result = Query.FirstOrDefault(x => x.NGANHANG_ID == id);
            if (result!= null)
            {
                return true;
            }
            return false;
        }

        public QLQ_LAISUATNH_HDR GetById(long id)
        {
            return Getbykey(id);
        }

        public List<QLQ_LAISUATNH_HDR> Search(string timkiem)
        {
            var list = Query;
            if (!string.IsNullOrEmpty(timkiem))
            {
                string[] tutimkiem = timkiem.Split(',');
                foreach (var s in tutimkiem)
                {
                    list = list.Where(x => x.SO_CVDEN.Contains(s) || x.SO_CVNH.Contains(s) || x.THONGBAO_NH.Contains(s));
                }
            }
            return list.ToList();
        }
    }
}
