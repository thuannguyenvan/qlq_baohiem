﻿using FX.Data;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using System.Collections.Generic;
using System.Linq;

namespace QLQ.Core.ServiceImp
{
    public class DM_CHITIEUService : BaseService<QLQ_DMCHITIEU, long?>, IDM_CHITIEUService
    {
        public DM_CHITIEUService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {
        }
        
        public QLQ_DMCHITIEU GetById(long? id)
        {
            var result = Query.FirstOrDefault(x => x.chitieu_id == id);
            return result;
        }
        public QLQ_DMCHITIEU GetByMa(string ma)
        {
            var result = Query.FirstOrDefault(x => x.ma_chitieu == ma);
            return result;
        }

        public List<QLQ_DMCHITIEU> Search(string timkiem)
        {
            if (timkiem == null)
                timkiem = "";
            return Query.Where(x => x.ma_chitieu.ToUpper().Contains(timkiem.ToUpper()) || x.ten_chitieu.ToUpper().Contains(timkiem.ToUpper())).ToList();
        }
    }
}
