﻿using FX.Data;
using System.Collections.Generic;
using System.Linq;
using QLQ.Core.IService;
using QLQ.Core.Domain;

namespace QLQ.Core.ServiceImp
{
    class DM_HINHTHUCDAUTHAUService : BaseService<QLQ_DMHINHTHUCDAUTHAU, long?>, IDM_HINHTHUCDAUTHAUService
    {
        public DM_HINHTHUCDAUTHAUService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {
        }
        public QLQ_DMHINHTHUCDAUTHAU GetById(long id)
        {
            var result = Query.FirstOrDefault(x => x.hinhthuc_id == id);
            return result;
        }
        public QLQ_DMHINHTHUCDAUTHAU GetByMa(string ma)
        {
            var result = Query.FirstOrDefault(x => x.ma_hinhthuc == ma);
            return result;
        }

        public List<QLQ_DMHINHTHUCDAUTHAU> Search(string timkiem)
        {
            if (timkiem == null)
                timkiem = "";
            return Query.Where(x => x.ma_hinhthuc.ToUpper().Contains(timkiem.ToUpper()) || x.ten_hinhthuc.ToUpper().Contains(timkiem.ToUpper())).ToList();
        }
    }
}
