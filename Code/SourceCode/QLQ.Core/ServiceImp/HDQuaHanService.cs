﻿using FX.Data;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using System.Collections.Generic;
using System.Linq;

namespace QLQ.Core.ServiceImp
{
    public class HDQuaHanService : BaseService<QLQ_HDQUAHAN, long>, IHDQuaHanService
    {
        public HDQuaHanService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {
        }
        public QLQ_HDQUAHAN GetHdQuaHanById(long id)
        {
            QLQ_HDQUAHAN hd = new QLQ_HDQUAHAN();
            if (Query != null)
                hd = Query.FirstOrDefault(t => t.hdquahan_id == id);
            return hd;
        }
        public List<QLQ_HDQUAHAN> Search(QLQ_HDQUAHAN hdqh)
        {
            List<QLQ_HDQUAHAN> list=new List<QLQ_HDQUAHAN>();
            var dshdong = Query;
            if(hdqh.goc_lai!=string.Empty)
            {
                dshdong.Where(t => t.goc_lai == hdqh.goc_lai);
            }
            return list;
        }
    }
}
