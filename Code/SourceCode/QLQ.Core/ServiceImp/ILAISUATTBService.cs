﻿using FX.Data;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using System;
using System.Collections.Generic;
using System.Linq;

namespace QLQ.Core.ServiceImp
{
    public class LAISUATTBService : BaseService<QLQ_LAISUATTB, decimal?>, ILAISUATTBService
    {
        public LAISUATTBService(string sessionFactoryConfigPath) : base(sessionFactoryConfigPath)
        {
        }

        public List<QLQ_LAISUATTB> GetByDay(DateTime day)
        {
            var daunam = DateTime.Parse(day.Year.ToString());
            var data = Query.Where(x => x.NGAY_THAYDOI > daunam && x.NGAY_THAYDOI < daunam).ToList();
            return data;
        }
    }
}
