﻿using FX.Data;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using System.Collections.Generic;
using System.Linq;

namespace QLQ.Core.ServiceImp
{
    class DM_TOCHUCCUNGCAPDICHVUDAUTHAUService : BaseService<QLQ_DMTOCHUCCUNGCAPDICHVUDAUTHAU, long?>, IDM_TOCHUCCUNGCAPDICHVUDAUTHAUService
    {
        public DM_TOCHUCCUNGCAPDICHVUDAUTHAUService(string sessionFactoryConfigPath) : base(sessionFactoryConfigPath)
        {
        }


        public QLQ_DMTOCHUCCUNGCAPDICHVUDAUTHAU GetById(long? id)
        {
            var result = Query.FirstOrDefault(x => x.tochuccungcapdichvudauthau_id == id);
            return result;
        }

        public QLQ_DMTOCHUCCUNGCAPDICHVUDAUTHAU GetByMa(string ma)
        {
            var result = Query.FirstOrDefault(x => x.ma_tochuccungcapdichvudauthau == ma);
            return result;
        }

        public List<QLQ_DMTOCHUCCUNGCAPDICHVUDAUTHAU> Search(string timkiem)
        {
            if (timkiem == null)
                timkiem = "";
            return Query.Where(x => x.ma_tochuccungcapdichvudauthau.ToUpper().Contains(timkiem.ToUpper()) || x.ten_tochuccungcapdichvudauthau.ToUpper().Contains(timkiem.ToUpper())).ToList();
        }
    }
}

