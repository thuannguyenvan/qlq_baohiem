﻿using FX.Data;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using System.Linq;

namespace QLQ.Core.ServiceImp
{
    public class QLQ_ROLEService : BaseService<QLQ_ROLE, long?>, IQLQ_ROLEService
    {
        public QLQ_ROLEService(string sessionFactoryConfigPath) : base(sessionFactoryConfigPath)
        {
        }
        
        public QLQ_ROLE GetById(long? id)
        {
            return Query.FirstOrDefault(x => x.role_id == id);
        }
    }
}
