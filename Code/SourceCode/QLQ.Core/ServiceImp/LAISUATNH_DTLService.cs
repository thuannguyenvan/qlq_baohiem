﻿using FX.Data;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using System.Collections.Generic;
using System.Linq;

namespace QLQ.Core.ServiceImp
{
    public class LAISUATNH_DTLService : BaseService<QLQ_LAISUATNH_DTL, long?>, ILAISUATNH_DTLService
    {
        public LAISUATNH_DTLService(string sessionFactoryConfigPath) : base(sessionFactoryConfigPath)
        {
        }

        public List<QLQ_LAISUATNH_DTL> GetByIdLaiSuatNh(long? id)
        {
            var data = Query.Where(x => x.LAISUATNH_ID == id).OrderBy(x => x.LAISUATNH_DTL_ID).ToList();
            return data;
        }
    }
}
