﻿using FX.Data;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using QLQ.Core.CustomView;
using System.Data;
using QLQ.Core.Common;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Xml.Serialization;

using System.Linq.Expressions;

namespace QLQ.Core.ServiceImp
{
    public class SoChiTietLaiDauTuService : BaseService<QLQ_SOCHITIETLAIDAUTU, long>, ISoChiTietLaiDauTuService
    {
        public SoChiTietLaiDauTuService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {
        }
    }
}
