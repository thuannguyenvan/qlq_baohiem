﻿using FX.Data;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using System.Collections.Generic;
using System.Linq;
using System;

namespace QLQ.Core.ServiceImp
{
    public class DM_KYHANService : BaseService<QLQ_DMKYHAN, long?>, IDM_KYHANService
    {
        public DM_KYHANService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {
        }
        //public IQueryable<QLQ_DMDAUTU> GetAllDmDauTu()
        //{
        //    var data = Query;
        //    return data;
        //}

        public QLQ_DMKYHAN GetById(long? id)
        {
            var result = Query.FirstOrDefault(x => x.kyhan_id == id);
            return result;
        }

        public IQueryable<QLQ_DMKYHAN> Search(string timkiem)
        {
            if (timkiem == null)
                timkiem = "";
            return Query.Where(x => x.ten_kyhan.Contains(timkiem));
        }

        public IQueryable<QLQ_DMKYHAN> GetActiveRecords()
        {
            return Query.Where(t => t.active > 0);
        }

        public bool CheckExistDmKyHan(string ten_kyhan)
        {
            return Query.Any(x => x.ten_kyhan == ten_kyhan);
        }
    }
}
