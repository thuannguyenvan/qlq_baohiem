﻿using FX.Data;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using System.Collections.Generic;
using System.Linq;

namespace QLQ.Core.ServiceImp
{
    public class DM_DANGKYLUUKYService : BaseService<QLQ_DMDANGKYLUUKY, long?>, IDM_DANGKYLUUKYService
    {
        public DM_DANGKYLUUKYService(string sessionFactoryConfigPath) : base(sessionFactoryConfigPath)
        {
        }


        public QLQ_DMDANGKYLUUKY GetById(long? id)
        {
            var result = Query.FirstOrDefault(x => x.dangkyluuky_id == id);
            return result;
        }

        public QLQ_DMDANGKYLUUKY GetByMa(string ma)
        {
            var result = Query.FirstOrDefault(x => x.ma_chutaikhoan == ma);
            return result;
        }

        public List<QLQ_DMDANGKYLUUKY> Search(string timkiem)
        {
            if (timkiem == null)
                timkiem = "";
            return Query.Where(x => x.ma_chutaikhoan.ToUpper().Contains(timkiem.ToUpper()) || x.ten_chutaikhoan.ToUpper().Contains(timkiem.ToUpper())).ToList();
        }
    }
}