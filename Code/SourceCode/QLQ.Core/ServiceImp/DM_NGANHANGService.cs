﻿using FX.Data;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using System.Collections.Generic;
using System.Linq;

namespace QLQ.Core.ServiceImp
{
    public class DM_NGANHANGService : BaseService<QLQ_DMNGANHANG, long?>, IDM_NGANHANGService
    {
        public DM_NGANHANGService(string sessionFactoryConfigPath) : base(sessionFactoryConfigPath)
        {
        }       
        
        public IQueryable<QLQ_DMNGANHANG> GetbyActiveDmNganHang()
        {
            var data = Query.Where(x => x.ACTIVE == 1);
            return data;
        }
        public QLQ_DMNGANHANG GetById(long? id)
        {
            var result = Query.FirstOrDefault(x => x.NGANHANG_ID == id);
            return result;
        }
        public QLQ_DMNGANHANG GetByMa(string ma)
        {
            var result = Query.FirstOrDefault(x => x.MA_NGANHANG == ma);
            return result;
        }

        public List<QLQ_DMNGANHANG> Search(string timkiem)
        {
            if (timkiem == null)
                timkiem = "";
            return Query.Where(x => x.MA_NGANHANG.ToUpper().Contains(timkiem.ToUpper()) || x.TEN_NGANHANG.ToUpper().Contains(timkiem.ToUpper())).ToList();
        }
    }
}
