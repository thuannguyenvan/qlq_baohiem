﻿using FX.Data;
using System.Collections.Generic;
using System.Linq;
using QLQ.Core.IService;
using QLQ.Core.Domain;
namespace QLQ.Core.ServiceImp
{
    public class DM_HINHTHUCDTUService : BaseService<QLQ_DMHINHTHUCDTU, long?>, IDM_HINHTHUCDTUService
    {
        public DM_HINHTHUCDTUService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {
        }

        public QLQ_DMHINHTHUCDTU GetById(long? id)
        {
            var result = Query.FirstOrDefault(x => x.hinhthuc_id == id);
            return result;
        }
        public QLQ_DMHINHTHUCDTU GetByMa(string ma)
        {
            var result = Query.FirstOrDefault(x => x.ma_hinhthuc == ma);
            return result;
        }

        public List<QLQ_DMHINHTHUCDTU> Search(string timkiem)
        {
            if (timkiem == null)
                timkiem = "";
            return Query.Where(x => x.ma_hinhthuc.ToUpper().Contains(timkiem.ToUpper()) || x.ten_hinhthuc.ToUpper().Contains(timkiem.ToUpper())).ToList();
        }
    }
}
