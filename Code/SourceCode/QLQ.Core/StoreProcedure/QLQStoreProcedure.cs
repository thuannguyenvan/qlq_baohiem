﻿using log4net;
using QLQ.Core.Common;
using QLQ.Core.CustomView;
using QLQ.Core.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;

namespace QLQ.Core.StoreProcedure
{
    public class QLQStoreProcedure
    {
        public string ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        private readonly log4net.ILog log = LogManager.GetLogger(typeof(QLQStoreProcedure));
        public List<DM_DAILY> sp_fn_CBH_Chi(int? ID, int chons, string maDVQL)
        {

            VSAOracleHelpers vsaOracleHelper = new VSAOracleHelpers();
            List<DM_DAILY> records = new List<DM_DAILY>();
            using (OracleConnection conn = new OracleConnection(ConnectionString))
            {
                string cmdText = "VSA_NGHIEPVU.sp_fn_CBH_Chi";
                using (OracleCommand cmd = new OracleCommand(cmdText, conn))
                {
                    try
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new OracleParameter("i_maDVQL", OracleType.Int32)).Value = maDVQL;
                        cmd.Parameters.Add(new OracleParameter("soID", OracleType.Int32)).Value = ID;
                        cmd.Parameters.Add(new OracleParameter("chon", OracleType.Int32)).Value = chons;
                        cmd.Parameters.Add(new OracleParameter("cur", OracleType.Cursor)).Direction =
                            ParameterDirection.Output;
                        conn.Open();
                        using (OracleDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                                records = Helpers.GetPOBaseTListFromReader<DM_DAILY>(reader);
                        }
                        vsaOracleHelper.Commit();
                    }
                    catch (Exception ex)
                    {
                        vsaOracleHelper.Rollback();
                        log.Error(log.Logger.Name + cmdText + ex.Message);
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
            return records;
        }
        public List<record_SoTheoDoi> sp_SoTheoDoi(long? NganHang, int year, int type)
        {
            VSAOracleHelpers OracleHelper = new VSAOracleHelpers();
            List<record_SoTheoDoi> record = new List<record_SoTheoDoi>();
            using (var conn = new OracleConnection(ConnectionString))
            {
                string cmdText = "QLQ_LAISUAT_PKG.SP_SOTHEODOI";
                using (OracleCommand cmd = new OracleCommand(cmdText, conn))
                {
                    try
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new OracleParameter("p_nganhang", OracleType.Int32)).Value = NganHang;
                        cmd.Parameters.Add(new OracleParameter("p_year", OracleType.Int32)).Value = year;
                        cmd.Parameters.Add(new OracleParameter("p_type", OracleType.Int32)).Value = type;
                        cmd.Parameters.Add(new OracleParameter("p_cursor", OracleType.Cursor)).Direction = ParameterDirection.Output;
                        conn.Open();
                        using (OracleDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                                record = Helpers.GetPOBaseTListFromReader<record_SoTheoDoi>(reader);
                        }
                        OracleHelper.Commit();
                    }
                    catch (Exception ex)
                    {
                        OracleHelper.Rollback();
                        log.Error(log.Logger.Name + cmdText + ex.Message);
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
            return record;
        }
        public List<LaiSuatHuyDongNHModel> sp_LaiSuatBinhQuan(DateTime date)
        {
            VSAOracleHelpers OracleHelper = new VSAOracleHelpers();
            List<LaiSuatHuyDongNHModel> record = new List<LaiSuatHuyDongNHModel>();
            using (OracleConnection conn = new OracleConnection(ConnectionString))
            {
                string cmdText = "QLQ_LAISUAT_PKG.SP_LAISUATBINHQUAN";
                using (OracleCommand cmd = new OracleCommand(cmdText, conn))
                {
                    try
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new OracleParameter("p_year", OracleType.Int32)).Value = date.Year;
                        cmd.Parameters.Add(new OracleParameter("p_date", OracleType.DateTime)).Value = date;
                        cmd.Parameters.Add(new OracleParameter("p_cursor", OracleType.Cursor)).Direction = ParameterDirection.Output;
                        conn.Open();
                        using (OracleDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                                record = Helpers.GetPOBaseTListFromReader<LaiSuatHuyDongNHModel>(reader);
                        }
                        OracleHelper.Commit();
                    }
                    catch (Exception ex)
                    {
                        OracleHelper.Rollback();
                        log.Error(log.Logger.Name + cmdText + ex.Message);
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
            return record;
        }

        public List<AccountPermission> SP_GetAccountPermissionByAccountId(long accountId)
        {

            var vsaOracleHelper = new VSAOracleHelpers();
            var records = new List<AccountPermission>();

            using (var conn = new OracleConnection(ConnectionString))
            {
                string cmdText = "QLQ_PERMISSION_PKG.SP_GETUSERPERMISSIONS_BYID";
                using (OracleCommand cmd = new OracleCommand(cmdText, conn))
                {
                    try
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new OracleParameter("p_accountID", OracleType.Number)).Value = accountId;
                        cmd.Parameters.Add(new OracleParameter("p_cursor", OracleType.Cursor)).Direction = ParameterDirection.Output;
                        conn.Open();
                        using (OracleDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                                records = Helpers.GetPOBaseTListFromReader<AccountPermission>(reader);
                        }
                        vsaOracleHelper.Commit();
                    }
                    catch (Exception ex)
                    {
                        vsaOracleHelper.Rollback();
                        log.Error(log.Logger.Name + cmdText + ex.Message);
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
            return records;
        }

        public List<SoDuDauTuCommonModel> SpGetSoDuDauTuChiTiet(GetSoDuDauTuRequest request)
        {
            var result = new List<SoDuDauTuCommonModel>();
            for (int i = 1; i <= 30; i++)
            {
                result.Add(new SoDuDauTuCommonModel
                {
                    HopDongId = i,
                    DenNgay = new DateTime(2017, 8, i),
                    LaiSuat = 0.01M * i,
                    NgayKy = new DateTime(2017, 7, i),
                    SoHopDong = i + "/2017/HĐ" + i,
                    SoTien = i * 10000000,
                    TuNgay = new DateTime(2017, 7, i)
                });
            }            
            return result;
        }

        public List<SoDuDauTuCommonModel> SpGetSoDuDauTuDenHan(GetSoDuDauTuRequest request)
        {
            var result = new List<SoDuDauTuCommonModel>();
            for (int i = 1; i <= 30; i++)
            {
                result.Add(new SoDuDauTuCommonModel
                {
                    HopDongId = i,
                    DenNgay = new DateTime(2017, 8, i),
                    LaiSuat = 0.01M * i,
                    NgayKy = new DateTime(2017, 7, i),
                    SoHopDong = i + "/2017/HĐ" + i,
                    SoTien = i * 10000000,
                    TuNgay = new DateTime(2017, 7, i)
                });
            }
            return result;
        }

        public List<LaiSuatDauTu_TheoThangModel> SP_GetLaiSuatDauTuTheoThang(DateTime ngayXem, long hopdongId = 0)
        {

            var vsaOracleHelper = new VSAOracleHelpers();
            var records = new List<LaiSuatDauTu_TheoThangModel>();

            using (var conn = new OracleConnection(ConnectionString))
            {
                string cmdText = "PKG_TEST.SP_LAISUATDAUTU_THEOTHANG";
                using (OracleCommand cmd = new OracleCommand(cmdText, conn))
                {
                    try
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new OracleParameter("p_ngayxem", OracleType.DateTime)).Value = ngayXem;
                        cmd.Parameters.Add(new OracleParameter("p_hopdongid", OracleType.Number)).Value = hopdongId;
                        cmd.Parameters.Add(new OracleParameter("p_out", OracleType.Cursor)).Direction = ParameterDirection.Output;
                        conn.Open();
                        using (OracleDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                                records = Helpers.GetPOBaseTListFromReader<LaiSuatDauTu_TheoThangModel>(reader);
                        }
                        vsaOracleHelper.Commit();
                    }
                    catch (Exception ex)
                    {
                        vsaOracleHelper.Rollback();
                        log.Error(log.Logger.Name + cmdText + ex.Message);
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
            return records;
        }

        public List<SoDuDauTuModel> SP_TongHopSoDuDauTu(DateTime ngayXem)
        {

            var vsaOracleHelper = new VSAOracleHelpers();
            var records = new List<SoDuDauTuModel>();

            using (var conn = new OracleConnection(ConnectionString))
            {
                string cmdText = "QLQ_HOPDONG_PKG.SP_SODAUTU_HINHTHUCDT";
                using (OracleCommand cmd = new OracleCommand(cmdText, conn))
                {
                    try
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new OracleParameter("p_ngayxem", OracleType.DateTime)).Value = ngayXem;
                        cmd.Parameters.Add(new OracleParameter("p_out", OracleType.Cursor)).Direction = ParameterDirection.Output;
                        conn.Open();
                        using (OracleDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                                records = Helpers.GetPOBaseTListFromReader<SoDuDauTuModel>(reader);
                        }
                        vsaOracleHelper.Commit();
                    }
                    catch (Exception ex)
                    {
                        vsaOracleHelper.Rollback();
                        log.Error(log.Logger.Name + cmdText + ex.Message);
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
            return records;
        }

        public List<HopDongDenHanModel> SP_GetHopDongDenHan(DateTime ngayXem)
        {

            var vsaOracleHelper = new VSAOracleHelpers();
            var records = new List<HopDongDenHanModel>();

            using (var conn = new OracleConnection(ConnectionString))
            {
                string cmdText = "PKG_TEST.SP_GETHOPDONGDENHAN";
                using (OracleCommand cmd = new OracleCommand(cmdText, conn))
                {
                    try
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new OracleParameter("p_ngayxem", OracleType.DateTime)).Value = ngayXem;
                        cmd.Parameters.Add(new OracleParameter("p_out", OracleType.Cursor)).Direction = ParameterDirection.Output;
                        conn.Open();
                        using (OracleDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                                records = Helpers.GetPOBaseTListFromReader<HopDongDenHanModel>(reader);
                        }
                        vsaOracleHelper.Commit();
                    }
                    catch (Exception ex)
                    {
                        vsaOracleHelper.Rollback();
                        log.Error(log.Logger.Name + cmdText + ex.Message);
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
            return records;
        }
    }
}
