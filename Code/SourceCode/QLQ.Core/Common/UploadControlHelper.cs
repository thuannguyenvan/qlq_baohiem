﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Web.UI;
using DevExpress.Web;
using QLQ.Core.Common;

namespace QLQ.Core.Common
{

        public class UploadControlHelper
        {
            public const string LocalHost = "~/eis/Content/UploadControl/UploadFolder/";
            public const string UploadDirectory = @"/Uploads/";

            public static readonly UploadControlValidationSettings UploadValidationSettings = new UploadControlValidationSettings
            {
                AllowedFileExtensions = new string[] { ".xml", ".xls", ".xlsx",".dbf",".DBF" },
                MaxFileSize = 83886080,
                NotAllowedFileExtensionErrorText = "Văn bản không đúng định dạng yêu cầu!",
                GeneralErrorText = "Có lỗi trong quá trình upload văn bản!"
            };

            public static void ucMultiSelection_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
            {
                string resultFileName = Path.GetRandomFileName() + "_" + e.UploadedFile.FileName;
                string resultFileUrl = UploadDirectory + resultFileName;
                //Custom host show file
                string hostFileUrl = LocalHost + resultFileName;
                string resultFilePath = HttpContext.Current.Request.MapPath(resultFileUrl);
                e.UploadedFile.SaveAs(resultFilePath);

                //UploadingUtils.RemoveFileWithDelay(resultFileName, resultFilePath, 5);

                IUrlResolutionService urlResolver = sender as IUrlResolutionService;
                if (urlResolver != null)
                {
                    string url = urlResolver.ResolveClientUrl(hostFileUrl);
                    e.CallbackData = GetCallbackData(e.UploadedFile, url);
                }
                if (System.Web.HttpContext.Current.Session["URL"] == null)
                {
                    System.Web.HttpContext.Current.Session["URL"] = resultFilePath;
                }
                else
                {
                    System.Web.HttpContext.Current.Session["URL"] += '|' + resultFilePath;
                }

            }
            static string GetCallbackData(UploadedFile uploadedFile, string fileUrl)
            {
                string name = uploadedFile.FileName;
                long sizeInKilobytes = uploadedFile.ContentLength / 1024;
                string sizeText = sizeInKilobytes.ToString() + " KB";

                return name + "|" + fileUrl + "|" + sizeText;
            }
            /*
             Creation by : HaiPL
             Created date : 19/08/2016
             Des : Return string filename + datetime
             */
            public static string SaveFile(object sender, FileUploadCompleteEventArgs e)
            {
                string strFileName = e.UploadedFile.FileName;
                var arr = strFileName.Split(new char[] { '/', '\\' });
                strFileName = arr[arr.Length - 1];
                string resultFileName = GetFileNameWithTime(strFileName);
                string resultFileUrl = UploadDirectory + resultFileName;
                
                //Custom host show file
                //string hostFileUrl = LocalHost + resultFileName;
                string resultFilePath = HttpContext.Current.Request.MapPath(resultFileUrl);
                e.UploadedFile.SaveAs(resultFilePath);
                return resultFilePath;
            }
            public static string GetFileNameWithTime(string strFileName)
            {
                string strFileNameShort = string.Join(" ", strFileName.Split('.'), 0, strFileName.Split('.').Length - 1);
                string strSystemCode = Common.AutoCodeWithTime();
                strFileName = string.Format("{1}_{0}", strSystemCode, Common.RemoveVietNamChar(strFileNameShort.Replace(" ", "_"))) + "." + "dbf";

                var arr = strFileName.Split(new char[] { '/', '\\' });
                strFileName = arr[arr.Length - 1];
                if (strFileName.IndexOf(@"\") == -1)
                {
                    strFileName = @"\" + strFileName;
                }
                return strFileName;
            }
        }

        public static class UploadControlDemosHelper
        {
            public const string LocalHost = "~/eis/Content/UploadControl/UploadFolder/";
            public const string UploadDirectory = @"/Uploads/HoSo/";

            public static readonly UploadControlValidationSettings ValidationSettings = new UploadControlValidationSettings
            {
                AllowedFileExtensions = new string[] { ".jpg", ".jpeg", ".png"},
                MaxFileSize = 20971520,
            };

            public static void uc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
            {
                if (e.UploadedFile.IsValid)
                {
                    string resultFilePath = HttpContext.Current.Request.MapPath(UploadDirectory + e.UploadedFile.FileName);

                    //Lưu file upload vào thư mục
                    e.UploadedFile.SaveAs(resultFilePath, true);
                    IUrlResolutionService urlResolver = sender as IUrlResolutionService;
                    if (urlResolver != null)
                    {
                        e.CallbackData = urlResolver.ResolveClientUrl(resultFilePath);
                    }
                }
            }
        }
  
}