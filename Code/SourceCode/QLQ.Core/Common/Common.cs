﻿using QLQ.Core.Domain;
using FX.Core;
using IdentityManagement.Service;
using NHibernate;
using NHibernate.Cfg;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using QLQ.Core.CustomView;

namespace QLQ.Core.Common
{
    public class Common
    {
        public static List<int> GetNam()
        {
            List<int> lstNam = new List<int>();
            for (int i = 2008; i < 2050; i++)
            {
                lstNam.Add(i);
            }
            return lstNam;
        }

        #region "convert tien tu so sang chu"
        public static string moneyToString(decimal number)
        {
            string s = number.ToString("#");
            string[] so = new string[] { "không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín" };
            string[] hang = new string[] { "", "nghìn", "triệu", "tỷ" };
            int i, j, donvi, chuc, tram;
            string str = " ";
            bool booAm = false; decimal decS = 0;
            //Tung addnew
            if (number != 0)
            {
                try
                {
                    decS = Convert.ToDecimal(s.ToString());
                }
                catch
                { }
            }

            if (decS < 0)
            {
                decS = -decS;
                s = decS.ToString();
                booAm = true;
            }
            i = s.Length;
            if (i == 0)
                str = so[0] + str;
            else
            {
                j = 0;
                while (i > 0)
                {
                    donvi = Convert.ToInt32(s.Substring(i - 1, 1));
                    i--;
                    if (i > 0)
                        chuc = Convert.ToInt32(s.Substring(i - 1, 1));
                    else
                        chuc = -1;
                    i--;
                    if (i > 0)
                        tram = Convert.ToInt32(s.Substring(i - 1, 1));
                    else
                        tram = -1;
                    i--;
                    if ((donvi > 0) || (chuc > 0) || (tram > 0) || (j == 3))
                        str = hang[j] + str;
                    j++;
                    if (j > 3) j = 1;
                    if ((donvi == 1) && (chuc > 1))
                        str = "một " + str;
                    else
                    {
                        if ((donvi == 5) && (chuc > 0))
                            str = "lăm " + str;
                        else if (donvi > 0)
                            str = so[donvi] + " " + str;
                    }
                    if (chuc < 0)
                        break;
                    else
                    {
                        if ((chuc == 0) && (donvi > 0)) str = "lẻ " + str;
                        if (chuc == 1) str = "mười " + str;
                        if (chuc > 1) str = so[chuc] + " mươi " + str;
                    }
                    if (tram < 0) break;
                    else
                    {
                        if ((tram > 0) || (chuc > 0) || (donvi > 0)) str = so[tram] + " trăm " + str;
                    }
                    str = " " + str;
                }
            }
            if (booAm) str = "Âm " + str;
            //if (str == "không "){
            //    return "";
            //}
            return str + "đồng";
        }
        public static string moneyToString(double number)
        {
            string s = number.ToString("#");
            string[] so = new string[] { "không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín" };
            string[] hang = new string[] { "", "nghìn", "triệu", "tỷ" };
            int i, j, donvi, chuc, tram;
            string str = " ";
            bool booAm = false;
            double decS = 0;
            //Tung addnew
            try
            {
                decS = Convert.ToDouble(s.ToString());
            }
            catch
            {
            }
            if (decS < 0)
            {
                decS = -decS;
                s = decS.ToString();
                booAm = true;
            }
            i = s.Length;
            if (i == 0)
                str = so[0] + str;
            else
            {
                j = 0;
                while (i > 0)
                {
                    donvi = Convert.ToInt32(s.Substring(i - 1, 1));
                    i--;
                    if (i > 0)
                        chuc = Convert.ToInt32(s.Substring(i - 1, 1));
                    else
                        chuc = -1;
                    i--;
                    if (i > 0)
                        tram = Convert.ToInt32(s.Substring(i - 1, 1));
                    else
                        tram = -1;
                    i--;
                    if ((donvi > 0) || (chuc > 0) || (tram > 0) || (j == 3))
                        str = hang[j] + str;
                    j++;
                    if (j > 3) j = 1;
                    if ((donvi == 1) && (chuc > 1))
                        str = "một " + str;
                    else
                    {
                        if ((donvi == 5) && (chuc > 0))
                            str = "lăm " + str;
                        else if (donvi > 0)
                            str = so[donvi] + " " + str;
                    }
                    if (chuc < 0)
                        break;
                    else
                    {
                        if ((chuc == 0) && (donvi > 0)) str = "lẻ " + str;
                        if (chuc == 1) str = "mười " + str;
                        if (chuc > 1) str = so[chuc] + " mươi " + str;
                    }
                    if (tram < 0) break;
                    else
                    {
                        if ((tram > 0) || (chuc > 0) || (donvi > 0)) str = so[tram] + " trăm " + str;
                    }
                    str = " " + str;
                }
            }
            if (booAm) str = "Âm " + str;
            if (str == "không ")
            {
                return "";
            }
            return str + "đồng";
        }
        #endregion

        #region Bộ lọc thời gian

        public enum eThoiGian
        {
            nam = 0,
            quy = 1,
            thang = 2,
            ngay = 3
        }
        public static List<int> thangs = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
        private static List<int> getNams()
        {
            List<int> nams = new List<int>();
            int now = DateTime.Now.Year;
            for (int i = now; i >= now - 50; i--)
                nams.Add(i);
            return nams;
        }
        public static List<int> nams = getNams();
        public static Dictionary<int, string> thoiGians = new Dictionary<int, string>()
        {
            {(int)eThoiGian.nam, "Năm"},
            {(int)eThoiGian.quy, "Quý"},
            {(int)eThoiGian.thang, "Tháng"},
            {(int)eThoiGian.ngay, "Giai đoạn"},
        };
        public static Dictionary<int, string> thoiGianShorts = new Dictionary<int, string>()
        {
            {(int)eThoiGian.nam, "Năm"},
            {(int)eThoiGian.quy, "Quý"},
            {(int)eThoiGian.thang, "Tháng"},
           
        };
        public static Dictionary<int, string> Month = new Dictionary<int, string>()
        {
            {(int)eThoiGian.thang, "Tháng"},
           
        };
        public static Dictionary<int, string> thangQuyNam = new Dictionary<int, string>()
        {
              { 0, "Tháng"},
               {1, "Quý"},
            {2, "Năm"},
           
          
           
        };

        public static Dictionary<int, string> thangQuy = new Dictionary<int, string>()
        {
              { 0, "Tháng"},
               {1, "Quý"},
        };
        public static Dictionary<int, string> thoiGianQuy = new Dictionary<int, string>()
        {
            {(int)eThoiGian.nam, "Năm"},
            {(int)eThoiGian.quy, "Quý"}          
        };
        public enum eQuy
        {
            I = 1,
            II = 2,
            III = 3,
            IV = 4
        }
        public static Dictionary<int, string> quys = new Dictionary<int, string>()
        {
            {(int)eQuy.I, "I"},
            {(int)eQuy.II, "II"},
            {(int)eQuy.III, "III"},
            {(int)eQuy.IV, "IV"},
        };
        #endregion

        #region session Nhibernate
        public static ISessionFactory factory;
        public static ISession OpenSession()
        {
            if (factory == null)
            {
                Configuration conf = new Configuration();

                conf.Configure(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "/Config", "NHibernateConfig.xml"));

                conf.AddAssembly(Assembly.GetCallingAssembly());
                factory = conf.BuildSessionFactory();
            }

            return factory.OpenSession();
        }
        #endregion

        #region Permission
        public static bool DoCheckPermission(string username, string permission)
        {
            // Trung VA Update phương thức check Permission
            #region Old function
            //IuserService UserDataService = IoC.Resolve<IuserService>();
            //var userdata = UserDataService.Query.FirstOrDefault(m => m.username == username);
            //if (userdata == null) return false;
            //var _lstRoles = userdata.Roles;
            //if (_lstRoles.Any())
            //{
            //    bool IssPermission = false;
            //    foreach (var item in _lstRoles)
            //    {
            //        var _lstpms = item.Permissions.Select(m => m.name).ToList();
            //        if (_lstpms.Any())
            //        {
            //            bool _checkLstPms = _lstpms.Contains(permission);
            //            if (_checkLstPms)
            //                IssPermission = true;
            //        }
            //    }
            //    return IssPermission;
            //}
            #endregion
            
            // New function 
            //var nguoidung = ((QLQ.Core.ACCContext)FX.Context.FXContext.Current).CurrentNguoidung;
            //if (nguoidung == null) return false;
            //if (nguoidung.ListPermission!=null && nguoidung.ListPermission.Contains(permission))
            //{
            //    return true;
            //}

            return false;

        }
        #endregion

        #region fnCTOS
        //xu ly ngay thang fnCTOS
        public string fnCTOS(string K, string date)                  //trả về thời gian cuối cùng
        {
            string result = string.Empty;
            int l = date.Length;
            if (!(new List<string>() { "Y", "Q", "M", "D" }).Contains(K))          //mặc định trả về theo độ dài của date
            {
                result = l == 4 ? date.Substring(0, 4) : l == 5 ? date.Substring(0, 4) + "4" : l == 6 ? date.Substring(0, 4) + "12" : "";
            }
            else if (K.Equals("Y"))
            {
                result = date.Substring(0, 4);
            }
            else if (K.Equals("Q"))
            {
                result = date.Substring(0, 4);
                result += l == 4 ? "4" : l == 5 ? date.Substring(4, 1) : getQbyM(date.Substring(4, 2));
            }
            else if (K.Equals("M"))
            {
                result = date.Substring(0, 4); result += l == 4 ? "12" : l == 5 ? getLastMbyQ(date.Substring(4, 1)) : date.Substring(4, 2);
            }
            else if (K.Equals("D"))
            {
                result = date.Substring(0, 4);
                result += l == 4 ? "" : l == 5 ? getLastDbyQ(date.Substring(4, 1))
                        : date.Substring(4, 2) + DateTime.DaysInMonth(int.Parse(result), int.Parse(date.Substring(4, 2)));
            }
            return result;
        }

        public string getQbyM(string M)
        {
            int m = int.Parse(M);
            return m <= 3 ? "1" : m <= 6 ? "2" : m <= 9 ? "3" : "4";
        }

        public string getLastMbyQ(string Q)
        {
            int q = int.Parse(Q);
            return q == 1 ? "03" : q == 2 ? "06" : q == 3 ? "09" : "12";
        }

        public string getLastMbyM(string M)
        {
            int m = int.Parse(M);
            return m >= 1 && m <= 3 ? "03" : m >= 4 && m <= 6 ? "06" : m >= 7 && m <= 9 ? "09" : "12";
        }

        public string getLastDbyQ(string Q)
        {
            int q = int.Parse(Q);
            return q == 1 ? "0331" : q == 2 ? "0630" : q == 3 ? "0930" : "1231";
        }
        #endregion

        #region function convert data

        public static string ConvertKeySL(string idChungtu, string co_ct1)
        {
            // Loại bỏ 4 ký tự MA_DVQL khi convert từ VSA cũ
            if (idChungtu.Length == 14) idChungtu = idChungtu.Remove(10);
            return idChungtu + co_ct1;
        }
        public static string GetTextThoiGianByKy(string kybaocao)
        {
            string thoigian = "";
            switch (kybaocao.Length)
            {
                case 4:
                    thoigian = string.Format("Năm {0}", kybaocao);
                    break;
                case 5:
                    thoigian = string.Format("Quý {0} năm {1}", kybaocao.Substring(4, 1), kybaocao.Substring(0, 4));
                    break;
                case 6:
                    thoigian = string.Format("Tháng {0} năm {1}", kybaocao.Substring(4, 2), kybaocao.Substring(0, 4));
                    break;

            }
            return thoigian;
        }
        /*
        Creation by : Vương anh Trung
        Created date : 2/8/2016
        Description : Lấy giá trị Kỳ báo cáo dựa theo thời gian
        Status : Create
        */
        public static string GetKyBaoCao(int thoigian, int? nam, int? quy, int? thang)
        {
            string time = nam.ToString();
            if (thoigian == (int)eThoiGian.quy)
            {
                time = string.Format("{0}{1}", nam, quy);
            }
            if (thoigian == (int)eThoiGian.thang)
            {
                time = string.Format("{0}{1}", nam, Convert.ToInt32(thang).ToString("00"));
            }
            return time;
        }
        /*
      Creation by : Vương anh Trung
      Created date : 2/8/2016
      Description : Lấy giá trị Kỳ báo cáo tháng dựa theo thời gian
      Status : Create
      */
        public static List<string> GetKyThangByTime(int thoigian, int? nam, int? quy, int? thang)
        {
            List<string> listkq = new List<string>();
            switch (thoigian)
            {
                case (int)eThoiGian.nam:
                    {
                        for (int i = 1; i <= 12; i++)
                        {
                            string stthang = i.ToString("00");
                            string nam_thang = string.Format("{0}{1}", nam, stthang);
                            listkq.Add(nam_thang);
                        }
                        break;
                    }
                case (int)eThoiGian.quy:
                    {
                        int quy2 = quy ?? 0;
                        int dauquy = quy2 * 3 - 2;
                        int cuoiquy = quy2 * 3;
                        for (int i = dauquy; i <= cuoiquy; i++)
                        {
                            string stthang = i.ToString("00");
                            string nam_thang = string.Format("{0}{1}", nam, stthang);
                            listkq.Add(nam_thang);
                        }
                        break;
                    }
                case (int)eThoiGian.thang:
                    {

                        string stthang = (thang ?? 0).ToString("00");
                        string nam_thang = string.Format("{0}{1}", nam, stthang);
                        listkq.Add(nam_thang);
                        break;
                    }

            }
            return listkq;
        }
        /*
          Creation by : Vương anh Trung
          Created date : 2/8/2016
          Description : Lấy giá trị Kỳ báo cáo dựa theo kỳ báo cáo
          Status : Create
          */
        public static List<string> GetKyThangByKy(string KyBaocao)
        {
            if (string.IsNullOrEmpty(KyBaocao)) return null;

            List<string> listkq = new List<string>();
            switch (KyBaocao.Length)
            {
                case 4:
                    for (int i = 1; i <= 12; i++)
                    {
                        string stthang = i.ToString("00");
                        string nam_thang = string.Format("{0}{1}", KyBaocao, stthang);
                        listkq.Add(nam_thang);
                    }
                    break;
                case 5:
                    int quy = Convert.ToInt32(KyBaocao.Substring(KyBaocao.Length - 1));
                    string nam = KyBaocao.Substring(0, 4);
                    int dauquy = quy * 3 - 2;
                    int cuoiquy = quy * 3;
                    for (int i = dauquy; i <= cuoiquy; i++)
                    {
                        string stthang = i.ToString("00");
                        string namThang = string.Format("{0}{1}", nam, stthang);
                        listkq.Add(namThang);
                    }
                    break;
                case 6:
                    listkq.Add(KyBaocao);
                    break;
            }
            return listkq;
        }
        /*
        Creation by : Vương anh Trung
        Created date : 2/8/2016
        Description : Lấy giá trị Ngày cuối cùng trong kỳ dựa theo thời gian
        Status : Create
        */
        public static DateTime GetNgayCuoiKyByTime(int thoiGian, int? nam, int? quy, int? thang)
        {
            if (thoiGian == (int)eThoiGian.nam)
            {
                if (nam != null)
                    return new DateTime(nam.Value, 12, 31);

            }
            else if (thoiGian == (int)eThoiGian.quy)
            {
                if (nam != null && quy != null)
                    return new DateTime(nam.Value, quy.Value * 3 + 1, 1).AddDays(-1);

            }
            else if (thoiGian == (int)eThoiGian.thang)
            {
                if (nam != null && thang != null)
                    return new DateTime(nam.Value, thang.Value,
                        (new DateTime(nam.Value, thang.Value, 1)).AddMonths(1).AddDays(-1).Day);

            }

            return DateTime.Now;
        }
        
       

        /*
        Creation by : Vương anh Trung
        Created date : 2/8/2016
        Description : Lấy giá trị Ngày cuối cùng trong kỳ dựa theo kỳ
        Status : Create
        */
        public static DateTime GetNgayCuoiKyByKy(string KyBaocao)
        {

            if (string.IsNullOrEmpty(KyBaocao)) return DateTime.Now;

            switch (KyBaocao.Length)
            {
                case 4:
                    return new DateTime(Convert.ToInt32(KyBaocao), 12, 31);

                case 5:
                    int quy = Convert.ToInt32(KyBaocao.Substring(4, 1)); int nam = Convert.ToInt32(KyBaocao.Substring(0, 4));
                    int cuoiquy = quy * 3;
                    if (cuoiquy + 1 <= 12)
                    {
                        return new DateTime(nam, cuoiquy + 1, 1).AddDays(-1);
                    }
                    else
                    {
                        return new DateTime(nam, 12, 31);
                    }

                case 6:
                    int thang = Convert.ToInt32(KyBaocao.Substring(4, 2));
                    int nam2 = Convert.ToInt32(KyBaocao.Substring(0, 4));

                    return new DateTime(nam2, thang, 1).AddMonths(1).AddDays(-1);
            }


            return DateTime.Now;
        }

        /// <summary>
        /// Author: Phạm Văn Thủy
        /// Tính số hiệu cho 4 form THU: B02a_TS, B02b_TS, B02c_TS, B02d_TS, 
        /// </summary>
        /// <param name="ky_baocao"></param>
        /// <returns></returns>

        public static string GetNamThangByTimeFilter(TimeFilter time)
        {
            string namthang = time.nam.ToString();
            if (time.thoiGian == (int)eThoiGian.quy)
            {
                namthang = string.Format("{0}{1}", time.nam, time.quy);
            }
            if (time.thoiGian == (int)eThoiGian.thang)
            {
                namthang = string.Format("{0}{1}", time.nam, Convert.ToInt32(time.thang).ToString("00"));
            }
            //else
            //{
            //    // Hệ thống SMS không cho phép lấy dữ liệu theo năm => lấy dữ liệu quý hiện tại nếu chọn năm
            //    int QuyNow = (DateTime.Now.Month + 2)/3;
            //    namthang = string.Format("{0}{1}", time.nam, QuyNow);
            //}
            return namthang;
        }
        #endregion

        #region các list sử dụng cho các bộ lọc
        public enum ePhamviBaoCao
        {
            chiTiet = 0,
            tongHop = 1,
        }

        public enum echitietTongHop
        {
            tungdonvi = 0,
            groupdonvi = 1,
            toanHuyen = 2,
        }

        public static Dictionary<int, string> phamViBaoCao = new Dictionary<int, string>()
      {
          {(int) ePhamviBaoCao.chiTiet, "Chi tiết chứng từ"},
          {(int) ePhamviBaoCao.tongHop, "Tổng hợp theo đại lý"},
      };
        public static Dictionary<int, string> phamViBaoCao2 = new Dictionary<int, string>()
      {
          {(int) ePhamviBaoCao.chiTiet, "Dữ liệu chi tiết"},
          {(int) ePhamviBaoCao.tongHop, "Dữ liệu toàn tỉnh"},
      };
        public static Dictionary<int, string> phamViBaoCao3 = new Dictionary<int, string>()
      {
          {(int) ePhamviBaoCao.chiTiet, "Đã thanh toán"},
          {(int) ePhamviBaoCao.tongHop, "Chưa thanh toán"},
      };
        public static Dictionary<int, string> print_BCC73 = new Dictionary<int, string>()
      {
          {(int) ePhamviBaoCao.chiTiet, "In và hạch toán theo chi tiết"},
          {(int) ePhamviBaoCao.tongHop, "In và hạch toán theo nhóm"},
      };
        public static Dictionary<int, string> phamViBaoCaoS01H = new Dictionary<int, string>()
      {
          { 1, "Chứng từ chi tiết"},
          {2, "Chứng từ tổng hợp"},
      };
        public static Dictionary<int, string> chitietTongHop = new Dictionary<int, string>()
      {
          {(int) echitietTongHop.tungdonvi, "Từng đơn vị"},
          {(int) echitietTongHop.groupdonvi, "Group đơn vị"},
          {(int) echitietTongHop.toanHuyen, "Toàn Huyện"},
      };
        public static Dictionary<int, string> IndayDu_rutGon = new Dictionary<int, string>()
      {
          {0,"In đầy đủ"},{1,"In rút gọn"}
      };
        public static Dictionary<int, string> dayDu_rutGon = new Dictionary<int, string>()
      {
          {0,"Đầy đủ"},{1,"Rút gọn"}
      };
        public static Dictionary<int, string> chitiettonghop = new Dictionary<int, string>()
      {
          {0,"Tổng Hợp"},{1,"Chi Tiết"}
      };
        public static Dictionary<int, string> ngangdoc = new Dictionary<int, string>()
      {
          {0, "Sổ mẫu ngang"},
          {1, "Sổ mẫu dọc"}
      };

      public static Dictionary<int, string> KhoGiay = new Dictionary<int, string>()
      {
          
          {0, "Khổ A3"},
          {1, "Khổ A4"}
      };

      #endregion



      #region hạch toán

      public enum eHachToanTK
        {
            thanhcong = 0,
            NoCoKhongDung = 1,
            NoKhongDung = 2,
            CoKhongDung = 3
        }

        public enum eMauUyNhiemChi
        {
            LienVietPostBank = 0,
            Agribank = 1,
            Bidv = 2,
            VietinBank = 3,
            VietComBank = 4,
            UNC_KB = 5,
            MBBank = 6,
            OCENBANK = 7,
            Agribank2 = 8,
        }
        public static Dictionary<int, string> HachToanTKs = new Dictionary<int, string>()
        {
            {(int)eHachToanTK.thanhcong, "Thành công"},
            {(int)eHachToanTK.NoCoKhongDung, "Hạch toán TKNO TKCO không chính xác"},
            {(int)eHachToanTK.NoKhongDung, "Hạch toán tài khoản nợ không chính xác"},
            {(int)eHachToanTK.CoKhongDung, "Hạch toán tài khoản có không chính xác"},
        };
        public static Dictionary<int, string> MauUyNhiemChis = new Dictionary<int, string>()
        {
            {(int)eMauUyNhiemChi.LienVietPostBank, "LIENVIETPOSTBANK"},
            {(int)eMauUyNhiemChi.Agribank, "AGRIBANK M1"},
            {(int)eMauUyNhiemChi.Agribank2, "AGRIBANK M2"},
            {(int)eMauUyNhiemChi.Bidv, "BIDV"},
            {(int)eMauUyNhiemChi.VietinBank, "VIETINBANK"},
            {(int)eMauUyNhiemChi.VietComBank, "VIETCOMBANK"},
            {(int)eMauUyNhiemChi.UNC_KB, "UNC_KB"},
            {(int)eMauUyNhiemChi.MBBank, "MBBANK"},
            {(int)eMauUyNhiemChi.OCENBANK, "OCENBANK"}
        };


        #endregion
        public enum LoaiUser
        {
            Quantri = 0,
            TW = 1,
            Tinh = 2,
            Huyen = 3
        }
        public enum ChonMauThuBHXH
        {
            TienMat = 0,
            TienGui = 1,
            NopOTinh = 2,
            ThanhToan = 3
        }
        public enum ChonMauChiBHXH
        {
            ChuaChi = 0,
            DaChi = 1,
            TatCa = 2
        }
        public static Dictionary<int, string> MauChiBHXHs = new Dictionary<int, string>()
        {
            {(int)ChonMauChiBHXH.ChuaChi, "Chưa chi"},
            {(int)ChonMauChiBHXH.DaChi, "Đã chi"},
            {(int)ChonMauChiBHXH.TatCa, "Tất cả"}

        };
        public static Dictionary<int, string> MauThuBHXHs = new Dictionary<int, string>()
        {
            {(int)ChonMauThuBHXH.TienMat, "Tiền gửi"},
            {(int)ChonMauThuBHXH.TienGui, "Tiền mặt"},
            {(int)ChonMauThuBHXH.NopOTinh, "Nộp ở tỉnh"},
            {(int)ChonMauThuBHXH.ThanhToan, "Thanh toán"},

        };
        /*
         Create by : HaiPL
         Cretion by : 19/08/2016
         Desciption : return chuỗi tự động với thời gian
         */
        public static string AutoCodeWithTime()
        {
            return (DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString());
        }
        public static string RemoveVietNamChar(string s)
        {
            for (int i = 1; i < VietnameseSigns.Length; i++)
            {
                for (int j = 0; j < VietnameseSigns[i].Length; j++)

                    s = s.Replace(VietnameseSigns[i][j], VietnameseSigns[0][i - 1]);
            }

            return s;
        }

        private static readonly string[] VietnameseSigns = new string[]

            {
                "aAeEoOuUiIdDyY",
                "áàạảãâấầậẩẫăắằặẳẵ",
                "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",
                "éèẹẻẽêếềệểễ",
                "ÉÈẸẺẼÊẾỀỆỂỄ",
                "óòọỏõôốồộổỗơớờợởỡ",
                "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",
                "úùụủũưứừựửữ",
                "ÚÙỤỦŨƯỨỪỰỬỮ",
                "íìịỉĩ",
                "ÍÌỊỈĨ",
                "đ",
                "Đ",
                "ýỳỵỷỹ",
                "ÝỲỴỶỸ"
            };

        /// <summary>
        /// Made By Phạm Văn Thủy
        /// Mục đích: Chuyển DateTime về dạng VD: "24/05/2016"
        /// </summary>
        /// <param name="ngay_thang"></param>
        /// <returns></returns>
        public static string DateTimeToNgayThang(DateTime ngay_thang)
        {

            string tmp = ngay_thang.ToString("dd/MM/yyyy");
            return tmp;
        }

        /// <summary>
        /// Made By Phạm Văn Thủy
        /// Mục đích: Chuyển DateTime về dạng VD: "29-Jan-2016"
        /// </summary>
        /// <param name="ngay_thang"></param>
        /// <returns></returns>
        public static string DatimeToDDMMMYYY(DateTime ngay_thang)
        {
            string d = ngay_thang.ToString("dd");
            string m = ngay_thang.ToString("MM");
            string y = ngay_thang.ToString("yyyy");

            string M = null;
            int month = Convert.ToInt32(m);

            switch (month)
            {
                case 1: M = "Jan"; break;
                case 2: M = "Feb"; break;
                case 3: M = "Mar"; break;
                case 4: M = "Apr"; break;
                case 5: M = "May"; break;
                case 6: M = "Jun"; break;
                case 7: M = "Jul"; break;
                case 8: M = "Aug"; break;
                case 9: M = "Sep"; break;
                case 10: M = "Oct"; break;
                case 11: M = "Nov"; break;
                case 12: M = "Dec"; break;
            }

            return (d + "-" + M + "-" + y);
        }

        /// <summary>
        /// Made By Phạm Văn Thủy
        /// Mục đích: Chuyển DateTime về dạng VD: "29-Jan-16"
        /// </summary>
        /// <param name="ngay_thang"></param>
        /// <returns></returns>
        public static string DatimeToDDMMYY(DateTime ngay_thang)
        {
            string d = ngay_thang.ToString("dd");
            string m = ngay_thang.ToString("MM");
            string y = ngay_thang.ToString("yyyy");
            y = y.Substring(2, 2);

            string M = null;
            int month = Convert.ToInt32(m);

            switch (month)
            {
                case 1: M = "Jan"; break;
                case 2: M = "Feb"; break;
                case 3: M = "Mar"; break;
                case 4: M = "Apr"; break;
                case 5: M = "May"; break;
                case 6: M = "Jun"; break;
                case 7: M = "Jul"; break;
                case 8: M = "Aug"; break;
                case 9: M = "Sep"; break;
                case 10: M = "Oct"; break;
                case 11: M = "Nov"; break;
                case 12: M = "Dec"; break;
            }

            return (d + "-" + M + "-" + y);
        }


        public static string DatimeToNgayIn(DateTime ngay_thang)
        {
            string d = ngay_thang.ToString("dd");
            string m = ngay_thang.ToString("MM");
            string y = ngay_thang.ToString("yyyy");

            string result = "Ngày " + d + " tháng " + m + " năm " + y;

            return result;
        }

        /// <summary>
        /// Cắt chuỗi
        /// </summary>
        /// <param name="str"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string CatChuoiByLength(string str, int length)
        {
            string val = "";

            if (str.Length > length) val = str.Substring(0, length);
            else val = str;

            return val;
        }


        public static TimeFilter GetDateByYear(int? Ky_baocao)
        {
            int nam = Ky_baocao ?? DateTime.Now.Year;
            TimeFilter timeFilter = new TimeFilter();
            timeFilter.tuNgay = new DateTime(nam, 1, 1);
            timeFilter.denNgay = new DateTime(nam, 12, 31);
            return timeFilter;
        }
        public static TimeFilter GetDateByMonth(int? nam, int? thang)
        {
            int _nam = nam ?? DateTime.Now.Year;
            int _thang = thang ?? DateTime.Now.Month;
            TimeFilter timeFilter = new TimeFilter();
            var firstDayOfMonth = new DateTime(_nam, _thang, 1);
            timeFilter.tuNgay = firstDayOfMonth;
            timeFilter.denNgay = firstDayOfMonth.AddMonths(1).AddDays(-1);
            return timeFilter;
        }

        public static TimeFilter GetDateByQuarter(int? nam, int? quy)
        {
            int _nam = nam ?? DateTime.Now.Year;
            int _quy = quy ?? (DateTime.Now.Month - 1) / 3 + 1; ;
            TimeFilter timeFilter = new TimeFilter();
            DateTime firstDayOfQuarter = new DateTime(_nam, (_quy - 1) * 3 + 1, 1);
            timeFilter.tuNgay = firstDayOfQuarter;
            timeFilter.denNgay = firstDayOfQuarter.AddMonths(3).AddDays(-1);
            return timeFilter;
        }


        public static int? GetLastMonthByQuarter(int? quarter)
        {
            int? Thang = 2;
            for (int i = 1; i <= 4; i++)
            {
                for (int j = 3 * i; j >= 3 * i - 2; j--)
                {
                    if (i == quarter)
                    {
                        Thang = j;
                        break;
                    }
                }

            }
            return Thang;

        }
    }
    public class ImportDbfHelpers
    {
        // This is the file header for a DBF. We do this special layout with everything
        // packed so we can read straight from disk into the structure to populate it
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
        private struct DBFHeader
        {
            public byte version;
            public byte updateYear;
            public byte updateMonth;
            public byte updateDay;
            public Int32 numRecords;
            public Int16 headerLen;
            public Int16 recordLen;
            public Int16 reserved1;
            public byte incompleteTrans;
            public byte encryptionFlag;
            public Int32 reserved2;
            public Int64 reserved3;
            public byte MDX;
            public byte language;
            public Int16 reserved4;
        }

        // This is the field descriptor structure. There will be one of these for each column in the table.
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
        private struct FieldDescriptor
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 11)]
            public string fieldName;
            public char fieldType;
            public Int32 address;
            public byte fieldLen;
            public byte count;
            public Int16 reserved1;
            public byte workArea;
            public Int16 reserved2;
            public byte flag;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 7)]
            public byte[] reserved3;
            public byte indexFlag;
        }
        public static byte[] ReadFile(string filePath)
        {
            byte[] buffer;
            FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            try
            {
                int length = (int)fileStream.Length;  // get file length    
                buffer = new byte[length];            // create buffer     
                int count;                            // actual number of bytes read     
                int sum = 0;                          // total number of bytes read    

                // read until Read method returns 0 (end of the stream has been reached)    
                while ((count = fileStream.Read(buffer, sum, length - sum)) > 0)
                    sum += count;  // sum is a buffer offset for next reading
            }
            finally
            {
                fileStream.Close();
            }
            return buffer;
        }
        // Read an entire standard DBF file into a DataTable
        // Read an entire standard DBF file into a DataTable
        public static DataTable ReadDBF(string dbfFile)
        {
            long start = DateTime.Now.Ticks;
            DataTable dt = new DataTable();
            BinaryReader recReader;
            string number;
            string year;
            string month;
            string day;
            long lDate;
            long lTime;
            DataRow row;
            int fieldIndex;

            // If there isn't even a file, just return an empty DataTable
            if ((false == File.Exists(dbfFile)))
            {
                return dt;
            }

            BinaryReader br = null;
            try
            {
                // Read the header into a buffer
                br = new BinaryReader(File.OpenRead(dbfFile));
                byte[] buffer = br.ReadBytes(Marshal.SizeOf(typeof(DBFHeader)));

                // Marshall the header into a DBFHeader structure
                GCHandle handle = GCHandle.Alloc(buffer, GCHandleType.Pinned);
                DBFHeader header = (DBFHeader)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(DBFHeader));
                handle.Free();

                // Read in all the field descriptors. Per the spec, 13 (0D) marks the end of the field descriptors
                ArrayList fields = new ArrayList();


                while ((13 != br.PeekChar()))
                {
                    buffer = br.ReadBytes(Marshal.SizeOf(typeof(FieldDescriptor)));
                    handle = GCHandle.Alloc(buffer, GCHandleType.Pinned);
                    FieldDescriptor tmp = (FieldDescriptor)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(FieldDescriptor));

                    //Bỏ đi cột cuối cùng
                    if (tmp.fieldName != "_NullFlags")
                    {
                        fields.Add(tmp);
                    }

                    handle.Free();
                }// Read in the first row of records, we need this to help determine column types below
                ((FileStream)br.BaseStream).Seek(header.headerLen + 1, SeekOrigin.Begin);
                buffer = br.ReadBytes(header.recordLen);
                recReader = new BinaryReader(new MemoryStream(buffer));
                // Create the columns in our new DataTable
                DataColumn col = null;
                foreach (FieldDescriptor field in fields)
                {
                    number = Encoding.ASCII.GetString(recReader.ReadBytes(field.fieldLen));
                    switch (field.fieldType)
                    {
                        case 'N':
                            if (number.IndexOf(".") > -1)
                            {
                                col = new DataColumn(field.fieldName, typeof(decimal));
                            }
                            else
                            {
                                col = new DataColumn(field.fieldName, typeof(int));
                            }
                            break;
                        case 'C':
                            col = new DataColumn(field.fieldName, typeof(string));
                            break;
                        case 'T':
                            // You can uncomment this to see the time component in the grid
                            //col = new DataColumn(field.fieldName, typeof(string));
                            col = new DataColumn(field.fieldName, typeof(DateTime));
                            break;
                        case 'D':
                            col = new DataColumn(field.fieldName, typeof(DateTime));
                            break;
                        case 'L':
                            col = new DataColumn(field.fieldName, typeof(bool));
                            break;
                        case 'F':
                            col = new DataColumn(field.fieldName, typeof(Double));
                            break;
                        default:
                            // Mặc định định dạng khi không xác định được sẽ là string
                             col = new DataColumn(field.fieldName, typeof(string));
                            break;
                    }
                    dt.Columns.Add(col);
                }

                // Skip past the end of the header. 
                ((FileStream)br.BaseStream).Seek(header.headerLen, SeekOrigin.Begin);

                // Read in all the records
                for (int counter = 0; counter <= header.numRecords - 1; counter++)
                {
                    // First we'll read the entire record into a buffer and then read each field from the buffer
                    // This helps account for any extra space at the end of each record and probably performs better
                    buffer = br.ReadBytes(header.recordLen);
                    recReader = new BinaryReader(new MemoryStream(buffer));

                    // All dbf field records begin with a deleted flag field. Deleted - 0x2A (asterisk) else 0x20 (space)
                    if (recReader.ReadChar() == '*')
                    {
                        continue;
                    }

                    // Loop through each field in a record
                    fieldIndex = 0;
                    row = dt.NewRow();
                    foreach (FieldDescriptor field in fields)
                    {
                        FieldInfo field1;
                        switch (field.fieldType)
                        {
                            case 'N':  // Number
                                // If you port this to .NET 2.0, use the Decimal.TryParse method
                                number = Encoding.ASCII.GetString(recReader.ReadBytes(field.fieldLen)).ToString();
                              
                       
                                if (string.IsNullOrWhiteSpace(number))
                                {
                                    row[fieldIndex] = 0;
                                }
                                else
                                {
                                    number = number.Trim();
                                    // nếu ký tự đầu tiên  là dấu . không đọc được
                                    if (number[0] == '.')
                                    {
                                        row[fieldIndex] = 0;
                                    }
                                    else
                                    {
                                        Decimal val = decimal.Parse(number.Trim());
                                        row[fieldIndex] = val;
                                    }
                                  
                                }
                                // Chuyển tất cả sang decimal

                                //if (IsNumber(number.Trim()))
                                //{
                                //    if (number.IndexOf(".") > -1)
                                //    {
                                //        row[fieldIndex] = decimal.Parse(number.Trim());
                                //    }
                                //    else
                                //    {
                                //        row[fieldIndex] = int.Parse(number.Trim());
                                //    }
                                //}
                                //else
                                //{
                                //    row[fieldIndex] = 0;
                                //}

                                break;

                            case 'C': // String
                                string strValue = Encoding.Default.GetString(recReader.ReadBytes(field.fieldLen));
                                row[fieldIndex] = QLQ.Core.Common.ConvertCommon.ConvertFontTCVN3.TCVN3ToUnicode(strValue);
                                //row[fieldIndex] = Encoding.ASCII.GetString(recReader.ReadBytes(field.fieldLen));
                                break;

                            case 'D': // Date (YYYYMMDD)
                                year = Encoding.ASCII.GetString(recReader.ReadBytes(4));
                                month = Encoding.ASCII.GetString(recReader.ReadBytes(2));
                                day = Encoding.ASCII.GetString(recReader.ReadBytes(2));
                                row[fieldIndex] = System.DBNull.Value;
                                if (IsNumber(year) && IsNumber(month) && IsNumber(day))
                                {
                                    if ((Int32.Parse(year) > 1900))
                                    {
                                        row[fieldIndex] = new DateTime(Int32.Parse(year), Int32.Parse(month), Int32.Parse(day));
                                    }
                                }



                                break;

                            case 'T': // Timestamp, 8 bytes - two integers, first for date, second for time
                                // Date is the number of days since 01/01/4713 BC (Julian Days)
                                // Time is hours * 3600000L + minutes * 60000L + Seconds * 1000L (Milliseconds since midnight)
                                lDate = recReader.ReadInt32();
                                lTime = recReader.ReadInt32() * 10000L;
                                row[fieldIndex] = JulianToDateTime(lDate).AddTicks(lTime);
                                break;

                            case 'L': // Boolean (Y/N)
                                if ('Y' == recReader.ReadByte())
                                {
                                    row[fieldIndex] = true;
                                }
                                else
                                {
                                    row[fieldIndex] = false;
                                }

                                break;

                            case 'F':

                                number = Encoding.ASCII.GetString(recReader.ReadBytes(field.fieldLen));
                                // Chuyển tất cả sang decimal
                                if (string.IsNullOrWhiteSpace(number))
                                {
                                    row[fieldIndex] = 0;

                                }
                                else
                                {
                                    Decimal val1 = decimal.Parse(number.Trim());
                                    row[fieldIndex] = val1;
                                }

                                //if (IsNumber(number))
                                //{
                                //    row[fieldIndex] = double.Parse(number);
                                //}
                                //else
                                //{
                                //    row[fieldIndex] = 0.0F;
                                //}
                                break;
                            default:
                                row[fieldIndex] = Encoding.Default.GetString(recReader.ReadBytes(field.fieldLen));
                                break;
                        }
                        fieldIndex++;
                    }

                    recReader.Close();
                    dt.Rows.Add(row);
                }
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                if (null != br)
                {
                    br.Close();
                }
            }

            long count = DateTime.Now.Ticks - start;

            return dt;
        }

        /// <summary>
        /// Simple function to test is a string can be parsed. There may be a better way, but this works
        /// If you port this to .NET 2.0, use the new TryParse methods instead of this
        ///   *Thanks to wu.qingman on code project for fixing a bug in this for me
        /// </summary>
        /// <param name="number">string to test for parsing</param>
        /// <returns>true if string can be parsed</returns>
        public static bool IsNumber(string numberString)
        {
            char[] numbers = numberString.ToCharArray();
            int number_count = 0;
            int point_count = 0;
            int space_count = 0;

            foreach (char number in numbers)
            {
                if ((number >= 48 && number <= 57))
                {
                    number_count += 1;
                }
                else if (number == 46)
                {
                    point_count += 1;
                }
                else if (number == 32)
                {
                    space_count += 1;
                }
                else
                {
                    return false;
                }
            }

            return (number_count > 0 && point_count < 2);
        }

        /// <summary>
        /// Convert a Julian Date to a .NET DateTime structure
        /// Implemented from pseudo code at http://en.wikipedia.org/wiki/Julian_day
        /// </summary>
        /// <param name="lJDN">Julian Date to convert (days since 01/01/4713 BC)</param>
        /// <returns>DateTime</returns>
        private static DateTime JulianToDateTime(long lJDN)
        {
            double p = Convert.ToDouble(lJDN);
            double s1 = p + 68569;
            double n = Math.Floor(4 * s1 / 146097);
            double s2 = s1 - Math.Floor((146097 * n + 3) / 4);
            double i = Math.Floor(4000 * (s2 + 1) / 1461001);
            double s3 = s2 - Math.Floor(1461 * i / 4) + 31;
            double q = Math.Floor(80 * s3 / 2447);
            double d = s3 - Math.Floor(2447 * q / 80);
            double s4 = Math.Floor(q / 11);
            double m = q + 2 - 12 * s4;
            double j = 100 * (n - 49) + i + s4;
            return new DateTime(Convert.ToInt32(j), Convert.ToInt32(m), Convert.ToInt32(d));
        }





    }
}
