﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace QLQ.Core.Common
{
    public class CheckChangePassword
    {
        private string password;

        public CheckChangePassword()
        {
            this.password = "123456";
        }
        public string GetMd5Hash(MD5 md5Hash, string input)
        {

            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }

        public bool CheckChange(string input)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                string hash_pass = GetMd5Hash(md5Hash, input);
                string hash_pass_first = GetMd5Hash(md5Hash, this.password);

                if (hash_pass != hash_pass_first)
                {
                    return true;
                }
            }

            return false;
        }

        public bool CheckChangePass(string input)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                string hash_pass_first = GetMd5Hash(md5Hash, this.password);

                if (input.ToUpper() != hash_pass_first.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

        public bool VerifyMd5Hash(MD5 md5Hash, string input, string hash)
        {
            string hashOfInput = GetMd5Hash(md5Hash, input);

            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}