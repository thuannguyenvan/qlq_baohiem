﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Xml;

namespace QLQ.Core.Common
{
    public class SoapServiceCls
    {
       

        public List<T> GetDataService<T>(HeaderParams hParam, Dictionary<string, string> lstParamBody)
        {
            string strXml = "";
            try
            {
                string soap = GetSoap(hParam, lstParamBody);
                  
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://vipotd.bhxh.vn:8081/soa-infra/services/STANDARD/ArchServices/SOAPServiceBHXH");
                req.Headers.Add(String.Format("SOAPAction: \"{0}\"",
                            "baohiemxahoi.gov.vn/SOAPServiceBHXH/" + hParam.SvcName));
                req.ContentType = "application/soap+xml;charset=\"utf-8\"";
                req.Accept = "gzip,deflate";
                req.Method = "POST";
            
                using (Stream stm = req.GetRequestStream())
                {
                    using (StreamWriter stmw = new StreamWriter(stm))
                    {
                        stmw.Write(soap);
                    }
                }

                HttpWebResponse response = (HttpWebResponse)req.GetResponse();
                HttpStatusCode status = response.StatusCode;
                int HTTPcode = Convert.ToInt32(status);

                StreamReader srd = new StreamReader(response.GetResponseStream());

                strXml = srd.ReadToEnd();
                srd.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //response.Close();
            }

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(strXml);

            XmlNodeList nodeList = doc.GetElementsByTagName(hParam.DataTagName);

            List<T> records = new List<T>();

            for (int i = 0; i < nodeList.Count; i++) // Từng bản ghi
            {

                T entity = Activator.CreateInstance<T>();

                XmlNode xn = nodeList[i];

                // Duyệt node con
                for (var j = 0; j < xn.ChildNodes.Count; j++) // Từng thuộc tính
                {
                    var attr = xn.ChildNodes[j].Name;
                    attr = attr.Substring(4, attr.Length - 4);
                    var value = xn.ChildNodes[j].InnerText;

                    SetValueProperty(entity, attr, value);
                }

                records.Add(entity);

            }

            return records;
        }

        #region Utilities

        private string GetSoap(HeaderParams hParam, Dictionary<string, string> lstParamBody)
        {
            string body = GetBoDyParam(hParam.SvcName, lstParamBody);

            string soap = @"<soap:Envelope xmlns:soap=""http://www.w3.org/2003/05/soap-envelope"" xmlns:req=""http://request.register.soa/"">" +
             "<soap:Header>" +
                "<req:AppHdr>" +
                   "<req:From>" + hParam.From + "</req:From>" +
                   "<req:To>" + hParam.To + "</req:To>" +
                   "<req:CreDt>" + hParam.CreDt + "</req:CreDt>" +
                   "<req:Prty>" + hParam.Prty + "</req:Prty>" +
                   "<req:Sgntr>" + hParam.Sgntr + "</req:Sgntr>" +
                   "<req:SvcName>" + hParam.SvcName + "</req:SvcName>" + "</req:AppHdr> </soap:Header>" +
             "<soap:Body>" + body + "</soap:Body> </soap:Envelope>";

            return soap;
        }

        private string GetBoDyParam(string SvcName, Dictionary<string, string> lstParamBody)
        {
            string body = "";
            string inner = "";
            string method = SvcName + "Request";

            foreach (KeyValuePair<string, string> entry in lstParamBody)
            {
                inner = inner + "<req:" + entry.Key + ">" + entry.Value + "</req:" + entry.Key + ">";
            }

            body = "<" + method + ">" + inner + "</" + method + ">";

            return body;
        }

        //Set Value for a attribute of a entity
        private void SetValueProperty(object entity, string colName, object val)
        {
            Type instanceType = entity.GetType();

            //Get Property
            PropertyInfo pi = instanceType.GetProperty(colName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

            if (pi != null)
            {
                string type = pi.PropertyType.FullName;

                // Các kiểu dữ liệu
                switch (type)
                {
                    case "System.Int32": val = Convert.ToInt32(val); break;
                    case "System.Int64": val = Convert.ToInt64(val); break;
                    case "System.Decimal": val = Convert.ToDecimal(val); break;
                    case "System.DateTime": val = Convert.ToDateTime(val); break;
                    case "System.Double": val = Convert.ToDouble(val); break;
                    default: val = val.ToString(); break;
                }

                pi.SetValue(entity, val, null);
            }
        }

        #endregion

    }

    public class HeaderParams
    {
        public string From { get; set; }
        public string To { get; set; }
        public string CreDt { get; set; }
        public string Prty { get; set; }
        public string Sgntr { get; set; }

        //method
        public string SvcName { get; set; }

        //tên thẻ bao bọc dữ liệu
        public string DataTagName { get; set; }
    }
}