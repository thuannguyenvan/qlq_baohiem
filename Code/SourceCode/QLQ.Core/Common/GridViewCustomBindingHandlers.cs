﻿using DevExpress.Data;
using DevExpress.Data.Linq.Helpers;
using DevExpress.Web.Mvc;
using log4net;
using System;
using System.Linq;
using QLQ.Core.Domain;

//using System.Text;

namespace QLQ.Core.Common
{
    public class GridViewCustomBindingHandlers
    {
        public IQueryable Model { get; set; }
        private static readonly ILog log = LogManager.GetLogger(typeof(GridViewCustomBindingHandlers));
        //private NGUOIDUNG nguoiDung = ((QLQ.Core.ACCContext)FX.Context.FXContext.Current).CurrentNguoidung;
        public void GetDataRowCountSimple(GridViewCustomBindingGetDataRowCountArgs e)
        {
            //Lấy số lượng bản ghi thực tế gán vào biến DataRowCount
            e.DataRowCount = Model.Count();
        }
        public void GetDataSimple(GridViewCustomBindingGetDataArgs e)
        {
            // Lấy Data thực tế dựa trên trường tìm kiếm, trang tìm kiếm, pageSize;
            try
            {
                e.Data = Model
             .ApplySorting(e.State.SortedColumns)
             .Skip(e.StartDataRowIndex)
             .Take(e.DataRowCount);
            }
            catch (Exception ex)
            {

                log.Error( " GetDataSimple Exception: " + ex.InnerException.Message);
            }

        }
        public void GetDataRowCountAdvanced(GridViewCustomBindingGetDataRowCountArgs e)
        {
            // Lấy giá trị RowCount theo điều kiện, cột tương ứng
            log.Error( " GetDataRowCountAdvanced " + "1.5.7.1:" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff",
                                      System.Globalization.CultureInfo.InvariantCulture));
            int rowCount;
            try
            {
                if (GridViewCustomBindingSummaryCache.TryGetCount(e.FilterExpression, out rowCount))
                    e.DataRowCount = rowCount;
                else
                    e.DataRowCount = Model.ApplyFilter(e.FilterExpression).Count();
            }
            catch (Exception ex)
            {
                log.Error( " GetDataRowCountAdvanced Exception: " + ex.InnerException.Message);
            }

            log.Error( " GetDataRowCountAdvanced " + "1.5.7.2:" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff",
                                      System.Globalization.CultureInfo.InvariantCulture));
        }
        public void GetUniqueHeaderFilterValuesAdvanced(GridViewCustomBindingGetUniqueHeaderFilterValuesArgs e)
        {
            log.Error( " GetUniqueHeaderFilterValuesAdvanced " + "1.5.7.11:" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff",
                                     System.Globalization.CultureInfo.InvariantCulture));
            try
            {
                e.Data = Model
             .ApplyFilter(e.FilterExpression)
             .UniqueValuesForField(e.FieldName);
            }
            catch (Exception ex)
            {

                log.Error( " GetUniqueHeaderFilterValuesAdvanced Exception: " + ex.InnerException.Message);
            }


            log.Error( " GetUniqueHeaderFilterValuesAdvanced " + "1.5.7.12:" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff",
                                    System.Globalization.CultureInfo.InvariantCulture));
        }
        public void GetGroupingInfoAdvanced(GridViewCustomBindingGetGroupingInfoArgs e)
        {
            log.Error( " GetGroupingInfoAdvanced " + "1.5.7.9:" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff",
                                      System.Globalization.CultureInfo.InvariantCulture));
            try
            {
                e.Data = Model
             .ApplyFilter(e.FilterExpression)
             .ApplyFilter(e.GroupInfoList)
             .GetGroupInfo(e.FieldName, e.SortOrder);
            }
            catch (Exception ex)
            {
                log.Error( " GetUniqueHeaderFilterValuesAdvanced Exception: " + ex.InnerException.Message);
            }

            log.Error( " GetGroupingInfoAdvanced " + "1.5.7.10:" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff",
                                     System.Globalization.CultureInfo.InvariantCulture));
        }
        public void GetDataAdvanced(GridViewCustomBindingGetDataArgs e)
        {
            log.Error( " GetDataAdvanced " + "1.5.7.3:" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff",
                                     System.Globalization.CultureInfo.InvariantCulture));
            try
            {
                e.Data = Model
                    .ApplyFilter(e.FilterExpression)
                    .ApplyFilter(e.GroupInfoList)
                    .ApplySorting(e.State.SortedColumns)
                    .Skip(e.StartDataRowIndex)
                    .Take(e.DataRowCount);
            }
            catch (Exception ex)
            {

                log.Error( " GetDataAdvanced Exception: " + ex.InnerException.Message);
            }
            log.Error( " GetDataAdvanced " + "1.5.7.4:" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff",
                                     System.Globalization.CultureInfo.InvariantCulture));
        }
        public void GetSummaryValuesAdvanced(GridViewCustomBindingGetSummaryValuesArgs e)
        {
            log.Error( " GetSummaryValuesAdvanced " + "1.5.7.5:" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff",
                                      System.Globalization.CultureInfo.InvariantCulture));
            try
            {
                var query = Model
              .ApplyFilter(e.FilterExpression)
              .ApplyFilter(e.GroupInfoList);
                log.Error( " GetSummaryValuesAdvanced " + "1.5.7.6:" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff",
                                          System.Globalization.CultureInfo.InvariantCulture));
                var summaryValues = query.CalculateSummary(e.SummaryItems);
                e.Data = summaryValues;
                log.Error( " GetSummaryValuesAdvanced " + "1.5.7.7:" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff",
                                          System.Globalization.CultureInfo.InvariantCulture));
                var countSummaryItem = e.SummaryItems.FirstOrDefault(i => i.SummaryType == SummaryItemType.Count);
                if (e.GroupInfoList.Count == 0 && countSummaryItem != null)
                {
                    log.Error( " GetSummaryValuesAdvanced " + "1.5.7.7.1:" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff",
                                          System.Globalization.CultureInfo.InvariantCulture));
                    var itemIndex = e.SummaryItems.IndexOf(countSummaryItem);
                    var count = summaryValues[itemIndex] != null ? Convert.ToInt32(summaryValues[itemIndex]) : 0;
                    GridViewCustomBindingSummaryCache.SaveCount(e.FilterExpression, count);
                    log.Error( " GetSummaryValuesAdvanced " + "1.5.7.7.2:" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff",
                                          System.Globalization.CultureInfo.InvariantCulture));
                }
            }
            catch (Exception ex)
            {
                log.Error( " GetSummaryValuesAdvanced Exception: " + ex.InnerException.Message);
            }
          
            log.Error( " GetSummaryValuesAdvanced " + "1.5.7.8:" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff",
                                      System.Globalization.CultureInfo.InvariantCulture));
        }
    }
}
