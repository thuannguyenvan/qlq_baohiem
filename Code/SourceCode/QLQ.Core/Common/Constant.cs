﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QLQ.Core.Common
{
    public abstract class Constant
    {
        public abstract class Session
        {
            // tuyen_ seesion madvql , namlv
            public const string _SessionUserData = "_SessionUserData";
            public const string _SessionDanhMucKH = "_SessionDanhMucKH"; 
        }
        #region List Code
        public abstract class ListCode
        {
            //Trạng thái Hoá đơn
            public const string DNK = "Đã nhập kho";
            public const string DM = "Đang mở";
        }
        #endregion        
        #region ConfigKey

        public class ConfigKey
        {

            public const string DisplayDateFormat = "DisplayDateFormat";
            public const string PageSize = "PageSize";
        }

        #endregion
        #region EmailSubject
        public abstract class EmailSubject
        {
            public const string ChangePassword = "ChangePassword";
            public const string NewPassword = "NewPassword";
        }
        #endregion
        #region DatabaseType
        public abstract class DatabaseType
        {
            public const string Oracle = "Oracle";
            public const string Sql = "Sql";
            public const string Odbc = "Odbc";
            public const string OleDb = "OleDb";
        }
        #endregion
    }
}