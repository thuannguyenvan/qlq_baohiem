﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web;
using Newtonsoft.Json;

namespace QLQ.Core.Common
{
    public static class ConvertCommon
    {
        // Clone Value của 1 object dữ liệu của list Clone sau khi thay đổi không ảnh hưởng đến list gốc
        // Đối được được Clone phải đặt [Serializable] trước Class
        public static T Clone<T>(this T source)
        {
            var serialized = JsonConvert.SerializeObject(source);
            return JsonConvert.DeserializeObject<T>(serialized);
        }
        public static TSelf TrimStringProperties<TSelf>(this TSelf input)
        {
            var stringProperties = input.GetType().GetProperties()
                .Where(p => p.PropertyType == typeof (string));

            foreach (var stringProperty in stringProperties)
            {
                string currentValue = (string) stringProperty.GetValue(input, null);
                if (!string.IsNullOrEmpty(currentValue) && stringProperty.CanWrite)
                    stringProperty.SetValue(input, currentValue.Trim(), null);
            }
            return input;
        }
        public static TSelf DateTimeProperties<TSelf>(this TSelf input)
        {
            var name = new[] { "NGAY", "NGAY_THANG", "NGAYTHANG" };  
            var dateProperties = input.GetType().GetProperties()
                .Where(p => p.PropertyType == typeof(DateTime));

            foreach (var dateProp in dateProperties)
            {
                
                if (name.Contains(dateProp.Name.ToUpper()))
                {
                    DateTime currentValue = (DateTime)dateProp.GetValue(input, null);
                    dateProp.SetValue(input, currentValue.Date, null);
                }
               
            }
            var nullableDateProp = input.GetType().GetProperties().Where(p => p.PropertyType == typeof(DateTime?));
            foreach (var dateProp in nullableDateProp)
            {
                DateTime? currentValue = (DateTime?)dateProp.GetValue(input, null);
                if (currentValue != null && name.Contains(dateProp.Name.ToUpper()))
                {
                    dateProp.SetValue(input, ((DateTime)currentValue).Date, null);
                }
               
            }
            return input;
        }
        public static T FormatPropBeforeSaveDb<T>(this T input)
        {
            // Trim tất cả dữ liệu dạng string có trong Object
            var stringProperties = input.GetType().GetProperties()
               .Where(p => p.PropertyType == typeof(string));

            foreach (var stringProperty in stringProperties)
            {
                string currentValue = (string)stringProperty.GetValue(input, null);
                if (!string.IsNullOrEmpty(currentValue))
                    stringProperty.SetValue(input, currentValue.Trim(), null);
            }
            // Chuẩn hóa tất cả giá trị DateTime về short Date trong Object
            var dateProperties = input.GetType().GetProperties()

                .Where(p => p.PropertyType == typeof(DateTime));

            foreach (var dateProp in dateProperties)
            {
                DateTime currentValue = (DateTime)dateProp.GetValue(input, null);
                dateProp.SetValue(input, currentValue.Date, null);
            }
            // Chuẩn hóa tất cả giá trị DateTime? về short Date nếu có dữ liệu trong Object
            var nullableDateProp = input.GetType().GetProperties().Where(p => p.PropertyType == typeof(DateTime?));
            foreach (var dateProp in nullableDateProp)
            {
                DateTime? currentValue = (DateTime?)dateProp.GetValue(input, null);
                if (currentValue != null)
                {
                    dateProp.SetValue(input, ((DateTime)currentValue).Date, null);
                }

            }
            return input;
        }
        public static DateTime ConvertToDateTimeExcel(this string str)
        {
            DateTime ngaythang;
            if (str.IndexOf('/') > 0)
            {
                ngaythang = Convert.ToDateTime(str);
            }
            else
            {
                double d = double.Parse(str);
                ngaythang = DateTime.FromOADate(d);
            }
            return ngaythang;
        }

        public class ConvertFontTCVN3
        {
            private static char[] tcvnchars = {
        'µ', '¸', '¶', '·', '¹', 
        '¨', '»', '¾', '¼', '½', 'Æ', 
        '©', 'Ç', 'Ê', 'È', 'É', 'Ë', 
        '®', 'Ì', 'Ð', 'Î', 'Ï', 'Ñ', 
        'ª', 'Ò', 'Õ', 'Ó', 'Ô', 'Ö', 
        '×', 'Ý', 'Ø', 'Ü', 'Þ', 
        'ß', 'ã', 'á', 'â', 'ä', 
        '«', 'å', 'è', 'æ', 'ç', 'é', 
        '¬', 'ê', 'í', 'ë', 'ì', 'î', 
        'ï', 'ó', 'ñ', 'ò', 'ô', '­', 'õ', 'ø', 'ö', '÷', 'ù', 
        'ú', 'ý', 'û', 'ü', 'þ', 
        '¡', '¢', '§', '£', '¤', '¥', '¦'
    };

            private static char[] unichars = {
        'à', 'á', 'ả', 'ã', 'ạ', 
        'ă', 'ằ', 'ắ', 'ẳ', 'ẵ', 'ặ', 
        'â', 'ầ', 'ấ', 'ẩ', 'ẫ', 'ậ', 
        'đ', 'è', 'é', 'ẻ', 'ẽ', 'ẹ', 
        'ê', 'ề', 'ế', 'ể', 'ễ', 'ệ', 
        'ì', 'í', 'ỉ', 'ĩ', 'ị', 
        'ò', 'ó', 'ỏ', 'õ', 'ọ', 
        'ô', 'ồ', 'ố', 'ổ', 'ỗ', 'ộ', 
        'ơ', 'ờ', 'ớ', 'ở', 'ỡ', 'ợ', 
        'ù', 'ú', 'ủ', 'ũ', 'ụ', 
        'ư', 'ừ', 'ứ', 'ử', 'ữ', 'ự', 
        'ỳ', 'ý', 'ỷ', 'ỹ', 'ỵ', 
        'Ă', 'Â', 'Đ', 'Ê', 'Ô', 'Ơ', 'Ư'
    };

            private static char[] convertTable;

            static ConvertFontTCVN3()
            {
                convertTable = new char[256];
                for (int i = 0; i < 256; i++)
                    convertTable[i] = (char)i;
                for (int i = 0; i < tcvnchars.Length; i++)
                    convertTable[tcvnchars[i]] = unichars[i];
            }

            public static string TCVN3ToUnicode(string value)
            {
                char[] chars = value.ToCharArray();
                for (int i = 0; i < chars.Length; i++)
                    if (chars[i] < (char)256)
                        chars[i] = convertTable[chars[i]];
                return new string(chars);
            }

            /// <summary>
            /// Author: Phạm Văn Thủy
            /// Descriptions: Convert Font from Unicode To VN3
            /// Mapping 1-1 theo chỉ số nếu tìm thấy
            /// </summary>
            /// <param name="value"></param>
            /// <returns></returns>
            public static string UnicodeToTCVN3(string value)
            {
                if (string.IsNullOrEmpty(value))
                {
                    return "";
                }

                //Chuyển sang array character
                char[] chars = value.ToCharArray();

                //Mapping 1-1: unichar = vn3char
                for (int i = 0; i < chars.Length; i++)
                {
                    char c = chars[i];

                    //Tìm chỉ số của c trong unichars
                    int index = Array.IndexOf(unichars, c);

                    // Nếu index > -1 (tìm thấy) thì gán chars[i] = vn3char[index]
                    if (index > -1)
                    {
                        char temp = tcvnchars[index];
                        chars[i] = temp;
                    }

                }

                return new string(chars);
            }
        }
    }
}