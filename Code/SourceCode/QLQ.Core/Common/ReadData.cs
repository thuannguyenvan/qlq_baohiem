﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Reflection;
using System.Text;
using FX.Utils;
using Simple.Data.Ado;

namespace QLQ.Core.Common
{
    public class ReadData
    {

        public static Object Check<T>(T entity, IDataRecord record, int i)
        {
            Object s61 = new object();

            Type type = entity.GetType();
            if (DBNull.Value != record[i])
            {
                PropertyInfo property = type.GetProperty(record.GetName(i),
                    BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

                if (property != null)
                {
                    s61 = record.GetValue(i);
                }
            }

            return s61;
        }
        public static Decimal CheckNull<T>(T entity, IDataRecord record, int i)
        {
            Decimal s61 = 0;


            if (DBNull.Value != record[i])
            {

                Decimal.TryParse(record.GetValue(i).ToString(), out s61);
            }
            else
            {
                try
                {
                    Decimal.TryParse(record.GetValue(i).ToString(), out s61);
                }
                catch (Exception)
                {

                    return 0;
                }
            }

            return s61;
        }
        public static Object CheckName<T>(T entity, IDataRecord record, int i)
        {
            Object s61 = new object();

            Type type = entity.GetType();
            if (DBNull.Value != record[i])
            {
                PropertyInfo property = type.GetProperty(record.GetName(i),
                    BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

                if (property != null)
                {
                    s61 = record.GetName(i);
                }
            }

            return s61;
        }
        public static string CheckNameNull<T>(T entity, IDataRecord record, int i)
        {
            string s61 = "";
            if (DBNull.Value != record[i])
            {

                s61 = record.GetName(i).ToString() ?? "";
            }
            else
            {
                try
                {
                    s61 = record.GetName(i).ToString() ?? "";
                }
                catch (Exception)
                {

                    return "";
                }
            }

            return s61;
        }

        public static decimal ParseDecimal(string s)
        {
            try
            {

                return Decimal.Parse(s);
            }
            catch
            {
                return 0;
            }
        }
         
        public static decimal ParseDecimal(object s)
        {
            try
            {
                decimal b;
                //object a = Check<T>(entity, record, 4);
                // Decimal.TryParse(a.ToString(), out b);
                Decimal.TryParse(s.ToString(), out b);
                return b;
            }
            catch
            {
                return 0;
            }
        }
    }

    public class Read_S01
    {
        public static Decimal CheckNull<T>(T entity, IDataRecord record, int i)
        {
            Decimal s61 = 0;


            if (DBNull.Value != record[i])
            {

                Decimal.TryParse(record.GetValue(i).ToString(), out s61);
            }
            else
            {
                try
                {
                    Decimal.TryParse(record.GetValue(i).ToString(), out s61);
                }
                catch (Exception)
                {

                    return 0;
                }
            }

            return s61;
        }public static string CheckStringNull<T>(T entity, IDataRecord record, int i)
        {
            string s61 = "";


            if (DBNull.Value != record[i])
            {

               s61= record.GetValue(i).ToString();
            }
            else
            {
                try
                {
                    s61 = record.GetValue(i).ToString();
                }
                catch (Exception)
                {

                    return "";
                }
            }

            return s61;
        }
        public static string CheckNameNull<T>(T entity, IDataRecord record, int i)
        {
            string s61 = "";
            if (DBNull.Value != record[i])
            {

                s61 = record.GetName(i).ToString() ?? "";
            }
            else
            {
                try
                {
                    s61 = record.GetName(i).ToString() ?? "";
                }
                catch (Exception)
                {

                    return "";
                }
            }

            return s61;
        }
        public static Object Check<T>(T entity, IDataRecord record, int i)
        {
            Object s61 = new object();

            Type type = entity.GetType();
            if (DBNull.Value != record[i])
            {
                PropertyInfo property = type.GetProperty(record.GetName(i),
                    BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

                if (property != null)
                {
                    s61 = record.GetValue(i);
                }
            }

            return s61;
        }
    }

    public class Read_S01A_List_Con
    {

        public static List<string> GetPOBaseTListFromReader<T>(OracleDataReader reader)
        {
            // List<T> records = new List<T>();
            // records = PopulateEntities<T>(reader);
            List<string> records = new List<string>();
            records = PopulateEntities<T>(reader);
            return records;
        }

        public static List<string> PopulateEntities<T>(IDataReader dr)
        {
            
            List<string> listS01 = new List<string>();
            while (dr.Read())
            {
                
              //  T ent = Activator.CreateInstance<T>();
                string s01 = "";PopulateEntity<T>(  dr, s01);
                //  entities.Add(ent);
                listS01.Add(s01);
            }
            return listS01;
        }

        public static void PopulateEntity<T>(  IDataRecord record, string s01)
        {
            if (record != null && record.FieldCount > 0)
            {
                
                

               
                for (int i = 6; i < record.FieldCount; i++)
                {
                    
                    s01 = record.FieldCount > i ? CheckNameNull<T>(  record, i).Substring(2) : "";
                    i++;

                }
            }
        }
       
        public static string CheckNameNull<T>(  IDataRecord record, int i)
        {
            string s61 = "";
            if (DBNull.Value != record[i])
            {

                s61 = record.GetName(i).ToString() ?? "";
            }
            else
            {
                try
                {
                    s61 = record.GetName(i).ToString() ?? "";
                }
                catch (Exception)
                {

                    return "";
                }
            }

            return s61;
        }
       
    }
    public class Read_S01B_List_Con
    {


        public static List<string> GetPOBaseTListFromReader<T>(OracleDataReader reader)
        {
            // List<T> records = new List<T>();
            // records = PopulateEntities<T>(reader);
            List<string> records = new List<string>();
            records = PopulateEntities<T>(reader);
            return records;
        }

        public static List<string> PopulateEntities<T>(IDataReader dr)
        {

            List<string> listS01 = new List<string>();
            while (dr.Read())
            {
                string s01 = "";
                T ent = Activator.CreateInstance<T>();
                PopulateEntity<T>(ent, dr, s01);
                //  entities.Add(ent);
                listS01.Add(s01);
            }
            return listS01;
        }

        public static void PopulateEntity<T>(T entity, IDataRecord record, string s01)
        {
            if (record != null && record.FieldCount > 0)
            {




                for (int i = 6; i < record.FieldCount; i++)
                {

                    s01 = record.FieldCount > i ? CheckNameNull<T>(entity, record, i).Substring(2) : "";
                    i++;

                }
            }
        }

        public static string CheckNameNull<T>(T entity, IDataRecord record, int i)
        {
            string s61 = "";
            if (DBNull.Value != record[i])
            {

                s61 = record.GetName(i).ToString() ?? "";
            }
            else
            {
                try
                {
                    s61 = record.GetName(i).ToString() ?? "";
                }
                catch (Exception)
                {

                    return "";
                }
            }

            return s61;
        }

    }
}
