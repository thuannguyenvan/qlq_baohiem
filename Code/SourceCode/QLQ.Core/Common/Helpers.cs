﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.ComponentModel;
using QLQ.Core.Domain;

namespace QLQ.Core.Common
{
    public class Helpers
    {

        /// <summary>
        /// Trả về 1 list đối tượng (DTO) từ một DataReader
        /// Create by: Phạm Lê Hải
        /// Create date : 7/7/2016
        /// Update by: Phạm Lê Hải
        /// Update date: 7/7/2016
        /// </summary>
        public static List<T> GetPOBaseTListFromReader<T>(OracleDataReader reader)
        {
            List<T> records = new List<T>();
            records = PopulateEntities<T>(reader);
            return records;
        }
        /// <summary>
        /// Mapping tự động các giá trị từ IDataReader sang 1 list DTO
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dr"></param>
        /// <returns></returns>
        public static List<T> PopulateEntities<T>(IDataReader dr)
        {
            List<T> entities = new List<T>();
            while (dr.Read())
            {
                T ent = Activator.CreateInstance<T>();
                PopulateEntity<T>(ent, dr);
                entities.Add(ent);
            }
            return entities;
        }
        /// <summary>
        /// Tự động mapping các giá trị của IDataRecord sang đối tượng tương ứng
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <param name="record"></param>
        public static void PopulateEntity<T>(T entity, IDataRecord record)
        {
            if (record != null && record.FieldCount > 0){
                Type type = entity.GetType();

                for (int i = 0; i < record.FieldCount; i++)
                {
                    if (DBNull.Value != record[i])
                    {
                        PropertyInfo property = type.GetProperty(record.GetName(i), BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                        if (property != null)
                        {
                            property.SetValue(entity, record[property.Name], null);
                        }
                    }
                }
            }
        }

        public static DataTable ToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }

           /////////////////////////////////////////////////////
          ///                                               ///
         ///     Author: Phạm Văn Thủy                     ///
        ///      BÁO CÁO B16_BH2                          ///
       ///                                               ///
      /////////////////////////////////////////////////////

        public static Object Check<T>(T entity, IDataRecord record, int i)
        {
            Object bc = new object();

            Type type = entity.GetType();
            if (DBNull.Value != record[i])
            {
                PropertyInfo property = type.GetProperty(record.GetName(i),
                    BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

                if (property != null)
                {
                    bc = record.GetValue(i); //Lấy giá trị cột thứ i của bản ghi
                }
            }

            return bc;
        }
        public static Decimal CheckNull<T>(T entity, IDataRecord record, int i)
        {
            Decimal bc = 0;


            if (DBNull.Value != record[i])
            {

                Decimal.TryParse(record.GetValue(i).ToString(), out bc);
            }
            else
            {
                try
                {
                    Decimal.TryParse(record.GetValue(i).ToString(), out bc);
                }
                catch (Exception)
                {

                    return 0;
                }
            }

            return bc;
        }


        public static string Return_colname(int Cap_dvql, string ma_dvql)
        {

            string colname = "";

        
                if (Cap_dvql == 3)
                {

                    if (ma_dvql.Substring(2, 2).Equals("00"))
                    {
                        colname = "CHITIETTINH";
                        return colname;
                    }
                    colname = "CHITIET1";
                    return colname;
                }
                else if (Cap_dvql == 2)
                {
                    colname = "CHITIETTINH";
                    return colname;
                }
                else if (Cap_dvql == 1)
                {
                    colname = "CHITIETTW";
                    return colname;
                }
                else return colname = "CHITIET1";
           
           
        }
        public static string Return_colname(int? cap_theodoi, int Cap_dvql, string ma_dvql)
        {

            string colname = "";

            if (cap_theodoi == 1)
            {
                if (Cap_dvql == 3)
                {

                    if (ma_dvql.Substring(2, 2).Equals("00"))
                    {
                        colname = "CHITIETTINH";
                        return colname;
                    }
                    colname = "CHITIET1";
                    return colname;
                }
                else if (Cap_dvql == 2)
                {
                    colname = "CHITIETTINH";
                    return colname;
                }
                else if (Cap_dvql == 1)
                {
                    colname = "CHITIETTW";
                    return colname;
                }
                else return colname = "CHITIET1";
            }
            else return colname = "CHITIET1";

        }

        public static string RandomTextGenerator(int length)
        {
            string allChar = "0,1,2,3,4,5,6,7,8,9,q,w,e,r,t,y,u,i,o,p,a,s,f,g,h,j,k,l,z,x,c,v,b,n,m";
            string[] allCharArray = allChar.Split(',');
            StringBuilder builder = new StringBuilder();
            Random rand = RandomProvider.GetThreadRandom();
            for (int i = 0; i < length; i++)
            {
                int t = rand.Next(allCharArray.Length);
                builder.Append(allCharArray[t]);
            } return builder.ToString();
        }

        public static List<T> ConvertTo<T>(DataTable datatable) where T : new()
        {
            List<T> Temp = new List<T>();
            try
            {
                List<string> columnsNames = new List<string>();
                foreach (DataColumn DataColumn in datatable.Columns)
                    columnsNames.Add(DataColumn.ColumnName);
                Temp = datatable.AsEnumerable().ToList().ConvertAll<T>(row => getObject<T>(row, columnsNames));
                return Temp;
            }
            catch
            {
                return Temp;
            }

        }
        public static T getObject<T>(DataRow row, List<string> columnsName) where T : new()
        {
            T obj = new T();
            try
            {
                string columnname = "";
                string value = "";
                PropertyInfo[] Properties;
                Properties = typeof(T).GetProperties();
                foreach (PropertyInfo objProperty in Properties)
                {
                    columnname = columnsName.Find(name => name.ToLower() == objProperty.Name.ToLower());
                    if (!string.IsNullOrEmpty(columnname))
                    {
                        value = row[columnname].ToString();
                        if (!string.IsNullOrEmpty(value))
                        {
                            if (Nullable.GetUnderlyingType(objProperty.PropertyType) != null)
                            {
                                value = row[columnname].ToString().Replace("$", "").Replace(",", "");
                                objProperty.SetValue(obj, Convert.ChangeType(value, Type.GetType(Nullable.GetUnderlyingType(objProperty.PropertyType).ToString())), null);
                            }
                            else
                            {
                                value = row[columnname].ToString().Replace("%", "");
                                objProperty.SetValue(obj, Convert.ChangeType(value, Type.GetType(objProperty.PropertyType.ToString())), null);
                            }
                        }
                    }
                }
                return obj;
            }
            catch
            {
                return obj;
            }
        }

        public static Oracle.DataAccess.Client.OracleParameter GetParam(IDbCommand command, Oracle.DataAccess.Client.OracleDbType dbType, string name, object value = null, ParameterDirection direction = ParameterDirection.Input, int paraSize = 0)
        {
            var result = (Oracle.DataAccess.Client.OracleParameter)command.CreateParameter();
            result.ParameterName = name;
            result.OracleDbType = dbType;
            if (paraSize != 0)
            {
                result.Size = paraSize;
            }
            result.Direction = direction;
            result.Value = value;
            return result;
        }
    }
}
