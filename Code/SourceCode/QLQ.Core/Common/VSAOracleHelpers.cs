﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;

namespace QLQ.Core.Common
{
    /*
     Creation by : HaiPL
     Created date : 11/09/2016
     Description : Ham dung chung cho co che commit,rollback cua Oracle
     */
    public class VSAOracleHelpers
       {
           /// <summary>
           /// Oracle server connection string
           /// </summary>
           /// 
           string strConStr = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString; //Globals.Settings.All.ConnectionString;
           /// <summary>
           /// Global SQL server connection
           /// </summary>
           public OracleConnection connection = null;
           public OracleTransaction trans = null;

           public VSAOracleHelpers()
           {
               if (connection == null) { connection = new OracleConnection(strConStr); }
           }

           public OracleConnection GetConnection()
           {
               if (connection.State == ConnectionState.Closed)
               {
                   connection.Open();
                   return connection;
               }
               else
                   return connection;
           }

           public bool Open()
           {
               try
               {
                   if (connection.State == ConnectionState.Closed)
                   {
                       connection.Open();
                       return true;
                   }
                   else
                       return true;
               }
               catch { }
               return false;
           }

           public bool Close()
           {
               try
               {
                   if (connection.State == ConnectionState.Open)
                   {
                       connection.Close();
                       return true;
                   }
               }
               catch { }
               return false;
           }

           public void Dispose()
           {
               try
               {
                   if (trans != null)
                   {
                       trans.Dispose();
                   }
                   if (connection != null)
                   {
                       connection.Dispose();
                   }
               }
               catch { }
           }

           public OracleTransaction BeginTransaction()
           {
               trans = GetConnection().BeginTransaction();
               return trans;
           }

           public bool Commit()
           {
               try
               {
                   if (trans != null)
                   {
                       trans.Commit();
                       return true;
                   }
               }
               catch
               { }
               return false;
           }

           public bool Rollback()
           {
               try
               {
                   if (trans != null)
                   {
                       trans.Rollback();
                       return true;
                   }
               }
               catch
               { }
               return false;
           }

           //public OracleDataReader ExecuteReader(string strText,CommandType cmdType,params OracleParameter[] param)
           //{
           //    if (!Helpers.IsNullOrEmpty(trans))
           //    {
           //        return OracleHelper.ExecuteReader(trans, cmdType, strText, param);
           //    }
           //    else
           //    {
           //        return OracleHelper.ExecuteReader(connection, cmdType, strText, param);
           //    }
           //}

           //public int ExecuteScalar(string strText, CommandType cmdType, params OracleParameter[] param)
           //{
           //    if (!Helpers.IsNullOrEmpty(trans))
           //    {
           //        return (int)OracleHelper.ExecuteScalar(trans, cmdType, strText, param);
           //    }
           //    else
           //    {
           //        return (int)OracleHelper.ExecuteScalar(connection, cmdType, strText, param);
           //    }
           //}

           //public int ExecuteNonQuery(string strText, CommandType cmdType, params OracleParameter[] param)
           //{
           //    if (!Helpers.IsNullOrEmpty(trans))
           //    {
           //        return OracleHelper.ExecuteNonQuery(trans, cmdType, strText, param);
           //    }
           //    else
           //    {
           //        return OracleHelper.ExecuteNonQuery(connection, cmdType, strText, param);
           //    }
           //}
           #region DB Access Functions
           #endregion
       }
}
