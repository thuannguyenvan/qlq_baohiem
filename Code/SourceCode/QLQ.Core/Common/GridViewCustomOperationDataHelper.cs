﻿using DevExpress.Data;
using DevExpress.Data.Filtering;
using DevExpress.Data.Linq;
using DevExpress.Data.Linq.Helpers;
using DevExpress.Web.Mvc;
using System.Linq.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using DevExpress.Data.Filtering.Helpers;
using log4net;

namespace QLQ.Core.Common
{
    public static class GridViewCustomOperationDataHelper
    {
        static ICriteriaToExpressionConverter Converter { get { return new CriteriaToExpressionConverter(); } }
        private static readonly ILog log = LogManager.GetLogger(typeof(GridViewCustomOperationDataHelper));
        public static IQueryable Select(this IQueryable query, string fieldName)
        {
            return query.MakeSelect(Converter, new OperandProperty(fieldName));
        }

        public static IQueryable ApplySorting(this IQueryable query, IEnumerable<GridViewColumnState> sortedColumns)
        {
            log.Error("1.5.7.3.3:" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff",
                                      System.Globalization.CultureInfo.InvariantCulture));
            ServerModeOrderDescriptor[] orderDescriptors = sortedColumns
                .Select(c => new ServerModeOrderDescriptor(new OperandProperty(c.FieldName), c.SortOrder == ColumnSortOrder.Descending))
                .ToArray();
            log.Error("1.5.7.3.4:" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff",
                                      System.Globalization.CultureInfo.InvariantCulture));
            return query.MakeOrderBy(Converter, orderDescriptors);
        }

        public static IQueryable ApplyFilter(this IQueryable query, IList<GridViewGroupInfo> groupInfoList)
        {
            log.Error("1.5.7.3.2:" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff",
                                      System.Globalization.CultureInfo.InvariantCulture));
            var criteria = GroupOperator.And(
                groupInfoList.Select(i => new BinaryOperator(i.FieldName, i.KeyValue, BinaryOperatorType.Equal))
            );
            return query.ApplyFilter(CriteriaOperator.ToString(criteria));
        }
        public static IQueryable ApplyFilter(this IQueryable query, string filterExpression){
            filterExpression = GetFilterQuery(filterExpression);
            CriteriaOperator criteria = CriteriaOperator.Parse(filterExpression);
            if (CriteriaValidator.IsCriteriaOperatorValid(criteria))
                query = query.AppendWhere(Converter, criteria);
            return query;

            //log.Error("1.5.7.3.1:" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff",
            //                          System.Globalization.CultureInfo.InvariantCulture));
            //return query.AppendWhere(Converter, CriteriaOperator.Parse(filterExpression));
        }
        public static string GetFilterQuery(string filter)
        {
            if (!string.IsNullOrEmpty(filter))
            {
                string queryFilter = ""; 
                var array = filter.Split(new string[] { "And" }, StringSplitOptions.None);
                foreach (var item in array)
                {var col_filter = getColFilter('[', ']', item);

                    if (item.IndexOf('\'') >= 0)
                    {
                        var filter_string = item;
                        var data_filter = getFilter('\'', item);
                        if (col_filter == "[SO_TIEN]" || col_filter == "[SOTIEN]")
                        {
                            filter_string =filter_string.Replace(".", string.Empty)
                                    .Replace(",", string.Empty)
                                    .Replace("'", string.Empty);
                        }
                        else
                        {
                            filter_string = item.Replace(col_filter, "UPPER(" + col_filter + ")");
                        }
                        
                        
                        if (!string.IsNullOrEmpty(data_filter))
                        {
                            filter_string = filter_string.Replace("'" + data_filter + "'", "'" + data_filter.Trim() + "'");
                        }
                        
                       queryFilter += filter_string.ToUpper() + " And";
                    }
                    else
                    {
                        queryFilter += item + " And";

                    }
                }
                queryFilter = queryFilter.Remove(queryFilter.Length - 3);
                return queryFilter;
            }
            else
            {
                return filter;
            }
        }
        public static string getColFilter(char char_start, char char_end, string s)
        {
            int start = s.IndexOf(char_start);
            int end = s.IndexOf(char_end) - start;
            if (start >= 0)
            {
                return s.Substring(start , end+1 );
            }
            return "";
        }
        public static string getFilter(char char_sub, string s)
        {
            var array = s.Split(char_sub);
            return array[1];
        }
        public static IQueryable UniqueValuesForField(this IQueryable query, string fieldName)
        {
            query = query.Select(fieldName);
            var expression = Expression.Call(typeof(Queryable), "Distinct", new Type[] { query.ElementType }, query.Expression);
            return query.Provider.CreateQuery(expression);
        }

        public static IEnumerable<GridViewGroupInfo> GetGroupInfo(this IQueryable query, string fieldName, ColumnSortOrder order)
        {
            var rowType = query.ElementType;
            query = query.MakeGroupBy(Converter, new OperandProperty(fieldName));
            query = query.MakeOrderBy(Converter, new ServerModeOrderDescriptor(new OperandProperty("Key"), order == ColumnSortOrder.Descending));
            query = query.ApplyGroupInfoExpression(rowType);

            var list = new List<GridViewGroupInfo>();
            foreach (var item in query)
            {
                var obj = (object[])item;
                list.Add(new GridViewGroupInfo() { KeyValue = obj[0], DataRowCount = (int)obj[1] });
            }
            return list;
        }

        public static object[] CalculateSummary(this IQueryable query, List<GridViewSummaryItemState> summaryItems)
        {
            var elementType = query.ElementType;

            query = query.MakeGroupBy(Converter, new OperandValue(0));
            var groupParam = Expression.Parameter(query.ElementType, string.Empty);

            var expressions = GetAggregateExpressions(elementType, summaryItems, groupParam);
            query = query.ApplyExpressions(expressions, groupParam);

            var groupValue = query.ToArray();
            return groupValue.Length > 0 ? groupValue[0] as object[] : new object[summaryItems.Count];
        }

        static List<Expression> GetAggregateExpressions(Type elementType, List<GridViewSummaryItemState> summaryItems, ParameterExpression groupParam)
        {
            var list = new List<Expression>();
            var elementParam = Expression.Parameter(elementType, "elem");
            foreach (var item in summaryItems)
            {
                Expression e;
                LambdaExpression elementExpr = null;
                if (!string.IsNullOrEmpty(item.FieldName))
                    elementExpr = Expression.Lambda(Converter.Convert(elementParam, new OperandProperty(item.FieldName)), elementParam);

                switch (item.SummaryType)
                {
                    case SummaryItemType.Count:
                        e = Expression.Call(typeof(Enumerable), "Count", new Type[] { elementType }, groupParam);
                        break;
                    case SummaryItemType.Sum:
                        e = Expression.Call(typeof(Enumerable), "Sum", new Type[] { elementType }, groupParam, elementExpr);
                        break;
                    case SummaryItemType.Min:
                        e = Expression.Call(typeof(Enumerable), "Min", new Type[] { elementType }, groupParam, elementExpr);
                        break;
                    case SummaryItemType.Max:
                        e = Expression.Call(typeof(Enumerable), "Max", new Type[] { elementType }, groupParam, elementExpr);
                        break;
                    case SummaryItemType.Average:
                        e = Expression.Call(typeof(Enumerable), "Average", new Type[] { elementType }, groupParam, elementExpr);
                        break;
                    default:
                        throw new NotSupportedException(item.SummaryType.ToString());
                }
                list.Add(e);
            }return list;
        }

        static IQueryable ApplyExpressions(this IQueryable query, IEnumerable<Expression> expressions, ParameterExpression param)
        {
            var combinedExpr = Expression.NewArrayInit(typeof(object), expressions.Select(expr => Expression.Convert(expr, typeof(object))).ToArray());
            return query.ApplyExpression(combinedExpr, param);
        }

        static IQueryable ApplyExpression(this IQueryable query, Expression expression, ParameterExpression param)
        {
            var lambda = Expression.Lambda(expression, param);
            var callExpr = Expression.Call(typeof(Queryable), "Select", new Type[] { query.ElementType, lambda.Body.Type }, query.Expression, Expression.Quote(lambda));
            return query.Provider.CreateQuery(callExpr);
        }

        static object[] ToArray(this IQueryable query)
        {
            var list = new ArrayList();
            foreach (var item in query)
                list.Add(item);
            return list.ToArray();
        }

        static IQueryable ApplyGroupInfoExpression(this IQueryable query, Type rowType)
        {
            var param = Expression.Parameter(query.ElementType, string.Empty);
            return query.ApplyExpressions(new Expression[] {
                Expression.Property(param, "Key"),
                Expression.Call(typeof(Enumerable), "Count", new Type[] { rowType }, param) },
            param);
        }
    }
    public class CriteriaValidator : EvaluatorCriteriaValidator
    {
        private bool isCriteriaOperatorValid = true;
        private CriteriaValidator() : base(null) { }
        public static bool IsCriteriaOperatorValid(CriteriaOperator criteria)
        {
            CriteriaValidator validator = new CriteriaValidator();
            validator.Validate(criteria);
            return validator.isCriteriaOperatorValid;
        }
        public override void Visit(OperandValue theOperand)
        {
            if (theOperand.Value == null)
                isCriteriaOperatorValid = false;
        }
        public override void Visit(JoinOperand theOperand) { }
        public override void Visit(OperandProperty theOperand) { }
        public override void Visit(AggregateOperand theOperand) { }
    }
}
