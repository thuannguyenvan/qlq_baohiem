﻿using QLQ.Core.Domain;
using FX.Data;
using System.Collections.Generic;

namespace QLQ.Core.IService
{
    interface IHDQuaHanService:IBaseService<QLQ_HDQUAHAN,long>
    {
        QLQ_HDQUAHAN GetHdQuaHanById(long id);
        List<QLQ_HDQUAHAN> Search(QLQ_HDQUAHAN hdqh);
    }
}
