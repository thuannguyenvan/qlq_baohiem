﻿using QLQ.Core.Domain;
using FX.Data;
using System;
using System.Collections.Generic;
using QLQ.Core.CustomView;

namespace QLQ.Core.IService
{
    public interface ILAISUATTBService : IBaseService<QLQ_LAISUATTB, decimal?>
    {
        List<QLQ_LAISUATTB> GetByDay(DateTime day);
        IEnumerable<LaiSuatHuyDongNHModel> LayLaiSuatBinhQuan(DateTime day);
    }
}
