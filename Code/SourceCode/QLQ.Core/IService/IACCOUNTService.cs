﻿using QLQ.Core.Domain;
using FX.Data;
using System.Collections.Generic;

namespace QLQ.Core.IService
{
    public interface IACCOUNTService : IBaseService<QLQ_ACCOUNT, long?>
    {
        //List<QLQ_ACCOUNT> GetAllAccount();
        QLQ_ACCOUNT GetByUser(string username);
        QLQ_ACCOUNT GetById(int id);
        List<QLQ_ACCOUNT> Search(string timkiem);
    }
}
