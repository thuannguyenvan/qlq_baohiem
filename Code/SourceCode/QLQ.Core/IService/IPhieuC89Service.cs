﻿using FX.Data;
using QLQ.Core.CustomView;
using QLQ.Core.Domain;
using System;
using System.Collections.Generic;

namespace QLQ.Core.IService
{
    public interface IPhieuC89Service : IBaseService<PhieuLaiPhaiThuTrongNamC89, long>
    {
        List<PhieuC89Model> Search(PhieuC89SearchModel request);
    }
}
