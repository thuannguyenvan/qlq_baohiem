﻿using System.Collections.Generic;
using System.Linq;
using FX.Data;
using QLQ.Core.Domain;

namespace QLQ.Core.IService
{
    public interface IDM_TOCHUCCUNGCAPDICHVUDAUTHAUService: IBaseService<QLQ_DMTOCHUCCUNGCAPDICHVUDAUTHAU, long?>
    {
        QLQ_DMTOCHUCCUNGCAPDICHVUDAUTHAU GetById(long? id);
        QLQ_DMTOCHUCCUNGCAPDICHVUDAUTHAU GetByMa(string ma);
        List<QLQ_DMTOCHUCCUNGCAPDICHVUDAUTHAU> Search(string timkiem);
    }
}
