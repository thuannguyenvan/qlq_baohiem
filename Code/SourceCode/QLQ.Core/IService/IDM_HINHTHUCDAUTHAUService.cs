﻿using FX.Data;
using QLQ.Core.Domain;
using System.Collections.Generic;

namespace QLQ.Core.IService
{
    public interface IDM_HINHTHUCDAUTHAUService : IBaseService<QLQ_DMHINHTHUCDAUTHAU, long?>
    {
        QLQ_DMHINHTHUCDAUTHAU GetById(long id);

        QLQ_DMHINHTHUCDAUTHAU GetByMa(string ma);
        List<QLQ_DMHINHTHUCDAUTHAU> Search(string timkiem);
    }
}
