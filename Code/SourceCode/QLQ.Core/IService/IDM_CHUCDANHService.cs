﻿using FX.Data;
using QLQ.Core.Domain;
using System.Collections.Generic;
using System.Linq;

namespace QLQ.Core.IService
{
   public interface IDM_CHUCDANHService : IBaseService<QLQ_DMCHUCDANH, long?>
    {
        //IQueryable<QLQ_DMCHUCDANH> GetAllDmChucDanh();
        QLQ_DMCHUCDANH GetById(long? id);
        QLQ_DMCHUCDANH GetByMa(string ma);
        List<QLQ_DMCHUCDANH> Search(string timkiem);
    }
}
