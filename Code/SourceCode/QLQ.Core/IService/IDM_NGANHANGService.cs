﻿using QLQ.Core.Domain;
using FX.Data;
using System.Collections.Generic;
using System.Linq;

namespace QLQ.Core.IService
{
    public interface IDM_NGANHANGService : IBaseService<QLQ_DMNGANHANG, long?>
    {
        IQueryable<QLQ_DMNGANHANG> GetbyActiveDmNganHang();
        QLQ_DMNGANHANG GetById(long? id);
        QLQ_DMNGANHANG GetByMa(string ma);
        List<QLQ_DMNGANHANG> Search(string timkiem);
    }
}
