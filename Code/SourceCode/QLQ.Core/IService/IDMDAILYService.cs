﻿using QLQ.Core.Domain;
using FX.Data;
using System.Collections.Generic;
using System.Linq;

namespace QLQ.Core.IService
{
    public interface IDMDAILYService : IBaseService<DM_DAILY, dmDaiLyPK>
    {
        //void ClearCache();
        List<DM_DAILY> GetDmDaiLy(string madvql);
        //List<DM_DAILY> GetAllDmDVQL();
        IQueryable<DM_DAILY> GetAllDmDaiLy1();
        IQueryable<DM_DAILY> GetAllDmDaiLy(string madvql);
        IQueryable<DM_DAILY> GetAlLforC73Hd();
        string TimKiem { set; get; }
        IQueryable<DM_DAILY> Filter(string madvql);
        DM_DAILY getByIdDm_DaiLy(string ma, string madv);
        List<string> GetNoiDung(string madvql);
        DM_DAILY GetDaily(string maTk);
        string MaDvql { get; set; }
    }
}
