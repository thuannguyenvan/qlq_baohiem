﻿using FX.Data;
using QLQ.Core.Domain;
using System.Collections.Generic;

namespace QLQ.Core.IService
{
    public interface IQLQ_ROLEPERMISSIONService : IBaseService<QLQ_ROLEPERMISSION, long?>
    {

        QLQ_ROLEPERMISSION GetById(long? id);
        QLQ_ROLEPERMISSION GetObj(long? roleId,long? perId);

        //List<QLQ_ROLEPERMISSION> Getdata();
        List<QLQ_ROLEPERMISSION> GetByRoleId(long? id);
    }
}
