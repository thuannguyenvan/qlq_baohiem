﻿using QLQ.Core.Domain;
using FX.Data;
using System.Collections.Generic;
using System.Linq;

namespace QLQ.Core.IService
{
    public interface IDM_CHITIEUService : IBaseService<QLQ_DMCHITIEU, long?>
    {
        //IQueryable<QLQ_DMCHITIEU> GetAllDmChiTieu();
        QLQ_DMCHITIEU GetById(long? id);
        QLQ_DMCHITIEU GetByMa(string ma);
        List<QLQ_DMCHITIEU> Search(string timkiem);
    }
}
