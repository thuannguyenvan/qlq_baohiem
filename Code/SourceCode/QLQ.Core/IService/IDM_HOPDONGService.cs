﻿using System.Collections.Generic;
using FX.Data;
using QLQ.Core.Domain;

namespace QLQ.Core.IService
{
 public interface IDM_HOPDONGService: IBaseService<QLQ_DMHOPDONG, long?> 
    {
     QLQ_DMHOPDONG GetById(long? id);
     QLQ_DMHOPDONG GetByMa(string ma);
     List<QLQ_DMHOPDONG> Search(string timkiem);
    }
}
