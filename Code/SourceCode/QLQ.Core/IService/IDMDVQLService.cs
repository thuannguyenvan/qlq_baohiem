﻿using QLQ.Core.Domain;
using FX.Data;
using System.Collections.Generic;
using System.Linq;

namespace QLQ.Core.IService
{
    public interface IDMDVQLService : IBaseService<DM_DVQL, string>
    {
        string TimkiemS { get; set; }

        //List<DM_DVQL> GetAllDmdvql();
        List<DM_DVQL> GetAllid();
        DM_DVQL GetDmDvqlId(string id);
        DM_DVQL GetDvqlPrintReport(string maDvql);
        IQueryable<DM_DVQL> SearchDvql(string maCapTren);
        IQueryable<DM_DVQL> GetDmDvqlsByMaCapTren(string maCapTren);
        IQueryable<DM_DVQL> GetDmDvqlsByMadvql(string madvql);
        IQueryable<DM_DVQL> GetDmDvqlsByCap(int cap);
        List<string> GetNoiDung(string macaptren);
        IQueryable<DM_DVQL> Getbymacaptren(string macaptren);
        IQueryable<DM_DVQL> GetByPhamVi(string maDvql,string phamvi);
        IQueryable<DM_DVQL> GetDonViCon(string maDvql);

    }
}
