﻿using System.Collections.Generic;
using System.Linq;
using FX.Data;
using QLQ.Core.Domain;

namespace QLQ.Core.IService
{
    public interface IDM_DAUTUService : IBaseService<QLQ_DMDAUTU, long?>
    {
        //IQueryable<QLQ_DMDAUTU> GetAllDmDauTu();
        QLQ_DMDAUTU GetById(long? id);
        QLQ_DMDAUTU GetByMa(string ma);
        List<QLQ_DMDAUTU> Search(string timkiem);

        bool CheckExistDmDauTu(long id);
        //QLQ_DMDAUTU GetByReadOnly(int p);
    }
}
