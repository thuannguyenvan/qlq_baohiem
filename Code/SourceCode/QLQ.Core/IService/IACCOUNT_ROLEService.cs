﻿using QLQ.Core.Domain;
using FX.Data;
using System.Collections.Generic;

namespace QLQ.Core.IService
{
    public interface IACCOUNT_ROLEService : IBaseService<QLQ_ACCOUNTROLE, long?>
    {
        List<QLQ_ACCOUNTROLE> GetByAccount(long accountId);
        QLQ_ACCOUNTROLE GetId(long accountId, long roleId, int active);
        QLQ_ACCOUNTROLE GetById(long? id);
    }
}
