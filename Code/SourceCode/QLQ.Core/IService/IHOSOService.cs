﻿using QLQ.Core.Domain;
using FX.Data;


namespace QLQ.Core.IService
{
    public interface IHOSOService : IBaseService<HOSO, long>
    {
        HOSO GetById(long? hosoId);
        bool CheckExistNganHangIdInHoSo(long id);
    }
}
