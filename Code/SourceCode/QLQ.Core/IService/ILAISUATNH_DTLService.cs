﻿using QLQ.Core.Domain;
using FX.Data;
using System.Collections.Generic;


namespace QLQ.Core.IService
{
    public interface ILAISUATNH_DTLService : IBaseService<QLQ_LAISUATNH_DTL, long?>
    {
        List<QLQ_LAISUATNH_DTL> GetByIdLaiSuatNh(long? id);
    }
}
