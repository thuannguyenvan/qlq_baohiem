﻿using QLQ.Core.Domain;
using FX.Data;
using System.Collections.Generic;

namespace QLQ.Core.IService
{
    public interface IOBJECTService : IBaseService<QLQ_OBJECT, long?>
    {
        //List<QLQ_OBJECT> GetAll();
        QLQ_OBJECT GetbyId(long? id);
    }
}
