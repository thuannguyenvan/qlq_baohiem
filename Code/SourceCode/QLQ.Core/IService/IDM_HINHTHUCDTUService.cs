﻿using FX.Data;
using QLQ.Core.Domain;
using System.Collections.Generic;

namespace QLQ.Core.IService
{
    public interface IDM_HINHTHUCDTUService : IBaseService<QLQ_DMHINHTHUCDTU, long?>
    {
        QLQ_DMHINHTHUCDTU GetById(long? id);
        QLQ_DMHINHTHUCDTU GetByMa(string ma);
        List<QLQ_DMHINHTHUCDTU> Search(string timkiem);
    }
}
