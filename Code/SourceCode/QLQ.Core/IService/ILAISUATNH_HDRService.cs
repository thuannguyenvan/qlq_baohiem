﻿using QLQ.Core.Domain;
using FX.Data;
using System;
using System.Collections.Generic;

namespace QLQ.Core.IService
{
    public interface ILAISUATNH_HDRService : IBaseService<QLQ_LAISUATNH_HDR, long?>
    {
        QLQ_LAISUATNH_HDR GetId(long idNganhang, DateTime? dateHieuLuc, string txtCvDen, DateTime? dateCvDen, string txtCv, DateTime? dateCv, string txtNh, DateTime? dateNh);

        bool CheckIdNganHangInLaiSuat(long id);

        QLQ_LAISUATNH_HDR GetById(long id);

        List<QLQ_LAISUATNH_HDR> Search(string timkiem);
    }
}
