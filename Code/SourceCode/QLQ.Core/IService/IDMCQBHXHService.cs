﻿using System.Linq;
using QLQ.Core.Domain;
using FX.Data;

namespace QLQ.Core.IService
{
    public interface IDMCQBHXHService : IBaseService<QLQ_DMCQBHXH, long>
    {
        IQueryable<QLQ_DMCQBHXH> GetAllData();
        QLQ_DMCQBHXH GetById(long? id);
        string TimKiem { get; set; }
    }
}
