﻿using QLQ.Core.Domain;
using FX.Data;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;


namespace QLQ.Core.IService
{
    public interface IHS_XEPLOAINHService : IBaseService<HS_XEPLOAINH, long>
    {
        HS_XEPLOAINH GetById(long? xepLoaiId);
        List<HS_XEPLOAINH> Search(int? timkiem);
    }
}
