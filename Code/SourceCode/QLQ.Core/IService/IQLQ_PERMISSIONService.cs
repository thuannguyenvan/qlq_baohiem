﻿using FX.Data;
using QLQ.Core.Domain;
using System.Collections.Generic;

namespace QLQ.Core.IService
{
    public interface IQLQ_PERMISSIONService : IBaseService<QLQ_PERMISSION, long?>
    {

        QLQ_PERMISSION GetById(long? id);

        //List<QLQ_PERMISSION> Getdata();
        List<QLQ_PERMISSION> GetByObjId(int? id);
    }
}
