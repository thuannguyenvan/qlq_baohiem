﻿using System.Collections.Generic;
using FX.Data;
using QLQ.Core.Domain;
using QLQ.Core.CustomView;
using System;

namespace QLQ.Core.IService
{
    public interface ISoChiTietLaiDauTuService : IBaseService<QLQ_SOCHITIETLAIDAUTU, long>
    {
    }
}
