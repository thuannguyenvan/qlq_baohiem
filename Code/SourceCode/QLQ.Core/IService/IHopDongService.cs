﻿using System.Collections.Generic;
using FX.Data;
using QLQ.Core.Domain;
using QLQ.Core.CustomView;
using System;

namespace QLQ.Core.IService
{
    public interface IHopDongService : IBaseService<QLQ_HOPDONG, long>
    {
        QLQ_HOPDONG getHopDongBySo_HD(string soh);
        List<QLQ_HOPDONG> Search(QLQ_HOPDONG hd);
        List<HopDongDenHanModel> GetHopDongDenHanByDate(DateTime date);
        bool CheckDmHopDongId(long id);
        bool CheckDmKhachHangId(long id);
        bool SoftDelete(long id);
        bool CheckExistDM_Dautu(long id);
        List<SoDuDauTuCommonModel> GetSoDuDauTuChiTiet(GetSoDuDauTuRequest request);
        List<SoDuDauTuCommonModel> GetSoDuDauTuDenHan(GetSoDuDauTuRequest request);
        List<SoDuDauTuCommonModel> GetSoDuDauTuMoi(GetSoDuDauTuRequest request);
        List<QLQ_HOPDONG> GetSoDuDauTu(DateTime? thoigian);
        LaiSuatDauTuModel GetLaiSuatDauTu(long hopDongId);
        LaiSuatDauTuModel GetLaiSuatDauTu(long hopDongId, DateTime ngayxem);
        GocVaLaiModel GetGocVaLai(long hinhthucId, DateTime ngayxem);
        C90Model GetPhieuC90(long khachhangid, int nam);
        PhieuLaiPhaiThuTrongNamC89 GetPhieuC89(long hopDongId, int nam);
        HopDongQuaHanListModel GetHopDongQuaHanList(SearchHopDongQuaHanRequest request);
    }
}
