﻿using FX.Data;
using QLQ.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QLQ.Core.IService
{
    public interface IHopDongDieuChinhLaiSuatService : IBaseService<QLQ_HOPDONGLAISUATDC, long?>
    {
        IQueryable<QLQ_HOPDONGLAISUATDC> Search(string timKiem, long? id);
    }
}
