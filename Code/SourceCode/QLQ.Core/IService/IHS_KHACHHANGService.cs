﻿using QLQ.Core.Domain;
using FX.Data;
using System.Linq;


namespace QLQ.Core.IService
{
    public interface IHS_KHACHHANGService : IBaseService<HS_KHACHHANG, long>
    {
        IQueryable<HS_KHACHHANG> Search(string text);
        bool CheckExistDM_Dautu(long id);
    }
}
