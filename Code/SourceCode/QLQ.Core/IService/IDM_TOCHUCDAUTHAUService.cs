﻿using System.Collections.Generic;
using System.Linq;
using FX.Data;
using QLQ.Core.Domain;

namespace QLQ.Core.IService
{
    public interface IDM_TOCHUCDAUTHAUService : IBaseService<QLQ_DMTOCHUCDAUTHAU, long?>
    {
        QLQ_DMTOCHUCDAUTHAU GetById(long? id);
        QLQ_DMTOCHUCDAUTHAU GetByMa(string ma);
        List<QLQ_DMTOCHUCDAUTHAU> Search(string timkiem);
    }
}
