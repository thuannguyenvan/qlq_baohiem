﻿using QLQ.Core.Domain;
using FX.Data;
using System.Collections.Generic;

namespace QLQ.Core.IService
{
    public interface IQLQ_ROLEService : IBaseService<QLQ_ROLE, long?>
    {
        //List<QLQ_ROLE> GetAllRole();
        QLQ_ROLE GetById(long? id);
       
        //List<QLQ_ROLE> Getdata();
    }
}
