﻿using System.Collections.Generic;
using System.Linq;
using FX.Data;
using QLQ.Core.Domain;

namespace QLQ.Core.IService
{
    public interface IDM_KYHANService : IBaseService<QLQ_DMKYHAN, long?>
    {
        QLQ_DMKYHAN GetById(long? id);
        IQueryable<QLQ_DMKYHAN> Search(string timkiem);

        IQueryable<QLQ_DMKYHAN> GetActiveRecords();

        bool CheckExistDmKyHan(string ten_kyhan);
    }
}
