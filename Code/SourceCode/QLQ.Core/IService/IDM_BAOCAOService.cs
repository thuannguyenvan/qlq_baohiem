﻿using FX.Data;
using QLQ.Core.Domain;
using System.Collections.Generic;
using System.Linq;

namespace QLQ.Core.IService
{
    public interface IDM_BAOCAOService : IBaseService<QLQ_DMBAOCAO, long?>
    {
        //IQueryable<QLQ_DMBAOCAO> GetAllDmBaoCao();
        QLQ_DMBAOCAO GetById(long? id);
        QLQ_DMBAOCAO GetByMa(string ma);
        List<QLQ_DMBAOCAO> Search(string timkiem);
    }
    
}
