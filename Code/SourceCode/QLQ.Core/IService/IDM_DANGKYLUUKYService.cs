﻿using System.Collections.Generic;
using System.Linq;
using FX.Data;
using QLQ.Core.Domain;

namespace QLQ.Core.IService
{
    public interface IDM_DANGKYLUUKYService : IBaseService<QLQ_DMDANGKYLUUKY, long?>
    {
        QLQ_DMDANGKYLUUKY GetById(long? id);
        QLQ_DMDANGKYLUUKY GetByMa(string ma);
        List<QLQ_DMDANGKYLUUKY> Search(string timkiem);
    }
}
