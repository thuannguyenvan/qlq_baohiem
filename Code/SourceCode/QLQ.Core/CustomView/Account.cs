﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QLQ.Core.CustomView
{
    public class Account
    {
        public string user { get; set; }
        public bool isFromNewLogin { get; set; }
        public string authenticationDate { get; set; }
        public string successfulAuthenticationHandlers { get; set; }
        public string type { get; set; }
        public string maCqBhxh { get; set; }
        public string tenCqBhxh { get; set; }
        public string tinhTrangHoatDong { get; set; }
        public string sysLdapAdmin { get; set; }
        public string credential { get; set; }
        public string authenticationMethod { get; set; }
        public string longTermAuthenticationRequestTokenUsed { get; set; }
        public string email { get; set; }
    }
}
