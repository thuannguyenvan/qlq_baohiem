﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace QLQ.Core.CustomView
{
    [XmlRoot("danhsachphieuc89")]
    public class PhieuC89ListModel
    {
        [XmlElement("phieuc89")]
        public List<PhieuC89Model> Items { get; set; }
    }

    public class PhieuC89Model
    {
        [XmlElement("id")]
        public long Id { get; set; }
        [XmlElement("sohopdong")]
        public string SoHopDong { get; set; }
        [XmlElement("sotien")]
        public decimal SoTien { get; set; }
        [XmlElement("laisuat")]
        public decimal LaiSuat { get; set; }
        [XmlElement("sotienlai")]
        public decimal SoTienLai { get; set; }
        [XmlElement("laidenhan")]
        public decimal LaiDenHan { get; set; }
        [XmlElement("laichuathu")]
        public decimal LaiChuaThu { get; set; }
    }

    public class PhieuC89SearchModel
    {
        public long? HinhThucDauTuId { get; set; }
        public long? DauTuId { get; set; }
        public long? DanhMucHopDongId { get; set; }
        public long? HopDongId { get; set; }
        public string ThoiGian { get; set; }
        public DateTime? ThoiGianDate
        {
            get
            {
                DateTime date;
                if (DateTime.TryParseExact(ThoiGian, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    return date;
                }
                return null;
            }
        }
    }
}
