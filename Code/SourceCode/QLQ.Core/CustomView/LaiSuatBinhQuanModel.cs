﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QLQ.Core.CustomView
{
    public class LaiSuatBinhQuanModel
    {
        public DateTime ngay_thaydoi { get; set; }
        public decimal? KKH { get; set; }
        public decimal? Unde1Month { get; set; }
        public decimal? Month1 { get; set; }
        public decimal? Month2 { get; set; }
        public decimal? Month3 { get; set; }
        public decimal? Month4 { get; set; }
        public decimal? Month5 { get; set; }
        public decimal? Month6 { get; set; }
        public decimal? Month7 { get; set; }
        public decimal? Month8 { get; set; }
        public decimal? Month9 { get; set; }
        public decimal? Month10 { get; set; }
        public decimal? Month11 { get; set; }
        public decimal? Month12 { get; set; }
        public decimal? Month18 { get; set; }
        public decimal? Month24 { get; set; }
        public decimal? Month36 { get; set; }
        public decimal? Month60 { get; set; }
    }
}
