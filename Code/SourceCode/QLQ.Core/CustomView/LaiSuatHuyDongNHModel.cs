﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QLQ.Core.CustomView
{
    public class LaiSuatHuyDongNHModel
    {
        public virtual DateTime NGAY_HIEULUC { get; set; }
        public virtual decimal KYHAN_ID { get; set; }
        public virtual decimal CANHAN_HANGTHANG { get; set; }
        public virtual decimal CANHAN_CUOIKY { get; set; }
        public virtual decimal TOCHUC_HANGTHANG { get; set; }
        public virtual decimal TOCHUC_CUOIKY { get; set; }
        public virtual decimal NGANHANG_ID { get; set; }
        public virtual decimal Value { get; set; }

    }
}
