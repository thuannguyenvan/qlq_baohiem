﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QLQ.Core.CustomView
{
    public class QLQ_LAISUATBINHQUAN
    {
        public int KYHAN_ID { get; set; }
        public Decimal CANHAN_CUOIKY { get; set; }
        public Decimal CANHAN_HANGTHANG { get; set; }
        public Decimal TOCHUC_CUOIKY { get; set; }
        public Decimal TOCHUC_HANGTHANG { get; set; }
        public DateTime NGAY_HIEULUC { get; set; }
        public long NGANHANG_ID { get; set; }
    }
}
