﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QLQ.Core.CustomView
{
    public class BaoCao2Model
    {
        public long HopdongId { get; set; }
        public DateTime? NgayGhiSo { get; set; }
        public string DienGiai { get; set; }
        public decimal? TienGocPhaiThu { get; set; }
        public decimal? SoTienGocDaThu { get; set; }
        public decimal? SoConPhaiThu { get { var v = TienGocPhaiThu - SoTienGocDaThu; if (v < 0) v = 0; return v; } }
        public decimal? SoTienLaiPhaiThu { get; set; }
        public decimal? SoTienLaiDaThu { get; set; }
        public decimal? SoTienLaiThuThieu { get { var v = SoTienLaiPhaiThu - SoTienLaiDaThu;  if (v < 0) v = 0; return v; } }
        public decimal? SoTienLaiThuThua { get { var v = SoTienLaiDaThu - SoTienLaiPhaiThu; if (v < 0) v = 0; return v; } }

        public static BaoCao2Model CreateFromGocVaLaiModel(GocVaLaiSuatDauTuHopdongTheoThangModel model)
        {
            var result = new BaoCao2Model();

            if (model != null)
            {
                result.HopdongId = model.ThongTinHopDong.HopDongId;
                result.TienGocPhaiThu = model.ThongTinHopDong.SoTien;
                var tiengoc = model.LaiSuat.Items.Where(l => l.IsGoc == 1);
                result.SoTienGocDaThu = tiengoc.Where(l => !string.IsNullOrEmpty(l.SoChungTu) && l.NgayChungTu.HasValue).Sum(t => t.SoTienDaThu);

                var tienlai = model.LaiSuat.Items.Where(l => l.IsGoc == 0);
                result.SoTienLaiPhaiThu = tienlai.Sum(l => l.SoTienPhaiThu);
                result.SoTienLaiDaThu = tienlai.Where(l => !string.IsNullOrEmpty(l.SoChungTu) && l.NgayChungTu.HasValue).Sum(l => l.SoTienDaThu);

                DateTime now = DateTime.Now;
                if (model.ThongTinHopDong.HanHopDong.Year == now.Year
                    && model.ThongTinHopDong.HanHopDong.Month == now.Month)
                {
                    result.NgayGhiSo = tiengoc.OrderByDescending(t => t.NgayGhiSo).Select(t => t.NgayGhiSo).FirstOrDefault();
                }

                if (!result.NgayGhiSo.HasValue)
                {
                    result.NgayGhiSo = tienlai.Where(t => t.NgayGhiSo.HasValue).Select(t => t.NgayGhiSo).FirstOrDefault();
                }
            }
            return result;
        }
    }
}
