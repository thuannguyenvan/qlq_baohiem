﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace QLQ.Core.CustomView
{
    [XmlRoot("danhsachhopdongquahan")]
    public class HopDongQuaHanListModel
    {
        [XmlElement("hopdongquahan")]
        public List<HopDongQuaHanModel> Items { get; set; }
    }

    public class HopDongQuaHanModel
    {
        [XmlElement("hopdongid")]
        public long HopDongId { get; set; }
        [XmlElement("sohopdong")]
        public string SoHopDong { get; set; }
        [XmlElement("laisuatquahan")]
        public decimal LaiSuatQuaHan { get; set; }
        [XmlElement("ngayhopdong")]
        public DateTime NgayHopDong { get; set; }
        [XmlElement("sotienquahan")]
        public decimal SoTienQuaHan { get; set; }
        [XmlElement("ngayquahan")]
        public decimal NgayQuaHan { get; set; }
        [XmlElement("tienlaiquahan")]
        public decimal TienLaiQuaHan { get; set; }
    }

    public class SearchHopDongQuaHanRequest
    {
        public string LoaiGocLai { get; set; }
        public long? DauTuId { get; set; }
        public long? DanhMucHopDongId { get; set; }
        public long? HopDongId { get; set; }
        public DateTime DenNgay { get; set; }
    }
}
