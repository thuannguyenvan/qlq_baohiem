﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QLQ.Core.CustomView
{
    public class AccountModel
    {
        public virtual long? ACCOUNT_ID { get; set; }
        public virtual string USERNAME { get; set; }
        public virtual string FULL_NAME { get; set; }
        public virtual string EMAIL { get; set; }
        public virtual long ACTIVE { get; set; }
        public virtual long? SYS_LDAP_ADMIN { get; set; }
        public virtual string TYPE { get; set; }
        public virtual string MA_CO_QUAN_BHXH { get; set; }
        public virtual string TEN_CO_QUAN_BHXH { get; set; }
        public virtual DateTime AUTHENTICATION_DATE { get; set; }
    }
}
