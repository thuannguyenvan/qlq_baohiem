﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace QLQ.Core.CustomView
{
    public class SoDuDauTuCommonModel
    {
        public long HopDongId { get; set; }
        public string SoHopDong { get; set; }
        public DateTime NgayKy { get; set; }
        public decimal LaiSuat { get; set; }
        public DateTime TuNgay { get; set; }
        public DateTime DenNgay { get; set; }
        public decimal SoTien { get; set; }
    }

    public class GetSoDuDauTuRequest
    {
        public string DauTuIdStr { get; set; }
        public string KhachHangIdStr { get; set; }
        public string FromDateStr { get; set; }
        public string ToDateStr { get; set; }
        public long? DauTuId
        {
            get
            {
                long id = 0;
                long.TryParse(DauTuIdStr, out id);
                return id == 0 ? (long?)null : id;
            }
        }
        public long? KhachHangId
        {
            get
            {
                long id = 0;
                long.TryParse(KhachHangIdStr, out id);
                return id == 0 ? (long?)null : id;
            }
        }
        public DateTime? FromDate
        {
            get
            {
                DateTime date;
                if (DateTime.TryParseExact(FromDateStr, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    return date;
                }
                return null;
            }
        }
        public DateTime? ToDate
        {
            get
            {
                DateTime date;
                if (DateTime.TryParseExact(ToDateStr, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    return date.AddDays(1);
                }
                return null;
            }
        }
    }

    public class SoDuDauTuModel
    {
        public decimal hinhthuc_id { get; set; }
        public decimal tongsotien { get; set; }
    }
}
