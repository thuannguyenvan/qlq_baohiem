﻿using QLQ.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QLQ.Core.CustomView
{
    public class LaiSuatNHModel
    {
        private List<QLQ_LAISUATNH_DTL> _lst_LaiSuat;
        public List<QLQ_DMKYHAN> lst_KyHan;

        public List<QLQ_LAISUATNH_DTL> lst_LaiSuat
        {
            get { return _lst_LaiSuat; }
            set
            {
                _lst_LaiSuat = value;
            }
        }

        public LaiSuatNHModel()
        {
            lst_KyHan = new List<QLQ_DMKYHAN>()
            {
                new QLQ_DMKYHAN() {  kyhan_id = 1, ten_kyhan = "KKH", so_thang = -1},
                new QLQ_DMKYHAN() {  kyhan_id = 2, ten_kyhan = "< 1 tháng", so_thang = 0},
                new QLQ_DMKYHAN() {  kyhan_id = 3, ten_kyhan = "01 tháng", so_thang = 1},
                new QLQ_DMKYHAN() {  kyhan_id = 4, ten_kyhan = "02 tháng", so_thang = 2},
                new QLQ_DMKYHAN() {  kyhan_id = 5, ten_kyhan = "03 tháng", so_thang = 3},
                new QLQ_DMKYHAN() {  kyhan_id = 6, ten_kyhan = "04 tháng", so_thang = 4},
                new QLQ_DMKYHAN() {  kyhan_id = 7, ten_kyhan = "05 tháng", so_thang = 5},
                new QLQ_DMKYHAN() {  kyhan_id = 8, ten_kyhan = "06 tháng", so_thang = 6},
                new QLQ_DMKYHAN() {  kyhan_id = 9, ten_kyhan = "07 tháng", so_thang = 7},
                new QLQ_DMKYHAN() {  kyhan_id = 10, ten_kyhan = "08 tháng", so_thang = 8},
                new QLQ_DMKYHAN() {  kyhan_id = 11, ten_kyhan = "09 tháng", so_thang = 9},
                new QLQ_DMKYHAN() {  kyhan_id = 12, ten_kyhan = "10 tháng", so_thang = 10},
                new QLQ_DMKYHAN() {  kyhan_id = 13, ten_kyhan = "11 tháng", so_thang = 11},
                new QLQ_DMKYHAN() {  kyhan_id = 14, ten_kyhan = "12 tháng", so_thang = 12},
                new QLQ_DMKYHAN() {  kyhan_id = 15, ten_kyhan = "18 tháng", so_thang = 18},
                new QLQ_DMKYHAN() {  kyhan_id = 16, ten_kyhan = "24 tháng", so_thang = 24},
                new QLQ_DMKYHAN() {  kyhan_id = 17, ten_kyhan = "36 tháng", so_thang = 36},
                new QLQ_DMKYHAN() {  kyhan_id = 18, ten_kyhan = "60 tháng", so_thang = 60}
            };
        }

        public long? getSoThang(decimal kyhan_id) { return lst_KyHan.Where(t => t.kyhan_id == kyhan_id).Select(t => t.so_thang).FirstOrDefault(); }

        public List<QLQ_LAISUATNH_DTL> LayLaiSuat()
        {
            List<QLQ_LAISUATNH_DTL> ketqua = new List<QLQ_LAISUATNH_DTL>();
            if (lst_KyHan != null)
            {
                foreach (var kyhan in lst_KyHan)
                {
                    QLQ_LAISUATNH_DTL moi = null;
                    if (_lst_LaiSuat != null)
                    {
                        foreach (var ls in _lst_LaiSuat)
                        {
                            if (ls.KYHAN_ID == kyhan.kyhan_id)
                            {
                                moi = ls;
                                break;
                            }
                        }
                    }
                    if (moi == null)
                        moi = new QLQ_LAISUATNH_DTL() { KYHAN_ID = kyhan.kyhan_id };
                    ketqua.Add(moi);
                }
            }
            return ketqua;
        }

        public QLQ_LAISUATNH_HDR laiSuat { get; set; }
    }
}