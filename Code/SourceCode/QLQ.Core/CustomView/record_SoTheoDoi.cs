﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QLQ.Core.CustomView
{
    public class record_SoTheoDoi
    {
        public decimal LaiSuatId { get; set; }
        public string SO_CVDEN { get; set; }
        public virtual DateTime? NGAY_CVDEN { get; set; }
        public virtual string SO_CVNH { get; set; }
        public virtual DateTime? NGAY_CVNH { get; set; }
        public virtual string THONGBAO_NH { get; set; }
        public virtual DateTime? NGAY_THONGBAO { get; set; }
        public virtual DateTime? NGAY_HIEULUC { get; set; }
        public string KYHAN { get; set; }
        public decimal Value { get; set; }

        public decimal KYHAN_ID { get; set; }
    }
}
