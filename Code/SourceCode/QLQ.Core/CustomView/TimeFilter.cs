﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QLQ.Core.CustomView
{
    public class TimeFilter
    {
        public int? thoiGian { get; set; }
        public int? nam { get; set; }
        public int? quy { get; set; }
        public Dictionary<int, string> quys { get; set; }
        public int? thang { get; set; }
        public DateTime? _tuNgay;

        public DateTime? tuNgay
        {
            get
            {
                if (thoiGian == (int) QLQ.Core.Common.Common.eThoiGian.nam)
                {
                    if (nam != null)
                        return new DateTime(nam.Value, 1, 1);
                    else return null;
                }
                else if (thoiGian == (int)QLQ.Core.Common.Common.eThoiGian.quy)
                {
                    if (nam != null && quy != null)
                        return new DateTime(nam.Value, quy.Value*3 - 2, 1);
                    else return null;}
                else if (thoiGian == (int)QLQ.Core.Common.Common.eThoiGian.thang)
                {
                    if (nam != null && thang != null)
                        return new DateTime(nam.Value, thang.Value,1);
                    else return null;
                }
                else if (thoiGian == (int)QLQ.Core.Common.Common.eThoiGian.ngay)
                {
                    return _tuNgay;
                }
                return null;
            }
            set { _tuNgay = value; }
        }

        public DateTime? _denNgay;

        public DateTime? denNgay
        {
            get
            {
                if (thoiGian == (int)QLQ.Core.Common.Common.eThoiGian.nam)
                {
                    if (nam != null)
                        return new DateTime(nam.Value, 12, 31);
                    else return null;
                }
                else if (thoiGian == (int)QLQ.Core.Common.Common.eThoiGian.quy)
                {
                    if (nam != null && quy != null)
                        return new DateTime(nam.Value, quy.Value*3,
                            quy == (int)QLQ.Core.Common.Common.eQuy.I || quy == (int)QLQ.Core.Common.Common.eQuy.IV
                                ? 31
                                : 30);
                    else return null;
                }
                else if (thoiGian == (int)QLQ.Core.Common.Common.eThoiGian.thang)
                {
                    if (nam != null && thang != null)
                        return new DateTime(nam.Value, thang.Value,
                            (new DateTime(nam.Value, thang.Value, 1)).AddMonths(1).AddDays(-1).Day);
                    else return null;
                }
                else if (thoiGian == (int)QLQ.Core.Common.Common.eThoiGian.ngay)
                {
                    return _denNgay;
                }
                return null;
            }
            set { _denNgay = value; }
        }

    }
}
