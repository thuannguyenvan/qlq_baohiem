﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QLQ.Core.CustomView
{
    public class C90Model
    {
        public List<SoTienVayModel> LstSoTienVay { get; set; }
        public List<SoTienLaiModel> LstSoTienLai { get; set; }

        public C90Model()
        {
            LstSoTienLai = new List<SoTienLaiModel>();
            LstSoTienVay = new List<SoTienVayModel>();
        }

        public static C90Model CreateFromGocVaLaiModel(GocVaLaiModel model)
        {
            C90Model result = new C90Model();

            if (model != null)
            {
                if (model.DsHopDong != null)
                {
                    foreach (var hopdong in model.DsHopDong)
                    {

                        // tinh so tien van
                        var sotienThuDungHan = hopdong.LaiSuat.Items.Where(t => t.IsGoc == 1 && t.NgayChungTu.HasValue && t.NgayChungTu <= hopdong.ThongTinHopDong.HanHopDong).Sum(t => t.SoTienDaThu);
                        decimal tronghan = 0;
                        if (hopdong.ThongTinHopDong.HanHopDong > DateTime.Now)
                        { // tat ca van trong han
                            tronghan = hopdong.ThongTinHopDong.SoTien;
                        } else
                        {
                            tronghan = sotienThuDungHan;
                        }
                        var tienvay = new SoTienVayModel()
                        {
                            NgayBatDau = hopdong.ThongTinHopDong.NgayHieuLuc,
                            NgayHetHan = hopdong.ThongTinHopDong.HanHopDong,
                            NgayKyHopDong = hopdong.ThongTinHopDong.NgayLap,
                            SoHopDong = hopdong.ThongTinHopDong.SoHopDong,
                            TongSo = hopdong.ThongTinHopDong.SoTien,
                            TrongHan = sotienThuDungHan,
                            QuaHan = hopdong.ThongTinHopDong.SoTien - tronghan
                        };
                        result.LstSoTienVay.Add(tienvay);

                        // add so tien lai
                        // calculate so tien phat
                        var dsLai = hopdong.LaiSuat.Items.Where(t => t.IsGoc == 0).OrderBy(t =>t.SoChiTietId).ToList();
                        decimal mangsangkysau = hopdong.ThongTinHopDong.SoDuKyTruoc;
                        for (int i = 0; i < dsLai.Count(); i++)
                        {
                            var lai = dsLai[i];
                            var sotienThieu = lai.SoTienPhaiThu + mangsangkysau - lai.SoTienDaThu; // cong don so tien sang ky sau
                            var songayphat = 0;
                            if (sotienThieu > 0)
                            {
                                int nextindex = i + 1;

                                if (nextindex < dsLai.Count())
                                {
                                    // van con thang tiep theo
                                    var nextitem = dsLai[nextindex];
                                    var ngayketthuc = nextitem.NgayChungTu.HasValue ? nextitem.NgayChungTu.Value : nextitem.NgayKetThuc;
                                    songayphat = (ngayketthuc - lai.NgayKetThuc).Days + 1;
                                    
                                }
                                else
                                {
                                    // day la ngay cuoi cung
                                    var ngayketthuc = DateTime.Now.Date;
                                    if (lai.NgayKetThuc.Date < ngayketthuc)
                                        songayphat = (ngayketthuc - lai.NgayKetThuc.Date).Days + 1;
                                }
                            }
                            else if (sotienThieu == 0)
                            {
                                // neu tra du nhung van khong dung ngay
                                if (lai.NgayChungTu.HasValue && lai.NgayChungTu.Value.Date > lai.NgayKetThuc.Date)
                                {
                                    songayphat = (lai.NgayChungTu.Value.Date - lai.NgayKetThuc.Date).Days + 1;
                                }

                            }
                            lai.SoTienLaiPhat = hopdong.ThongTinHopDong.Phat * songayphat * sotienThieu / 100;

                            // tinh so tien mang sang
                            mangsangkysau = sotienThieu + lai.SoTienLaiPhat;

                        }

                        var tienlai = new SoTienLaiModel()
                        {
                            LaiDaThuTrongKy = dsLai.Sum(t => t.SoTienDaThu),
                            LaiPhaiThuKyTruoc = hopdong.ThongTinHopDong.SoDuKyTruoc,
                            LaiPhaiThuTheoHopDong = dsLai.Sum(t => t.SoTienPhaiThu),
                            NgayKyHopDong = hopdong.ThongTinHopDong.NgayLap,
                            SoHopDong = hopdong.ThongTinHopDong.SoHopDong,
                            SoTienVay = hopdong.ThongTinHopDong.SoTien,
                            TienPhatChamTraLai = dsLai.Sum(t => t.SoTienLaiPhat),
                        };


                        result.LstSoTienLai.Add(tienlai);
                    }
                }
            }

            return result;
        }
    }

    public class SoTienVayModel
    {
        public string SoHopDong { get; set; }
        public DateTime NgayKyHopDong { get; set; }
        public DateTime NgayBatDau { get; set; }
        public DateTime NgayHetHan { get; set; }
        public decimal TongSo { get; set; }
        public decimal? TrongHan { get; set; }
        public decimal? QuaHan { get; set; }
    }

    public class SoTienLaiModel
    {
        public string SoHopDong { get; set; }
        public DateTime NgayKyHopDong { get; set; }
        public decimal SoTienVay { get; set; }
        public decimal? LaiPhaiThuKyTruoc { get; set; }
        public decimal LaiPhaiThuTheoHopDong { get; set; }
        public decimal TienPhatChamTraLai { get; set; }
        public decimal TongSo
        {
            get { return LaiPhaiThuTheoHopDong + TienPhatChamTraLai; }
        }
        public decimal LaiDaThuTrongKy { get; set; }
        public decimal LaiPhaiThuChuyenKySau { get { return TienPhatChamTraLai + LaiPhaiThuTheoHopDong + (LaiPhaiThuKyTruoc.HasValue ? LaiPhaiThuKyTruoc.Value : 0) - LaiDaThuTrongKy; } }
    }
}
