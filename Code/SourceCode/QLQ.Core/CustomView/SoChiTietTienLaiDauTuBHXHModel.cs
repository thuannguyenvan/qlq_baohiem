﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QLQ.Core.CustomView
{
    public class SoChiTietTienLaiDauTuBHXHModel
    {
        public virtual DateTime? ngay_ghiso { get; set; }
        public virtual long? soct  { get; set; }
        public virtual DateTime? ngay_ct { get; set; }
        public virtual string diengiai { get; set; }
        public virtual long? tienvay { get; set; }
        public virtual string laisuat { get; set; }
        public virtual decimal? songaytinhlai { get; set; }
        public virtual decimal? phaithu { get; set; }
        public virtual decimal? dathu { get; set; }
        public virtual long? cl { get; set; }       
    }
}
