﻿using QLQ.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace QLQ.Core.CustomView
{
    [XmlRoot("thongtinlaisuat")]
    public class LaiSuatDauTuModel
    {
        [XmlElement("thongtinhopdong")]
        public ThongTinHopDong ThongTinHopDong { get; set; }
        [XmlElement("dslaisuat")]
        public DanhSachLaiSuat DanhSachLaiSuat { get; set; }

        public List<ThongTinLaiSuat> ViewItems
        {
            get
            {
                List<ThongTinLaiSuat> result = new List<ThongTinLaiSuat>();
                int sequence = 0;
                long sochitietid = 0;
                if (DanhSachLaiSuat!= null && DanhSachLaiSuat.Items != null)
                {
                    
                    for (int i = 0; i < DanhSachLaiSuat.Items.Count; i++)
                    {
                        sequence++;
                        var item = DanhSachLaiSuat.Items[i];
                        sochitietid = item.SoChiTietId;
                        if (item.IsGoc == 1)
                        {
                            item.DienGiai = string.Format("Chuyển tiền theo hợp đồng số {0}", ThongTinHopDong.SoHopDong);
                            item.LaiSuatId = string.Format("{0}_{1}", ThongTinHopDong.HopDongId, sequence);
                            item.IsThu = false;
                            result.Add(item);
                        }
                        else
                        {
                            // lai suat phai thu
                            ThongTinLaiSuat phaithu = new ThongTinLaiSuat()
                            {
                                IsGoc = 0,
                                IsThu = false,
                                LaiSuat = item.LaiSuat,
                                SoChiTietId = item.SoChiTietId,
                                SoTienPhaiThu = item.SoTienPhaiThu,
                                SoNgayTinhLai = item.SoNgayTinhLai,
                                NgayBatDau = item.NgayBatDau,
                                NgayKetThuc = item.NgayKetThuc,
                                DienGiai = string.Format("Lãi phải thu hợp đồng số {0} từ ngày {1} đến ngày {2}", ThongTinHopDong.SoHopDong, item.NgayBatDau.ToString("dd/MM/yyyy"), item.NgayKetThuc.ToString("dd/MM/yyyy")),
                                LaiSuatId = string.Format("{0}_{1}", ThongTinHopDong.HopDongId, sequence)
                            };
                            sequence++;
                            result.Add(phaithu);

                            // lai suat da thu
                            ThongTinLaiSuat dathu = new ThongTinLaiSuat()
                            {
                                IsGoc = 0,
                                IsThu = true,
                                LaiSuat = item.LaiSuat,
                                SoChiTietId = item.SoChiTietId,
                                SoTienDaThu = item.SoTienDaThu,
                                SoNgayTinhLai = item.SoNgayTinhLai,
                                NgayBatDau = item.NgayBatDau,
                                NgayKetThuc = item.NgayKetThuc,
                                DienGiai = string.Format("Trả lãi hợp đồng số {0} từ ngày {1} đến ngày {2}", ThongTinHopDong.SoHopDong, item.NgayBatDau.ToString("dd/MM/yyyy"), item.NgayKetThuc.ToString("dd/MM/yyyy")),
                                LaiSuatId = string.Format("{0}_{1}", ThongTinHopDong.HopDongId, sequence)
                            };
                            result.Add(dathu);

                            // add so du ky truoc
                            sequence++;
                            var sokytruocmangsang = new ThongTinLaiSuat()
                            {
                                LaiSuatId = string.Format("{0}_{1}", ThongTinHopDong.HopDongId, sequence),
                                SoChiTietId = sochitietid,
                                DienGiai = "Số kỳ trước mang sang",
                                SoTienPhaiThu = item.SoDuKyTruoc,
                                IsThu = false,
                                IsGoc = 0,
                                IsSoDuKyTruoc = true
                            };
                            result.Add(sokytruocmangsang);
                        }
                    }
                }
                result = result.OrderByDescending(x => x.IsSoDuKyTruoc).ThenByDescending(x => x.IsGoc).ThenBy(x => x.IsThu).ToList();
               
                return result;
            }
        }
    }

    public class ThongTinHopDong
    {
        [XmlElement("hopdongid")]
        public long HopDongId { get; set; }
        [XmlElement("tenkhachhang")]
        public string TenKhachHang { get; set; }
        [XmlElement("sohopdong")]
        public string SoHopDong { get; set; }
        [XmlElement("ngayhopdong")]
        public DateTime NgayLap { get; set; }
        [XmlElement("ngayhieuluc")]
        public DateTime NgayHieuLuc { get; set; }
        [XmlElement("hanhopdong")]
        public DateTime HanHopDong { get; set; }
        [XmlElement("laisuat")]
        public decimal LaiSuat { get; set; }
        [XmlElement("sotien")]
        public decimal SoTien { get; set; }
        [XmlElement("sodukytruoc")]
        public decimal SoDuKyTruoc { get; set; }
        [XmlElement("phat")]
        public decimal Phat { get; set; }

        public string ThoiHan
        {
            get
            {
                return string.Format("từ ngày {0} đến ngày {1}", NgayHieuLuc.ToString("dd/MM/yyyy"), HanHopDong.ToString("dd/MM/yyyy"));
            }
        }
        public string TenHopDong
        {
            get
            {
                return string.Format("QĐ số {0} ngày {1}", SoHopDong, NgayLap.ToString("dd/MM/yyyy"));
            }
        }
        public string LaiSuatStr
        {
            get { return string.Format("{0}%", LaiSuat); }
        }
    }

    public class DanhSachLaiSuat
    {
        [XmlElement("laisuatchitiet")]
        public List<ThongTinLaiSuat> Items { get; set; }
    }

    public class ThongTinLaiSuat
    {
        [XmlElement("sochitietid")]
        public long SoChiTietId { get; set; }
        [XmlElement("ngayghiso")]
        public DateTime? NgayGhiSo { get; set; }
        [XmlElement("sochungtu")]
        public string SoChungTu { get; set; }
        [XmlElement("ngaychungtu")]
        public DateTime? NgayChungTu { get; set; }
        [XmlElement("laisuat")]
        public decimal LaiSuat { get; set; }
        [XmlElement("songaytinhlai")]
        public decimal SoNgayTinhLai { get; set; }
        [XmlElement("sotienphaithu")]
        public decimal SoTienPhaiThu { get; set; }
        [XmlElement("sodukytruoc")]
        public decimal SoDuKyTruoc { get; set; }
        [XmlElement("sotiendathu")]
        public decimal SoTienDaThu { get; set; }
        [XmlElement("ngaybatdau")]
        public DateTime NgayBatDau { get; set; }
        [XmlElement("ngayketthuc")]
        public DateTime NgayKetThuc { get; set; }
        public string DienGiai { get; set; }
        public string LaiSuatStr { get { return string.Format("{0}%", LaiSuat); } }
        public decimal? SoTienThuaThieu {
            get
            {
                if (IsGoc == 0 && IsThu)
                    return SoTienPhaiThu - SoTienDaThu;
                else
                    return null;
            }
        }

        [XmlElement("isgoc")]
        public int IsGoc { get; set; }
        public bool IsThu { get; set; }
        public bool IsSoDuKyTruoc { get; set; }

        public decimal? SoTienChoVay
        {
            get
            {
                if (IsGoc == 0)
                {
                    return null;
                }
                else
                {
                    return SoTienPhaiThu;
                }
            }
        }

        public decimal? SoPhaiThu
        {
            get
            {
                if (IsGoc == 0 && !IsThu)
                {
                    return SoTienPhaiThu;
                }
                else
                {
                    return null;
                }
            }
        }

        public string LaiSuatId { get; set; }

        public decimal SoTienLaiPhat { get; set; }
    }


    public class GocVaLaiSuatDauTuHopdongTheoThangModel
    {
        [XmlElement("thongtinhopdong")]
        public ThongTinHopDong ThongTinHopDong { get; set; }
        [XmlElement("dslaisuat")]
        public DanhSachLaiSuat LaiSuat { get; set; }
    }

    [XmlRoot("thongtinlaisuat")]
    public class GocVaLaiModel
    {
        [XmlElement("hopdong")]
        public List<GocVaLaiSuatDauTuHopdongTheoThangModel> DsHopDong { get; set; }
    }

    public class LaiSuatDauTu_TheoThangModel
    {
        public decimal hopdongid { get; set; }
        public string tenkhachhang { get; set; }
        public string sohopdong { get; set; }
        public DateTime ngayhopdong { get; set; }
        public DateTime ngayhieuluc { get; set; }
        public DateTime hanhopdong { get; set; }
        public decimal laisuat { get; set; }
        public decimal sotien { get; set; }
        public string ThoiHan
        {
            get
            {
                return string.Format("từ ngày {0} đến ngày {1}", ngayhieuluc.ToString("dd/MM/yyyy"), hanhopdong.ToString("dd/MM/yyyy"));
            }
        }
        public string TenHopDong
        {
            get
            {
                return string.Format("QĐ số {0} ngày {1}", sohopdong, ngayhopdong.ToString("dd/MM/yyyy"));
            }
        }
        public string LaiSuatStr
        {
            get { return string.Format("{0}%", laisuat); }
        }

        public decimal sochitietid { get; set; }
        public DateTime ngayghiso { get; set; }
        public string sochungtu { get; set; }
        public DateTime ngaychungtu { get; set; }
        public decimal laisuatdieuchinh { get; set; }
        public decimal songaytinhlai { get; set; }
        public decimal sotienphaithu { get; set; }
        public decimal sotiendathu { get; set; }

        public string DienGiai { get { return "Dien giai"; } }
        public decimal? SoTienChoVay
        {
            get
            {
                if (isgoc == 0)
                {
                    return null;
                }
                else
                {
                    return sotienphaithu;
                }
            }
        }

        public decimal? SoPhaiThu
        {
            get
            {
                if (isgoc == 0)
                {
                    return sotienphaithu;
                }
                else
                {
                    return null;
                }
            }
        }

        public string LaiSuatDieuChinhStr { get { return string.Format("{0}%", laisuatdieuchinh); } }
        public decimal SoTienThuaThieu { get { return sotienphaithu - sotiendathu; } }
        public decimal isgoc { get; set; }
    }

    public class HopDongDenHanModel 
    {
        public string so_hopdong { get; set; }
        public DateTime? han_hopdong { get; set; }
        public DateTime? ngayketthuc { get; set; }
    }
}
