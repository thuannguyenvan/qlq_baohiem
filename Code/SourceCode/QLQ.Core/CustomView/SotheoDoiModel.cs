﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QLQ.Core.CustomView
{
    public class SotheoDoiModel
    {
        public virtual DateTime? NGAY_HIEULUC { get; set; }
        public virtual string SO_CVDEN { get; set; }
        public virtual DateTime? NGAY_CVDEN { get; set; }
        public virtual string SO_CVNH { get; set; }
        public virtual DateTime? NGAY_CVNH { get; set; }
        public virtual string THONGBAO_NH { get; set; }
        public virtual DateTime? NGAY_THONGBAO { get; set; }
        public virtual decimal? KKH { get; set; }
        public virtual decimal? Under1Month { get; set; }
        public virtual decimal? Month1 { get; set; }
        public virtual decimal? Month2 { get; set; }
        public virtual decimal? Month3 { get; set; }
        public virtual decimal? Month4 { get; set; }
        public virtual decimal? Month5 { get; set; }
        public virtual decimal? Month6 { get; set; }
        public virtual decimal? Month7 { get; set; }
        public virtual decimal? Month8 { get; set; }
        public virtual decimal? Month9 { get; set; }
        public virtual decimal? Month10 { get; set; }
        public virtual decimal? Month11 { get; set; }
        public virtual decimal? Month12 { get; set; }
        public virtual decimal? Month18 { get; set; }
        public virtual decimal? Month24 { get; set; }
        public virtual decimal? Month36 { get; set; }
        public virtual decimal? Month60 { get; set; }
    }
}
