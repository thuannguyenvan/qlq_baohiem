﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QLQ.Core.CustomView
{
    public class KeHoachDauTu_KhachHang
    {
        public string STT { get; set; }
        public string Ten { get; set; }
        public decimal? SoDuDauKy { get; set; }
        public decimal? SoTangTrongThang { get; set; }
        public decimal? SoGiamTrongThang { get; set; }
        public decimal? SoDuCuoiKy { get; set; }
        public float? TyTrong { get; set; }
    }
}
