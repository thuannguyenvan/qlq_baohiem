﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace QLQ.Core.Domain
{
  public  class DM_DAILY
  {
      
        public virtual DM_DVQL DM_DVQL { get; set; }
        public virtual dmDaiLyPK DmDaiLyPk { get; set; }
      //[Required(ErrorMessage = "Ma is required")]
      //[StringLength(12, ErrorMessage = "Maximum length is {1}")]
      //public virtual string MA { get; set; }
        [Required(ErrorMessage = "Yêu cầu nhập nội dung")]
        [StringLength(100, ErrorMessage = "Độ dài tối đa là {1}")]
        public virtual string NOIDUNG { get; set; }
        [StringLength(2, ErrorMessage = "Độ dài tối đa là {1}")]
        public virtual string MA_TINH { get; set; }
        [StringLength(2, ErrorMessage = "Độ dài tối đa là {1}")]
        public virtual string MA_HUYEN { get; set; }
        [StringLength(2, ErrorMessage = "Độ dài tối đa là {1}")]
        public virtual string MA_XA { get; set; }
        [StringLength(100, ErrorMessage = "Độ dài tối đa là {1}")]
        public virtual string DIACHI { get; set; }
        [StringLength(30, ErrorMessage = "Độ dài tối đa là {1}")]
        public virtual string DAIDIEN { get; set; }
        [StringLength(12, ErrorMessage = "Độ dài tối đa là {1}")]
        public virtual string SO_CMND { get; set; }
        //[StringLength(12, ErrorMessage = "Độ dài tối đa là {1}")]
        public virtual string NGAYCAP { get; set; }
        [StringLength(50, ErrorMessage = "Độ dài tối đa là {1}")]
        public virtual string NOICAP { get; set; }
        [StringLength(20, ErrorMessage = "Độ dài tối đa là {1}")]
        public virtual string TAIKHOAN { get; set; }
        [StringLength(50, ErrorMessage = "Độ dài tối đa là {1}")]
        public virtual string NGANHANG { get; set; }
        [StringLength(20, ErrorMessage = "Độ dài tối đa là{1}")]
        public virtual string DIENTHOAI { get; set; }
        [StringLength(20, ErrorMessage = "Độ dài tối đa là {1}")]
        public virtual string FAX { get; set; }
        [StringLength(50, ErrorMessage = "Độ dài tối đa là {1}")]
        //[RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail không đúng định dạng!")]
        public virtual string EMAIL { get; set; }
        [StringLength(50, ErrorMessage = "Độ dài tối đa là {1}")]
        public virtual string WEBSITE { get; set; }
        [StringLength(50, ErrorMessage = "Độ dài tối đa là {1}")]
        public virtual string GHICHU { get; set; }
        //public virtual string ID_CHUNGTU { get; set; }
        [Required(ErrorMessage = "Yêu cầu nhập nội dung")]
        public virtual DateTime NGAY_BD { get; set; }
        [Required(ErrorMessage = "Yêu cầu nhập nội dung")]
        public virtual DateTime NGAY_KT { get; set; }
        public virtual int? READONLY { get; set; }
        public virtual int? STATUS { get; set; }
        [StringLength(50, ErrorMessage = "Độ dài tối đa là {1}")]
        public virtual string TINHTP { get; set; }
        [StringLength(50, ErrorMessage = "Độ dài tối đa là {1}")]
        public virtual string MASOTHUE { get; set; }
        public virtual Decimal? TYLE { get; set; }
        public virtual Decimal? TAM_UNG { get; set; }
        public virtual string NGUOI_TAO { get; set; }
        public virtual DateTime? NGAY_TAO { get; set; }
        public virtual string NGUOI_SUA { get; set; }
        public virtual DateTime? NGAY_SUA { get; set; }
        public virtual int? CHITAI_VP { get; set; }
       
        //#region NHibernate Composite Key Requirements
        //public override bool Equals(object obj)
        //{
        //    if (obj == null) return false;
        //    var t = obj as DM_DAILY;
        //    if (t == null) return false;
        //    if (DM_DVQL.MA_DVQL == t.DM_DVQL.MA_DVQL
        //     && MA == t.MA)
        //        return true;

        //    return false;
        //}
        //public override int GetHashCode()
        //{
        //    int hash = GetType().GetHashCode();
        //    hash = (hash * 397) ^ DM_DVQL.MA_DVQL.GetHashCode();
        //    hash = (hash * 397) ^ MA.GetHashCode();

        //    return hash;
        //}
        //#endregion
    }
  public class dmDaiLyPK
  {
      //[Required(ErrorMessage = "Yêu cầu nhập nội dung")]
      [StringLength(4, ErrorMessage = "Độ dài tối đa là {1}")]
      public virtual string MA_DVQL { get; set; }
      [Required(ErrorMessage = "Yêu cầu nhập nội dung")]
      [StringLength(10, ErrorMessage = "Độ dài tối đa là {1}")]
      public virtual string MA { get; set; }

      public override bool Equals(object obj)
      {
          if (obj == null || (string)obj == "") return false;
          var id = (dmDaiLyPK)obj;
          if (MA == id.MA && MA_DVQL == id.MA_DVQL) return true;
          return false;
      }

      public override int GetHashCode()
      {
          return (MA + "|" + MA_DVQL).GetHashCode();
      }
  }
}
