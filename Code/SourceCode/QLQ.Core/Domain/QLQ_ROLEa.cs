﻿using System;
using System.ComponentModel.DataAnnotations;

namespace QLQ.Core.Domain
{
    public class QLQ_ROLE
    {
        public virtual long? role_id { get; set; }
        public virtual string name { get; set; }
        public virtual string description { get; set; }
    }
}
