﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace QLQ.Core.Domain
{
   public class QLQ_DMDAUTU
    {
       public virtual long? dautu_id { get; set; }
       public virtual long hinhthuc_id { get; set; }
       [Required(ErrorMessage = "Bạn chưa nhập mã đầu tư!")]
       [StringLength(40, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
       public virtual string ma_dautu { get; set; }
       [Required(ErrorMessage = "Bạn chưa nhập tên đầu tư!")]
       [StringLength(40, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
       public virtual string ten_dautu { get; set; }
        public virtual long? active { get; set; }
        public virtual DateTime creation_date { get; set; }
        public virtual long created_by { get; set; }
        public virtual DateTime lastupdated_date { get; set; }
        public virtual long lastupdate_by { get; set; }
    }
}
