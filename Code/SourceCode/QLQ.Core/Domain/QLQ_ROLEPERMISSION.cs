﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QLQ.Core.Domain
{
    public class QLQ_ROLEPERMISSION
    {
        public virtual long? rolepermission_id { get; set; }
        public virtual long? role_id { get; set; }
        public virtual long? permission_id { get; set; }  
    }
}
