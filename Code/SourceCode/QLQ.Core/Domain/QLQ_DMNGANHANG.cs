﻿using System;
using System.ComponentModel.DataAnnotations;

namespace QLQ.Core.Domain
{
    public class QLQ_DMNGANHANG
    {
        public virtual long? NGANHANG_ID { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập mã ngân hàng!")]
        [StringLength(40, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
        public virtual string MA_NGANHANG { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập tên ngân hàng!")]
        [StringLength(200, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
        public virtual string TEN_NGANHANG { get; set; }
        public virtual decimal? ACTIVE { get; set; }
        public virtual DateTime CREATION_DATE { get; set; }
        public virtual long? CREATED_BY { get; set; }
        public virtual DateTime LASTUPDATED_DATE { get; set; }
        public virtual long? LASTUPDATE_BY { get; set; }
    }
}
