﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace QLQ.Core.Domain
{
    public class HS_XEPLOAINH
    {
        public virtual long? XEPLOAINH_ID { get; set; }
        public virtual int? NAM { get; set; }
        public virtual string UPLOAD { get; set; }
        public virtual DateTime? CREATION_DATE { get; set; }
        public virtual int? CREATED_BY { get; set; }
        public virtual DateTime? LASTUPDATED_DATE { get; set; }
        public virtual int? LASTUPDATE_BY { get; set; }
        
    }
}
