﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QLQ.Core.Domain
{
    public class QLQ_OBJECT
    {
        public virtual long? object_id { get; set; }
        public virtual string object_name { get; set; }
        public virtual int? active { get; set; }
    }
}
