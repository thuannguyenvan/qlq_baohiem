﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace QLQ.Core.Domain
{
    public class QLQ_DMHINHTHUCDAUTHAU
    {
        public virtual long hinhthuc_id { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập mã hình thức!")]
        [StringLength(40, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
        public virtual string ma_hinhthuc { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập tên hình thức!")]
        [StringLength(40, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
        public virtual string ten_hinhthuc { get; set; }
        public virtual int? active { get; set; }
        public virtual DateTime creation_date { get; set; }
        public virtual long created_by { get; set; }
        public virtual DateTime lastupdated_date { get; set; }
        public virtual long lastupdated_by { get; set; }
    }
}
