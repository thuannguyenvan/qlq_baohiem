﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace QLQ.Core.Domain
{
    public class HOSO
    {
        public virtual long? HOSO_ID { get; set; }
        public virtual long? NGANHANG_ID { get; set; }
        public virtual string UPLOADS_DLNH { get; set; }
        public virtual string UPLOADS_GPKD { get; set; }
        public virtual string UPLOADS_GUQ { get; set; }
        public virtual int? CREATED_BY { get; set; }
        public virtual DateTime? CREATION_DATE { get; set; }
        public virtual int? LASTUPDATE_BY { get; set; }
        public virtual DateTime? LASTUPDATED_DATE { get; set; }
        public virtual string UPLOADS_BCTC { get; set; }
    }
}
