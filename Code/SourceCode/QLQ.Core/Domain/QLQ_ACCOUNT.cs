﻿using System;

namespace QLQ.Core.Domain
{
    public class QLQ_ACCOUNT
    {
        public virtual long? ACCOUNT_ID { get; set; }
        public virtual string USERNAME { get; set; }
        public virtual string FULL_NAME { get; set; }
        public virtual string EMAIL { get; set; }
        public virtual long ACTIVE { get; set; }
        public virtual long? SYS_LDAP_ADMIN { get; set; }
        public virtual string TYPE { get; set; }
        public virtual string MA_CQ_BHXH { get; set; }
        public virtual string TEN_CQ_BHXH { get; set; }
        public virtual DateTime AUTH_DATE { get; set; }
    }
}
