﻿using System;

namespace QLQ.Core.Domain
{
    public class PhieuLaiPhaiThuTrongNamC89
    {
        public virtual long Id { get; set; }
        public virtual string SoPhieu { get; set; }
        public virtual long HopDongId { get; set; }
        public virtual int Nam { get; set; }
        public virtual float? LaiSuatDieuChinh { get; set; }
        public virtual DateTime TuNgay { get; set; }
        public virtual DateTime DenNgay { get; set; }
        public virtual Decimal TongSoLaiPhaiThu { get; set; }
        public virtual Decimal LaiPhaiThuTinhDenHan { get; set; }
        public virtual Decimal LaiPhaiThuChuaDenHan { get; set; }
        public virtual QLQ_HOPDONG HopDong { get; set; }
    }
}
