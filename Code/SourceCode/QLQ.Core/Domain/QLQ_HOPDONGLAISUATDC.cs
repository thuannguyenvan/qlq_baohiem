﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace QLQ.Core.Domain
{
    public class QLQ_HOPDONGLAISUATDC
    {             
        public virtual long laisuatdc_id { get; set; }
        [Required(ErrorMessage = "bạn chưa chọn hợp đồng !")]
        public virtual long? hopdong_id { get; set; }
        public virtual long? created_by { get; set; }
        public virtual DateTime? creation_date { get; set; }
        public virtual long? lastupdate_by { get; set; }
        public virtual DateTime? lastupdated_date { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập lãi suất điều chỉnh")]
        public virtual float laisuatdieuchinh { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập lãi suất cũ")]
        public virtual float laisuatcu { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập ngày hiệu lực")]
        public virtual DateTime? ngayhieuluc { get; set; }
        public virtual string ghichu { get; set; }
    }
}
