﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace QLQ.Core.Domain
{
    public class QLQ_DMCQBHXH
    {
        public virtual long? CQBHXH_ID { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập mã cơ quan BHXH!")]
        public virtual string MA_CQBHXH { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập tên cơ quan BHXH!")]
        public virtual string TEN_CQBHXH { get; set; }
        public virtual long? ACTIVE { get; set; }
        public virtual DateTime? CREATION_DATE { get; set; }
        public virtual long? CREATED_BY { get; set; }
        public virtual DateTime? LASTUPDATED_DATE { get; set; }
        public virtual long? LASTUPDATE_BY { get; set; }
        public virtual string DIACHI { get; set; }
        [RegularExpression(@"^\+[0-9]{1,3}\([0-9]{3}\)[0-9]{7}$", ErrorMessage = "Định dạng số fax chưa chính xác.")]
        public virtual string FAX { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập thồn tin tài khoản!")]
        public virtual string TAI_KHOAN { get; set; }
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone Number")]
        [Required(ErrorMessage = "Bạn chưa nhập số điện thoại!")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Định dạng số điện thoại chưa chính xác!")]
        public virtual long DIEN_THOAI { get; set; }
        [Required(ErrorMessage = "Bạn chưa chọn văn bản ủy quyền!")]
        public virtual string VBUY_QUYEN { get; set; }
    }
}
