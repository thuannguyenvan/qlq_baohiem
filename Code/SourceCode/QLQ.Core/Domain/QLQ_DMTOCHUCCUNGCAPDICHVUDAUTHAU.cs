﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace QLQ.Core.Domain
{
    public class QLQ_DMTOCHUCCUNGCAPDICHVUDAUTHAU
    {
        public virtual long? tochuccungcapdichvudauthau_id { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập mã tổ chức cung cấp dịch vụ đấu thầu!")]
        [StringLength(40, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
        public virtual string ma_tochuccungcapdichvudauthau { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập tên tổ chức cung cấp dịch vụ đấu thầu!")]
        [StringLength(200, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
        public virtual string ten_tochuccungcapdichvudauthau { get; set; }
        public virtual string dia_chi { get; set; }
        public virtual string dien_thoai { get; set; }
        public virtual string fax { get; set; }
        public virtual string so_taikhoanthanhtoan { get; set; }
        public virtual string so_gptl { get; set; }
        public virtual DateTime? ngaycap { get; set; }
        public virtual string noicap { get; set; }
        public virtual long? active { get; set; }
        public virtual DateTime creation_date { get; set; }
        public virtual long created_by { get; set; }
        public virtual DateTime lastupdated_date { get; set; }
        public virtual long lastupdate_by { get; set; }
    }
}
