﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QLQ.Core.Domain
{
    public class QLQ_LAISUATTB
    {
        public virtual DateTime NGAY_THAYDOI { get; set; }
        public virtual decimal KKH { get; set; }
        public virtual decimal UNDE1MONTH { get; set; }
        public virtual decimal MONTH1 { get; set; }
        public virtual decimal MONTH2 { get; set; }
        public virtual decimal MONTH3 { get; set; }
        public virtual decimal MONTH4 { get; set; }
        public virtual decimal MONTH5 { get; set; }
        public virtual decimal MONTH6 { get; set; }
        public virtual decimal MONTH7 { get; set; }
        public virtual decimal MONTH8 { get; set; }
        public virtual decimal MONTH9 { get; set; }
        public virtual decimal MONTH10 { get; set; }
        public virtual decimal MONTH11 { get; set; }
        public virtual decimal MONTH12 { get; set; }
        public virtual decimal MONTH18 { get; set; }
        public virtual decimal MONTH24 { get; set; }
        public virtual decimal MONTH36 { get; set; }
        public virtual decimal MONTH60 { get; set; }
        public virtual decimal TYPE { get; set; }
        public virtual decimal? LAISUATTB_ID { get; set; }
    }
}
