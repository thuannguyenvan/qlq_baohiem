﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QLQ.Core.Domain
{
    public class QLQ_ACCOUNTROLE
    {
         public virtual long? ACCOUNTROLE_ID { get; set; }
        public virtual long ACCOUNT_ID { get; set; }
        public virtual long ROLE_ID { get; set; }
        public virtual long ACTIVE { get; set; }
    }
}
