﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace QLQ.Core.Domain
{
   public class DM_DVQL                                                     //  (Tùng)
    {
       [Required(ErrorMessage = "Bạn chưa nhập mã!")]
       [StringLength(4, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
       public virtual string MA_DVQL { get; set; }

       [Required(ErrorMessage = "Bạn chưa nhập tên!")]
       [StringLength(150, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
       public virtual string TEN_DVQL { get; set; }

       [StringLength(4, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
       public virtual string MA_CAPTREN { get; set; }

       //[Required(ErrorMessage = "Bạn chưa nhập mã người dùng!")]
       [StringLength(20, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
       public virtual string USER_CODE { get; set; }

       [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
       public virtual DateTime DATE_UPDATE { get; set; }

       //[Range(0, 9, ErrorMessage = "Please enter valid integer Number")]
       //[RegularExpression("([0-9]+)")]
       [RegularExpression(@"^\d{1,3}$", ErrorMessage = "Vui lòng nhập số ít hơn 3 chữ số")]
       public virtual int CAP { get; set; }

       [StringLength(100, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
       public virtual string DIACHI { get; set; }

       [StringLength(50, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
       public virtual string MASOTHUE { get; set; }

       [StringLength(50, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
       public virtual string TAIKHOAN { get; set; }

       [StringLength(100, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
       public virtual string NGANHANG { get; set; }

       [StringLength(50, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
       public virtual string TINHTP { get; set; }

       
       [RegularExpression(@"^\d{1,12}$", ErrorMessage = "Vui lòng nhập nhỏ hơn 12 chữ số")]
       public virtual string DIENTHOAI { get; set; }

       [StringLength(20, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
       public virtual string FAX { get; set; }

       [StringLength(20, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
       public virtual string EMAIL { get; set; }

       [StringLength(50, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
       public virtual string WEBSITE { get; set; }

       [StringLength(50, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
       public virtual string DAIDIEN { get; set; }

       [Required(ErrorMessage = "Bạn chưa nhập ngày bắt đầu!")]
       [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
       public virtual DateTime? NGAY_BD { get; set; }

       [Required(ErrorMessage = "Bạn chưa nhập ngày kết thúc!")]
       [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
       public virtual DateTime? NGAY_KT { get; set; }

       [RegularExpression(@"^\d{1,3}$", ErrorMessage = "Vui lòng nhập số ít hơn 3 chữ số")]
       public virtual int? FONTSTYLE { get; set; }

       [RegularExpression(@"^\d{1,3}$", ErrorMessage = "Vui lòng nhập số ít hơn 3 chữ số")]
       public virtual int? READONLY { get; set; }

       [RegularExpression(@"^\d{1,3}$", ErrorMessage = "Vui lòng nhập số ít hơn 3 chữ số")]
       public virtual int? STATUS { get; set; }

       [RegularExpression(@"^\d{1,3}$", ErrorMessage = "Vui lòng nhập số ít hơn 3 chữ số")]
       public virtual int? ORDERIDX { get; set; }

       [RegularExpression(@"^\d{1,5}$", ErrorMessage = "Vui lòng nhập số ít hơn 5 chữ số")]
       public virtual decimal? TYLE { get; set; }

       public virtual string MA_HUYEN { get; set; }

       public virtual string CHUC_DANH { get; set; }

       public virtual string GIAM_DOC { get; set; }

       public virtual string KT_TRUONG { get; set; }

       [StringLength(50, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
       public virtual string MA_DVNS { get; set; }

       public virtual DateTime? NGAY_QD { get; set; }

       public virtual string SO_QD { get; set; }

       public virtual string THU_QUY { get; set; }

       public virtual string MAUIN_THUCHI { get; set; }
       public virtual string CHUCDANH_REP_KTT { get; set; }
       public virtual string NGUOI_TAO { get; set; }
       public virtual DateTime? NGAY_TAO { get; set; }
       public virtual string NGUOI_SUA { get; set; }
       public virtual DateTime? NGAY_SUA { get; set; }
       public virtual string NGUOILAP_THUCHI { get; set; }

       public virtual int GetCapTheoDoi()
       {
           if (MA_DVQL.Substring(2, 2) == "00" && CAP == 3)
               return 2;
           return CAP;
       }

    }
}
