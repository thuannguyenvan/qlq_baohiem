﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace QLQ.Core.Domain
{
    public class QLQ_DMDANGKYLUUKY
    {
        public virtual long? dangkyluuky_id { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập mã chủ tài khoản!")]
        [StringLength(40, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
        public virtual string ma_chutaikhoan { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập tên chủ tài khoản!")]
        [StringLength(200, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
        public virtual string ten_chutaikhoan { get; set; }
        [StringLength(250, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
        public virtual string so_taikhoanluuky { get; set; }
        public virtual string noimo_taikhoanluuky { get; set; }
        public virtual string so_dangkysohuu { get; set; }
        public virtual string noicap_dangkysohuu { get; set; }
        public virtual DateTime? ngaycap_dangkysohuu { get; set; }
        public virtual string quoctich { get; set; }
        public virtual string daidien { get; set; }
        public virtual string chucvu { get; set; }
        public virtual long? active { get; set; }
        public virtual DateTime creation_date { get; set; }
        public virtual long created_by { get; set; }
        public virtual DateTime lastupdated_date { get; set; }
        public virtual long lastupdate_by { get; set; }
    }
}
