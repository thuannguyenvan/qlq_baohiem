﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace QLQ.Core.Domain
{
    public class HS_KHACHHANG
    {
        public virtual long KHACHHANG_ID { get; set; }
        [Required(ErrorMessage = "Bạn chưa chọn DM đầu tư")]
        public virtual long DAUTU_ID { get; set; }
        [Required(ErrorMessage="Bạn chưa nhập tên")]
        public virtual string TEN_KHACHHANG { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập địa chỉ")]
        public virtual string DIA_CHI { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập số điện thoại")]
        public virtual string DIEN_THOAI { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập FAX")]
        public virtual string FAX { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập tài khoản")]
        public virtual string TAIKHOAN { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập người đại diện")]
        public virtual string DAIDIEN { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập chức vụ")]
        public virtual string CHUCVU { get; set; }
        public virtual string SO_GPKD { get; set; }
        public virtual int? ACTIVE { get; set; }
        public virtual DateTime? CREATION_DATE { get; set; }
        public virtual int? CREATED_BY { get; set; }
        public virtual DateTime? LASTUPDATED_DATE { get; set; }
        public virtual int? LASTUPDATE_BY { get; set; }
        public virtual int? GIOITINH { get; set; }
        public virtual string VBUY_QUYEN { get; set; }

    }
}
