﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QLQ.Core.Domain
{
    public class KeHoachDauTu
    {
        public virtual long Id { get; set; }
        public virtual DateTime NgayTao { get; set; }
        public virtual int Nam { get; set; }
        public virtual int Thang { get; set; }
        public virtual decimal? SoDuTaiKhoan { get; set; }
        public virtual decimal? ThuBHYTBHXH { get; set; }
        public virtual decimal? ChuyenTuTKThanhToan { get; set; }
        public virtual decimal? CapKinhPhiBHYTBHXH { get; set; }
        public virtual decimal? ChiPhiQuanLy { get; set; }
        public virtual decimal? ChiKTPL { get; set; }
        public virtual decimal? CapKinhPhiThangSau { get; set; }
        public virtual decimal? KetQuaDauTu { get; set; }
        public virtual string NoiNhan { get; set; }
        public virtual string ChucVuNguoiKy { get; set; }
        public virtual string NguoiKy { get; set; }

        public virtual decimal? ThuNoGocDenHan { get; set; }
        public virtual decimal? ThuLaiDauTu { get; set; }

        public virtual decimal? DuKienThu
        {
            get
            {
                return GetDecimalData(ThuBHYTBHXH) + GetDecimalData(ThuLaiDauTu) + GetDecimalData(ThuNoGocDenHan);
            }
        }
        public virtual decimal? DuKienKinhPhiDuocSuDung
        {
            get
            {
                return GetDecimalData(SoDuTaiKhoan) + GetDecimalData(DuKienThu);
            }
        }
        public virtual decimal? ChenhLechThuChi
        {
            get
            {
                return GetDecimalData(DuKienKinhPhiDuocSuDung) - GetDecimalData(CapKinhPhiBHYTBHXH) - GetDecimalData(ChiPhiQuanLy) - GetDecimalData(ChiKTPL) - GetDecimalData(ChuyenTuTKThanhToan) - GetDecimalData(CapKinhPhiThangSau);
            }
        }

        private decimal GetDecimalData(decimal? data)
        {
            return data.HasValue ? data.Value : 0;
        }
    }
}
