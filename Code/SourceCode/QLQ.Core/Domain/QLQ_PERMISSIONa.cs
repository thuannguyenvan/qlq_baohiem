﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QLQ.Core.Domain
{
    public class QLQ_PERMISSION
    {
        public virtual long? permission_id { get; set; }
        public virtual string name { get; set; }
        public virtual string description { get; set; }
        public virtual int object_id { get; set; }
    }
}
