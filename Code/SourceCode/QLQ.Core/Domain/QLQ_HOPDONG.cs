﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace QLQ.Core.Domain
{
    public class QLQ_HOPDONG
    {             
        public virtual long? qlhopdong_id { get; set; }
        [Required(ErrorMessage = "Bạn chưa chọn hình thức đầu tư!")]
        public virtual long? hinhthuc_id { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập số hợp đồng!")]
        [StringLength(40, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
        public virtual string so_hopdong { get; set; }
        [Required(ErrorMessage = "Bạn chưa chọn danh mục đầu tư!")]
        public virtual long DAUTU_ID { get; set; }
        [Required(ErrorMessage = "Bạn chưa chọn hợp đồng!")]   
        public virtual long hopdong_id { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập tên khách hàng!")]   
        public virtual long khachhang_id { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập số tiền gốc!")]   
        public virtual Decimal sotien { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập lãi suất!")]
        public virtual float laisuat { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập phạt quá hạn!")]
        public virtual Decimal? phat { get; set; }
        [Required(ErrorMessage = "Bạn chọn nhập hình thức trả lãi!")]  
        public virtual string lai_hinhthuc { get; set; }
        public virtual DateTime? ngay_tralai { get; set; }
        public virtual DateTime? NGAY_TRALAI_L2 { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập ngày lập hợp đồng!")]
        public virtual DateTime? NGAY_LAP { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập ngày hết hạn hợp đồng!")] 
        public virtual DateTime? han_hopdong { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập ngày hợp đồng có hiệu lực!")]  
        public virtual DateTime? ngay_hieuluc { get; set; }
        public virtual long? created_by { get; set; }
        public virtual DateTime? creation_date { get; set; }
        public virtual long? lastupdate_by { get; set; }
        public virtual DateTime? lastupdated_date { get; set; }
        public virtual float? LAISUATDIEUCHINH { get; set; }
        public virtual DateTime? NGAYHIEULUC { get; set; }
        public virtual string GHICHU { get; set; }
        public virtual Decimal IsDeleted { get; set; }
        public virtual long? CQBHXHId { get; set; }

        public virtual DateTime DenNgay { get; set; }
        public virtual int NgayQuaHan
        {
            get
            {
                if (han_hopdong.HasValue && han_hopdong.Value < DenNgay)
                {
                    return (DenNgay - han_hopdong.Value).Days;
                }
                return 0;
            }
        }

        public virtual Decimal SoTienQuaHan
        {
            get
            {
                return sotien * (phat/100 ?? 0) * NgayQuaHan;
            }
        }

        public virtual Decimal TienLaiQuaHan
        {
            get
            {
                return sotien * (decimal)(laisuat/100) * (phat/100 ?? 0) * NgayQuaHan;
            }
        }
    }
}
