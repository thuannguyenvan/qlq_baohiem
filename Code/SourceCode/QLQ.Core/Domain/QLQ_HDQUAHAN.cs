﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace QLQ.Core.Domain
{
    public class QLQ_HDQUAHAN
    {
        public virtual long? hdquahan_id { get; set; }
        [Required(ErrorMessage = "Bạn chưa chọn số hợp đồng!")]
        public virtual long qlhopdong_id { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập gốc lãi!")]
        [StringLength(15, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
        public virtual string goc_lai { get; set; }
        public virtual DateTime ngay_hopdong { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập đến ngày!")]
        public virtual DateTime den_ngay { get; set; }
        public virtual Decimal sotien_quahan { get; set; }
        public virtual DateTime ngay_quahan { get; set; }
        public virtual Decimal laisuat_qh { get; set; }
        public virtual Decimal tienlai_qh { get; set; }
    }
}
