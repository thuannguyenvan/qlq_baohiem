﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace QLQ.Core.Domain
{
   public class QLQ_DMCHITIEU
    {
        public virtual long chitieu_id { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập mã chỉ tiêu!")]
        [StringLength(40, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
        public virtual string ma_chitieu { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập tên chỉ tiêu!")]
        [StringLength(100, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
        public virtual string ten_chitieu { get; set; }
        public virtual int? active { get; set; }
        public virtual DateTime creation_date { get; set; }
        public virtual long created_by { get; set; }
        public virtual DateTime lastupdated_date { get; set; }
        public virtual long lastupdate_by { get; set; }
    }
}
