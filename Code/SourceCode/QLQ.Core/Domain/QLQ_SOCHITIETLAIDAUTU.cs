﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QLQ.Core.Domain
{
    public class QLQ_SOCHITIETLAIDAUTU
    {
        public virtual long SOCHITIET_ID { get; set; }
        public virtual DateTime? NGAYGHISO { get; set; }
        public virtual string SOCHUNGTU { get; set; }
        public virtual DateTime? NGAYCHUNGTU { get; set; }
        public virtual long HOPDONG_ID { get; set; }
        public virtual DateTime NGAYTAO { get; set; }
        public virtual DateTime NGAYBATDAU { get; set; }
        public virtual DateTime NGAYKETTHUC { get; set; }
        public virtual decimal? SOTIENDATHU { get; set; }
        public virtual decimal SOTIENPHAITHU { get; set; }
        public virtual decimal LAISUAT { get; set; }
        public virtual int ISGOC { get; set; }
    }
}
