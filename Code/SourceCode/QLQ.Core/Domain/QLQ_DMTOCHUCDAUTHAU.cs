﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace QLQ.Core.Domain
{
    public class QLQ_DMTOCHUCDAUTHAU
    {
        public virtual long? tochucdauthau_id { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập mã tổ chức đấu thầu!")]
        [StringLength(40, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
        public virtual string ma_tochucdauthau { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập tên tổ chức đấu thầu!")]
        [StringLength(200, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
        public virtual string ten_tochucdauthau { get; set; }
        [StringLength(250, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
        public virtual string dia_chi { get; set; }
        public virtual string daidien { get; set; }
        public virtual string chucvu { get; set; }
        public virtual string dien_thoai { get; set; }
        public virtual string fax { get; set; }
        [StringLength(20, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
        public virtual string so_taikhoanthanhtoan { get; set; }
        public virtual string noidk_taikhoanthanhtoan { get; set; }
        public virtual string so_cmt { get; set; }
        public virtual DateTime? ngaycap { get; set; }
        public virtual string noicap { get; set; }
        public virtual long? active { get; set; }
        public virtual DateTime creation_date { get; set; }
        public virtual long created_by { get; set; }
        public virtual DateTime lastupdated_date { get; set; }
        public virtual long lastupdate_by { get; set; }
    }
}
