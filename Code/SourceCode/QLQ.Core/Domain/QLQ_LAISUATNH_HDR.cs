﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QLQ.Core.Domain
{
    public class QLQ_LAISUATNH_HDR
    {
        public virtual long LAISUATNH_ID { get; set; }
        public virtual long NGANHANG_ID { get; set; }
        public virtual DateTime? NGAY_HIEULUC { get; set; }
        public virtual string SO_CVDEN { get; set; }
        public virtual DateTime? NGAY_CVDEN { get; set; }
        public virtual string SO_CVNH { get; set; }
        public virtual DateTime? NGAY_CVNH { get; set; }
        public virtual string THONGBAO_NH { get; set; }
        public virtual DateTime? NGAY_THONGBAO { get; set; }
        public virtual long? CREATED_BY { get; set; }
        public virtual DateTime? CREATION_DATE { get; set; }
        public virtual long? LASTUPDATE_BY { get; set; }
        public virtual DateTime? LASTUPDATED_DATE { get; set; }
    }
}
