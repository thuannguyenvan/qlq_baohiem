﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QLQ.Core.Domain
{
    public class QLQ_LAISUATNH_DTL
    {
        public virtual long LAISUATNH_ID { get; set; }
        public virtual long? LAISUATNH_DTL_ID { get; set; }
        public virtual long? KYHAN_ID { get; set; }
        public virtual string KYHAN { get; set; }
        public virtual Decimal CANHAN_HANGTHANG { get; set; }
        public virtual Decimal CANHAN_CUOIKY { get; set; }
        public virtual Decimal TOCHUC_HANGTHANG { get; set; }
        public virtual Decimal TOCHUC_CUOIKY { get; set; }
    }
}
