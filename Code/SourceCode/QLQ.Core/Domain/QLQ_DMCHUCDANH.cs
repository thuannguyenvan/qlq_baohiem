﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace QLQ.Core.Domain
{
   public class QLQ_DMCHUCDANH
    {
        public virtual long chucdanh_id { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập mã chức danh!")]
        [StringLength(40, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
        public virtual string ma_chucdanh { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập tên chức danh!")]
        [StringLength(40, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
        public virtual string ten_chucdanh { get; set; }
        public virtual int? active { get; set; }
        public virtual DateTime creation_date { get; set; }
        public virtual long created_by { get; set; }
        public virtual DateTime lastupdated_date { get; set; }
        public virtual long lastupdate_by { get; set; }
        public virtual string nguoidaidien { get; set; }
    }
}
