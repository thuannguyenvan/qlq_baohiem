﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace QLQ.Core.Domain
{
    public class QLQ_DMKYHAN
    {
        public virtual long? kyhan_id { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập tên đầu tư!")]
        [StringLength(40, ErrorMessage = "Vui lòng nhập chuỗi có độ dài ngắn hơn {1} ký tự!")]
        public virtual string ten_kyhan { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập số tháng!")]
        public virtual long? so_thang { get; set; }
        public virtual long? active { get; set; }
        public virtual DateTime creation_date { get; set; }
        public virtual long created_by { get; set; }
        public virtual DateTime lastupdated_date { get; set; }
        public virtual long lastupdate_by { get; set; }
    }
}
