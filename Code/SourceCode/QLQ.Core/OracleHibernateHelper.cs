﻿using QLQ.Core.Domain;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;

namespace ACC.Gateway
{
    public sealed class OracleHibernateHelper
    {
        public static string OracleDataIp { get; set; }
        public static string DbName { get; set; }
        public static string LoginName { get; set; }
        public static string LoginPassword { get; set; }

        public OracleHibernateHelper(string OracleDataIp, string dbName, string loginName, string loginPassword)
        {
            OracleDataIp = OracleDataIp;
            DbName = dbName;
            LoginName = loginName;
            LoginPassword = loginPassword;
        }

        static readonly object FactoryLock = new object();
        private static ISessionFactory _sessionFactory;
        private static ISessionFactory SessionFactory
        {
            get
            {
                lock (FactoryLock)
                {
                    if (_sessionFactory == null)
                    {
                        InitializeSessionFactory();
                    }
                }

                return _sessionFactory;
            }
        }

        private static void InitializeSessionFactory()
        {

            _sessionFactory = Fluently.Configure().Database(OracleClientConfiguration.Oracle10.ConnectionString(@"Data Source=10.0.139.22/QLQ;User Id=qlq;Password=qlq1qaz2wsx;").Driver<NHibernate.Driver.OracleClientDriver>()
                                        .ShowSql)
                                            //.Mappings(m => m.FluentMappings.AddFromAssemblyOf<C69_HD>())
                                            //.Mappings(m => m.FluentMappings.AddFromAssemblyOf<C69_DK>())
                                            //.Mappings(m => m.FluentMappings.AddFromAssemblyOf<C69_BS>())
                                            //.Mappings(m => m.FluentMappings.AddFromAssemblyOf<C69_OD>())
                                            //.Mappings(m => m.FluentMappings.AddFromAssemblyOf<C69_DC>())
                                            //.Mappings(m => m.FluentMappings.AddFromAssemblyOf<C69_NT>())
                                            //.Mappings(m => m.FluentMappings.AddFromAssemblyOf<C69_LA>())
                                            //.Mappings(m => m.FluentMappings.AddFromAssemblyOf<C69_YT_DK>())
                                            //.Mappings(m => m.FluentMappings.AddFromAssemblyOf<C69_YT_BS>())
                                            //.Mappings(m => m.FluentMappings.AddFromAssemblyOf<C47_ChiTiet>())
                                            //.Mappings(m => m.FluentMappings.AddFromAssemblyOf<C83_HD>())
                                            //.Mappings(m => m.FluentMappings.AddFromAssemblyOf<SMS_DM_DONVI>())
                                            //.Mappings(m => m.FluentMappings.AddFromAssemblyOf<SMS_KETOAN>())
                                                .BuildSessionFactory();
            //_sessionFactory = Fluently.Configure().Database(MsOracleConfiguration.MsOracle2008
            //                            .ConnectionString(@"Data Source=" + OracleDataIp + ";Initial Catalog=" + DbName + "; User Id=" + LoginName + "; Password=" + LoginPassword + ";")
            //    //.ConnectionString(@"Data Source=BLACKY-PC;Initial Catalog=MISBHXH1101;User ID=sa;Password=12345678")
            //                            .Driver<NHibernate.Driver.OracleClientDriver
            //                            >()
            //                            .ShowOracle())
            //                                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<C69_HD>())
            //                                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<C69_DK>())
            //                                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<C69_BS>())
            //                                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<C69_OD>())
            //                                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<C69_DC>())
            //                                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<C69_NT>())
            //                                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<C69_LA>())
            //                                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<C69_YT_DK>())
            //                                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<C69_YT_BS>())
            //                                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<C47_ChiTiet>())
            //                                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<C83_HD>())
            //                                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<SMS_DM_DONVI>())
            //                                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<SMS_KETOAN>())
            //                                    .BuildSessionFactory();
        }

        public static ISession OpenSession()
        {
            return SessionFactory.OpenSession();
        }

        public static void CloseSession()
        {
            SessionFactory.Close();
        }
    }
}
