﻿using System;
using System.Collections.Generic;

namespace QLQ.Core.Security
{
    public class FunctionAttribute : Attribute
    {
        public string FunctionCode { get; private set; }
        public FunctionAttribute(string functionCode)
        {
            FunctionCode = functionCode;
        }
    }

    public class ActionPermissionsAttribute : Attribute
    {
        public List<string> Actions { get; private set; }
        public ActionPermissionsAttribute(params string[] actions)
        {
            Actions = new List<string>(actions);

        }
    }
}
