﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QLQ.Core.Security
{
    public class QlqActions
    {
        public const string EDIT = "EDIT";
        public const string DELETE = "DELETE";
        public const string VIEW = "VIEW";
        public const string PRINT = "PRINT";
    }
}
