﻿namespace QLQ.Core.Security
{
    public class QlqFunctions
    {
        public const string DM_QUYEN = "DM_Quyen";

        public const string PHIEU_C89 = "Phieu_C89";


        public const string ACCOUMT = "Account";


        public const string DM_NGANHANG = "DM_NganHang";

        public const string DM_DAU_TU = "DM_DauTu";

        public const string DM_CHUCDANH = "DM_ChucDanh"; 
        
        public const string DM_CHITIEU = "DM_ChiTieu";

        public const string DM_BC = "DM_BC";

        public const string DM_CQBHXH = "DM_CQBHXH";

        public const string DM_HINHTHUCDAUTU = "DM_HinhThucDauTu";

        public const string DM_HOPDONG = "DM_HopDong";

        public const string DM_KIHAN = "DM_KyHan";

        public const string DM_TOCHUCDAUTHAU = "DM_ToChucDauThau";

        public const string DM_DANGKYLUUKY = "DM_DangKyLuuKy";

        public const string DM_TOCHUCCUNGCAPDICHVUDAUTHAU = "DM_DanhMucToChucCungCapDichVuDauThau";

        public const string QUANLYNGUOIDUNG = "QuanLyNguoiDung";

        public const string HOPDONG = "HopDong";

        public const string HOPDONGQUAHAN = "HopDongQuaHan";

        public const string HS_KHACHHANG = "HoSoKhachHang";

        public const string HOSO = "HoSo";

        public const string HS_XEPLOAINGANHANG = "HSXepLoaiNganHang";

        public const string OBJECT = "object";

        public const string ROLEPERMISSION = "RolePerMisson";

        public const string DIEUCHINHLAISUAT = "RolePerMisson";

        public const string SOCHITIETLAIDAUTU_01DTQ = "SoChiTietLaiDauTu_01DRQ";

        public const string SOCHTIETLAIDAUTUQUYBHXH = "SoChiTietTienLaiDauTuQuyBHXH";


        public const string LAISUATHUYDONG = "LaiSuatHuyDong";

        public const string SOTHEODOI = "SoTheoDoi";

        public const string LAISUATHDVBINHQUAN = "LaiSuatHDVBinhQuan";

        public const string BC_KEHOACHDAUTU = "BC_KeHoachDauTu";

        public const string BC_C90HD = "BC_C90HD";

        public const string BC_TINHHINHDAUTUTAICHINH = "BC_TinhHinhDauTuTaiChinh";

        public const string BC_THULAIDAUTUTAICHINH = "BC_ThuLaiDauTuTaiChinh";

        public const string BC_BANGDOICHIEU = "BC_BangDoiChieu";

        public const string TH_SODUDAUTUCHITIET = "TH_SoDuDauTuChiTiet";

        public const string TH_SODUDAUTUMOI = "TH_SoDuDauTuMoi";

        public const string TH_SODUDAUTUDENHAN = "TH_SoDuDauTuDenHan";

        public const string DM_HINHTHUCDAUTHAU = "DM_HINHTHUCDAUTHAU";

    }
}
