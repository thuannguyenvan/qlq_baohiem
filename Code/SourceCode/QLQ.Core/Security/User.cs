﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QLQ.Core.Security
{
    public class User
    {
        public long AccountId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public bool IsAdmin { get; set; }
        public string MaCoquan { get; set; }
        public string TenCoquan { get; set; }
                
        public Dictionary<string, List<string>> Permissons { get; set; }

        public User()
        {
            Permissons = new Dictionary<string, List<string>>();
        }
    }
}
