﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace QLQ.FEW.Helper
{
    public class UploadControlHelper
    {
        public const string LocalHost = "~/eis/Content/UploadControl/UploadFolder/";
        public const string UploadDirectory = @"/Uploads/";

        public static readonly UploadControlValidationSettings UploadValidationSettings = new UploadControlValidationSettings
        {
            AllowedFileExtensions = new string[] { ".xml", ".xls", ".xlsx", ".dbf", ".DBF" },
            MaxFileSize = 83886080,
            NotAllowedFileExtensionErrorText = "Văn bản không đúng định dạng yêu cầu!",
            GeneralErrorText = "Có lỗi trong quá trình upload văn bản!"
        };

        public static void ucMultiSelection_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            string resultFileName = Path.GetRandomFileName() + "_" + e.UploadedFile.FileName;
            string resultFileUrl = UploadDirectory + resultFileName;
            //Custom host show file
            string hostFileUrl = LocalHost + resultFileName;
            string resultFilePath = HttpContext.Current.Request.MapPath(resultFileUrl);
            e.UploadedFile.SaveAs(resultFilePath);

            //UploadingUtils.RemoveFileWithDelay(resultFileName, resultFilePath, 5);

            IUrlResolutionService urlResolver = sender as IUrlResolutionService;
            if (urlResolver != null)
            {
                string url = urlResolver.ResolveClientUrl(hostFileUrl);
                e.CallbackData = GetCallbackData(e.UploadedFile, url);
            }
            if (System.Web.HttpContext.Current.Session["URL"] == null)
            {
                System.Web.HttpContext.Current.Session["URL"] = resultFilePath;
            }
            else
            {
                System.Web.HttpContext.Current.Session["URL"] += '|' + resultFilePath;
            }

        }
        static string GetCallbackData(UploadedFile uploadedFile, string fileUrl)
        {
            string name = uploadedFile.FileName;
            long sizeInKilobytes = uploadedFile.ContentLength / 1024;
            string sizeText = sizeInKilobytes.ToString() + " KB";

            return name + "|" + fileUrl + "|" + sizeText;
        }
        
    }

    public static class UploadControlDemosHelper
    {
        public const string LocalHost = "~/eis/Content/UploadControl/UploadFolder/";
        public const string UploadDirectory = @"/Uploads/HoSo/";

        public static readonly UploadControlValidationSettings ValidationSettings = new UploadControlValidationSettings
        {
            AllowedFileExtensions = new string[] { ".jpg", ".jpeg", ".png" },
            MaxFileSize = 20971520,
        };

        public static void uc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                string resultFilePath = HttpContext.Current.Request.MapPath(UploadDirectory + e.UploadedFile.FileName);


                //Lưu file upload vào thư mục
                e.UploadedFile.SaveAs(resultFilePath, true);
                IUrlResolutionService urlResolver = sender as IUrlResolutionService;
                if (urlResolver != null)
                {
                    e.CallbackData = urlResolver.ResolveClientUrl(resultFilePath);
                }
            }
        }
    }
}