﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QLQ.FEW.Helper
{
    public enum ELoaiLaiSuat
    {
        CaNhanKyCuoi = 1,
        CaNhanHangThang = 2,
        ToChucKyCuoi = 3,
        ToChucHangThang = 4
    }
}