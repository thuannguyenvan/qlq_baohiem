﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Web.Mvc;
using DevExpress.XtraPrinting;
using System.IO;
using System.Linq;
using log4net;
using DevExpress.XtraPrintingLinks;

namespace DevExpress.Web.Demos
{
    public enum GridViewExportFormat { None, Pdf, Xls, Xlsx, Rtf, Csv }
    public delegate ActionResult GridViewExportMethod(GridViewSettings settings, object dataObject);
   
    public class GridViewExportDemoHelper
    {
        const int MAX_EXPORT_COUNT = 20000;
        static string ExcelDataAwareGridViewSettingsKey = "51172A18-2073-426B-A5CB-136347B3A79F";
        static string FormatConditionsExportGridViewSettingsKey = "14634B6F-E1DC-484A-9728-F9608615B628";
        private static readonly ILog log = LogManager.GetLogger(typeof(GridViewExportDemoHelper));
        static Dictionary<GridViewExportFormat, GridViewExportMethod> exportFormatsInfo;
        public static Dictionary<GridViewExportFormat, GridViewExportMethod> ExportFormatsInfo
        {
            get
            {
                if (exportFormatsInfo == null)
                    exportFormatsInfo = CreateExportFormatsInfo();
                return exportFormatsInfo;
            }
        }

        static IDictionary Context { get { return System.Web.HttpContext.Current.Items; } }

        static Dictionary<GridViewExportFormat, GridViewExportMethod> CreateExportFormatsInfo()
        {
            return new Dictionary<GridViewExportFormat, GridViewExportMethod> {
                { GridViewExportFormat.Pdf, GridViewExtension.ExportToPdf },
                {
                    GridViewExportFormat.Xls,
                    (settings, data) => GridViewExtension.ExportToXls(settings, data, new XlsExportOptionsEx { ExportType = DevExpress.Export.ExportType.WYSIWYG })
                },
                { 
                    GridViewExportFormat.Xlsx,
                    (settings, data) => GridViewExtension.ExportToXlsx(settings, data, new XlsxExportOptionsEx { ExportType = DevExpress.Export.ExportType.WYSIWYG })
                },
                { GridViewExportFormat.Rtf, GridViewExtension.ExportToRtf },
                {
                    GridViewExportFormat.Csv,
                    (settings, data) => GridViewExtension.ExportToCsv(settings, data, new CsvExportOptionsEx { ExportType = DevExpress.Export.ExportType.WYSIWYG })
                }
            };
        }

        static Dictionary<GridViewExportFormat, GridViewExportMethod> dataAwareExportFormatsInfo;
        public static Dictionary<GridViewExportFormat, GridViewExportMethod> DataAwareExportFormatsInfo
        {
            get
            {
                if (dataAwareExportFormatsInfo == null)
                    dataAwareExportFormatsInfo = CreateDataAwareExportFormatsInfo();
                return dataAwareExportFormatsInfo;
            }
        }
        static Dictionary<GridViewExportFormat, GridViewExportMethod> CreateDataAwareExportFormatsInfo()
        {
            return new Dictionary<GridViewExportFormat, GridViewExportMethod> {
                { GridViewExportFormat.Xls, GridViewExtension.ExportToXls },
                { GridViewExportFormat.Xlsx, GridViewExtension.ExportToXlsx },
                { GridViewExportFormat.Csv, GridViewExtension.ExportToCsv }
              
            };
        }

        static Dictionary<GridViewExportFormat, GridViewExportMethod> formatConditionsExportFormatsInfo;
        public static Dictionary<GridViewExportFormat, GridViewExportMethod> FormatConditionsExportFormatsInfo
        {
            get
            {
                if (formatConditionsExportFormatsInfo == null)
                    formatConditionsExportFormatsInfo = CreateFormatConditionsExportFormatsInfo();
                return formatConditionsExportFormatsInfo;
            }
        }
        static Dictionary<GridViewExportFormat, GridViewExportMethod> CreateFormatConditionsExportFormatsInfo()
        {
            return new Dictionary<GridViewExportFormat, GridViewExportMethod> {
                {GridViewExportFormat.Pdf, GridViewExtension.ExportToPdf}, {
                    GridViewExportFormat.Xls,
                    (settings, data) =>
                        GridViewExtension.ExportToXls(settings, data,
                            new XlsExportOptionsEx {ExportType = DevExpress.Export.ExportType.WYSIWYG})
                }, {
                    GridViewExportFormat.Xlsx,
                    (settings, data) =>
                        GridViewExtension.ExportToXlsx(settings, data,
                            new XlsxExportOptionsEx {ExportType = DevExpress.Export.ExportType.WYSIWYG})
                },
                {GridViewExportFormat.Rtf, GridViewExtension.ExportToRtf}
            };
        }

        public static string GetExportButtonTitle(GridViewExportFormat format)
        {
            if (format == GridViewExportFormat.None)
                return string.Empty;
            return string.Format("Export to {0}", format.ToString().ToUpper());
        }

        public static GridViewSettings FormatConditionsExportGridViewSettings
        {
            get
            {
                var settings = Context[FormatConditionsExportGridViewSettingsKey] as GridViewSettings;
                if (settings == null)
                    Context[FormatConditionsExportGridViewSettingsKey] = settings = CreateFormatConditionsExportGridViewSettings();
                return settings;
            }
        }
        static GridViewSettings CreateFormatConditionsExportGridViewSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "grid";
            settings.KeyFieldName = "ID";
            settings.EnableRowsCache = false;
            settings.Width = Unit.Percentage(100);
            settings.Columns.Add(s =>
            {
                s.FieldName = "ID";
                s.Caption = "ID";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "NAM_THANG";
                s.Caption = "Năm Tháng";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;

            });
            settings.Columns.Add(s =>
            {

                s.FieldName = "MA_NKP";
                s.Caption = "Nguồn quỹ";

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "MA_CD";
                s.Caption = " Chế Độ";
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "MA_DV";
                s.Caption = "Đơn vị";
            });


            settings.Columns.Add(s =>
            {
                s.FieldName = "MA_DVQL";
                s.Caption = "Mã QVDL";
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "SO_SO";
                s.Caption = "Số Sổ/Thẻ BHYT";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;

            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "SO_QD";
                s.Caption = "Số quyết định";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "HO_TEN";
                s.Caption = "Họ tên";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "NAM_TRUOC";
                s.Caption = "Năm Trước";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "NAM_NAY";
                s.Caption = "Năm Nay";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "NAM_SAU";
                s.Caption = "Năm sau";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "DIEN_GIAI";
                s.Caption = "Diễn giải";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;

            });
            return settings;
        }
        public static GridViewSettings FormatExportDSTCBHXHSettings
        {
            get
            {
                var settings = Context[FormatConditionsExportGridViewSettingsKey] as GridViewSettings;
                if (settings == null)
                    Context[FormatConditionsExportGridViewSettingsKey] = settings = CreateDSTCBHXHViewSettings();
                return settings;
            }
        }

        static GridViewSettings CreateDSTCBHXHViewSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "grid_DSTC";
            settings.KeyFieldName = "ID";
            settings.EnableRowsCache = false;
            settings.Width = Unit.Percentage(100);
            settings.Columns.Add(column =>
            {
                column.FieldName = "SO_QD";
                column.Caption = "Số quyết định";
            });
            settings.Columns.Add(column =>
            {
                column.FieldName = "SO_SO";
                column.Caption = "Số sổ ";
            });
            settings.Columns.Add(column =>
            {
                column.FieldName = "HO_TEN";
                column.Caption = "Họ và tên";
            });
            settings.Columns.Add(column =>
            {
                column.FieldName = "DIEN_GIAI";
                column.Caption = "Diễn giải";
            });
            settings.Columns.Add(column =>
            {
                column.FieldName = "SO_NGUOI";
                column.Caption = "Số người";
            }); settings.Columns.Add(column =>
            {
                column.FieldName = "CONG";
                column.Caption = "CONG";
            });
            settings.Columns.Add(column =>
            {
                column.FieldName = "LAN_DAU";
                column.Caption = "Trợ cấp lần đầu";
            });
            settings.Columns.Add(column =>
            {
                column.FieldName = "MOT_LAN";
                column.Caption = "Trợ cấp một lần";
            });
            settings.Columns.Add(column =>
            {
                column.FieldName = "MTP";
                column.Caption = "Mai táng phí";
            });
            settings.Columns.Add(column =>
            {
                column.FieldName = "PC_KV";
                column.Caption = "Trợ cấp KV một lần nguồn Quỹ";
            });
            settings.Columns.Add(column =>
            {
                column.FieldName = "PC_KV_NS";
                column.Caption = "Trợ cấp KV một lần nguồn NS";
            });
            settings.Columns.Add(column =>
            {
                column.FieldName = "TRO_CAP";
                column.Caption = "Trợ cấp một lần chết do TNLD/BNN";
            });
            settings.Columns.Add(column =>
            {
                column.FieldName = "SO_TIEN";
                column.Caption = "Số tiền nhận tháng này";
            });
            settings.Columns.Add(column =>
            {
                column.FieldName = "CHUA_NHAN";
                column.Caption = "Chưa nhận các tháng trước";
            });
            settings.Columns.Add(column =>
            {
                column.FieldName = "TRUY_LINH";
                column.Caption = "Truy lĩnh do đ/c mức hưởng hàng tháng";
            }); settings.Columns.Add(column =>
            {
                column.FieldName = "DIA_CHI";
                column.Caption = "Địa chỉ";
            });

            return settings;
        }

        public static FileStreamResult CreateExcelExportResult(CompositeLink link, string fileName)
        {
            XlsxExportOptions options = new XlsxExportOptions();
            options.ExportMode = XlsxExportMode.SingleFilePageByPage;
            MemoryStream stream = new MemoryStream();
            link.PrintingSystem.ExportToXlsx(stream, options);
            stream.Position = 0;
            FileStreamResult result = new FileStreamResult(stream, "application/xlsx");
            result.FileDownloadName = fileName + ".xlsx";
            return result;
        }
        public static FileStreamResult ExportExcelData<T> (IEnumerable<T> lstData, string filename, GridViewSettings settings)
        {
            try
            {
                PrintingSystem ps = new PrintingSystem();
                CompositeLink compositeLink = new CompositeLink(ps);
                var dataRowCount = lstData.Count();
                var sheetNummber = dataRowCount / MAX_EXPORT_COUNT;
                for (int i = 0; i < sheetNummber + 1; i++)
                {
                    int startIndex = i * MAX_EXPORT_COUNT;
                    int end = Math.Min(dataRowCount, startIndex + MAX_EXPORT_COUNT);
                    var data = lstData.Skip(startIndex).Take(end);
                    PrintableComponentLink phatSinh = new PrintableComponentLink(ps);
                    phatSinh.Component = GridViewExtension.CreatePrintableObject(settings, data);
                    compositeLink.Links.AddRange(new object[] { phatSinh });
                }
                compositeLink.CreatePageForEachLink();
                FileStreamResult result = CreateExcelExportResult(compositeLink, filename);
                ps.Dispose();

                return result;
            }
            catch (Exception ex)
            {
                log.Error("ExportExcel: " + ex.InnerException.Data);
            }
            return null;
        }
        public static GridViewSettings FormatConditionsExportC78HDHDRGridViewSettings
        {
            get
            {
                var settings = Context[FormatConditionsExportGridViewSettingsKey] as GridViewSettings;
                if (settings == null)
                    Context[FormatConditionsExportGridViewSettingsKey] = settings = CreateFormatConditionsExportC78HDHDRGridViewSettings();
                return settings;
            }
        }
        static GridViewSettings CreateFormatConditionsExportC78HDHDRGridViewSettings()
        {
            var settings = new GridViewSettings();
            settings.Name = "grid";
            settings.KeyFieldName = "ID";
            settings.EnableRowsCache = false;
            settings.Width = Unit.Percentage(100);
            settings.Columns.Add(s =>
            {
                s.FieldName = "ID";
                s.Caption = "ID";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = false;

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "NAM_THANG";
                s.Caption = "Năm Tháng";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "SO_QD";
                s.Caption = "Số Q/Đ/HS";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "HO_TEN";
                s.Caption = "Họ và Tên";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;
            });
            settings.Columns.Add(col =>
            {
                col.FieldName = "NGAY_SINH";
                col.Caption = "Ngày sinh ";
                col.ColumnType = MVCxGridViewColumnType.DateEdit;
                col.Width = System.Web.UI.WebControls.Unit.Pixel(95);
                col.PropertiesEdit.DisplayFormatString = "dd/MM/yyyy";
                var deProp = col.PropertiesEdit as DateEditProperties;
                deProp.DisplayFormatInEditMode = true;
                deProp.DisplayFormatString = "dd/MM/yyyy";
                deProp.EditFormat = EditFormat.Custom;
                deProp.EditFormatString = "dd/MM/yyyy";
                col.CellStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "GIOI_TINH";
                s.Caption = "Giới tính";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "DIA_CHI";
                s.Caption = "Địa chỉ";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "SO_SO";
                s.Caption = "Mã Thẻ BHYT";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;
            });
            settings.Columns.Add(col =>
            {
                col.FieldName = "TUNGAY";
                col.Caption = "Giá trị từ :";
                col.ColumnType = MVCxGridViewColumnType.DateEdit;
                col.Width = System.Web.UI.WebControls.Unit.Pixel(95);
                col.PropertiesEdit.DisplayFormatString = "dd/MM/yyyy";
                var deProp = col.PropertiesEdit as DateEditProperties;
                deProp.DisplayFormatInEditMode = true;
                deProp.DisplayFormatString = "dd/MM/yyyy";
                deProp.EditFormat = EditFormat.Custom;
                deProp.EditFormatString = "dd/MM/yyyy";
                col.CellStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;

            });
            settings.Columns.Add(col =>
            {
                col.FieldName = "DENNGAY";
                col.Caption = " đến :";
                col.ColumnType = MVCxGridViewColumnType.DateEdit;
                col.Width = System.Web.UI.WebControls.Unit.Pixel(95);
                col.PropertiesEdit.DisplayFormatString = "dd/MM/yyyy";
                var deProp = col.PropertiesEdit as DateEditProperties;
                deProp.DisplayFormatInEditMode = true;
                deProp.DisplayFormatString = "dd/MM/yyyy";
                deProp.EditFormat = EditFormat.Custom;
                deProp.EditFormatString = "dd/MM/yyyy";
                col.CellStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "DK_KCB";
                s.Caption = "ĐK KCB BĐ";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "MA_KCB";
                s.Caption = "Điều trị tại";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "HANG";
                s.Caption = "Hạng bệnh viện";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "DIEU_TRI";
                s.Caption = "Loại hình điều trị";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;
            });
            settings.Columns.Add(col =>
            {
                col.FieldName = "NGAY_BD";
                col.Caption = "Điều trị từ ngày : ";
                col.ColumnType = MVCxGridViewColumnType.DateEdit;
                col.Width = System.Web.UI.WebControls.Unit.Pixel(95);
                col.PropertiesEdit.DisplayFormatString = "dd/MM/yyyy";
                var deProp = col.PropertiesEdit as DateEditProperties;
                deProp.DisplayFormatInEditMode = true;
                deProp.DisplayFormatString = "dd/MM/yyyy";
                deProp.EditFormat = EditFormat.Custom;
                deProp.EditFormatString = "dd/MM/yyyy";
                col.CellStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;

            });
            settings.Columns.Add(col =>
            {
                col.FieldName = "NGAY_KT";
                col.Caption = "đến ngày";
                col.ColumnType = MVCxGridViewColumnType.DateEdit;
                col.Width = System.Web.UI.WebControls.Unit.Pixel(95);
                col.PropertiesEdit.DisplayFormatString = "dd/MM/yyyy";
                var deProp = col.PropertiesEdit as DateEditProperties;
                deProp.DisplayFormatInEditMode = true;
                deProp.DisplayFormatString = "dd/MM/yyyy";
                deProp.EditFormat = EditFormat.Custom;
                deProp.EditFormatString = "dd/MM/yyyy";
                col.CellStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "MA_CD";
                s.Caption = "Mã chuẩn đoán";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "DIEN_GIAI";
                s.Caption = "Diễn giải";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "BENH_NHAN";
                s.Caption = "Người bệnh cùng chi trả";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "NGOAI_TRU";
                s.Caption = "Cơ quan BHXH thanh toán";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;
            });
            return settings;
        }


        #region ExportGridSettings DM đối tượng
        public static GridViewSettings FormatConditionsExportGridViewSettingsDMDOITUONG
        {
            get
            {
                var settings = Context[FormatConditionsExportGridViewSettingsKey] as GridViewSettings;
                if (settings == null)
                    Context[FormatConditionsExportGridViewSettingsKey] = settings = CreateFormatConditionsExportGridViewSettingsDMDOITUONG();
                return settings;
            }
        }
        static GridViewSettings CreateFormatConditionsExportGridViewSettingsDMDOITUONG()
        {
            var settings = new GridViewSettings();
            settings.Name = "grid";
            settings.KeyFieldName = "ID";
            settings.EnableRowsCache = false;
            settings.Width = Unit.Percentage(100);
            settings.Columns.Add(s =>
            {
                s.FieldName = "MA";
                s.Caption = "Mã";
                //s.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;

            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "MA_NHOM";
                s.Caption = "Mã Nhóm";
                //s.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "NOIDUNG";
                s.Caption = "Nội Dung";
                //s.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "MASO";
                s.Caption = "Mã Số";
                //s.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "BHXH";
                s.Caption = "Bảo Hiểm Xã Hội";
                //s.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "BHYT";
                s.Caption = "Bảo Hiểm Y Tế";
                //s.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "BHTN";
                s.Caption = "Bảo Hiểm Thất Nghiệp";
                //s.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;

            });


            settings.Columns.Add(col =>
            {
                col.FieldName = "NGAY_BD";
                col.Caption = "Ngày bắt đầu";
                col.ColumnType = MVCxGridViewColumnType.DateEdit;
                col.Width = System.Web.UI.WebControls.Unit.Pixel(95);
                col.PropertiesEdit.DisplayFormatString = "dd/MM/yyyy";
                var deProp = col.PropertiesEdit as DateEditProperties;
                deProp.DisplayFormatInEditMode = true;
                deProp.DisplayFormatString = "dd/MM/yyyy";
                deProp.EditFormat = EditFormat.Custom;
                deProp.EditFormatString = "dd/MM/yyyy";
                col.CellStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;
                col.Visible = true;
            });
            settings.Columns.Add(col =>
            {
                col.FieldName = "NGAY_KT";
                col.Caption = "Ngày kết thúc";
                col.ColumnType = MVCxGridViewColumnType.DateEdit;
                col.Width = System.Web.UI.WebControls.Unit.Pixel(95);
                col.PropertiesEdit.DisplayFormatString = "dd/MM/yyyy";
                var deProp = col.PropertiesEdit as DateEditProperties;
                deProp.DisplayFormatInEditMode = true;
                deProp.DisplayFormatString = "dd/MM/yyyy";
                deProp.EditFormat = EditFormat.Custom;
                deProp.EditFormatString = "dd/MM/yyyy";
                col.CellStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;
                col.Visible = true;
            });
            return settings;
        }
        #endregion

        public static GridViewSettings FormatConditionsExportGridViewSettingsSP_PSCT
        {
            get
            {
                var settings = Context[FormatConditionsExportGridViewSettingsKey] as GridViewSettings;
                if (settings == null)
                    Context[FormatConditionsExportGridViewSettingsKey] = settings = CreateFormatConditionsExportGridViewSettingsSP_PSCT();
                return settings;
            }
        }
        static GridViewSettings CreateFormatConditionsExportGridViewSettingsSP_PSCT()
        {
            var settings = new GridViewSettings();
            settings.Name = "Tạm thu";
            settings.KeyFieldName = "ID_CHUNGTU;ID";
            settings.EnableRowsCache = false;
            settings.Width = Unit.Percentage(100);
            settings.Columns.Add(s =>
            {
                s.FieldName = "so_hieu";
                s.Caption = "Số hiệu";
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Percentage(10);

            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "ngay_thang";
                s.Caption = @"Ngày tháng";
                s.PropertiesEdit.DisplayFormatString = @"dd/MM/yyyy";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;

                s.Width = Unit.Percentage(10);

            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "dien_giai";
                s.Caption = "Diễn giải";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.FooterCellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.FooterCellStyle.Font.Bold = true;
                s.Width = Unit.Percentage(15);

            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "no_ct1";
                s.Caption = "SHTK";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.ColumnType = MVCxGridViewColumnType.ComboBox;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(10);
            });


            settings.Columns.Add(s =>
            {
                s.FieldName = "co_ct1";
                s.Caption = "Đơn vị";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.ColumnType = MVCxGridViewColumnType.ComboBox;

                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(10);
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "dia_chi";
                s.Caption = "Tên đơn vị";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                s.ColumnType = MVCxGridViewColumnType.TextBox;
                s.Width = Unit.Percentage(20);
                var tb = s.PropertiesEdit as TextBoxProperties;

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "so_tien";
                s.Caption = "Số tiền";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = @"C0";
                s.Width = Unit.Percentage(15);

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "sms";
                s.Caption = "Đã gửi sms";
                s.Width = Unit.Percentage(5);
                s.PropertiesEdit.EnableClientSideAPI = true;
            });
            return settings;
        }

        public static GridViewSettings CreateGridTamThuSettings(string name)
        {
            var settings = new GridViewSettings();
            settings.Name = name;
            settings.KeyFieldName = "ID_CHUNGTU;ID";
            settings.EnableRowsCache = false;
            settings.Width = Unit.Percentage(100);
            settings.Columns.Add(s =>
            {
                s.FieldName = "so_hieu";
                s.Caption = "Số hiệu";
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Percentage(10);

            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "ngay_thang";
                s.Caption = @"Ngày tháng";
                s.PropertiesEdit.DisplayFormatString = @"dd/MM/yyyy";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;

                s.Width = Unit.Percentage(10);

            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "dien_giai";
                s.Caption = "Diễn giải";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(15);

            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "no_ct1";
                s.Caption = "SHTK";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.ColumnType = MVCxGridViewColumnType.ComboBox;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(10);
            });


            settings.Columns.Add(s =>
            {
                s.FieldName = "co_ct1";
                s.Caption = "Đơn vị";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.ColumnType = MVCxGridViewColumnType.TextBox;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(10);
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "dia_chi";
                s.Caption = "Tên đơn vị";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                s.ColumnType = MVCxGridViewColumnType.TextBox;
                s.Width = Unit.Percentage(20);
                var tb = s.PropertiesEdit as TextBoxProperties;

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "so_tien";
                s.Caption = "Số tiền";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = @"C0";
                s.Width = Unit.Percentage(15);

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "sms";
                s.Caption = "Đã gửi sms";
                s.Width = Unit.Percentage(5);
                s.PropertiesEdit.EnableClientSideAPI = true;
            });
            return settings;
        }

        public static GridViewSettings CreateGridChungTuSettings(string name)
        {
            var settings = new GridViewSettings();
            settings.Name = name;
            settings.KeyFieldName = "PK_VCHITIETCHUNGTU.ID_CHUNGTU;PK_VCHITIETCHUNGTU.ID_DKHOAN;PK_VCHITIETCHUNGTU.ID_CHITIET";
            settings.EnableRowsCache = false;
            settings.Width = Unit.Percentage(100);
            //settings.Columns.Add(s =>
            //{
            //    s.FieldName = "PK_VCHITIETCHUNGTU.ID_CHUNGTU";
            //    s.Caption = "ID_CHUNGTU";
            //    s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            //    s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
            //});
            //settings.Columns.Add(s =>
            //{
            //    s.FieldName = "PK_VCHITIETCHUNGTU.ID_DKHOAN";
            //    s.Caption = "ID_DKHOAN";
            //    s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            //    s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
            //});
            settings.Columns.Add(s =>
            {
                s.FieldName = "USERNAME";
                s.Caption = "USERNAME";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "MA_DVQL";
                s.Caption = "MA_DVQL";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "MA_NHOM";
                s.Caption = "MA_NHOM";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "SO_CTGS";
                s.Caption = "SO_CTGS";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "SO_HIEU";
                s.Caption = "SO_HIEU";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "NGAY_THANG";
                s.Caption = "NGAY_THANG";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                s.PropertiesEdit.DisplayFormatString = @"dd/MM/yyyy";
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "HO_TEN";
                s.Caption = "HO_TEN";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "SO_SO";
                s.Caption = "SO_SO";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "SO_CMND";
                s.Caption = "SO_CMND";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "MA_CHUNGTU";
                s.Caption = "MA";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "DIA_CHI";
                s.Caption = "DIA_CHI";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "NGHIEPVU";
                s.Caption = "NGHIEPVU";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "DIEN_GIAI";
                s.Caption = "DIEN_GIAI";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "TKNO";
                s.Caption = "TKNO";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "NO_CT1";
                s.Caption = "NO_CT1";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "NO_CT2";
                s.Caption = "NO_CT2";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "NO_CT3";
                s.Caption = "NO_CT3";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "TKCO";
                s.Caption = "TKCO";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "CO_CT1";
                s.Caption = "CO_CT1";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "CO_CT2";
                s.Caption = "CO_CT2";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "CO_CT3";
                s.Caption = "CO_CT3";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "SO_TIEN_CHITIET";
                s.Caption = "SO_TIEN";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "SO_LUONG";
                s.Caption = "SO_LUONG";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "DON_GIA";
                s.Caption = "DON_GIA";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            });

            return settings;
        }
        public static GridViewSettings CreateGridKiemSoatSettings(string name)
        {
            var settings = new GridViewSettings();
            settings.Name = name;
            settings.KeyFieldName = "ID_CHUNGTU";
            settings.EnableRowsCache = false;
            settings.Width = Unit.Percentage(100);
            settings.Columns.Add(s =>
            {
                s.FieldName = "MA_NHOM";
                s.Caption = "MA_NHOM";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "SO_CTGS";
                s.Caption = "SO_CTGS";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "SO_HIEU";
                s.Caption = "SO_HIEU";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "NGAY_THANG";
                s.Caption = "NGAY_THANG";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                s.PropertiesEdit.DisplayFormatString = @"dd/MM/yyyy";
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "HO_TEN";
                s.Caption = "HO_TEN";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "SOTIEN";
                s.Caption = "SOTIEN";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = @"#,#";
            }); settings.Columns.Add(s =>
             {
                 s.FieldName = "SO_SO";
                 s.Caption = "SO_SO";
                 s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                 s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
             });
            settings.Columns.Add(s =>
            {
                s.FieldName = "SO_CMND";
                s.Caption = "SO_CMND";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "DIA_CHI";
                s.Caption = "DIA_CHI";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "DIEN_GIAI";
                s.Caption = "DIEN_GIAI";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "KEM_THEO";
                s.Caption = "KT";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "JOBTYPE";
                s.Caption = "JT";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "READONLY";
                s.Caption = "KIEM_SOAT";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "USERNAME";
                s.Caption = "USERNAME";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "USERDATE";
                s.Caption = "USERDATE";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            });

            return settings;
        }

        public static GridViewSettings CreateGridCMND_CTSettings(string name)
        {
            var settings = new GridViewSettings();
            settings.Name = name;
            settings.KeyFieldName = "ID_CHUNGTU";
            settings.EnableRowsCache = false;
            settings.Width = Unit.Percentage(100);
            settings.Columns.Add(s =>
            {
                s.FieldName = "ID_CHUNGTU";
                s.Visible = false;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "SO_SO";
                s.Caption = "SỐ SỔ";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "SO_HIEU";
                s.Caption = "SỐ HIỆU";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "NGAY_THANG";
                s.Caption = "NGÀY";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "HO_TEN";
                s.Caption = "HỌ TÊN";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "DIEN_GIAI";
                s.Caption = "DIỄN GIẢI";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "SOTIEN";
                s.Caption = "SỐ TIỀN";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "DIA_CHI";
                s.Caption = "ĐỊA CHỈ";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
            });

            return settings;
        }


        /// <summary>
        /// Author: Phạm Văn Thủy
        /// Export Bảng kê CTNH
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static GridViewSettings CreateGridBKCTNHSettings(string name)
        {
            var settings = new GridViewSettings();
            settings.Name = name;
            settings.KeyFieldName = "ID_CHUNGTU";
            settings.EnableRowsCache = false;
            settings.Width = Unit.Percentage(100);

            //1
            settings.Columns.Add(s =>
            {
                s.FieldName = "CHON";
                s.Caption = @"CHON".ToLower();

                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.Width = Unit.Percentage(3);
            });

            //2
            settings.Columns.Add(s =>
            {
                s.FieldName = "ID_CHUNGTU";
                s.Caption = @"ID_CHUNGTU".ToLower();
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(10);
            });


            //3
            settings.Columns.Add(s =>
            {
                s.FieldName = "LOAILENH";
                s.Caption = @"LOAILENH".ToLower();
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(3);
            });


            //4
            settings.Columns.Add(s =>
            {
                s.FieldName = "SOHIEU";
                s.Caption = "SOHIEU".ToLower();
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(6);
            });




            // 5 
            settings.Columns.Add(s =>
            {
                s.FieldName = "NGAY_THANG_STR";
                s.Caption = @"NGAY_THANG".ToLower();

                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(10);
            });

            //6
            settings.Columns.Add(s =>
            {
                s.FieldName = "MSNH";
                s.Caption = @"MSNH".ToLower();

                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(7);
            });

            //7
            settings.Columns.Add(s =>
            {
                s.FieldName = "TKCHUYEN";
                s.Caption = @"TKCHUYEN".ToLower();

                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(15);
            });

            //8
            settings.Columns.Add(s =>
            {
                s.FieldName = "TEN_DVQL";
                s.Caption = @"TEN_DVQL".ToLower();

                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(20);
            });

            //9
            settings.Columns.Add(s =>
            {
                s.FieldName = "NHCHUYEN";
                s.Caption = @"NHCHUYEN".ToLower();

                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(20);
            });

            //10
            settings.Columns.Add(s =>
            {
                s.FieldName = "MAKH";
                s.Caption = @"MAKH".ToLower();

                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(10);
            });

            //11
            settings.Columns.Add(s =>
            {
                s.FieldName = "HO_TEN";
                s.Caption = @"HO_TEN".ToLower();

                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(25);
            });

            //12
            settings.Columns.Add(s =>
            {
                s.FieldName = "DIA_CHI";
                s.Caption = @"DIA_CHI".ToLower();

                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(25);
            });

            //13
            settings.Columns.Add(s =>
            {
                s.FieldName = "DIEN_GIAI";
                s.Caption = @"DIEN_GIAI".ToLower();

                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(25);
            });

            //14
            settings.Columns.Add(s =>
            {
                s.FieldName = "LOAINT";
                s.Caption = @"LOAINT".ToLower();

                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(25);
            });

            //15
            settings.Columns.Add(s =>
            {
                s.FieldName = "SO_TIEN";
                s.Caption = "SO_TIEN".ToLower();

                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
              //  s.PropertiesEdit.DisplayFormatString = @"C0";
                s.Width = Unit.Percentage(15);
            });

            //16
            settings.Columns.Add(s =>
            {
                s.FieldName = "TKNO";
                s.Caption = "TKNO".ToLower();

                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                s.FooterCellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.FooterCellStyle.Font.Bold = true;
                s.Width = Unit.Percentage(6);
            });

            //17
            settings.Columns.Add(s =>
            {
                s.FieldName = "TKCO";
                s.Caption = "TKCO".ToLower();

                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                s.FooterCellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.FooterCellStyle.Font.Bold = true;
                s.Width = Unit.Percentage(6);
            });


            
            //18
            settings.Columns.Add(s =>
            {
                s.FieldName = "TKKH";
                s.Caption = "TKKH".ToLower();

                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                s.FooterCellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.FooterCellStyle.Font.Bold = true;
                s.Width = Unit.Percentage(15);
            });


            //19
            settings.Columns.Add(s =>
            {
                s.FieldName = "TENTKKH";
                s.Caption = "TENTKKH".ToLower();

                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                s.FooterCellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.FooterCellStyle.Font.Bold = true;
                s.Width = Unit.Percentage(20);
            });


            //20
            settings.Columns.Add(s =>
            {
                s.FieldName = "TINHTP";
                s.Caption = "TINHTP".ToLower();

                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                s.FooterCellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.FooterCellStyle.Font.Bold = true;
                s.Width = Unit.Percentage(20);
            });


            //20
            settings.Columns.Add(s =>
            {
                s.FieldName = "MASOTHUE";
                s.Caption = "MASOTHUE".ToLower();

                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                s.FooterCellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.FooterCellStyle.Font.Bold = true;
                s.Width = Unit.Percentage(20);
            });



            return settings;
        }
        public static GridViewSettings CreateGridThuBHXHTNSettings(string name)
        {
            var settings = new GridViewSettings();
            settings.Name = name;
            settings.KeyFieldName = "ID_CHUNGTU;ID";
            settings.EnableRowsCache = false;
            settings.Width = Unit.Percentage(100);
            settings.Columns.Add(s =>
            {
                s.FieldName = "so_hieu";
                s.Caption = "Số hiệu";
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Percentage(5);
                s.ReadOnly = true;

            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "ngay_thang";
                s.Caption = @"Ngày tháng";
                s.ColumnType = MVCxGridViewColumnType.DateEdit;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                s.PropertiesEdit.DisplayFormatString = @"dd/MM/yyyy";
                s.Width = Unit.Percentage(10);

            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "dien_giai";
                s.Caption = "Diễn giải";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(15);
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "no_ct1";
                s.Caption = "SHTK";
                s.PropertiesEdit.EnableClientSideAPI = true;
            });


            settings.Columns.Add(s =>
            {
                s.FieldName = "co_ct1";
                s.Caption = "Số sổ BHXH";
                s.PropertiesEdit.EnableClientSideAPI = true;

            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "co_ct2";
                s.Caption = "Đơn vị";
                s.PropertiesEdit.EnableClientSideAPI = true;

            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "ho_ten";
                s.Caption = "Họ tên";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.ColumnType = MVCxGridViewColumnType.TextBox;
                s.Width = Unit.Percentage(15);

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "so_tien";
                s.Caption = "Số tiền";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = @"C0";
                s.Width = Unit.Percentage(10);

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "so_luong";
                s.Caption = "TP";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.ColumnType = MVCxGridViewColumnType.TextBox;
                s.Width = Unit.Percentage(5);

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "co_ct2";
                s.Caption = "Đơn vị";
                s.Visible = false;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.ColumnType = MVCxGridViewColumnType.ComboBox;

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "dia_chi";
                s.Caption = "Địa chỉ";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.ColumnType = MVCxGridViewColumnType.TextBox;
                s.Width = Unit.Percentage(20);

            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "sms";
                s.Caption = "Đã gửi sms";
                s.Width = Unit.Percentage(5);
                s.PropertiesEdit.EnableClientSideAPI = true;
            });
            return settings;
        }

        public static GridViewSettings CreateGridThoaiThuBBSetings(string name)
        {
            var settings = new GridViewSettings();
            settings.Name = name;
            settings.KeyFieldName = "ID_CHUNGTU;ID";
            settings.Width = Unit.Percentage(100);
            settings.Columns.Add(s =>
            {
                s.FieldName = "so_hieu";
                s.Caption = "Số hiệu";
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Percentage(10); ;
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "ngay_thang";
                s.Caption = @"Ngày tháng";
                s.ColumnType = MVCxGridViewColumnType.DateEdit;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                s.PropertiesEdit.DisplayFormatString = @"dd/MM/yyyy";
                s.Width = Unit.Percentage(10);

            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "dien_giai";
                s.Caption = "Diễn giải";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(20);

            });


            settings.Columns.Add(s =>
            {
                s.FieldName = "no_ct1";
                s.Caption = "Đơn vị";
                s.ColumnType = MVCxGridViewColumnType.TextBox;
                s.Width = Unit.Percentage(10);
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "dia_chi";
                s.Caption = "Tên đơn vị";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.ColumnType = MVCxGridViewColumnType.TextBox;
                s.Width = Unit.Percentage(25);

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "so_tien";
                s.Caption = "Số tiền";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = @"C0";
                s.Width = Unit.Percentage(15);

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "sms";
                s.Caption = "Đã gửi sms";
                s.Width = Unit.Percentage(5);
                s.EditFormSettings.Visible = DefaultBoolean.False;
                s.ColumnType = MVCxGridViewColumnType.CheckBox;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "tkno";

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "tkco";
            });
            return settings;
        }
        public static GridViewSettings CreateGridThoaiThuTNSettings(string name)
        {
            var settings = new GridViewSettings();
            settings.Name = name;
            settings.KeyFieldName = "ID_CHUNGTU;ID";
            settings.Width = Unit.Percentage(100);
            settings.Columns.Add(s =>
            {
                s.FieldName = "so_hieu";
                s.Caption = "Số hiệu";
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Percentage(5);

            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "ngay_thang";
                s.Caption = @"Ngày tháng";
                s.ColumnType = MVCxGridViewColumnType.DateEdit;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                s.PropertiesEdit.DisplayFormatString = @"dd/MM/yyyy";
                s.Width = Unit.Percentage(10);

            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "dien_giai";
                s.Caption = "Diễn giải";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(20);
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "no_ct2";
                s.Caption = "Đơn vị";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(5);
            });


            settings.Columns.Add(s =>
            {
                s.FieldName = "no_ct1";
                s.Caption = "Số sổ BHXH";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.ColumnType = MVCxGridViewColumnType.ComboBox;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(10);
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "ho_ten";
                s.Caption = "Họ tên";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.ColumnType = MVCxGridViewColumnType.TextBox;
                s.Width = Unit.Percentage(15);

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "so_tien";
                s.Caption = "Số tiền";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = @"C0";
                s.Width = Unit.Percentage(10);

            });


            settings.Columns.Add(s =>
            {
                s.FieldName = "dia_chi";
                s.Caption = "Địa chỉ";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.ColumnType = MVCxGridViewColumnType.TextBox;
                s.Width = Unit.Percentage(20);

            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "sms";
                s.Caption = "Đã gửi sms";
                s.Width = Unit.Percentage(5);
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.EditFormSettings.Visible = DefaultBoolean.False;
                s.ColumnType = MVCxGridViewColumnType.CheckBox;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "tkno";

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "tkco";


            });
            return settings;
        }

        public static GridViewSettings CreateGridSMSUNC(string name)
        {
            var settings = new GridViewSettings();
            settings.Name = name;
            settings.KeyFieldName = "keySl";
            settings.Width = Unit.Percentage(100);
            settings.Columns.Add(s =>
            {
                s.FieldName = "keySl";
                s.Caption = "keySl";
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Percentage(10);
                s.ReadOnly = true;

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "soPhieu";
                s.Caption = "Số hiệu";
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Percentage(10);
                s.ReadOnly = true;

            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "ngay";
                s.Caption = @"Ngày tháng";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Percentage(10);

            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "noiDung";
                s.Caption = "Nội dung";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(30);

            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "maNh";
                s.Caption = "Mã ngân hàng";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(10);
            });


            settings.Columns.Add(s =>
            {
                s.FieldName = "ma";
                s.Caption = "Đơn vị";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(10);
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "tienVnd";
                s.Caption = "Số tiền";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = @"N0";
                s.Width = Unit.Percentage(20);

            });
            return settings;
        }

        public static GridViewSettings CreateGridSMSUNCTN(string name)
        {
            var settings = new GridViewSettings();
            settings.Name = name;
            settings.KeyFieldName = "keySl";
            settings.Width = Unit.Percentage(100);
            settings.Columns.Add(s =>
            {
                s.FieldName = "keySl";
                s.Caption = "keySl";
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Percentage(10);
                s.ReadOnly = true;

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "soPhieu";
                s.Caption = "Số hiệu";
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Percentage(10);
                s.ReadOnly = true;

            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "ngay";
                s.Caption = @"Ngày tháng";
                s.ColumnType = MVCxGridViewColumnType.DateEdit;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                s.PropertiesEdit.DisplayFormatString = @"dd/MM/yyyy";
                s.Width = Unit.Percentage(10);

            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "noiDung";
                s.Caption = "Nội dung";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;

                s.Width = Unit.Percentage(20);

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "soBhxh";
                s.Caption = "Số sổ BHXH";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(10);
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "hoTen";
                s.Caption = "Họ tên";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(15);
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "tienVnd";
                s.Caption = "Số tiền";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = @"C0";
                s.Width = Unit.Percentage(15);

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "phuongThuc";
                s.Caption = "PT";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = @"N0";
                s.Width = Unit.Percentage(5);

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "maDvi";
                s.Caption = @"Đơn vị";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(10);
            });
            return settings;
        }
        public static GridViewSettings CreateGridC69_HD(string name)
        {
            var settings = new GridViewSettings();
            settings.Name = name;
            settings.KeyFieldName = "NAM_THANG;MA_BAOCAO;MA";
            settings.EnableRowsCache = false;
            settings.Width = Unit.Percentage(100);
            settings.Columns.Add(s =>
            {
                s.FieldName = "NAM_THANG";
                s.Caption = "Năm tháng";
                s.ToolTip = "Năm tháng";
                s.Visible = false;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "MA_BAOCAO";
                s.Caption = "Mã báo cáo";
                s.ToolTip = "Mã báo cáo";
                s.Visible = false;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "MA";
                s.Caption = "Mã";
                s.ToolTip = "Mã";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.MinWidth = 100;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "NOIDUNG";
                s.Caption = "Nội dung";
                s.ToolTip = "Nội dung";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Pixel(400);
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "F_01";
                s.Caption = "Năm trước";
                s.ToolTip = "Truy thu BHYT năm trước";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = "N0";
                s.MinWidth = 110;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "F_02";
                s.Caption = "Năm nay";
                s.ToolTip = "BHYT năm nay";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = "N0";
                s.MinWidth = 110;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "F_03";
                s.Caption = "Năm sau";
                s.ToolTip = "BHYT năm sau";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = "N0";
                s.MinWidth = 110;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "F_04";
                s.Caption = "Lãi";
                s.ToolTip = "Lãi do chậm nộp";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = "N0";
                s.MinWidth = 110;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "F_05";
                s.Caption = "Số thu";
                s.ToolTip = "Đơn vị sử dụng lao động, người lao động đóng";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = "N0";
                s.MinWidth = 110;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "F_06";
                s.Caption = "NSNN";
                s.ToolTip = "Ngân sách nhà nước phải hỗ trợ";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = "N0";
                s.MinWidth = 110;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "F_07";
                s.Caption = "Lãi";
                s.ToolTip = "Lãi do chậm nộp";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = "N0";
                s.MinWidth = 110;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "F_08";
                s.Caption = "Số thu";
                s.ToolTip = "Thu bảo hiểm xã hội";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = "N0";
                s.MinWidth = 110;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "F_09";
                s.Caption = "Lãi";
                s.ToolTip = "Lãi do chậm nộp";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = "N0";
                s.MinWidth = 110;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "F_10";
                s.Caption = "Ghi thu";
                s.ToolTip = "Ghi tăng thu 2% để lại";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = "N0";
                s.MinWidth = 110;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "TONGCONG";
                s.Caption = "Tổng";
                s.ToolTip = "Tổng cộng";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = "N0";
                s.MinWidth = 200;
            });
            return settings;
        }

        // B04a_TS

        public static GridViewSettings CreateGridB04a_TS(string name)
        {
            var settings = new GridViewSettings();
            settings.Name = name;
            settings.KeyFieldName = "MA";
            settings.EnableRowsCache = false;
            settings.Width = Unit.Percentage(100);

            settings.Columns.Add(s =>
            {
                s.FieldName = "MA_DVQL";
                s.Caption = "Mã ĐVQL";

                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                s.MinWidth = 100;
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "KY_BAOCAO";
                s.Caption = "Kỳ báo cáo";

                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                s.MinWidth = 100;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "MA";
                s.Caption = "Mã";
                
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.MinWidth = 100;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "NOIDUNG";
                s.Caption = "Nội dung";
               
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Pixel(400);
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "F_01";
                s.Caption = "F_01";
                
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = "N0";
                s.MinWidth = 110;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "F_02";
                s.Caption = "F_02";
               
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = "N0";
                s.MinWidth = 110;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "F_03";
                s.Caption = "F_03";
               
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = "N0";
                s.MinWidth = 110;
            });
           
            return settings;
        }

        public static GridViewSettings CreateGridC83_HD(string name)
        {
            var settings = new GridViewSettings();
            settings.Name = name;
            settings.KeyFieldName = "NAM_THANG;MA_BAOCAO;MA";
            settings.EnableRowsCache = false;
            settings.Width = Unit.Percentage(100);
            settings.Columns.Add(s =>
            {
                s.FieldName = "NAM_THANG";
                s.Caption = "Năm tháng";
                s.ToolTip = "Năm tháng";
                s.Visible = false;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "MA_BAOCAO";
                s.Caption = "Mã báo cáo";
                s.ToolTip = "Mã báo cáo";
                s.Visible = false;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "MA";
                s.Caption = "Mã";
                s.ToolTip = "Mã";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.MinWidth = 100;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "NOIDUNG";
                s.Caption = "Nội dung";
                s.ToolTip = "Nội dung";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Pixel(400);
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "F_01";
                s.Caption = "Năm trước";
                s.ToolTip = "Truy thu BHYT năm trước";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = "N0";
                s.MinWidth = 100;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "F_02";
                s.Caption = "Năm nay";
                s.ToolTip = "BHYT năm nay";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = "N0";
                s.MinWidth = 100;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "F_03";
                s.Caption = "Năm sau";
                s.ToolTip = "BHYT năm sau";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = "N0";
                s.MinWidth = 100;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "F_04";
                s.Caption = "Lãi";
                s.ToolTip = "Lãi do chậm nộp";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = "N0";
                s.MinWidth = 100;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "F_05";
                s.Caption = "Số thu";
                s.ToolTip = "Đơn vị sử dụng lao động, người lao động đóng";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = "N0";
                s.MinWidth = 100;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "F_06";
                s.Caption = "NSNN";
                s.ToolTip = "Ngân sách nhà nước phải hỗ trợ";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = "N0";
                s.MinWidth = 100;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "F_07";
                s.Caption = "Lãi";
                s.ToolTip = "Lãi do chậm nộp";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = "N0";
                s.MinWidth = 100;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "F_08";
                s.Caption = "Số thu";
                s.ToolTip = "Thu bảo hiểm xã hội";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.PropertiesEdit.DisplayFormatString = "N0";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = "N0";
                s.MinWidth = 100;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "F_09";
                s.Caption = "Lãi";
                s.ToolTip = "Lãi do chậm nộp";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = "N0";
                s.MinWidth = 100;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "F_10";
                s.Caption = "Ghi thu";
                s.ToolTip = "Ghi tăng thu 2% để lại";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = "N0";
                s.MinWidth = 100;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "TONGCONG";
                s.Caption = "Tổng";
                s.ToolTip = "Tổng cộng";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = "N0";
                s.Width = Unit.Pixel(200);
            });
            return settings;
        }
        #region ExportGridSettings Thu BHXHTN
        public static GridViewSettings FormatConditionsExportGridViewSettingsSP_PSCT_BHXHTN
        {
            get
            {
                var settings = Context[FormatConditionsExportGridViewSettingsKey] as GridViewSettings;
                if (settings == null)
                    Context[FormatConditionsExportGridViewSettingsKey] = settings = CreateFormatConditionsExportGridViewSettingsSP_PSCT_BHXHTN();
                return settings;
            }
        }
        static GridViewSettings CreateFormatConditionsExportGridViewSettingsSP_PSCT_BHXHTN()
        {
            var settings = new GridViewSettings();
            settings.Name = "ThuBHXHTN";
            settings.KeyFieldName = "ID_CHUNGTU;ID";
            settings.EnableRowsCache = false;
            settings.Width = Unit.Percentage(100);
            settings.Columns.Add(s =>
            {
                s.FieldName = "so_hieu";
                s.Caption = "Số hiệu";
                s.Visible = true;
                //  s.FixedStyle = GridViewColumnFixedStyle.Left;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                //   s.EditFormSettings.Visible = DefaultBoolean.True;
                s.Width = Unit.Percentage(5);
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "ngay_thang";
                s.Caption = "Ngày tháng";
                s.ColumnType = MVCxGridViewColumnType.DateEdit;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                s.PropertiesEdit.DisplayFormatString = @"dd/MM/yyyy";
                //  s.EditFormSettings.Visible = DefaultBoolean.True;
                // s.Width = Unit.Pixel(150);
                s.Width = Unit.Percentage(14);
                s.Visible = true;
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "dien_giai";
                s.Caption = "Diễn giải";
                // s.FixedStyle = GridViewColumnFixedStyle.Left;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.FooterCellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.FooterCellStyle.Font.Bold = true;
                s.Visible = true;
                //   s.EditFormSettings.Visible = DefaultBoolean.True;
                s.Width = Unit.Percentage(14);
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "no_ct1";
                s.Caption = "SHTK";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.ColumnType = MVCxGridViewColumnType.ComboBox;
                s.Visible = true;
                //var cp = s.PropertiesEdit as ComboBoxProperties;                        // cách 1
                //cp.DataSource = ViewData["SHTK"];
                //cp.TextFormatString = "{0} ";
                //cp.ValueType = typeof(string);
                //cp.TextField = "NOIDUNG";
                //cp.ValueField = "MA";
                ////cp.FilterMinLength = 0;
                //cp.Columns.Add("MA", "Mã", Unit.Percentage(20));
                //cp.Columns.Add("NOIDUNG", "Nội dung", Unit.Percentage(80));

                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(14);

            });
            //settings.Columns.Add(s =>
            //{
            //    s.FieldName = "so_so";
            //    s.Caption = "Số sổ BHXH";
            //    s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
            //    s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            //    // s.Width = Unit.Pixel(75);
            //});

            settings.Columns.Add(s =>
            {
                s.FieldName = "so_so";
                s.Caption = "Số sổ BHXH";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.ColumnType = MVCxGridViewColumnType.ComboBox;

                //var cp = s.PropertiesEdit as ComboBoxProperties;                        // cách 1
                //cp.DataSource = ViewData["DS"];
                //cp.TextFormatString = "{0}";
                //cp.ValueType = typeof(string);
                //cp.TextField = "NOIDUNG";
                //cp.ValueField = "MA";
                ////cp.FilterMinLength = 0;
                //cp.Columns.Add("MA", "Mã", Unit.Percentage(20));
                //cp.Columns.Add("NOIDUNG", "Nội dung", Unit.Percentage(80));
                // cp.ClientSideEvents.SelectedIndexChanged = "so_so_SelectedIndexChanged";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(14);
                s.Visible = true;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "HO_TEN";
                s.Caption = "Họ và tên";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Percentage(14);
                s.ReadOnly = true;
                s.Visible = true;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "so_luong";
                s.Caption = "TP";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Percentage(14);
                s.Visible = true;
            });


            settings.Columns.Add(s =>
            {
                s.FieldName = "dia_chi";
                s.Caption = "Đơn vị";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.ColumnType = MVCxGridViewColumnType.ComboBox;
                s.Visible = true;
                //var cp = s.PropertiesEdit as ComboBoxProperties;                        // cách 1
                //cp.DataSource = ViewData["DMDV"];
                //cp.TextFormatString = "{1} ";
                //cp.ValueType = typeof(string);
                //cp.TextField = "NOIDUNG";
                //cp.ValueField = "DM_DONVI_PK.MA";
                ////cp.FilterMinLength = 0;
                //cp.Columns.Add("DM_DONVI_PK.MA", "Mã", Unit.Percentage(30));
                //cp.Columns.Add("NOIDUNG", "Nội dung", Unit.Percentage(70));
                s.Width = Unit.Pixel(100);
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(14);
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "so_tien";
                s.Caption = "Số tiền";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Visible = true;
                s.Width = Unit.Percentage(14);
                s.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
                s.PropertiesEdit.DisplayFormatString = "c";
            });
            return settings;
        }
        #endregion
        public static GridViewSettings gridView_MTP(string name)
        {
            var settings = new GridViewSettings();
            settings.Name = name;
            settings.KeyFieldName = "ID";
            settings.EnableRowsCache = false;
            settings.Width = Unit.Percentage(100);
            settings.Columns.Add(s =>
            {
                s.FieldName = "NAM_THANG";
                s.ToolTip = "NAM_THANG";
                s.Caption = "Năm tháng";
                s.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;
                s.Width = Unit.Pixel(80);

                s.FixedStyle = GridViewColumnFixedStyle.Left;

            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "MASO";
                s.Caption = "Mã số";
                s.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;
                s.Width = Unit.Pixel(70);

            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "SO_QD";
                s.Caption = "Số QĐ";
                s.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;
                s.Width = Unit.Pixel(110);
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "SO_SO";
                s.Caption = "Số sổ ";
                s.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;
                s.Width = Unit.Pixel(100);
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "HO_TEN";
                s.Caption = "Họ tên";
                s.Settings.AutoFilterCondition = AutoFilterCondition.Contains;

                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Pixel(150);
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "CONG";
                s.Caption = "Cộng";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Pixel(100);
                s.PropertiesEdit.DisplayFormatString = "#,#";
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "LAN_DAU";
                s.Caption = "Lần đầu";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Pixel(90);
                s.PropertiesEdit.DisplayFormatString = "#,#";
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "MOT_LAN";
                s.Caption = "Một lần";
                s.Width = Unit.Pixel(90);
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.PropertiesEdit.DisplayFormatString = "#,#";
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "MTP";
                s.ToolTip = "Mai táng phí";
                s.Caption = "MTP";
                s.Width = Unit.Pixel(90);
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.PropertiesEdit.DisplayFormatString = "#,#";
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "PC_KV";
                s.ToolTip = "Trợ cấp KV một lần nguồn Quỹ";
                s.Caption = "PC_KV ";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.PropertiesEdit.DisplayFormatString = "#,#";
                s.Width = Unit.Pixel(90);
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "PC_KV_NS";
                s.ToolTip = "Trợ cấp KV một lần nguồn NS";
                s.Caption = "PC_KV_NS";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.PropertiesEdit.DisplayFormatString = "#,#";
                s.Width = Unit.Pixel(90);
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "TRO_CAP";
                s.ToolTip = "Trợ cấp một lần chết do TNLD/BNN";
                s.Caption = "Trợ cấp";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.PropertiesEdit.DisplayFormatString = "#,#";
                s.Width = Unit.Pixel(90);
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "SO_TIEN";
                s.Caption = "Số tiền";
                s.ToolTip = "SO_TIEN";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Pixel(100);
                s.PropertiesEdit.DisplayFormatString = "#,#";
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "CHUA_NHAN";
                s.ToolTip = "Chưa nhận các tháng trước";
                s.Caption = "Chưa nhận";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Pixel(80);
                s.PropertiesEdit.DisplayFormatString = "#,#";
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "TRUY_LINH";
                s.ToolTip = "Truy lĩnh do đ/c mức hưởng hàng tháng";
                s.Caption = "Truy lĩnh";
                s.PropertiesEdit.DisplayFormatString = "#,#";
                s.Width = Unit.Pixel(80);
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "ID_CHUNGTU";
                s.Caption = "ID_CHUNGTU";
                s.Visible = false;
            });


            settings.Columns.Add(col =>
            {
                col.FieldName = "DIEN_GIAI";
                col.Caption = "diễn giải";
                col.Width = Unit.Pixel(250);
                col.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

            });
            return settings;
        }
        public static GridViewSettings gridView_ODTS(string name)
        {
            var settings = new GridViewSettings();
            settings.Name = name;
            settings.KeyFieldName = "ID;MA_DV";
            settings.EnableRowsCache = false;
            settings.Width = Unit.Percentage(100);
            settings.Columns.Add(s =>
            {
                s.FieldName = "ID";
                s.Caption = "ID";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.Visible = false;
                s.EditFormSettings.Visible = DefaultBoolean.False;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "ID_CHUNGTU";
                s.Visible = false;

            });
            //settings.Columns.Add(s =>
            //{
            //    s.FieldName = "CMT";
            // //   s.Visible = false;
            //});

            settings.Columns.Add(s =>
            {

                s.FieldName = "SO_HIEU";
                s.Caption = "Số hiệu";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                //  s.Visible = false;
                s.Width = Unit.Percentage(7);
                s.FixedStyle = GridViewColumnFixedStyle.Left;

            });
            settings.Columns.Add(s =>
            {
                //s.Visible = false;
                s.FieldName = "NGAY_THANG";
                s.Caption = "Ngày tháng";
                s.ColumnType = MVCxGridViewColumnType.DateEdit;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Percentage(7);
                s.FixedStyle = GridViewColumnFixedStyle.Left;
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "NGAY";
                s.Caption = "Ngày";
                s.ColumnType = MVCxGridViewColumnType.DateEdit;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.PropertiesEdit.DisplayFormatString = "dd/MM/yyyy";
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Percentage(7);
                s.FixedStyle = GridViewColumnFixedStyle.Left;
            });
            settings.Columns.Add(s =>
            {
                s.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                s.FieldName = "MA_DV";
                s.Caption = "Mã đơn vị";
                s.FixedStyle = GridViewColumnFixedStyle.Left;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Percentage(10);
            });

            settings.Columns.Add(s =>
            {
                s.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                s.FieldName = "NOIDUNG";
                s.Caption = "Tên đơn vị";
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Percentage(35);
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "HOTEN";
                s.Caption = "Họ tên";
                s.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Pixel(200);
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "SOBHXH";
                s.Caption = "Sổ BHXH";
                s.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Pixel(200);
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "CMT";
                s.Caption = "Số CMT";
                s.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Pixel(100);
            });
            settings.Columns.Add(s =>
            {
                s.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                s.FieldName = "SO_TIEN";
                s.Caption = "Cộng";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Percentage(17);
                s.PropertiesEdit.DisplayFormatString = "#,#";

            });
            settings.Columns.Add(s =>
            {
                s.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                s.FieldName = "OMDAU_ST";
                s.Caption = "Ốm đau";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Percentage(7);
                s.PropertiesEdit.DisplayFormatString = "#,#";
            });
            settings.Columns.Add(s =>
            {
                s.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                s.FieldName = "THAISAN_ST";
                s.Caption = "Thai sản";
                s.PropertiesEdit.EnableClientSideAPI = true;
                //    s.FixedStyle = GridViewColumnFixedStyle.Left;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Percentage(7);
                s.PropertiesEdit.DisplayFormatString = "#,#";
            });
            settings.Columns.Add(s =>
            {
                s.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                s.FieldName = "DSUCOD_ST";
                s.Caption = "DS-OD";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Percentage(7);
                s.PropertiesEdit.DisplayFormatString = "#,#";
            });
            settings.Columns.Add(s =>
            {
                s.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                s.FieldName = "DSUCTS_ST";
                s.Caption = "DS-TS";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Percentage(7);
                s.PropertiesEdit.DisplayFormatString = "#,#";
            });
            settings.Columns.Add(s =>
            {
                s.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                s.FieldName = "DSUCTN_ST";
                s.Caption = "DS-TN";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Percentage(7);
                s.PropertiesEdit.DisplayFormatString = "#,#";
            });

            settings.Columns.Add(s =>
            {
                s.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                s.Name = "TU";
                s.FieldName = "ID_CHAPNHAN";
                s.Caption = "Trạng thái";
                s.Width = Unit.Percentage(10);


            });
            return settings;
        }
        public static GridViewSettings CreateGridDMDonViSettings(string name)
        {
            var settings = new GridViewSettings();
            settings.Name = name;
            settings.KeyFieldName = "DM_DONVI_PK.MA_DVQL;DM_DONVI_PK.MA";
            settings.EnableRowsCache = false;
            settings.Width = Unit.Percentage(100);
            settings.Columns.Add(s =>
            {
                s.FieldName = "DM_DONVI_PK.MA_DVQL";
                s.Caption = "MA_DVQL";
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Percentage(5);
            });


            settings.Columns.Add(col =>
            {
                col.FieldName = "DM_DONVI_PK.MA";
                col.Caption = "Mã";
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            });

            settings.Columns.Add(col =>
            {
                col.FieldName = "KHOI";
                col.Caption = "Khối ";
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            });

            settings.Columns.Add(col =>
            {
                col.FieldName = "NOIDUNG";
                col.Caption = "Nội Dung ";
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                col.Width = 250;
            });
            settings.Columns.Add(col =>
            {
                col.FieldName = "DIACHI";
                col.Caption = "Địa Chỉ";
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                col.Width = 300;

            });
            settings.Columns.Add(col =>
            {
                col.FieldName = "KHOIBCT";
                col.Caption = "Khối BCT"; col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            });
            settings.Columns.Add(col =>
            {
                col.FieldName = "READONLY";
                col.Caption = "READONLY";
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            });

            settings.Columns.Add(col =>
            {
                col.FieldName = "DAIDIEN";
                col.Caption = "Đại Diện";
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                col.Width = 200;
            });
            settings.Columns.Add(col =>
            {
                col.FieldName = "TAIKHOAN";
                col.Caption = "Tài Khoản";
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                col.Width = 120;
            });
            settings.Columns.Add(col =>
            {
                col.FieldName = "MASOTHUE";
                col.Caption = "Mã Số Thuế";
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                col.Width = 100;
            });
            settings.Columns.Add(col =>
            {
                col.FieldName = "NGANHANG";
                col.Caption = "Ngân Hàng";
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                col.Width = 130;
            });
            settings.Columns.Add(col =>
            {
                col.FieldName = "FAX";
                col.Caption = "FAX";
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            });
            settings.Columns.Add(col =>
            {
                col.FieldName = "TINHTP";
                col.Caption = "Tỉnh TP";
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                col.Width = 250;
            });
            settings.Columns.Add(col =>
            {
                col.FieldName = "DIENTHOAI";
                col.Caption = "Điện Thoại";
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            });

            settings.Columns.Add(col =>
            {
                col.FieldName = "EMAIL";
                col.Caption = "Email";
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            });
            settings.Columns.Add(col =>
            {
                col.FieldName = "GIAODICH";
                col.Caption = "Giao Dịch";
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                col.Width = 120;
            });


            settings.Columns.Add(col =>
            {
                col.FieldName = "KHOIKCB";
                col.Caption = "Khối KCB ";
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                col.Width = 100;
            });
            settings.Columns.Add(col =>
            {
                col.FieldName = "NSTN";
                col.Caption = "NSTN";
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Right;
            });
            settings.Columns.Add(col =>
            {
                col.FieldName = "TYLE";
                col.Caption = "TYLE";
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Right;
            });

            settings.Columns.Add(col =>
            {
                col.FieldName = "NGAY_BD";
                col.Caption = "Ngày bắt đầu";
                col.ColumnType = MVCxGridViewColumnType.DateEdit;
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                col.Width = 100;
            });

            settings.Columns.Add(col =>
            {
                col.FieldName = "NGAY_KT";
                col.Caption = "Ngày kết thúc";
                col.ColumnType = MVCxGridViewColumnType.DateEdit;
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                col.Width = 100;

            });
            return settings;
        }
        public static GridViewSettings CreateGridDMBenhVienSettings(string name)
        {
            var settings = new GridViewSettings();
            settings.Name = name;
            settings.KeyFieldName = "DM_BenhvienPK.MA_BV;DM_BenhvienPK.MA_TINH";
            settings.EnableRowsCache = false;
            settings.Width = Unit.Percentage(100);
            settings.Columns.Add(col =>
            {
                col.FieldName = "DM_BenhvienPK.MA_TINH";
                col.Caption = "Mã tỉnh";
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            });
            settings.Columns.Add(col =>
            {
                col.FieldName = "MA";
                col.Caption = "Mã";
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            });
           
            settings.Columns.Add(col =>
            {
                col.FieldName = "TEN_BV";
                col.Caption = "Tên bệnh viện ";
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            });

            settings.Columns.Add(col =>
            {
                col.FieldName = "DIACHI";
                col.Caption = "Địa Chỉ";
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                col.Width = 300;

            });
            settings.Columns.Add(col =>
            {
                col.FieldName = "TUYEN_BV";
                col.Caption = "Tuyến BV"; 
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            });
            settings.Columns.Add(col =>
            {
                col.FieldName = "HANG_BV";
                col.Caption = "Hạng BV";
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            });

            settings.Columns.Add(col =>
            {
                col.FieldName = "DAIDIEN";
                col.Caption = "Đại Diện";
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                col.Width = 200;
            });
            settings.Columns.Add(col =>
            {
                col.FieldName = "MASOTHUE";
                col.Caption = "Mã Số Thuế";
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                col.Width = 100;
            });
            settings.Columns.Add(col =>
            {
                col.FieldName = "TAIKHOAN";
                col.Caption = "Tài Khoản";
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                col.Width = 120;
            });
            settings.Columns.Add(col =>
            {
                col.FieldName = "NGANHANG";
                col.Caption = "Ngân Hàng";
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                col.Width = 130;
            });

            settings.Columns.Add(col =>
            {
                col.FieldName = "DIENTHOAI";
                col.Caption = "Điện Thoại";
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            });



            settings.Columns.Add(col =>
            {
                col.FieldName = "NGAY_BD";
                col.Caption = "Ngày bắt đầu";
                col.ColumnType = MVCxGridViewColumnType.DateEdit;
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                col.Width = 100;
            });

            settings.Columns.Add(col =>
            {
                col.FieldName = "NGAY_KT";
                col.Caption = "Ngày kết thúc";
                col.ColumnType = MVCxGridViewColumnType.DateEdit;
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                col.Width = 100;

            });
            return settings;
        }
        public static GridViewSettings CreateFormatConditionsExportGridViewVSA_THQT(string name, string stringND, string stringMASO, string stringHeader, decimal COUNTCOLUMN, string GHICHUVISIABLE)
        {
            var settings = new GridViewSettings();
            settings.Name = name;
            settings.KeyFieldName = "MA_BAOCAO;MA_CHITIEU";
            settings.EnableRowsCache = false;
            settings.Width = Unit.Percentage(100);
            settings.DataBound = (sender, e) =>
            {
                MVCxGridView grids = (MVCxGridView)sender;
                string[] ArrayHeaders = stringHeader.Split(';');
                //string[] ArrayCot = stringCot.Split(';');
                for (int i = 0; i < grids.Columns.Count; i++)
                {
                    if (i > 5 && i <= 5 + COUNTCOLUMN)
                    {

                        if (COUNTCOLUMN > 0)
                        {
                            if (i != (5 + COUNTCOLUMN))
                            {
                                grids.Columns[i].Visible = true;
                            }
                            else
                            {
                                if (GHICHUVISIABLE == "")
                                {
                                    grids.Columns[i].Visible = true;
                                }
                                else
                                {
                                    grids.Columns[i].Visible = false;
                                }
                            }
                        }
                    }
                    else if (i <= 5)
                    {
                        grids.Columns[i].Visible = true;
                        if (grids.Columns[i].Caption == "")
                        {
                            grids.Columns[i].Visible = false;
                        }

                        if (grids.Columns[i].Caption == "NOIDUNG" && stringND != "")
                        {
                            grids.Columns[i].Visible = false;
                        }
                        if (grids.Columns[i].Caption == "MASO" && stringMASO != "")
                        {
                            grids.Columns[i].Visible = false;
                        }

                    }
                    else
                    {
                        grids.Columns[i].Visible = false;
                    }

                }
                if (GHICHUVISIABLE != "")
                {
                    grids.Columns["GHICHU"].Visible = true;
                }
            };

            settings.Columns.Add(s =>
            {
                s.FieldName = "MA_BAOCAO";
                s.Caption = "MA_BAOCAO";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Pixel(100);
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "MA_DVQL";
                s.Caption = "MA_DVQL";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Pixel(100);
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "MA_CHITIEU";
                s.Caption = "MA_CHITIEU";
                s.PropertiesEdit.DisplayFormatString = "";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Pixel(100);
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "STT";
                s.Caption = "STT";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Pixel(100);
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "NOIDUNG";
                s.Caption = "NOIDUNG";

                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                s.Width = Unit.Percentage(50);
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "MASO";
                s.Caption = "MASO";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                s.Width = Unit.Pixel(100);
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "F_01";
                s.Caption = "F_01";

                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = "#,#";
                s.Width = Unit.Pixel(100);
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "F_02";
                s.Caption = "F_02";

                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = "#,#";
                s.Width = Unit.Pixel(100);
            }); settings.Columns.Add(s =>
            {
                s.FieldName = "F_03";
                s.Caption = "F_03";

                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = "#,#";
                s.Width = Unit.Pixel(100);
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "F_04";
                s.Caption = "F_04";

                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.Width = Unit.Pixel(130);
                s.PropertiesEdit.DisplayFormatString = "#,#";
                s.Width = Unit.Pixel(100);
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "F_05";
                s.Caption = "F_05";

                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = "#,#";
                s.Width = Unit.Pixel(100);
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "F_06";
                s.Caption = "F_06";

                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.PropertiesEdit.DisplayFormatString = "#,#";
                s.Width = Unit.Pixel(100);
            }); settings.Columns.Add(s =>
            {
                s.FieldName = "F_07";
                s.Caption = "F_07";

                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.Width = Unit.Pixel(100);
                s.PropertiesEdit.DisplayFormatString = "#,#";
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "F_08";
                s.Caption = "F_08";

                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.Width = Unit.Pixel(100);
                s.PropertiesEdit.DisplayFormatString = "#,#";
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "F_09";
                s.Caption = "F_09";

                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.Width = Unit.Pixel(100);
                s.PropertiesEdit.DisplayFormatString = "#,#";
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "F_10";
                s.Caption = "F_10";

                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.Width = Unit.Pixel(100);
                s.PropertiesEdit.DisplayFormatString = "#,#";
            }); settings.Columns.Add(s =>
            {
                s.FieldName = "F_11";
                s.Caption = "F_11";

                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.Width = Unit.Pixel(100);
                s.PropertiesEdit.DisplayFormatString = "#,#";
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "F_12";
                s.Caption = "F_12";

                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.Width = Unit.Pixel(100);
                s.PropertiesEdit.DisplayFormatString = "#,#";
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "F_13";
                s.Caption = "F_13";

                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.Width = Unit.Pixel(100);
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "F_14";
                s.Caption = "F_14";

                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.Width = Unit.Pixel(100);
                s.PropertiesEdit.DisplayFormatString = "#,#";
            }); settings.Columns.Add(s =>
            {
                s.FieldName = "F_15";
                s.Caption = "F_15";

                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.Width = Unit.Pixel(100);
                s.PropertiesEdit.DisplayFormatString = "#,#";
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "F_16";
                s.Caption = "F_16";

                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.Width = Unit.Pixel(100);
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "F_17";
                s.Caption = "F_17";

                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.Width = Unit.Pixel(100);
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "F_18";
                s.Caption = "F_18";

                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.Width = Unit.Pixel(100);
            }); settings.Columns.Add(s =>
            {
                s.FieldName = "F_19";
                s.Caption = "F_19";

                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.Width = Unit.Pixel(100);
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "F_20";
                s.Caption = "F_20";

                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.Width = Unit.Pixel(130);
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "F_21";
                s.Caption = "F_21";

                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.Width = Unit.Pixel(100);
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "F_22";
                s.Caption = "F_22";

                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.Width = Unit.Pixel(100);
            }); settings.Columns.Add(s =>
            {
                s.FieldName = "F_23";
                s.Caption = "F_23";

                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.Width = Unit.Pixel(100);
            });

            settings.Columns.Add(s =>
            {
                s.FieldName = "F_24";
                s.Caption = "F_24";

                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.Width = Unit.Pixel(100);
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "F_25";
                s.Caption = "F_25";

                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.Width = Unit.Pixel(100);
            });
            settings.Columns.Add(s =>
            {
                s.FieldName = "GHICHU";
                s.Caption = "GHICHU";
                s.PropertiesEdit.EnableClientSideAPI = true;
                s.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                s.CellStyle.HorizontalAlign = HorizontalAlign.Right;
                s.Width = Unit.Pixel(130);
            });
            return settings;
        }
        public static GridViewSettings CreateGridImPortCT_Settings(string name)
        {
            var settings = new GridViewSettings();
            settings.Name = name;
            settings.KeyFieldName = "ID_CHUNGTU";
            settings.EnableRowsCache = false;
            settings.Width = Unit.Percentage(100);
            settings.Columns.Add(col =>
            {
                col.FieldName = "LOAICHUNGTU";
                col.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                col.PropertiesEdit.EnableClientSideAPI = true;
                col.Width = System.Web.UI.WebControls.Unit.Pixel(100);
                col.CellStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;
            });
            settings.Columns.Add(col =>
            {
                col.FieldName = "HOTEN";
                col.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;
                col.Width = Unit.Pixel(70);
            });
            settings.Columns.Add(col =>
            {
                col.FieldName = "SOSO";
                col.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;
                col.Width = Unit.Pixel(70);
            });
            settings.Columns.Add(col =>
            {
                col.FieldName = "SOCMND";
                col.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;
                col.Width = Unit.Pixel(70);
            });
            settings.Columns.Add(col =>
            {
                col.FieldName = "KYQT";
                col.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;
                col.Width = Unit.Pixel(70);
            });
            settings.Columns.Add(col =>
            {
                col.FieldName = "SOHIEU";
                col.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;
                col.Width = Unit.Pixel(80);
            });
            settings.Columns.Add(col =>
            {
                col.FieldName = "NGAYTHANG";
                col.ColumnType = MVCxGridViewColumnType.DateEdit;
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                col.Width = 100;
            });

            settings.Columns.Add(col =>
            {
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.FieldName = "DIACHI";
                col.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                col.PropertiesEdit.EnableClientSideAPI = true;
                col.Width = System.Web.UI.WebControls.Unit.Pixel(250);
            });

            settings.Columns.Add(col =>
            {
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.FieldName = "DIENGIAI";
                col.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                col.PropertiesEdit.EnableClientSideAPI = true;
                col.CellStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;
                col.Width = System.Web.UI.WebControls.Unit.Pixel(250);
            });
            settings.Columns.Add(col =>
            {
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.FieldName = "NGHIEPVU";
                col.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                col.CellStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;
                col.Width = System.Web.UI.WebControls.Unit.Pixel(100);
            });
            settings.Columns.Add(col =>
            {
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.FieldName = "TKNO";
                col.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                col.CellStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;
                col.Width = System.Web.UI.WebControls.Unit.Pixel(100);
            });

            settings.Columns.Add(col =>
            {
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.FieldName = "NO_CT1";
                col.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                col.PropertiesEdit.EnableClientSideAPI = true;
                col.CellStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;
                col.Width = System.Web.UI.WebControls.Unit.Pixel(100);
            });
            settings.Columns.Add(col =>
            {
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.FieldName = "NO_CT2";
                col.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                col.PropertiesEdit.EnableClientSideAPI = true;
                col.CellStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;
                col.Width = System.Web.UI.WebControls.Unit.Pixel(100);
            });
            settings.Columns.Add(col =>
            {
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.FieldName = "NO_CT3";
                col.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                col.PropertiesEdit.EnableClientSideAPI = true;
                col.CellStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;
                col.Width = System.Web.UI.WebControls.Unit.Pixel(100);
            });
            settings.Columns.Add(col =>
            {
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.FieldName = "TKCO";
                col.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                col.PropertiesEdit.EnableClientSideAPI = true;
                col.CellStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;
                col.Width = 100;
            });
            settings.Columns.Add(col =>
            {
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.FieldName = "CO_CT1";
                col.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                col.PropertiesEdit.EnableClientSideAPI = true;
                col.CellStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;
                col.Width = 100;
            });
            settings.Columns.Add(col =>
            {
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.FieldName = "CO_CT2";
                col.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                col.PropertiesEdit.EnableClientSideAPI = true;
                col.CellStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;
                col.Width = 100;
            });
            settings.Columns.Add(col =>
            {
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.FieldName = "CO_CT3";
                col.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                col.PropertiesEdit.EnableClientSideAPI = true;
                col.CellStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;
                col.Width = 100;
            });
            settings.Columns.Add(col =>
            {
                col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                col.FieldName = "SOTIEN";
                col.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
                col.CellStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Right;
                col.Width = Unit.Pixel(135);
            });            
            return settings;
        }
    }
}
