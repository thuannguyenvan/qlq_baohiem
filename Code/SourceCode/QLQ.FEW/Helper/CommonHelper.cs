﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using QLQ.FEW.Models;
using DevExpress.Web;
using DevExpress.Web.Mvc;
using System;
using System.Web.UI.WebControls;
using FX.Core;
using NHibernate;
using NHibernate.Cfg;
using System.IO;
using System.Reflection;
using System.Net;
using QLQ.Core.Common;
//using System.Configuration;

namespace QLQ.Core.Helper
{
    public class CommonHelper
    {
        public static string GetVisitorIPAddress(bool GetLan = false)
            {
                string visitorIPAddress = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (String.IsNullOrEmpty(visitorIPAddress))
                    visitorIPAddress = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

                if (string.IsNullOrEmpty(visitorIPAddress))
                    visitorIPAddress = System.Web.HttpContext.Current.Request.UserHostAddress;

                if (string.IsNullOrEmpty(visitorIPAddress) || visitorIPAddress.Trim() == "::1")
                {
                    GetLan = true;
                    visitorIPAddress = string.Empty;
                }

                if (GetLan && string.IsNullOrEmpty(visitorIPAddress))
                {
                    //This is for Local(LAN) Connected ID Address
                    string stringHostName = Dns.GetHostName();
                    //Get Ip Host Entry
                    IPHostEntry ipHostEntries = Dns.GetHostEntry(stringHostName);
                    //Get Ip Address From The Ip Host Entry Address List
                    IPAddress[] arrIpAddress = ipHostEntries.AddressList;

                    try
                    {
                        visitorIPAddress = arrIpAddress[arrIpAddress.Length - 2].ToString();
                    }
                    catch
                    {
                        try
                        {
                            visitorIPAddress = arrIpAddress[0].ToString();
                        }
                        catch
                        {
                            try
                            {
                                arrIpAddress = Dns.GetHostAddresses(stringHostName);
                                visitorIPAddress = arrIpAddress[0].ToString();
                            }
                            catch
                            {
                                visitorIPAddress = "127.0.0.1";
                            }
                        }
                    }

                }
                return visitorIPAddress;
            }

        static Action<TextBoxSettings> textBoxSettingsMethod;
        static Action<LabelSettings> labelSettingsMethod;
        static Action<MVCxFormLayoutItem> formLayoutItemSettingsMethod;

        public static Action<TextBoxSettings> TextBoxSettingsMethod
        {
            get
            {
                if (textBoxSettingsMethod == null)
                    textBoxSettingsMethod = CreateTextBoxSettingsMethod();
                return textBoxSettingsMethod;
            }
        }
        public static Action<LabelSettings> LabelSettingsMethod
        {
            get
            {
                if (labelSettingsMethod == null)
                    labelSettingsMethod = CreateLabelSettingsMethod();
                return labelSettingsMethod;
            }
        }
        public static Action<MVCxFormLayoutItem> FormLayoutItemSettingsMethod
        {
            get
            {
                if (formLayoutItemSettingsMethod == null)
                    formLayoutItemSettingsMethod = CreateFormLayoutItemSettingsMethod();
                return formLayoutItemSettingsMethod;
            }
        }
        static Action<TextBoxSettings> CreateTextBoxSettingsMethod()
        {
            return settings =>
            {
                settings.ControlStyle.CssClass = "editor";
                settings.ShowModelErrors = true;
                settings.Properties.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithText;
            };
        }
        static Action<LabelSettings> CreateLabelSettingsMethod()
        {
            return settings => { settings.ControlStyle.CssClass = "label"; };
        }
        static Action<MVCxFormLayoutItem> CreateFormLayoutItemSettingsMethod()
        {
            return itemSettings =>
            {
                dynamic editorSettings = itemSettings.NestedExtensionSettings;
                editorSettings.Properties.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithText;
                editorSettings.ShowModelErrors = true;
                editorSettings.Width = Unit.Pixel(230);
            };
        }

        //genarate string 
        public static string GenerateRandomString(int length)
        {
            //var random = new Random();
            //const string characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            //var result = new StringBuilder(18);
            //var nguoidung = ((QLQ.Core.ACCContext)FX.Context.FXContext.Current).CurrentNguoidung;
            //string ma_dvql = nguoidung == null ? "" : nguoidung.MA_DVQL;
            //var timenow = ma_dvql + DateTime.Now.Year.ToString();
            //for (var i = 0; i < length; i++)
            //{
            //    if (i == 0)
            //    {
            //        result.Append("_");
            //    }
            //    result.Append(characters[random.Next(characters.Length)]);
            //}
            //result.Append(timenow);
            //return result.ToString();
            const string characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            var builder = new StringBuilder(18);
            var nguoidung = "";//((QLQ.Core.ACCContext)FX.Context.FXContext.Current).CurrentNguoidung;
            string ma_dvql = "0109";
            var rand = RandomProvider.GetThreadRandom();
            for (var i = 0; i < 14; i++)
            {
                int t = rand.Next(characters.Length);
                builder.Append(characters[t]);
            }
            builder.Append(ma_dvql);
            return builder.ToString();
        }
        //genarate string 
        public static int GenerateRandomNumber()
        {
            Random r = new Random();
            int n = r.Next();

            return n;
        }
        #region GetDM
        //get DM
        private readonly IDMDAILYService _iDmdailyService;

        public CommonHelper()
        {
            _iDmdailyService = IoC.Resolve<IDMDAILYService>();
        }
        #endregion
        public static string ConvertDecimalToString(decimal number)
        {
            string s = number.ToString("#");
            string[] so = new string[] { "không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín" };
            string[] hang = new string[] { "", "nghìn", "triệu", "tỷ" };
            int i, j, donvi, chuc, tram;
            string str = " ";
            bool booAm = false;
            decimal decS = 0;
            //Tung addnew
            try
            {
                if (!string.IsNullOrEmpty(s))
                {
                    decS = Convert.ToDecimal(s);
                }
                
            }
            catch
            {
            }
            if (decS < 0)
            {
                decS = -decS;
                s = decS.ToString();
                booAm = true;
            }
            i = s.Length;
            if (i == 0)
                str = so[0] + str;
            else
            {
                j = 0;
                while (i > 0)
                {
                    donvi = Convert.ToInt32(s.Substring(i - 1, 1));
                    i--;
                    if (i > 0)
                        chuc = Convert.ToInt32(s.Substring(i - 1, 1));
                    else
                        chuc = -1;
                    i--;
                    if (i > 0)
                        tram = Convert.ToInt32(s.Substring(i - 1, 1));
                    else
                        tram = -1;
                    i--;
                    if ((donvi > 0) || (chuc > 0) || (tram > 0) || (j == 3))
                        str = hang[j] + str;
                    j++;
                    if (j > 3) j = 1;
                    if ((donvi == 1) && (chuc > 1))
                        str = "một " + str;
                    else
                    {
                        if ((donvi == 5) && (chuc > 0))
                            str = "lăm " + str;
                        else if (donvi > 0)
                            str = so[donvi] + " " + str;
                    }
                    if (chuc < 0)
                        break;
                    else
                    {
                        if ((chuc == 0) && (donvi > 0)) str = "lẻ " + str;
                        if (chuc == 1) str = "mười " + str;
                        if (chuc > 1) str = so[chuc] + " mươi " + str;
                    }
                    if (tram < 0) break;
                    else
                    {
                        if ((tram > 0) || (chuc > 0) || (donvi > 0)) str = so[tram] + " trăm " + str;
                    }
                    str = " " + str;
                }
            }
            if (booAm) str = "Âm " + str;

            str = str.Trim();
            if(s.Length>=3 && s.Substring(s.Length-1,1)=="0")
                return char.ToUpper(str[0]) + str.Substring(1) + " đồng chẵn";
            return char.ToUpper(str[0]) + str.Substring(1) + " đồng";
          

        }

        public static List<CommonValue> GetListNoCt(string loaiTk)
        {
            if (loaiTk == "0101")
            {
                return (List<CommonValue>)(System.Web.HttpContext.Current.Session["DM_TK1111"]);
            }
            if (loaiTk == "0201")
            {
                return (List<CommonValue>)(System.Web.HttpContext.Current.Session["DM_TK1121"]);
            }
            if (loaiTk == "0803")
            {
                return (List<CommonValue>)(System.Web.HttpContext.Current.Session["DM_TK33183"]);
            }
            if (loaiTk == "0907")
            {
                return (List<CommonValue>)(System.Web.HttpContext.Current.Session["DM_TK353"]);
            }
            return new List<CommonValue>();
        }
        //ket noi nhibernate
        public ISessionFactory factory;

        public ISession OpenSession()
        {
            if (factory == null)
            {
                Configuration conf = new Configuration();

                conf.Configure(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "/Config", "NHibernateConfig.xml"));

                conf.AddAssembly(Assembly.GetCallingAssembly());
                factory = conf.BuildSessionFactory();
            }

            return factory.OpenSession();
        }

        //xu ly ngay thang fnCTOS
        public string fnCTOS(string K, string date)                  //trả về thời gian cuối cùng
        {
            string result = null;
            int l = date.Length;
            if (!(new List<string>() { "Y", "Q", "M", "D" }).Contains(K))          //mặc định trả về theo độ dài của date
            {
                result = l == 4 ? date.Substring(0, 4) : l == 5 ? date.Substring(0, 4) + "4" : l == 6 ? date.Substring(0, 4) + "12" : "";
            }
            else if (K.Equals("Y"))
            {
                result = date.Substring(0, 4);
            }
            else if (K.Equals("Q"))
            {
                result = date.Substring(0, 4);
                result += l == 4 ? "4" : l == 5 ? date.Substring(4, 1) : getQbyM(date.Substring(4, 2));
            }
            else if (K.Equals("M"))
            {
                result = date.Substring(0, 4);
                result += l == 4 ? "12" : l == 5 ? getLastMbyQ(date.Substring(4, 1)) : date.Substring(4, 2);
            }
            else if (K.Equals("D"))
            {
                result = date.Substring(0, 4);
                result += l == 4 ? "" : l == 5 ? getLastDbyQ(date.Substring(4, 1))
                        : date.Substring(4, 2) + DateTime.DaysInMonth(int.Parse(result), int.Parse(date.Substring(4, 2)));
            }
            return result;
        }

        public string getQbyM(string M)
        {
            int m = int.Parse(M);
            return m <= 3 ? "1" : m <= 6 ? "2" : m <= 9 ? "3" : "4";
        }

        public string getLastMbyQ(string Q)
        {
            int q = int.Parse(Q);
            return q == 1 ? "03" : q == 2 ? "06" : q == 3 ? "09" : "12";
        }

        public string getLastMbyM(string M)
        {
            int m = int.Parse(M);
            return m >= 1 && m <= 3 ? "03" : m >= 4 && m <= 6 ? "06" : m >= 7 && m <= 9 ? "09" : "12";
        }

        public string getLastDbyQ(string Q)
        {
            int q = int.Parse(Q);
            return q == 1 ? "0331" : q == 2 ? "0630" : q == 3 ? "0930" : "1231";
        }
        public static decimal ParseDecimal(object s)
        {
            try
            {
                return Decimal.Parse(s.ToString());
            }
            catch
            {
                return 0;
            }
        }
        /// <summary>
        /// Lấy giá trị trong webconfig dựa vào tên truyền vào
        /// </summary>
        /// Creation by : HaiPL
        /// Created date : 18/05/2017
        /// <returns></returns>
        public static string GetConfigKey(string msgConstantKey)
        {
            string msg;
            try
            {
                msg = System.Configuration.ConfigurationManager.AppSettings[msgConstantKey];
            }
            catch
            {
                msg = string.Empty;
                //throw new Exception(msgConstantKey + " not found!!!");
            }
            return msg;
        }
    }
}