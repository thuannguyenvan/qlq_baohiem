using FX.Context;
using FX.Utils;
using log4net;
using log4net.Config;
using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Configuration;
using System.Security.Principal;

namespace QLQ.FEW
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(MvcApplication));
        protected void Application_Start()
        {
            //AreaRegistration.RegisterAllAreas();

            //WebApiConfig.Register(GlobalConfiguration.Configuration);
            //FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            //RouteConfig.RegisterRoutes(RouteTable.Routes);
            //AuthConfig.RegisterAuth();
            XmlConfigurator.ConfigureAndWatch(new FileInfo(Server.MapPath("~/") + "config/logging.config"));

            AreaRegistration.RegisterAllAreas();

            //WebApiConfig.Register(GlobalConfiguration.Configuration);
            RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            //AuthConfig.RegisterAuth();
            Bootstrapper.InitializeContainer();
            ModelBinders.Binders.DefaultBinder = new DevExpress.Web.Mvc.DevExpressEditorsBinder();

            DevExpress.Web.ASPxWebControl.CallbackError += Application_Error;
        }
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        //protected void Application_AuthenticateRequest(Object sender, EventArgs e)
        //{
        //    var urlRedirect = "https://local.baohiemxahoi.gov.vn:8443/cas/login?service=" + ConfigurationManager.AppSettings["QLQ"];
        //    Response.Redirect(urlRedirect, false);
        //}

        //private void GetCurrentSite(HttpContext context)
        //{
        //    IFXContext _FXcontext = FXContext.Current;
        //    try
        //    {
        //        string siteUrl = UrlUtil.GetSiteUrl();
        //        _FXcontext.PhysicalSiteDataDirectory = Context.Server.MapPath("SiteData");
        //        string sitepath = FXContext.Current.PhysicalSiteDataDirectory;
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("An unexpected error occured while setting the current site context.", ex);
        //    }
        //}

        //protected void Application_BeginRequest(object sender, EventArgs e)
        //{
        //    var httpCookie = Request.Cookies["Ticket"];
        //    string tkcheck = httpCookie?.Value;
        //    if (tkcheck== null)
        //    {
        //        var urlRedirect = "https://local.baohiemxahoi.gov.vn:8443/cas/login?service=" + ConfigurationManager.AppSettings["QLQ"];
        //        Response.Redirect(urlRedirect, false);
        //    }
        //}
        protected void Application_Error(object sender, EventArgs e) 
        {
            Exception exception = System.Web.HttpContext.Current.Server.GetLastError();

            //TODO: Handle Exception
        }
    }
}