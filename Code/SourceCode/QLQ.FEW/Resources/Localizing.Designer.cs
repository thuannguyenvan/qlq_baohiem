﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QLQ.FEW.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Localizing {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Localizing() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("QLQ.FEW.Resources.Localizing", typeof(Localizing).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Đã thực hiện chi rồi , bạn phải xóa chứng từ trước khi sửa!.
        /// </summary>
        internal static string CheckIdChungTu {
            get {
                return ResourceManager.GetString("CheckIdChungTu", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to dd/MM/yyyy.
        /// </summary>
        internal static string DateFormat {
            get {
                return ResourceManager.GetString("DateFormat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 4909.
        /// </summary>
        internal static string MA_DVQL {
            get {
                return ResourceManager.GetString("MA_DVQL", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 0803.
        /// </summary>
        internal static string Ma_Nhom_Cac_Khoan_Phai_Tra {
            get {
                return ResourceManager.GetString("Ma_Nhom_Cac_Khoan_Phai_Tra", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 0907.
        /// </summary>
        internal static string Ma_Nhom_Cac_Khoan_Thanh_Toan {
            get {
                return ResourceManager.GetString("Ma_Nhom_Cac_Khoan_Thanh_Toan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 0101.
        /// </summary>
        internal static string Ma_Nhom_TienMat {
            get {
                return ResourceManager.GetString("Ma_Nhom_TienMat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 0201.
        /// </summary>
        internal static string Ma_Nhom_Uy_Nhiem_Thu {
            get {
                return ResourceManager.GetString("Ma_Nhom_Uy_Nhiem_Thu", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Người Thực Trả phải nhỏ hơn hoặc bằng Người Phải Trả !.
        /// </summary>
        internal static string Message_C7374_1 {
            get {
                return ResourceManager.GetString("Message_C7374_1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tiền Thực Trả phải nhỏ hơn hoặc bằng Tiền Phải Trả !.
        /// </summary>
        internal static string Message_C7374_2 {
            get {
                return ResourceManager.GetString("Message_C7374_2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tiền Chưa Trả phải nhỏ hơn Tiền Phải Trả !.
        /// </summary>
        internal static string Message_C7374_3 {
            get {
                return ResourceManager.GetString("Message_C7374_3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Người Chưa Trả phải nhỏ hơn Người Phải Trả !.
        /// </summary>
        internal static string Message_C7374_4 {
            get {
                return ResourceManager.GetString("Message_C7374_4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tổng số tiền chi tiết không vượt mức số tiền định khoản!.
        /// </summary>
        internal static string MessageChiTiet {
            get {
                return ResourceManager.GetString("MessageChiTiet", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bạn chưa định khoản cho chứng từ!.
        /// </summary>
        internal static string MessageChungTu {
            get {
                return ResourceManager.GetString("MessageChungTu", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bạn vui lòng kiểm tra lại dữ liệu!.
        /// </summary>
        internal static string MessageCommon {
            get {
                return ResourceManager.GetString("MessageCommon", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bạn vui lòng nhập ngày bắt đầu nhỏ hơn ngày kết thúc!.
        /// </summary>
        internal static string MessageDate {
            get {
                return ResourceManager.GetString("MessageDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bạn vui lòng nhập ngày kết thúc!.
        /// </summary>
        internal static string MessageDateEnd {
            get {
                return ResourceManager.GetString("MessageDateEnd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bạn vui lòng nhập ngày kết thúc lớn hơn ngày hiện tại.
        /// </summary>
        internal static string MessageDateLargeCurrent {
            get {
                return ResourceManager.GetString("MessageDateLargeCurrent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bạn vui lòng nhập ngày bắt đầu nhỏ hơn ngày hiện tại.
        /// </summary>
        internal static string MessageDateSmallCurrent {
            get {
                return ResourceManager.GetString("MessageDateSmallCurrent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bạn vui lòng nhập ngày bắt đầu!.
        /// </summary>
        internal static string MessageDateStart {
            get {
                return ResourceManager.GetString("MessageDateStart", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bạn vui lòng nhập ngày bắt đầu và ngày kết thúc!.
        /// </summary>
        internal static string MessageDatetime {
            get {
                return ResourceManager.GetString("MessageDatetime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bạn đã xóa bản ghi thành công!.
        /// </summary>
        internal static string MessageDelete {
            get {
                return ResourceManager.GetString("MessageDelete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Xóa bản ghi không thành công!.
        /// </summary>
        internal static string MessageDeleteError {
            get {
                return ResourceManager.GetString("MessageDeleteError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bản ghi đang được sử dụng!.
        /// </summary>
        internal static string MessageDeleteExist {
            get {
                return ResourceManager.GetString("MessageDeleteExist", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thay đổi thất bại.
        /// </summary>
        internal static string MessageDeleteFalse {
            get {
                return ResourceManager.GetString("MessageDeleteFalse", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tổng số tiền định khoản không vượt mức số tiền chứng từ!.
        /// </summary>
        internal static string MessageDinhKhoan {
            get {
                return ResourceManager.GetString("MessageDinhKhoan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mã đã tồn tại. Vui lòng thử lại!.
        /// </summary>
        internal static string MessageDuplicatekey {
            get {
                return ResourceManager.GetString("MessageDuplicatekey", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mã đã tồn tại vui lòng nhập lại mã khác!.
        /// </summary>
        internal static string MessageID {
            get {
                return ResourceManager.GetString("MessageID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bạn Insert không thành công,vui lòng kiểm tra lại!.
        /// </summary>
        internal static string MessageInsert {
            get {
                return ResourceManager.GetString("MessageInsert", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bắt buộc nhập 1 trong 3 trường với số tiền khác 0!.
        /// </summary>
        internal static string MessageInsertSodu {
            get {
                return ResourceManager.GetString("MessageInsertSodu", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Chi tiết đã được nhập không được nhập lại!.
        /// </summary>
        internal static string MessageInsertSodu2 {
            get {
                return ResourceManager.GetString("MessageInsertSodu2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bạn chưa nhập loại tài khoản!.
        /// </summary>
        internal static string MessageLoaiTK {
            get {
                return ResourceManager.GetString("MessageLoaiTK", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Số sổ đã tồn tại, bạn vui lòng nhập lại!.
        /// </summary>
        internal static string MessageSoSo {
            get {
                return ResourceManager.GetString("MessageSoSo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bạn đã thực hiện thành công.
        /// </summary>
        internal static string MessageSuccess {
            get {
                return ResourceManager.GetString("MessageSuccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bạn update không thành công,vui lòng kiểm tra lại!.
        /// </summary>
        internal static string MessageUpdate {
            get {
                return ResourceManager.GetString("MessageUpdate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 572.
        /// </summary>
        internal static string Tiengui_TKC {
            get {
                return ResourceManager.GetString("Tiengui_TKC", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1111.
        /// </summary>
        internal static string TK_1111 {
            get {
                return ResourceManager.GetString("TK_1111", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1121.
        /// </summary>
        internal static string TK_1121 {
            get {
                return ResourceManager.GetString("TK_1121", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 33183.
        /// </summary>
        internal static string TK_33183 {
            get {
                return ResourceManager.GetString("TK_33183", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 353.
        /// </summary>
        internal static string TK_353 {
            get {
                return ResourceManager.GetString("TK_353", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 579.
        /// </summary>
        internal static string TK_579 {
            get {
                return ResourceManager.GetString("TK_579", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1.
        /// </summary>
        internal static string TypeSMSConnect {
            get {
                return ResourceManager.GetString("TypeSMSConnect", resourceCulture);
            }
        }
    }
}
