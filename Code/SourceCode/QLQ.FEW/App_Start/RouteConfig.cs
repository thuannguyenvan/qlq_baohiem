using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace QLQ.FEW {
    public class RouteConfig {
        public static void RegisterRoutes(RouteCollection routes) {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{resource}.ashx/{*pathInfo}");

            routes.MapRoute(
                name: "Default", // Route name
                url: "{controller}/{action}/{id}", // URL with parameters
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

            //routes.MapRoute(
            //    name: null, // Route name
            //    url: "{controller}/{action}/{CHUNGTU_MODEL}/{DSODTS06}", // URL with parameters
            //    defaults: new { controller = "EditChungTuShare", action = "GetFromModel", CHUNGTU_MODEL = UrlParameter.Optional, DSODTS06 = UrlParameter.Optional } // Parameter defaults
            //);

            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            var len = fvi.FileVersion.Length;
            _version = fvi.FileVersion.Substring(0, len - 4);
        }
        public static string Version
        {
            get { return _version; }
        }
        private static string _version;
    }
}