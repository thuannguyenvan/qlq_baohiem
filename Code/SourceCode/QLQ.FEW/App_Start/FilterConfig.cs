using System.Web;
using System.Web.Mvc;

namespace QLQ.FEW {
    public class FilterConfig {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters) {
            filters.Add(new HandleErrorAttribute());
            if (!HttpContext.Current.IsDebuggingEnabled)
            {
                filters.Add(new System.Web.Mvc.AuthorizeAttribute());
                filters.Add(new RequireHttpsAttribute());
            }
        }
    }
}