﻿using FX.Core;
using log4net;
using QLQ.Core.CustomView;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using QLQ.Core.Security;
using QLQ.FEW.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;


namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.LAISUATHDVBINHQUAN)]
    [Authorize]
    public class LaiSuatHDVBinhQuanController : BaseController
    {
        private readonly IDM_NGANHANGService _iDmNganHangService;
        private readonly ILAISUATNH_DTLService _iLaiSuatNhDtlService;
        private readonly ILAISUATNH_HDRService _iLaiSuatNhHdrService;
        private readonly ILAISUATTBService _iLaisuattbService;
        private static readonly ILog Log = LogManager.GetLogger(typeof(LaiSuatHuyDongController));
        public static List<LaiSuatHuyDongNHModel> LstLaisuathdv;
        public static List<LaiSuatBinhQuanModel> LstBinhquan;

        //
        // GET: /LaiSuatHDVBinhQuan/
        public LaiSuatHDVBinhQuanController()
        {
            _iDmNganHangService = IoC.Resolve<IDM_NGANHANGService>();
            _iLaiSuatNhDtlService = IoC.Resolve<ILAISUATNH_DTLService>();
            _iLaiSuatNhHdrService = IoC.Resolve<ILAISUATNH_HDRService>();
            _iLaisuattbService = IoC.Resolve<ILAISUATTBService>();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LaiSuatHDVBinhQuanPartial()
        {
            LstLaisuathdv = new List<LaiSuatHuyDongNHModel>();
            DateTime date = DateTime.Now;
            return Search(date, 0, 0);
        }

        [HttpPost]
        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult fn_InAn(DateTime date, int cnTc, int type)
        {
            var model = LayTuLaiSuatBinhQuan(_iLaisuattbService.LayLaiSuatBinhQuan(date), cnTc, type);
            LaiSuatHDVBinhQuan_Report report = new LaiSuatHDVBinhQuan_Report();
            report.DataSource = model;
            report.txt_name.Text = "LÃI SUẤT BÌNH QUÂN CÁC NGÂN HÀNG ĐẾN " + date.ToShortDateString();
            return PartialView("ReportLaiSuatHDVBinhQuan", report);
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Search(DateTime date, int cnTc, int type)
        {
            var model = LayTuLaiSuatBinhQuan(_iLaisuattbService.LayLaiSuatBinhQuan(date), cnTc, type);
            return PartialView("LaiSuatHDVBinhQuanPartial", model);
        }

        [ActionPermissions(QlqActions.VIEW)]
        private List<QLQ_LAISUATTB> LayTuLaiSuatBinhQuan(IEnumerable<LaiSuatHuyDongNHModel> lsBinhQuan, int cnTc, int type)
        {
            List<QLQ_LAISUATTB> result = new List<QLQ_LAISUATTB>();
            if (lsBinhQuan != null) {
                if (cnTc == 0 && type == 0)
                {
                    lsBinhQuan = from ls in lsBinhQuan
                                 select new LaiSuatHuyDongNHModel()
                                 {
                                     NGAY_HIEULUC = ls.NGAY_HIEULUC,
                                     KYHAN_ID = ls.KYHAN_ID,
                                     Value = ls.CANHAN_CUOIKY
                                 };
                }
                else if (cnTc == 0 && type == 1)
                {
                    lsBinhQuan = from ls in lsBinhQuan
                                 select new LaiSuatHuyDongNHModel()
                                 {
                                     NGAY_HIEULUC = ls.NGAY_HIEULUC,
                                     KYHAN_ID = ls.KYHAN_ID,
                                     Value = ls.CANHAN_HANGTHANG
                                 };
                }
                else if (cnTc == 1 && type == 0)
                {
                    lsBinhQuan = from ls in lsBinhQuan
                                 select new LaiSuatHuyDongNHModel()
                                 {
                                     NGAY_HIEULUC = ls.NGAY_HIEULUC,
                                     KYHAN_ID = ls.KYHAN_ID,
                                     Value = ls.TOCHUC_CUOIKY
                                 };
                }
                else
                {
                    lsBinhQuan = from ls in lsBinhQuan
                                 select new LaiSuatHuyDongNHModel()
                                 {
                                     NGAY_HIEULUC = ls.NGAY_HIEULUC,
                                     KYHAN_ID = ls.KYHAN_ID,
                                     Value = ls.TOCHUC_HANGTHANG
                                 };
                }
                lsBinhQuan = lsBinhQuan.OrderBy(t => t.NGAY_HIEULUC).ToList();
                DateTime ngayHieuLuc;
                var lsSuatBQ = lsBinhQuan.ToList();
                LaiSuatNHModel lsHuyDong = new LaiSuatNHModel();
                QLQ_LAISUATTB model = new QLQ_LAISUATTB();
                if (lsSuatBQ.Count > 0)
                {
                    ngayHieuLuc = lsSuatBQ[0].NGAY_HIEULUC;
                    foreach (var item in lsBinhQuan)
                    {
                        if (ngayHieuLuc == item.NGAY_HIEULUC)
                        {
                            //gán các trường vào model
                            setLaiSuatTB(ref model, item, lsHuyDong);
                        }
                        if (ngayHieuLuc != item.NGAY_HIEULUC)
                        {
                            // add the old to list
                            result.Add(model);
                            ngayHieuLuc = item.NGAY_HIEULUC;
                            // khoi tao cai moi
                            model = new QLQ_LAISUATTB();
                            setLaiSuatTB(ref model, item, lsHuyDong);
                        }
                    }
                    result.Add(model);
                }
            }
            return result;
        }

        [ActionPermissions(QlqActions.VIEW)]
        private void setLaiSuatTB(ref QLQ_LAISUATTB model, LaiSuatHuyDongNHModel item, LaiSuatNHModel lsHuyDong)
        {
            model.NGAY_THAYDOI = item.NGAY_HIEULUC;
            long? so_thang = lsHuyDong.getSoThang(item.KYHAN_ID);
            if (so_thang == null)
                so_thang = 0;
            if (so_thang == -1) model.KKH = item.Value;
            else if (so_thang == 0) model.UNDE1MONTH = item.Value;
            else if (so_thang == 1) model.MONTH1 = item.Value;
            else if (so_thang == 2) model.MONTH2 = item.Value;
            else if (so_thang == 3) model.MONTH3 = item.Value;
            else if (so_thang == 4) model.MONTH4 = item.Value;
            else if (so_thang == 5) model.MONTH5 = item.Value;
            else if (so_thang == 6) model.MONTH6 = item.Value;
            else if (so_thang == 7) model.MONTH7 = item.Value;
            else if (so_thang == 8) model.MONTH8 = item.Value;
            else if (so_thang == 9) model.MONTH9 = item.Value;
            else if (so_thang == 10) model.MONTH10 = item.Value;
            else if (so_thang == 11) model.MONTH11 = item.Value;
            else if (so_thang == 12) model.MONTH12 = item.Value;
            else if (so_thang == 18) model.MONTH18 = item.Value;
            else if (so_thang == 24) model.MONTH24 = item.Value;
            else if (so_thang == 36) model.MONTH36 = item.Value;
            else if (so_thang == 60) model.MONTH60 = item.Value;
        }
    }
}
