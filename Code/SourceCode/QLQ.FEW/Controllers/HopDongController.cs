﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using FX.Core;
using log4net;
using QLQ.Core.Helper;
using QLQ.Core.Security;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.HOPDONG)]
    [Authorize]
    public class HopDongController : BaseController
    {
        //
        // GET: /HopDong/

        private readonly IHopDongService _iHopDongService;
        private readonly IHS_KHACHHANGService _iKhachhangService;
        private readonly IDM_HINHTHUCDTUService _iDmHinhthucdtuService;
        private readonly IDM_DAUTUService _iDmDautuService;
        private readonly IHS_KHACHHANGService _iHsKhachhangService;
        private readonly IDM_CHUCDANHService _iDM_CHUCDANHService;
        private readonly IDMCQBHXHService _iDMCQBHXHService;
        private static readonly ILog Log = LogManager.GetLogger(typeof(HopDongController));
        private readonly IDM_HOPDONGService _iDmHopdongService;
        private List<QLQ_DMDAUTU> _listDmDauTu = new List<QLQ_DMDAUTU>();
        private List<QLQ_DMHINHTHUCDTU> _listDmHinhThuCdt = new List<QLQ_DMHINHTHUCDTU>();
        private List<QLQ_DMHOPDONG> _listDmHopDong = new List<QLQ_DMHOPDONG>();
        private List<HS_KHACHHANG> _listHsKhachhang = new List<HS_KHACHHANG>();
        private List<QLQ_DMCQBHXH> _listDMCQBHXH = new List<QLQ_DMCQBHXH>();
        //
        // GET: /DMNGANHANG/
        public HopDongController()
        {
            _iHopDongService = IoC.Resolve<IHopDongService>();
            _iDmDautuService = IoC.Resolve<IDM_DAUTUService>();
            _iDM_CHUCDANHService = IoC.Resolve<IDM_CHUCDANHService>();
            _iDmHopdongService = IoC.Resolve<IDM_HOPDONGService>();
            _iHsKhachhangService = IoC.Resolve<IHS_KHACHHANGService>();
            _iDmHinhthucdtuService = IoC.Resolve<IDM_HINHTHUCDTUService>();
            _iKhachhangService = IoC.Resolve<IHS_KHACHHANGService>();
            _iDMCQBHXHService = IoC.Resolve<IDMCQBHXHService>();
            GetCBFilter();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            return View();
        }

        public void GetCBFilter()
        {
            string chon = "Chọn";
            _listDmHinhThuCdt = _iDmHinhthucdtuService.GetAll().ToList();
            _listDmHinhThuCdt.Insert(0, new QLQ_DMHINHTHUCDTU { hinhthuc_id = 0, ten_hinhthuc = chon });
            ViewData["DSHinhThucDTU"] = _listDmHinhThuCdt;
            _listDmDauTu = _iDmDautuService.GetAll().ToList();
            _listDmDauTu.Insert(0, new QLQ_DMDAUTU { dautu_id = 0, ten_dautu = chon });
            ViewData["ListDauTu"] = _listDmDauTu;
            _listDmHopDong = _iDmHopdongService.GetAll().ToList();
            _listDmHopDong.Insert(0, new QLQ_DMHOPDONG { hopdong_id = 0, ten_hopdong = chon });
            ViewData["ListHopDong"] = _listDmHopDong;
            _listHsKhachhang = _iHsKhachhangService.GetAll();
            ViewData["ListHSKH"] = _iHsKhachhangService.GetAll();
            // Session["ListDSKH"] = _iHS_KHACHHANGService.GetAll();
            ViewData["DSHinhThucDTU1"] = _iDmHinhthucdtuService.GetAll();
            ViewData["ListDauTu1"] = _iDmDautuService.GetAll();
            ViewData["ListHopDong1"] = _iDmHopdongService.GetAll();
            ViewData["ListCQBHXH"] = _iDMCQBHXHService.GetAll();
        }

        public ActionResult HopDongPartial()
        {
            List<QLQ_HOPDONG> model = new List<QLQ_HOPDONG>();
            model = _iHopDongService.Query.Where(s => s.IsDeleted == 0).ToList();
            return PartialView("HopDongPartial", model);
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Search(QLQ_HOPDONG timkiem)
        {
            var list = _iHopDongService.Search(timkiem);
            if(list.Count==0)
                return Json("", JsonRequestBehavior.AllowGet);
            return PartialView("HopDongPartial", list);
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult TimKiemHopDong(long hinhthuc=0, long dautu=0, long hopdong=0)
        {
            if (hinhthuc == 0 && dautu == 0 && hopdong == 0)
                return Json("TSNull",JsonRequestBehavior.AllowGet);
            QLQ_HOPDONG hopdong1 = new QLQ_HOPDONG();
            hopdong1.hopdong_id = hopdong;
            hopdong1.hinhthuc_id = hinhthuc;
            hopdong1.DAUTU_ID = dautu;
            return Search(hopdong1);
        }


        public ActionResult NullPartial()
        {
            List<QLQ_HOPDONG> model = new List<QLQ_HOPDONG>();
            return PartialView("HopDongPartial", model);
        }


        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult AddNewPartial(QLQ_HOPDONG obj)
        {
            if (ModelState.IsValid)
            {
                if (_iHopDongService.getHopDongBySo_HD(obj.so_hopdong) == null)//kiểm tra tồn tại MA_NGANHANG
                {
                    try
                    {
                        if(obj.ngay_tralai!=null)
                        {
                            DateTime newDt = new DateTime(DateTime.Now.Year, obj.ngay_tralai.Value.Month, obj.ngay_tralai.Value.Day);
                            obj.ngay_tralai = newDt;
                        }
                        if (obj.NGAY_LAP.Value.Date <= obj.ngay_hieuluc.Value.Date && obj.ngay_hieuluc.Value.Date < obj.han_hopdong.Value.Date)
                        {
                            obj.creation_date = DateTime.Now;  //ngày tạo
                            //Do chương trình chưa tích hợp đăng nhập nên để gán mặc định là 22
                            obj.created_by = 22;
                            _iHopDongService.CreateNew(obj);
                            _iHopDongService.CommitChanges();
                            ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                        }
                        else
                        {
                            //Cần chỉnh lại thông báo lỗi
                            ViewData["EditError"] = Resources.Localizing.MessageDate;
                        }
                    }
                    catch (Exception e)
                    {
                        ViewData["EditError"] = e.Message;
                    }
                }
                else
                    ViewData["EditError"] = Resources.Localizing.MessageDuplicatekey;//in ra thông báo lỗi trùng mã
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return HopDongPartial();
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult UpdatePartial(QLQ_HOPDONG obj)
        {
            //cập nhật bản ghi
            if (ModelState.IsValid)
            {

                try
                {
                    if (obj.ngay_tralai != null)
                    {
                        DateTime newDt = new DateTime(DateTime.Now.Year, obj.ngay_tralai.Value.Month, obj.ngay_tralai.Value.Day);
                        obj.ngay_tralai = newDt;
                    }
                    if (obj.lai_hinhthuc != "2 lần/năm")
                    {
                        obj.NGAY_TRALAI_L2 = null;
                    }
                    if (obj.NGAY_LAP.Value.Date <= obj.ngay_hieuluc.Value.Date && obj.ngay_hieuluc.Value.Date < obj.han_hopdong.Value.Date)
                    {
                        QLQ_HOPDONG temp = _iHopDongService.getHopDongBySo_HD(obj.so_hopdong);
                        obj.creation_date = temp.creation_date;
                        obj.created_by = temp.created_by;
                        //TODO Do chương trình chưa tích hợp đăng nhập nên để gán mặc định là 22
                        obj.lastupdate_by = 22;   //người sửa
                        obj.lastupdated_date = DateTime.Now;
                        _iHopDongService.Clear();
                        _iHopDongService.Save(obj);
                        _iHopDongService.CommitChanges();
                        ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                    }
                    else
                    {
                        ViewData["EditError"] = Resources.Localizing.MessageDate;
                    }
                    //log.Error("Danh mục đơn vị quản lý - Sửa: " + nguoidung.TENDANGNHAP);
                }
                catch (Exception e)
                {
                    //ViewData["EditError"] = Resources.Localizing.MessageInsert;
                    ViewData["EditError"] = e.Message;
                }
            }
            else
            {
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            }
            return HopDongPartial();
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.DELETE)]
        public ActionResult DeletePartial(QLQ_HOPDONG obj)
        {
            try
            {
                _iHopDongService.SoftDelete(obj.qlhopdong_id.Value);
                _iHopDongService.CommitChanges();
                ViewData["EditSuccess"] = Resources.Localizing.MessageDelete;
            }
            catch (Exception)
            {
                ViewData["EditError"] = Resources.Localizing.MessageDate;
            }
            return HopDongPartial();
        }

        public bool CheckNgayThangHD(DateTime? ngaylap, DateTime? ngayhieuluc, DateTime? hanhopdong)
        {
            if (ngaylap.Value.Date <= ngayhieuluc.Value.Date && ngayhieuluc.Value.Date < hanhopdong.Value.Date)
                return true;
            return false;
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult ShowReport(long khang_id, long hdong_id, Decimal SoTien, float LaiSuat, string So_HD, DateTime Han_HD)
        {
            var list_qlhd = _iHopDongService.GetAll();
            var listhopdong = _iDmHopdongService.GetAll();
            var khach_hang = _iKhachhangService.Query.FirstOrDefault(t => t.KHACHHANG_ID == khang_id);
            var hopdong = listhopdong.Find(t => t.hopdong_id == hdong_id);
            var Chuc_danh = _iDM_CHUCDANHService.Query.FirstOrDefault(t => t.ma_chucdanh == "PTGĐ");
            var GT = khach_hang.GIOITINH == 1 ? "Ông" : "Bà";
            string SoTienChu = CommonHelper.ConvertDecimalToString(Convert.ToDecimal(SoTien));
            HopDong_Report report = new HopDong_Report();
            report.lbTenBenGuiTien.Text = report.lbTenBenGuiTien.Text + "Bảo hiểm xã hội Việt Nam (Sau đây gọi là Bên A)";
            //report.lbDenngay.Text = "       2. Thời hạn gửi: Thời hạn gửi tiền tính từ ngày Bên B nhận được tiền gửi theo  ngày, tháng, năm ghi trên chứng từ chuyển tiền của ngân hàng phục vụ) đến ngày " + Han_HD.ToShortDateString() + " (là ngày Bên A nhận được gốc tiền gửi theo chứng từ nhận tiền của bên A).";
            report.lbSoHD.Text = "Số: " + So_HD;
            report.lbNgayTaoHD.Text = "       Hôm nay, ngày ….. tháng ….. năm " + DateTime.Now.Year + " tại bảo hiểm xã hội Việt Nam, chúng tôi gồm:";
            report.lbTenKH.Text = report.lbTenKH.Text + khach_hang.TEN_KHACHHANG + " (Sau đây gọi là Bên B)";
            report.lbDiaChiKH.Text = report.lbDiaChiKH.Text + " " + khach_hang.DIA_CHI;
            report.lbDienThoaiKH.Text = "0"+khach_hang.DIEN_THOAI.ToString();
            report.lbFaxKH.Text = khach_hang.FAX;
            report.lbTaiKhoanKH.Text = report.lbTaiKhoanKH.Text + " " + khach_hang.TAIKHOAN;
            report.lbTenDaiDienKH.Text = GT + " " + khach_hang.DAIDIEN;
            report.lbChucVuDaiDienKH.Text = khach_hang.CHUCVU;
            report.lbTienGoc.Text = SoTien.ToString("#,#") + " " + "đồng.";
            report.lbLaiSuat.Text = LaiSuat + "%/năm";
            report.lbKyTenB.Text = khach_hang.DAIDIEN;
            report.lbSoTienBangChu.Text = report.lbSoTienBangChu.Text + SoTienChu + ").";
            report.lbUyQuyenGui.Text = "\n" + "       Văn bản uỷ quyền: Theo Quyết định số 9916/QĐ-BIDV ngày 01/12/2016 của Ngân hàng TMCP Đầu tư và Phát triển Việt Nam";
            report.lbUyQuyenNhan.Text = "\n" + "       Văn bản uỷ quyền: Theo Quyết định số 9916/QĐ-BIDV ngày 01/12/2016 của Ngân hàng TMCP Đầu tư và Phát triển Việt Nam";
            report.lbChucVuBenB.Text = khach_hang.CHUCVU == null ? "" : khach_hang.CHUCVU.ToUpper();
            report.lbDaiDienBH.Text = Chuc_danh.nguoidaidien;
            report.lbChucVuDD.Text = Chuc_danh.ten_chucdanh;
            report.lbChucVuBenA.Text = Chuc_danh.ten_chucdanh == null ? "" : Chuc_danh.ten_chucdanh.ToUpper();
            report.lbKyTenBenA.Text = Chuc_danh.nguoidaidien;
            report.lbHanHD.Text = Han_HD.Date.ToString("dd/MM/yyyy");
            return PartialView("ReportHopDong", report);
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult HopDonghChangedValueUpdateLayout(long hopdongId)
        {
            string sohd = "";
            List<int> dsSoHD = new List<int>();
            var maxsohd = _iHopDongService.Query.Max(t => t.qlhopdong_id) + 1;
            var hopdong = _iDmHopdongService.GetById(hopdongId);
            var DSSoHD = _iHopDongService.Query.Where(t => t.so_hopdong.EndsWith(DateTime.Now.Year + "/" + hopdong.ky_hieu));
            var dem = DSSoHD.ToList().Count();
            if (dem == 0)
            {
                sohd = "1" + "/" + DateTime.Now.Year + "/" + hopdong.ky_hieu;
            }
            else
            {
                foreach (var item in DSSoHD)
                {
                    string[] listsohd = new string[3];
                    listsohd = item.so_hopdong.Split('/');
                    int sodem = int.Parse(listsohd[0]);
                    dsSoHD.Add(sodem);
                }
                sohd = (dsSoHD.Max() + 1).ToString();
                sohd = sohd + "/" + DateTime.Now.Year + "/" + hopdong.ky_hieu;
            }
            //if(TrangThai!="")
            //{
            //    string[] mang=TrangThai.Split('/');
            //    sohd = mang[0] + "/" + mang[1] + "/" + hopdong.ky_hieu;
            //}            
            return Json(sohd, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DMDauTuPartial()
        {
            long hinhthuc_id = (Request.Params["hinhthuc_id"] != null) ? long.Parse((Request.Params["hinhthuc_id"])) : 0;
            var listdautu = _iDmDautuService.GetAll().Where(t => t.hinhthuc_id == Convert.ToInt64(hinhthuc_id)).ToList();
            return PartialView("DMDauTuPartial", listdautu);
        }

        public ActionResult KhachHangPartial()
        {
            long dautu_id = (Request.Params["dautu_ID"] != null) ? long.Parse((Request.Params["dautu_ID"])) : 0;
            var listkh = _iHsKhachhangService.GetAll().Where(t => t.DAUTU_ID == Convert.ToInt64(dautu_id)).ToList();
            return PartialView("KhachHangPartial", listkh);
        }

        public ActionResult ReturnDauTuPartial()
        {
            long hinthucid = Request.Params["cbDauTu_hinthucid"] == null ? 0 : long.Parse(Request.Params["cbDauTu_hinthucid"]);
            var list = _iDmDautuService.Query.Where(t => t.hinhthuc_id == hinthucid);
            return PartialView("ReturnDauTuPartial", list);
        }
    }
}
