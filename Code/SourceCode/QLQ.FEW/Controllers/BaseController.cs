﻿using FX.Core;
using QLQ.Core.Common;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using QLQ.Core.Security;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.Mvc;
using System.Xml;
using System.Linq;
using QLQ.Core.StoreProcedure;
using QLQ.Core.CustomView;

namespace QLQ.FEW.Controllers
{
    public class BaseController : Controller
    {
        protected readonly IACCOUNTService _iAccountService;
        protected readonly IHopDongService _iHopDongService;

        protected User CurrentUser
        {
            get
            {
                return (User)Session["User"];
            }
            set
            {
                Session["User"] = value;
            }
        }



        public object EditSuccessMessage
        {
            get { return ViewData["EditSuccess"]; }
            set { ViewData["EditSuccess"] = value; }
        }

        public object EditErrorMessage
        {
            get { return ViewData["EditError"]; }
            set { ViewData["EditError"] = value; }
        }


        public BaseController()
        {
            _iAccountService = IoC.Resolve<IACCOUNTService>();
            _iHopDongService = IoC.Resolve<IHopDongService>();

            GetListHopDongDenHanNotification();
        }

        public void GetListHopDongDenHanNotification()
        {
            if (ViewData["HDQuaHanNotification"] == null)
            {
                List<HopDongDenHanModel> lstHDDenHanNotification = null;
                lstHDDenHanNotification = _iHopDongService.GetHopDongDenHanByDate(DateTime.Now);
                ViewData["HDDenHanNotification"] = lstHDDenHanNotification;
            }
            
        }
        //public List<T> GetDataService<T>(string thamso, string data)
        //{
        //    XmlDocument doc = new XmlDocument();
        //    doc.LoadXml(data);

        //    XmlNodeList nodeList = doc.GetElementsByTagName(thamso);

        //    List<T> records = new List<T>();

        //    for (int i = 0; i < nodeList.Count; i++) // Từng bản ghi
        //    {
        //        T entity = Activator.CreateInstance<T>();
        //        XmlNode xn = nodeList[i];
        //        // Duyệt node con
        //        for (var j = 0; j < xn.ChildNodes.Count; j++) // Từng thuộc tính
        //        {
        //            var attr = xn.ChildNodes[j].Name;
        //            attr = attr.Substring(4, attr.Length - 4);
        //            var value = xn.ChildNodes[j].InnerText;
        //            SetValueProperty(entity, attr, value);
        //        }
        //        records.Add(entity);
        //    }
        //    return records;
        //}
        //private void SetValueProperty(object entity, string colName, object val)
        //{
        //    Type instanceType = entity.GetType();
        //    //Get Property
        //    PropertyInfo pi = instanceType.GetProperty(colName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
        //    if (pi != null)
        //    {
        //        string type = pi.PropertyType.FullName;
        //        // Các kiểu dữ liệu
        //        switch (type)
        //        {
        //            case "System.Int32": val = Convert.ToInt32(val); break;
        //            case "System.Int64": val = Convert.ToInt64(val); break;
        //            case "System.Decimal": val = Convert.ToDecimal(val); break;
        //            case "System.DateTime": val = Convert.ToDateTime(val); break;
        //            case "System.Double": val = Convert.ToDouble(val); break;
        //            case "System.Boolean": val = Convert.ToBoolean(val); break;
        //            default: val = val.ToString(); break;
        //        }
        //        pi.SetValue(entity, val, null);
        //    }
        //}

        #region Authorization
        protected override void OnAuthorization(AuthorizationContext filterContext)
        {
            var currentUser = CheckUser();

            if (currentUser != null && !currentUser.IsAdmin)
            {
                string function = ((FunctionAttribute)filterContext.Controller.GetType().GetCustomAttributes(typeof(FunctionAttribute), false).FirstOrDefault())?.FunctionCode;
                if (string.IsNullOrEmpty(function))
                {
                    // currently not check
                    //throw new Exception(string.Format("Controller {0} does not declare Function attribute", filterContext.Controller.GetType().Name));
                }
                else
                {
                    var actions = (ActionPermissionsAttribute)filterContext.ActionDescriptor.GetCustomAttributes(typeof(ActionPermissionsAttribute), true).FirstOrDefault();
                    if (actions == null)
                    {
                        // currently not check
                        //throw new Exception(string.Format("Not declare Action attribute. ControllerName: {0}, ActionName: {1}",
                        //    filterContext.Controller.GetType().Name,
                        //    filterContext.ActionDescriptor.ActionName));
                    }
                    else
                    {
                        //if (!currentUser.Permissons.ContainsKey(function) || currentUser.Permissons[function] == null || actions.Actions.Any(a => !currentUser.Permissons[function].Contains(a)))
                        //    throw new UnauthorizedAccessException(string.Format("User {0} does not have enough permissions to access to Controller '{0}', Action '{1}'",
                        //        filterContext.Controller.GetType().Name,
                        //        filterContext.ActionDescriptor.ActionName));
                    }
                }
            }

            base.OnAuthorization(filterContext);

        }

        private User CheckUser()
        {
            var currentUser = CurrentUser;
            if (currentUser == null)
            {
                string username = HttpContext.User.Identity.Name;
                if (!string.IsNullOrEmpty(username))
                {
                    var qlqUser = _iAccountService.GetByUser(username);
                    if (qlqUser == null)
                    {
                        _iAccountService.CreateNew(new QLQ_ACCOUNT() { USERNAME = username, EMAIL = username, ACTIVE = 1 });
                        qlqUser = _iAccountService.GetByUser(username);
                    }

                    currentUser = new User()
                    {
                        AccountId = qlqUser.ACCOUNT_ID.Value,
                        Email = qlqUser.EMAIL,
                        FullName = qlqUser.FULL_NAME,
                        IsAdmin = qlqUser.SYS_LDAP_ADMIN == 1,
                        MaCoquan = qlqUser.MA_CQ_BHXH,
                        UserName = qlqUser.USERNAME,
                        TenCoquan = qlqUser.TEN_CQ_BHXH
                    };

                    var sp = new QLQStoreProcedure();
                    var permissions = sp.SP_GetAccountPermissionByAccountId(qlqUser.ACCOUNT_ID.Value);
                    if (permissions != null)
                        permissions.ForEach(p =>
                        {
                            if (!currentUser.Permissons.ContainsKey(p.chucnang) || currentUser.Permissons[p.chucnang] == null)
                                currentUser.Permissons[p.chucnang] = new List<string>();
                            if (!currentUser.Permissons[p.chucnang].Contains(p.quyen))
                                currentUser.Permissons[p.chucnang].Add(p.quyen);
                        });

                    CurrentUser = currentUser;
                }
            }

            return currentUser;
        }
        #endregion
    }
}
