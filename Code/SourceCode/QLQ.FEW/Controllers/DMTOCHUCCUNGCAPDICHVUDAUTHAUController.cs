﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using FX.Core;
using log4net;
using QLQ.Core.Common;
using QLQ.Core.Security;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.DM_TOCHUCCUNGCAPDICHVUDAUTHAU)]
    [Authorize]
    public class DMToChucCungCapDichVuDauThauController : BaseController
    {
        //
        // GET: /DMTOCHUCCUNGCAPDICHVUDAUTHAU/

        private readonly IDM_TOCHUCCUNGCAPDICHVUDAUTHAUService _iDmToChucCungCapDichVuDauThauService;

        public DMToChucCungCapDichVuDauThauController()
        {
            _iDmToChucCungCapDichVuDauThauService = IoC.Resolve<IDM_TOCHUCCUNGCAPDICHVUDAUTHAUService>();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            //GetAllPermission();
            return View("Index");
        }

        public ActionResult FilterView()
        {
            return PartialView("FilterView");
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult DMTOCHUCCUNGCAPDICHVUDAUTHAUPartial(string timkiem)
        {
            List<QLQ_DMTOCHUCCUNGCAPDICHVUDAUTHAU> model = new List<QLQ_DMTOCHUCCUNGCAPDICHVUDAUTHAU>();
            model = _iDmToChucCungCapDichVuDauThauService.Search(timkiem);
            return PartialView("DMTOCHUCCUNGCAPDICHVUDAUTHAUPartial", model);
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Search(string timkiem)
        {
            return DMTOCHUCCUNGCAPDICHVUDAUTHAUPartial(timkiem);
        }

        //[RBACAuthorize(Permissions = "DM_TOCHUCCUNGCAPDICHVUDAUTHAU_ADD")]
        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult AddNewPartial(QLQ_DMTOCHUCCUNGCAPDICHVUDAUTHAU dmtochuccungcapdichvudauthau)
        {
            if (ModelState.IsValid)
            {
                if (_iDmToChucCungCapDichVuDauThauService.GetByMa(dmtochuccungcapdichvudauthau.ma_tochuccungcapdichvudauthau) == null)//kiểm tra tồn tại ma_tochucdauthau
                {
                    //thêm mới bản ghi
                    try
                    {
                        //khai báo các trường tự tự động điền
                        //obj.CREATED_BY=;  //người tạo
                        dmtochuccungcapdichvudauthau.creation_date = DateTime.Now.Date;  //ngày tạo
                        dmtochuccungcapdichvudauthau.active = dmtochuccungcapdichvudauthau.active == null ? 0 : dmtochuccungcapdichvudauthau.active;
                        _iDmToChucCungCapDichVuDauThauService.CreateNew(dmtochuccungcapdichvudauthau);
                        _iDmToChucCungCapDichVuDauThauService.CommitChanges();
                        ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                    }
                    catch (Exception e)
                    {
                        ViewData["EditError"] = e.Message;
                    }
                }
                else
                    ViewData["EditError"] = Resources.Localizing.MessageDuplicatekey;//in ra thông báo lỗi trùng mã
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return DMTOCHUCCUNGCAPDICHVUDAUTHAUPartial(null);
        }

        // [RBACAuthorize(Permissions = "DM_TOCHUCCUNGCAPDICHVUDAUTHAU_UPDATE")]
        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult UpdatePartial(QLQ_DMTOCHUCCUNGCAPDICHVUDAUTHAU dmtochuccungcapdichvudauthau)
        {

            if (ModelState.IsValid)
            {
                //cập nhật bản ghi
                try
                {
                    QLQ_DMTOCHUCCUNGCAPDICHVUDAUTHAU temp = _iDmToChucCungCapDichVuDauThauService.GetById(dmtochuccungcapdichvudauthau.tochuccungcapdichvudauthau_id);
                    temp.ma_tochuccungcapdichvudauthau = dmtochuccungcapdichvudauthau.ma_tochuccungcapdichvudauthau;
                    temp.ten_tochuccungcapdichvudauthau = dmtochuccungcapdichvudauthau.ten_tochuccungcapdichvudauthau;
                    temp.dia_chi = dmtochuccungcapdichvudauthau.dia_chi;
                    temp.dien_thoai = dmtochuccungcapdichvudauthau.dien_thoai;
                    temp.fax = dmtochuccungcapdichvudauthau.fax;
                    temp.so_taikhoanthanhtoan = dmtochuccungcapdichvudauthau.so_taikhoanthanhtoan;
                    temp.so_gptl = dmtochuccungcapdichvudauthau.so_gptl;
                    temp.ngaycap = dmtochuccungcapdichvudauthau.ngaycap;
                    temp.noicap = dmtochuccungcapdichvudauthau.noicap;
                    temp.active = dmtochuccungcapdichvudauthau.active == null ? 0 : dmtochuccungcapdichvudauthau.active;
                    //temp.LASTUPDATE_BY =;   //người sửa
                    temp.lastupdated_date = DateTime.Now.Date;
                    _iDmToChucCungCapDichVuDauThauService.Clear();
                    _iDmToChucCungCapDichVuDauThauService.Save(temp);
                    _iDmToChucCungCapDichVuDauThauService.CommitChanges();
                    _iDmToChucCungCapDichVuDauThauService.CommitChanges();
                    ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return DMTOCHUCCUNGCAPDICHVUDAUTHAUPartial(null);
        }

        //[RBACAuthorize(Permissions = "DM_TOCHUCCUNGCAPDICHVUDAUTHAU_DELETE")]
        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.DELETE)]
        public ActionResult DeletePartial(QLQ_DMTOCHUCCUNGCAPDICHVUDAUTHAU dmtochuccungcapdichvudauthau)
        {

            //code xóa bản ghi
            try
            {
                _iDmToChucCungCapDichVuDauThauService.Delete(dmtochuccungcapdichvudauthau);
                _iDmToChucCungCapDichVuDauThauService.CommitChanges();
                ViewData["EditSuccess"] = Resources.Localizing.MessageDelete;
            }
            catch (Exception)
            {
                ViewData["EditError"] = Resources.Localizing.MessageDate;
            }
            return DMTOCHUCCUNGCAPDICHVUDAUTHAUPartial(null);
        }

    }
}
