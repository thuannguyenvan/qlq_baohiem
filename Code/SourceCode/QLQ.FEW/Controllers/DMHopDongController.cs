﻿using FX.Core;
using log4net;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using QLQ.Core.Security;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace QLQ.FEW.Controllers
{

    [Function(QlqFunctions.DM_QUYEN)]
    [Authorize]
    public class DMHopDongController : BaseController
    {
        private readonly IDM_HOPDONGService _iDmHopdongService;
        private readonly IHopDongService _iHopDongService;
        private static readonly ILog Log = LogManager.GetLogger(typeof(DMChiTieuController));
        //
        // GET: /DMHopDong/
        public DMHopDongController()
        {
            _iDmHopdongService = IoC.Resolve<IDM_HOPDONGService>();
            _iHopDongService = IoC.Resolve<IHopDongService>();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DMHOPDONGPartial(string timkiem)
        {
            List<QLQ_DMHOPDONG> model = new List<QLQ_DMHOPDONG>();
            model = _iDmHopdongService.Search(timkiem);
            return PartialView("DMHOPDONGPartial", model);
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Search(string timkiem)
        {
            return DMHOPDONGPartial(timkiem);
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult AddNewPartial(QLQ_DMHOPDONG obj)
        {
            if (ModelState.IsValid)
            {
                if (_iDmHopdongService.GetByMa(obj.ma_hopdong) == null)//kiểm tra tồn tại ma_hopdong
                {
                    //thêm mới bản ghi
                    try
                    {
                        //khai báo các trường tự tự động điền
                        //obj.CREATED_BY=;  //người tạo
                        obj.creation_date = DateTime.Now.Date;  //ngày tạo
                        obj.ky_hieu = obj.ky_hieu;
                        obj.active = obj.active == null ? 0 : obj.active;
                        _iDmHopdongService.CreateNew(obj);
                        _iDmHopdongService.CommitChanges();
                        ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                        //log.Error("Danh mục đơn vị quản lý - Thêm mới: " + nguoidung.TENDANGNHAP);
                    }
                    catch (Exception e)
                    {
                        ViewData["EditError"] = e.Message;
                    }
                }
                else
                    ViewData["EditError"] = Resources.Localizing.MessageDuplicatekey;//in ra thông báo lỗi trùng mã
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return DMHOPDONGPartial(null);
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult UpdatePartial(QLQ_DMHOPDONG obj)
        {
            if (ModelState.IsValid)
            {
                //cập nhật bản ghi
                try
                {
                    QLQ_DMHOPDONG temp = _iDmHopdongService.GetById(obj.hopdong_id);
                    temp.ma_hopdong = obj.ma_hopdong;
                    temp.ky_hieu = obj.ky_hieu;
                    temp.ten_hopdong = obj.ten_hopdong;
                    temp.active = obj.active == null ? 0 : obj.active;
                    //temp.LASTUPDATE_BY =;   //người sửa
                    temp.lastupdated_date = DateTime.Now.Date;
                    _iDmHopdongService.Clear();
                    _iDmHopdongService.Save(temp);
                    _iDmHopdongService.CommitChanges();
                    ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                    //log.Error("Danh mục đơn vị quản lý - Sửa: " + nguoidung.TENDANGNHAP);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return DMHOPDONGPartial(null);
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.DELETE)]
        public ActionResult DeletePartial(QLQ_DMHOPDONG obj)
        {
            //code xóa bản ghi
            try
            {
                if (!_iHopDongService.CheckDmHopDongId(obj.hopdong_id))
                {
                    _iDmHopdongService.Delete(obj);
                    _iDmHopdongService.CommitChanges();
                    ViewData["EditSuccess"] = Resources.Localizing.MessageDelete;
                    //log.Error("Danh mục đơn vị quản lý - Xóa: " + nguoidung.TENDANGNHAP);  
                }
                else
                {
                    ViewData["EditSuccess"] = Resources.Localizing.MessageDeleteExist;
                }

            }
            catch (Exception)
            {
                ViewData["EditSuccess"] = Resources.Localizing.MessageDeleteError;
            }
            return DMHOPDONGPartial(null);
        }

    }
}
