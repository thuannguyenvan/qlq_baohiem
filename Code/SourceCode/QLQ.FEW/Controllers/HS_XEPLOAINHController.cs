﻿using System;
using System.Linq;
using System.Web.Mvc;
using QLQ.Core.IService;
using FX.Core;
using QLQ.Core.Domain;
using log4net;
using DevExpress.Web;
using DevExpress.Web.Mvc;
using QLQ.Core.Common;
using QLQ.Core.Security;
using System.Collections.Generic;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.HS_XEPLOAINGANHANG)]
    [Authorize]
    public class HS_XEPLOAINHController : BaseController
    {
        //
        // GET: /HOSO/
        public const string UploadDirectory = @"/Uploads/HoSo/";

        private readonly IHS_XEPLOAINHService _iHsXepLoaiNhSvc;
        private static readonly ILog Log = LogManager.GetLogger(typeof(HS_XEPLOAINHController));

        public HS_XEPLOAINHController()
        {
            _iHsXepLoaiNhSvc = IoC.Resolve<IHS_XEPLOAINHService>();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult HS_XEPLOAINHPartial()
        {
            ViewData["NAM"] = Common.GetNam();
            var xepLoaiNH = _iHsXepLoaiNhSvc.Query.Select(x => new { x.XEPLOAINH_ID, x.NAM, UPLOAD = UploadDirectory + x.UPLOAD });
            return PartialView("HS_XEPLOAINHPartial", xepLoaiNH);
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Search(string timkiem)
        {
            List<HS_XEPLOAINH> Listmodel = new List<HS_XEPLOAINH>();
            if (timkiem == null || timkiem == "" ) return HS_XEPLOAINHPartial();
            int _stimkiem = Convert.ToInt32(timkiem);
            var result = _iHsXepLoaiNhSvc.Search(_stimkiem);
            if (result.Count == 0)
            {
                return PartialView("HS_XEPLOAINHPartial", result);
            }
            foreach (var kq in result)
            {
                Listmodel.Add(new HS_XEPLOAINH { NAM = kq.NAM, XEPLOAINH_ID = kq.XEPLOAINH_ID, UPLOAD = UploadDirectory + kq.UPLOAD });
            }
            return PartialView("HS_XEPLOAINHPartial", Listmodel);
        }
        public ActionResult PopupSaveData()
        {
            ViewData["NAM"] = Common.GetNam();
            var xepLoaiNH = _iHsXepLoaiNhSvc.Query.Select(x => new { x.XEPLOAINH_ID, x.NAM, UPLOAD = UploadDirectory + x.UPLOAD });
            return PartialView();
        }

        public ActionResult DragAndDropImageUpload_FileDanhGia()
        {
            UploadedFile[] files = UploadControlExtension.GetUploadedFiles("ctrUploadFileDanhGia", UploadControlDemosHelper.ValidationSettings,uc_FileUploadComplete);
            return null;
        }

        public void uc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                string resultFilePath = Request.MapPath(UploadDirectory + e.UploadedFile.FileName);

                //Lưu file upload vào thư mục
                e.UploadedFile.SaveAs(resultFilePath, true);
                System.Web.UI.IUrlResolutionService urlResolver = sender as System.Web.UI.IUrlResolutionService;
                if (urlResolver != null)
                {
                    e.CallbackData = UploadDirectory + e.UploadedFile.FileName;
                }
            }
        }

        [ActionPermissions(QlqActions.DELETE)]
        public ActionResult DeletePartial(HS_XEPLOAINH obj)
        {
            try
            {
                _iHsXepLoaiNhSvc.Delete(obj);
                _iHsXepLoaiNhSvc.CommitChanges();
                ViewData["EditSuccess"] = "Xóa thành công";
            }
            catch (Exception ex)
            {
                ViewData["EditError"] = "Có lỗi xảy ra trong quá trình xóa dữ liệu";
                Log.Error("DeletePartial Xoa: " + ex.Message);
            }
            return HS_XEPLOAINHPartial();
        }

        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult LuuXepLoaiNH(HS_XEPLOAINH xepLoaiNh, string upload)
        {
            xepLoaiNh.UPLOAD = upload;
            try
            {
                if (xepLoaiNh.XEPLOAINH_ID == null)
                {
                    xepLoaiNh.CREATION_DATE = DateTime.Now;
                    _iHsXepLoaiNhSvc.Clear();
                    _iHsXepLoaiNhSvc.CreateNew(xepLoaiNh);
                }
                else
                {
                    var model = _iHsXepLoaiNhSvc.GetById(xepLoaiNh.XEPLOAINH_ID);
                    if (model != null)
                    {
                        model.NAM = xepLoaiNh.NAM;
                        model.UPLOAD = xepLoaiNh.UPLOAD;
                        model.LASTUPDATED_DATE = DateTime.Now;
                        _iHsXepLoaiNhSvc.Clear();
                        _iHsXepLoaiNhSvc.Update(model);
                    }
                    else
                    {
                        return Json("Fail", JsonRequestBehavior.AllowGet);
                    }
                }
                _iHsXepLoaiNhSvc.CommitChanges();
                return HS_XEPLOAINHPartial();
            }
            catch (Exception ex)
            {
                Log.Error("LuuXepLoaiNH: " + ex.Message);
                return Json("Fail", JsonRequestBehavior.AllowGet);
            }
        }

        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult SuaXepLoaiNH(long xeploainhId)
        {
            var xepLoaiNH = _iHsXepLoaiNhSvc.GetById(xeploainhId);
            if (xepLoaiNH == null)
            {
                return Json("Fail");
            }
            xepLoaiNH.UPLOAD = UploadDirectory + xepLoaiNH.UPLOAD;

            ViewData["NAM"] = Common.GetNam();
            return PartialView("PopupSaveData", xepLoaiNH);
        }
    }
}
