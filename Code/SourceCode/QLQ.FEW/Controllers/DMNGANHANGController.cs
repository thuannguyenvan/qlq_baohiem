﻿using FX.Core;
using log4net;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using QLQ.Core.Security;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.DM_NGANHANG)]
    [Authorize]
    public class DMNGANHANGController : BaseController
    {
        private readonly IDM_NGANHANGService _iDmNganhangService;
        private readonly IHOSOService _hosoService;
        private readonly ILAISUATNH_HDRService _laisuatnhHdrService;
        private static readonly ILog Log = LogManager.GetLogger(typeof(DMNGANHANGController));
        //
        // GET: /DMNGANHANG/
        public DMNGANHANGController()
        {
            _iDmNganhangService = IoC.Resolve<IDM_NGANHANGService>();
            _hosoService = IoC.Resolve<IHOSOService>();
            _laisuatnhHdrService = IoC.Resolve<ILAISUATNH_HDRService>();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            return View();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult DMNGANHANGPartial(string timkiem)
        {
            List<QLQ_DMNGANHANG> model = new List<QLQ_DMNGANHANG>();
            model = _iDmNganhangService.Search(timkiem);
            return PartialView("DMNGANHANGPartial", model);
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Search(string timkiem)
        {
            return DMNGANHANGPartial(timkiem);
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult AddNewPartial(QLQ_DMNGANHANG obj)
        {
            if (ModelState.IsValid)
            {
                if (_iDmNganhangService.GetByMa(obj.MA_NGANHANG) == null)//kiểm tra tồn tại MA_NGANHANG
                {
                    //thêm mới bản ghi
                    try
                    {
                        //khai báo các trường tự tự động điền
                        //obj.CREATED_BY= Session["user"].ToString();  //người tạo
                        obj.CREATION_DATE = DateTime.Now.Date;  //ngày tạo
                        obj.ACTIVE = obj.ACTIVE == null ? 0 : obj.ACTIVE;
                        _iDmNganhangService.CreateNew(obj);
                        _iDmNganhangService.CommitChanges();
                        ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                        //log.Error("Danh mục đơn vị quản lý - Thêm mới: " + nguoidung.TENDANGNHAP);
                    }
                    catch (Exception e)
                    {
                        ViewData["EditError"] = e.Message;
                    }
                }
                else
                    ViewData["EditError"] = Resources.Localizing.MessageDuplicatekey;//in ra thông báo lỗi trùng mã
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return DMNGANHANGPartial(null);
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult UpdatePartial(QLQ_DMNGANHANG obj)
        {
            if (ModelState.IsValid)
            {
                //cập nhật bản ghi
                try
                {
                    QLQ_DMNGANHANG temp = _iDmNganhangService.GetById(obj.NGANHANG_ID);
                    temp.MA_NGANHANG = obj.MA_NGANHANG;
                    temp.TEN_NGANHANG = obj.TEN_NGANHANG;
                    temp.ACTIVE = obj.ACTIVE == null ? 0 : obj.ACTIVE;
                    //temp.LASTUPDATE_BY =;   //người sửa
                    temp.LASTUPDATED_DATE = DateTime.Now.Date;
                    _iDmNganhangService.Save(temp);
                    _iDmNganhangService.CommitChanges();
                    ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                    //log.Error("Danh mục đơn vị quản lý - Sửa: " + nguoidung.TENDANGNHAP);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return DMNGANHANGPartial(null);
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.DELETE)]
        public ActionResult DeletePartial(QLQ_DMNGANHANG obj)
        {
            //code xóa bản ghi
            try
            {
                if(!_hosoService.CheckExistNganHangIdInHoSo(Convert.ToInt64(obj.NGANHANG_ID)) 
                    && !_laisuatnhHdrService.CheckIdNganHangInLaiSuat(Convert.ToInt64(obj.NGANHANG_ID)))
                {
                    _iDmNganhangService.Delete(obj);
                    _iDmNganhangService.CommitChanges();
                    ViewData["EditSuccess"] = Resources.Localizing.MessageDelete;
                    //log.Error("Danh mục đơn vị quản lý - Xóa: " + nguoidung.TENDANGNHAP);  
                }
                else
                {
                    ViewData["EditSuccess"] = Resources.Localizing.MessageDeleteExist;
                }
            }
            catch (Exception)
            {
                ViewData["EditSuccess"] = Resources.Localizing.MessageDeleteError;
            }
            return DMNGANHANGPartial(null);
        }
    }
}
