﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using FX.Core;
using log4net;
using QLQ.Core.Common;
using QLQ.Core.Security;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.DIEUCHINHLAISUAT)]
    [Authorize]
    public class DieuChinhLaiSuatController : BaseController
    {
        //
        // GET: /dmdautu/

        //public NGUOIDUNG nguoidung;
        //public string _HT_DT;
        private readonly IHopDongService _iHopDongService;
        private readonly IHopDongDieuChinhLaiSuatService _iLaiSuatDieuChinhService;
        private static readonly ILog Log = LogManager.GetLogger(typeof(DieuChinhLaiSuatController));       
        private GridViewCustomBindingHandlers _gridViewHanders = new GridViewCustomBindingHandlers();

        private long? HopDongId
        {
            get
            {
                long? id = null;
                if (TempData["hopdong_id"] != null)
                {
                    id = long.Parse(TempData["hopdong_id"].ToString());
                }
                return id;
            }
            set
            {
                TempData["hopdong_id"] = value;
            }
        }

        public DieuChinhLaiSuatController()
        {
            _iHopDongService = IoC.Resolve<IHopDongService>();
            _iLaiSuatDieuChinhService = IoC.Resolve<IHopDongDieuChinhLaiSuatService>();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            HopDongId = null;
            return View("Index");
        }
        //private void GetAllPermission()
        //{
        //    string username = nguoidung.TENDANGNHAP;
        //    ViewBag.DM_DAUTU_ADD = Common.DoCheckPermission(username, "DM_DAUTU_ADD");
        //    ViewBag.DM_DAUTU_UPDATE = Common.DoCheckPermission(username, "DM_DAUTU_UPDATE");
        //    ViewBag.DM_DAUTU_DELETE = Common.DoCheckPermission(username, "DM_DAUTU_DELETE");
        //}

        public ActionResult FilterView()
        {
            return PartialView("FilterView");
        }


        public ActionResult ChiTietHopDong(long id)
        {
            HopDongId = id;
            return View("Index");
        }

        public ActionResult DCLaiSuatPartial()
        {
            long? id = HopDongId;
            if (id.HasValue)
            {
                var hopdong = _iHopDongService.Getbykey(id.Value);
                if (hopdong != null)
                {
                    TempData["HopDong"] = new List<QLQ_HOPDONG>() { hopdong };
                }
            }
            else
            {
                TempData["HopDong"] = _iHopDongService.GetAll();
            }
            var dcLaiSuat = _iLaiSuatDieuChinhService.Search(null, id).ToList();
            HopDongId = id;
            return PartialView("DCLaiSuatPartial", dcLaiSuat);
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Search(string timkiem)
        {
            long? id = HopDongId;
            List<QLQ_HOPDONGLAISUATDC> model = _iLaiSuatDieuChinhService.Search(timkiem, id).ToList();
            return PartialView("DCLaiSuatPartial", model);
        }

        //[RBACAuthorize(Permissions = "DM_DAUTU_ADD")]
        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult AddNewPartial(QLQ_HOPDONGLAISUATDC model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //khai báo các trường tự tự động điền
                    //obj.CREATED_BY=;  //người tạo
                    model.creation_date = DateTime.Now.Date;  //ngày tạo
                    model.created_by = CurrentUser?.AccountId;
                    _iLaiSuatDieuChinhService.CreateNew(model);
                    _iLaiSuatDieuChinhService.CommitChanges();
                    ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                    //log.Error("Danh mục đơn vị quản lý - Thêm mới: " + nguoidung.TENDANGNHAP);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return DCLaiSuatPartial();
        }

       // [RBACAuthorize(Permissions = "DM_DAUTU_UPDATE")]
        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult UpdatePartial(QLQ_HOPDONGLAISUATDC model)
        {

            if (ModelState.IsValid)
            {
                //cập nhật bản ghi
                try
                {
                    model.lastupdated_date = DateTime.Now.Date;
                    model.lastupdate_by = CurrentUser?.AccountId;
                    _iLaiSuatDieuChinhService.Clear();
                    _iLaiSuatDieuChinhService.Save(model);
                    _iLaiSuatDieuChinhService.CommitChanges();
                    ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return DCLaiSuatPartial();
        }

        //[RBACAuthorize(Permissions = "DM_DAUTU_DELETE")]
        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.DELETE)]
        public ActionResult DeletePartial(QLQ_HOPDONGLAISUATDC model)
        {

            //code xóa bản ghi
            try
            {
                _iLaiSuatDieuChinhService.Delete(model);
                _iLaiSuatDieuChinhService.CommitChanges();
                ViewData["EditSuccess"] = Resources.Localizing.MessageDelete;
            }
            catch (Exception)
            {
                ViewData["EditError"] = Resources.Localizing.MessageDate;
            }
            return DCLaiSuatPartial();
        }        
    }
}
