﻿using FX.Core;
using log4net;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using QLQ.Core.Security;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.DM_HINHTHUCDAUTU)]
    [Authorize]
    public class DMHinhThucDTuController : BaseController
    {

        private readonly IDM_HINHTHUCDTUService _iDmHinhthucdtuService;
        private readonly IDM_DAUTUService _idDautuService;
        private static readonly ILog Log = LogManager.GetLogger(typeof(DMChiTieuController));
        //
        // GET: /DMHinhThucDTu/
        public DMHinhThucDTuController()
        {
            _iDmHinhthucdtuService = IoC.Resolve<IDM_HINHTHUCDTUService>();
            _idDautuService = IoC.Resolve<IDM_DAUTUService>();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            return View();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult DMHinhThucDTuPartial(string timkiem)
        {
            List<QLQ_DMHINHTHUCDTU> model = new List<QLQ_DMHINHTHUCDTU>();
            model = _iDmHinhthucdtuService.Search(timkiem);
            return PartialView("DMHinhThucDTuPartial", model);
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Search(string timkiem)
        {
            return DMHinhThucDTuPartial(timkiem);
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult AddNewPartial(QLQ_DMHINHTHUCDTU obj)
        {
            if (ModelState.IsValid)
            {
                if (_iDmHinhthucdtuService.GetByMa(obj.ma_hinhthuc) == null)//kiểm tra tồn tại MA_CHITIEU
                {
                    //thêm mới bản ghi
                    try
                    {
                        //khai báo các trường tự tự động điền
                        //obj.CREATED_BY=;  //người tạo
                        obj.creation_date = DateTime.Now.Date;  //ngày tạo
                        obj.active = obj.active == null ? 0 : obj.active;
                        _iDmHinhthucdtuService.CreateNew(obj);
                        _iDmHinhthucdtuService.CommitChanges();
                        ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                        //log.Error("Danh mục đơn vị quản lý - Thêm mới: " + nguoidung.TENDANGNHAP);
                    }
                    catch (Exception e)
                    {
                        ViewData["EditError"] = e.Message;
                    }
                }
                else
                    ViewData["EditError"] = Resources.Localizing.MessageDuplicatekey;//in ra thông báo lỗi trùng mã
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return DMHinhThucDTuPartial(null);
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult UpdatePartial(QLQ_DMHINHTHUCDTU obj)
        {
            if (ModelState.IsValid)
            {
                //cập nhật bản ghi
                try
                {
                    QLQ_DMHINHTHUCDTU temp = _iDmHinhthucdtuService.GetById(obj.hinhthuc_id);
                    temp.ma_hinhthuc = obj.ma_hinhthuc;
                    temp.ten_hinhthuc = obj.ten_hinhthuc;
                    temp.active = obj.active == null ? 0 : obj.active;
                    //temp.LASTUPDATE_BY =;   //người sửa
                    temp.lastupdated_date = DateTime.Now.Date;
                    _iDmHinhthucdtuService.Clear();
                    _iDmHinhthucdtuService.Save(temp);
                    _iDmHinhthucdtuService.CommitChanges();
                    ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                    //log.Error("Danh mục đơn vị quản lý - Sửa: " + nguoidung.TENDANGNHAP);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return DMHinhThucDTuPartial(null);
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.DELETE)]
        public ActionResult DeletePartial(QLQ_DMHINHTHUCDTU obj)
        {
            //code xóa bản ghi
            try
            {
                if (!_idDautuService.CheckExistDmDauTu(obj.hinhthuc_id))
                {
                    _iDmHinhthucdtuService.Delete(obj);
                    _iDmHinhthucdtuService.CommitChanges();
                    ViewData["EditSuccess"] = Resources.Localizing.MessageDelete;
                    //log.Error("Danh mục đơn vị quản lý - Xóa: " + nguoidung.TENDANGNHAP); 
                }
                else
                {
                    ViewData["EditSuccess"] = Resources.Localizing.MessageDeleteExist;
                }

            }
            catch (Exception)
            {
                ViewData["EditSuccess"] = Resources.Localizing.MessageDeleteError;
            }
            return DMHinhThucDTuPartial(null);
        }
    }
}
