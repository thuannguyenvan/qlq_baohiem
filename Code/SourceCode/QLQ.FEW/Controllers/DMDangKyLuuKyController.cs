﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using FX.Core;
using log4net;
using QLQ.Core.Common;
using QLQ.Core.Security;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.DM_DANGKYLUUKY)]
    [Authorize]
    public class DMDangKyLuuKyController : BaseController
    {
        //
        // GET: /DMDangKyLuuKy/
        private readonly IDM_DANGKYLUUKYService _iDmDangKyLuuKyService;

        public DMDangKyLuuKyController()
        {
            _iDmDangKyLuuKyService = IoC.Resolve<IDM_DANGKYLUUKYService>();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            //GetAllPermission();
            return View("Index");
        }

        public ActionResult FilterView()
        {
            return PartialView("FilterView");
        }


        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult DMDANGKYLUUKYPartial(string timkiem)
        {
            List<QLQ_DMDANGKYLUUKY> model = new List<QLQ_DMDANGKYLUUKY>();
            model = _iDmDangKyLuuKyService.Search(timkiem);
            return PartialView("DMDANGKYLUUKYPartial", model);
        }
        public ActionResult Search(string timkiem)
        {
            return DMDANGKYLUUKYPartial(timkiem);
        }

        //[RBACAuthorize(Permissions = "DM_DANGKYLUUKY_ADD")]
        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult AddNewPartial(QLQ_DMDANGKYLUUKY dmdangkyluuky)
        {
            if (ModelState.IsValid)
            {
                if (_iDmDangKyLuuKyService.GetByMa(dmdangkyluuky.ma_chutaikhoan) == null)//kiểm tra tồn tại ma_chutaikhoan
                {
                    //thêm mới bản ghi
                    try
                    {
                        //khai báo các trường tự tự động điền
                        //obj.CREATED_BY=;  //người tạo
                        dmdangkyluuky.creation_date = DateTime.Now.Date;  //ngày tạo
                        dmdangkyluuky.active = dmdangkyluuky.active == null ? 0 : dmdangkyluuky.active;
                        _iDmDangKyLuuKyService.CreateNew(dmdangkyluuky);
                        _iDmDangKyLuuKyService.CommitChanges();
                        ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                    }
                    catch (Exception e)
                    {
                        ViewData["EditError"] = e.Message;
                    }
                }
                else
                    ViewData["EditError"] = Resources.Localizing.MessageDuplicatekey;//in ra thông báo lỗi trùng mã
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return DMDANGKYLUUKYPartial(null);
        }

        // [RBACAuthorize(Permissions = "DM_TOCHUCDAUTHAU_UPDATE")]
        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult UpdatePartial(QLQ_DMDANGKYLUUKY dmdangkyluuky)
        {

            if (ModelState.IsValid)
            {
                //cập nhật bản ghi
                try
                {
                    QLQ_DMDANGKYLUUKY temp = _iDmDangKyLuuKyService.GetById(dmdangkyluuky.dangkyluuky_id);
                    temp.ma_chutaikhoan = dmdangkyluuky.ma_chutaikhoan;
                    temp.ten_chutaikhoan = dmdangkyluuky.ten_chutaikhoan;
                    temp.so_taikhoanluuky = dmdangkyluuky.so_taikhoanluuky;
                    temp.noimo_taikhoanluuky = dmdangkyluuky.noimo_taikhoanluuky;
                    temp.so_dangkysohuu = dmdangkyluuky.so_dangkysohuu;
                    temp.noicap_dangkysohuu = dmdangkyluuky.noicap_dangkysohuu;
                    temp.ngaycap_dangkysohuu = dmdangkyluuky.ngaycap_dangkysohuu;
                    temp.quoctich = dmdangkyluuky.quoctich;
                    temp.daidien = dmdangkyluuky.daidien;
                    temp.chucvu = dmdangkyluuky.chucvu;
                    temp.active = dmdangkyluuky.active == null ? 0 : dmdangkyluuky.active;
                    //temp.LASTUPDATE_BY =;   //người sửa
                    temp.lastupdated_date = DateTime.Now.Date;
                    _iDmDangKyLuuKyService.Clear();
                    _iDmDangKyLuuKyService.Save(temp);
                    _iDmDangKyLuuKyService.CommitChanges();
                    _iDmDangKyLuuKyService.CommitChanges();
                    ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return DMDANGKYLUUKYPartial(null);
        }

        //[RBACAuthorize(Permissions = "DM_TOCHUCDAUTHAU_DELETE")]
        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.DELETE)]
        public ActionResult DeletePartial(QLQ_DMDANGKYLUUKY dmdangkyluuky)
        {

            //code xóa bản ghi
            try
            {
                _iDmDangKyLuuKyService.Delete(dmdangkyluuky);
                _iDmDangKyLuuKyService.CommitChanges();
                ViewData["EditSuccess"] = Resources.Localizing.MessageDelete;
            }
            catch (Exception)
            {
                ViewData["EditError"] = Resources.Localizing.MessageDate;
            }
            return DMDANGKYLUUKYPartial(null);
        }
    }
}
