﻿using FX.Core;
using QLQ.Core.IService;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using log4net;
using QLQ.Core.Domain;
using QLQ.Core.Security;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.DM_BC)]
    [Authorize]
    public class DMBaoCaoController : BaseController
    {
        
        private readonly IDM_BAOCAOService _iDmBaocaoService;
        private static readonly ILog Log = LogManager.GetLogger(typeof(DMChiTieuController));
        //
        // GET: /DMBaoCao/
        public DMBaoCaoController()
        {
            _iDmBaocaoService = IoC.Resolve<IDM_BAOCAOService>();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            return View();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult DMBAOCAOPartial(string timkiem)
        {
            List<QLQ_DMBAOCAO> model = new List<QLQ_DMBAOCAO>();
            model = _iDmBaocaoService.Search(timkiem);
            return PartialView("DMBAOCAOPartial", model);
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Search(string timkiem)
        {
            return DMBAOCAOPartial(timkiem);
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult AddNewPartial(QLQ_DMBAOCAO obj)
        {
            if (ModelState.IsValid)
            {
                if (_iDmBaocaoService.GetByMa(obj.ma_baocao) == null)//kiểm tra tồn tại ma_baocao
                {
                    //thêm mới bản ghi
                    try
                    {
                        //khai báo các trường tự tự động điền
                        //obj.CREATED_BY=;  //người tạo
                        obj.creation_date = DateTime.Now.Date;  //ngày tạo
                        obj.active = obj.active == null ? 0 : obj.active;
                        _iDmBaocaoService.CreateNew(obj);
                        _iDmBaocaoService.CommitChanges();
                        ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                        //log.Error("Danh mục đơn vị quản lý - Thêm mới: " + nguoidung.TENDANGNHAP);
                    }
                    catch (Exception e)
                    {
                        ViewData["EditError"] = e.Message;
                    }
                }
                else
                    ViewData["EditError"] = Resources.Localizing.MessageDuplicatekey;//in ra thông báo lỗi trùng mã
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return DMBAOCAOPartial(null);
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult UpdatePartial(QLQ_DMBAOCAO obj)
        {
            if (ModelState.IsValid)
            {
                //cập nhật bản ghi
                try
                {
                    QLQ_DMBAOCAO temp = _iDmBaocaoService.GetById(obj.baocao_id);
                    temp.ma_baocao = obj.ma_baocao;
                    temp.ten_baocao = obj.ten_baocao;
                    temp.active = obj.active == null ? 0 : obj.active;
                    //temp.LASTUPDATE_BY =;   //người sửa
                    temp.lastupdated_date = DateTime.Now.Date;
                    _iDmBaocaoService.Clear();
                    _iDmBaocaoService.Save(temp);
                    _iDmBaocaoService.CommitChanges();
                    ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                    //log.Error("Danh mục đơn vị quản lý - Sửa: " + nguoidung.TENDANGNHAP);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return DMBAOCAOPartial(null);
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.DELETE)]
        public ActionResult DeletePartial(QLQ_DMBAOCAO obj)
        {
            //code xóa bản ghi
            try
            {
                _iDmBaocaoService.Delete(obj);
                _iDmBaocaoService.CommitChanges();
                ViewData["EditSuccess"] = Resources.Localizing.MessageDelete;
                //log.Error("Danh mục đơn vị quản lý - Xóa: " + nguoidung.TENDANGNHAP);                    
            }
            catch (Exception)
            {
                ViewData["EditError"] = Resources.Localizing.MessageDate;
            }
            return DMBAOCAOPartial(null);
        }

    }
}
