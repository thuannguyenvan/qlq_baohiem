﻿using DevExpress.Web.Mvc;
using FX.Core;
using QLQ.Core.CustomView;
using QLQ.Core.IService;
using QLQ.Core.Security;
using QLQ.Core.StoreProcedure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QLQ.FEW.Reports;
using System.Data;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.SOCHITIETLAIDAUTU_01DTQ)]
    [Authorize]
    public class So02DTQController : BaseController
    {
        //
        // GET: /SoChiTietLaiDauTu_01DTQ/
        private readonly IHopDongService _iHopDongService;
        private readonly ISoChiTietLaiDauTuService _iSoChiTietDauTu;
        private readonly QLQStoreProcedure _storeProcedure;
        private readonly IDM_HINHTHUCDTUService _iDmHinhthucdtuService;

        private decimal TongTienGocPhaiThu { get; set; }
        private decimal TongTienGocDaThu { get; set; }
        private decimal TongTienGocConPhaiThu { get; set; }
        private decimal TongTienLaiPhaiThu { get; set; }
        private decimal TongTienLaiDaThu { get; set; }
        private decimal TongTienLaiThuThieu { get; set; }
        private decimal TongTienLaiThuThua { get; set; }

        private long? HinhThucDauTuId
        {
            get
            {
                long? id = null;
                if (ViewData["HinhThucDauTuId"] != null)
                {
                    id = long.Parse(ViewData["HinhThucDauTuId"].ToString());
                }
                return id;
            }
            set
            {
                ViewData["HinhThucDauTuId"] = value;
            }
        }

        public So02DTQController()
        {
            _iHopDongService = IoC.Resolve<IHopDongService>();
            _iSoChiTietDauTu = IoC.Resolve<ISoChiTietLaiDauTuService>();
            _iDmHinhthucdtuService = IoC.Resolve<IDM_HINHTHUCDTUService>();
            _storeProcedure = new QLQStoreProcedure();
            GetCBFilter();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            return View();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Search(long hinhthucdautuid)
        {
            HinhThucDauTuId = hinhthucdautuid; // assign for next request

            List<BaoCao2Model> lstResult = new List<BaoCao2Model>();
            if (HinhThucDauTuId.HasValue)
            {
                var model = _iHopDongService.GetGocVaLai(HinhThucDauTuId.Value, DateTime.Now);
                if (model != null && model.DsHopDong != null && model.DsHopDong.Count > 0)
                {
                    lstResult = model.DsHopDong.Select(l => BaoCao2Model.CreateFromGocVaLaiModel(l)).ToList();
                }
            }

            HinhThucDauTuId = HinhThucDauTuId;
            return PartialView("So02ChiTietPartial", lstResult);
        }


        public void GetCBFilter()
        {
            var lstHinhThucDauTu = _iDmHinhthucdtuService.GetAll().ToList();
            if (lstHinhThucDauTu == null)
            {
                lstHinhThucDauTu = new List<Core.Domain.QLQ_DMHINHTHUCDTU>();
            }
            lstHinhThucDauTu.Insert(0, new Core.Domain.QLQ_DMHINHTHUCDTU() { hinhthuc_id = 0, ten_hinhthuc = "Tất cả" });
            ViewData["DSHinhThucDTU"] = lstHinhThucDauTu;
            if (lstHinhThucDauTu != null && lstHinhThucDauTu.Count > 0)
                HinhThucDauTuId = lstHinhThucDauTu[0].hinhthuc_id;
        }


        public ActionResult So02ChiTietPartial()
        {
            if (HinhThucDauTuId.HasValue)
                return Search(HinhThucDauTuId.Value);
            return PartialView();
        }

        public ActionResult fn_InAn()
        {
            int month = DateTime.Now.Month;
            int year = DateTime.Now.Year;

            // get data
            List<BaoCao2Model> dataReport = new List<BaoCao2Model>();
            if (HinhThucDauTuId.HasValue)
            {
                var model = _iHopDongService.GetGocVaLai(HinhThucDauTuId.Value, DateTime.Now);
                if (model != null && model.DsHopDong != null && model.DsHopDong.Count > 0)
                {
                    dataReport = model.DsHopDong.Select(l => BaoCao2Model.CreateFromGocVaLaiModel(l)).ToList();
                }
            }

            SoTheoDoi02a_DTQ report = new SoTheoDoi02a_DTQ();

            report.lbNamThang.Text = string.Format("Tháng {0} năm {1}", month, year);

            // tao datasource
            report.DataSource = FillDataBaoCao(dataReport).Tables[0];

            // bind data
            report.ngayThangNamGhiSo.DataBindings.Add("Text", null, "ngayThangNamGhiSo");
            report.dienGiai.DataBindings.Add("Text", null, "dienGiai");
            report.soTienVay_PhaiThu.DataBindings.Add("Text", null, "soTienVay_PhaiThu", "{0:n0}");
            report.soTienVay_DaThu.DataBindings.Add("Text", null, "soTienVay_DaThu", "{0:n0}");
            report.soTienVay_ConPhaiThu.DataBindings.Add("Text", null, "soTienVay_ConPhaiThu", "{0:n0}");
            report.soTienLai_TienLai_PhaiThu.DataBindings.Add("Text", null, "soTienLai_TienLai_PhaiThu", "{0:n0}");
            report.soTienLai_TienLai_DaThu.DataBindings.Add("Text", null, "soTienLai_TienLai_DaThu", "{0:n0}");
            report.soTienLai_ConPhaiThu_ThuThieu.DataBindings.Add("Text", null, "soTienLai_ConPhaiThu_ThuThieu", "{0:n0}");
            report.soTienLai_ConPhaiThu_ThuThua.DataBindings.Add("Text", null, "soTienLai_ConPhaiThu_ThuThua", "{0:n0}");
            report.ghiChu.DataBindings.Add("Text", null, "ghiChu");

            report.lbTongTienGocConPhaiThu_Nam.Text = TongTienGocPhaiThu.ToString("n0");
            report.lbTongTienGocDaThu_Nam.Text = TongTienGocDaThu.ToString("n0");
            report.lbTongTienGocConPhaiThu_Nam.Text = TongTienGocConPhaiThu.ToString("n0");
            report.lbTongTienLaiDaThu_Nam.Text = TongTienLaiDaThu.ToString("n0");
            report.lbTongTienLaiPhaiThu_Nam.Text = TongTienLaiPhaiThu.ToString("n0");
            report.lbTongTienLaiThuThieu_Nam.Text = TongTienLaiThuThieu.ToString("n0");
            report.lbTongTienLaiThuThua_Nam.Text = TongTienLaiThuThua.ToString("n0");

            return PartialView("Report02DTQPartial", report);
        }

        private DataSet FillDataBaoCao(List<BaoCao2Model> lstData)
        {
            TongTienGocPhaiThu = TongTienGocConPhaiThu = TongTienGocDaThu = TongTienLaiDaThu = TongTienLaiPhaiThu = TongTienLaiThuThieu = TongTienLaiThuThua = 0;

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            ds.Tables.Add(dt);

            dt.TableName = "bc02DTQ";
            dt.Columns.Add("ngayThangNamGhiSo", typeof(string));
            dt.Columns.Add("dienGiai", typeof(string));
            dt.Columns.Add("soTienVay_PhaiThu", typeof(long));
            dt.Columns.Add("soTienVay_DaThu", typeof(long));
            dt.Columns.Add("soTienVay_ConPhaiThu", typeof(long));
            dt.Columns.Add("soTienLai_TienLai_PhaiThu", typeof(long));
            dt.Columns.Add("soTienLai_TienLai_DaThu", typeof(long));
            dt.Columns.Add("soTienLai_ConPhaiThu_ThuThieu", typeof(long));
            dt.Columns.Add("soTienLai_ConPhaiThu_ThuThua", typeof(long));
            dt.Columns.Add("ghiChu", typeof(string));

            foreach (BaoCao2Model item in lstData)
            {
                var ngayghiSo = item.NgayGhiSo.HasValue ? item.NgayGhiSo.Value.ToString("dd/MM/yyyy") : string.Empty;

                if (item.TienGocPhaiThu != null)
                {
                    TongTienGocPhaiThu += (decimal)item.TienGocPhaiThu;
                }
                else
                {
                    TongTienGocPhaiThu += 0;
                }
                if (item.SoTienGocDaThu != null)
                {
                    TongTienGocDaThu += (decimal)item.SoTienGocDaThu;
                }
                else
                {
                    TongTienGocDaThu += 0;
                }
                if (item.SoConPhaiThu != null)
                {
                    TongTienGocConPhaiThu += (decimal)item.SoConPhaiThu;
                }
                else
                {
                    TongTienGocConPhaiThu += 0;
                }
                if (item.SoTienLaiDaThu != null)
                {
                    TongTienLaiDaThu += (decimal)item.SoTienLaiDaThu;
                }
                else
                {
                    TongTienLaiDaThu += 0;
                }
                if (item.SoTienLaiPhaiThu != null)
                {
                    TongTienLaiPhaiThu += (decimal)item.SoTienLaiPhaiThu;
                }
                else
                {
                    TongTienLaiPhaiThu += 0;
                }
                if (item.SoTienLaiThuThieu != null)
                {
                    TongTienLaiThuThieu += (decimal)item.SoTienLaiThuThieu;
                }
                else
                {
                    TongTienLaiThuThieu += 0;
                }
                if (item.SoTienLaiThuThua != null)
                {
                    TongTienLaiThuThua += (decimal)item.SoTienLaiThuThua;
                }
                else
                {
                    TongTienLaiThuThua += 0;
                }

                ds.Tables["bc02DTQ"].Rows.Add(new Object[] {
                    ngayghiSo, item.DienGiai, item.TienGocPhaiThu, item.SoTienGocDaThu, item.SoConPhaiThu,
                    item.SoTienLaiPhaiThu, item.SoTienLaiDaThu, item.SoTienLaiThuThieu, item.SoTienLaiThuThua,
                    "" // ghi chu
                });
            }

            return ds;
        }
    }
}
