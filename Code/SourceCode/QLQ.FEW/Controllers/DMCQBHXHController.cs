﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using FX.Core;
using log4net;
using QLQ.Core.Security;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.DM_CQBHXH)]
    [Authorize]
    public class DMCQBHXHController : BaseController
    {
        //
        // GET: /DMCQBHXH/
        private readonly IDMCQBHXHService _iDmcqbhxhService;
        private static readonly ILog Log = LogManager.GetLogger(typeof(DMCQBHXHController));

        public DMCQBHXHController()
        {
            _iDmcqbhxhService = IoC.Resolve<IDMCQBHXHService>();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DMCQBHXHPartial()
        {
            _iDmcqbhxhService.TimKiem = null;
            var list = _iDmcqbhxhService.GetAllData().ToList();
            return PartialView("DMCQBHXHPartial", list);
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Search(string timkiem)
        {
            List<QLQ_DMCQBHXH> model = new List<QLQ_DMCQBHXH>();
            _iDmcqbhxhService.TimKiem = timkiem;
            model = _iDmcqbhxhService.GetAllData().ToList();
            return PartialView("DMCQBHXHPartial", model);
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult AddNewPartial(QLQ_DMCQBHXH dmCqbhxh)
        {
            if (ModelState.IsValid)
            {
                if (_iDmcqbhxhService.GetById(dmCqbhxh.CQBHXH_ID) == null)
                {
                    try
                    {
                        dmCqbhxh.CREATION_DATE = DateTime.Now.Date;  //ngày tạo
                        dmCqbhxh.CREATED_BY = CurrentUser?.AccountId;
                        dmCqbhxh.ACTIVE = dmCqbhxh.ACTIVE == null ? 0 : dmCqbhxh.ACTIVE;
                        _iDmcqbhxhService.CreateNew(dmCqbhxh);
                        _iDmcqbhxhService.CommitChanges();
                        ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                        //log.Error("Danh mục đơn vị quản lý - Thêm mới: " + nguoidung.TENDANGNHAP);
                    }
                    catch (Exception e)
                    {
                        ViewData["EditError"] = e.Message;
                    }
                }
                else
                    ViewData["EditError"] = Resources.Localizing.MessageDuplicatekey;//in ra thông báo lỗi trùng mã
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return DMCQBHXHPartial();
        }

        // [RBACAuthorize(Permissions = "DM_DAUTU_UPDATE")]
        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult UpdatePartial(QLQ_DMCQBHXH dmCqbhxh)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    QLQ_DMCQBHXH temp = _iDmcqbhxhService.GetById(dmCqbhxh.CQBHXH_ID);
                    dmCqbhxh.ACTIVE = dmCqbhxh.ACTIVE == null ? 0 : dmCqbhxh.ACTIVE;
                    dmCqbhxh.CREATED_BY = temp.CREATED_BY;
                    dmCqbhxh.CREATION_DATE = temp.CREATION_DATE;
                    dmCqbhxh.LASTUPDATED_DATE = DateTime.Now;
                    dmCqbhxh.LASTUPDATE_BY = CurrentUser?.AccountId;
                    _iDmcqbhxhService.Clear();
                    _iDmcqbhxhService.Save(dmCqbhxh);
                    _iDmcqbhxhService.CommitChanges();
                    ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                    //log.Error("Danh mục đơn vị quản lý - Sửa: " + nguoidung.TENDANGNHAP);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                    //log.Error("Danh mục đơn vị quản lý - Sửa: " + nguoidung.FULL_NAME);
                }
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return DMCQBHXHPartial();
        }

        //[RBACAuthorize(Permissions = "DM_DAUTU_DELETE")]
        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.DELETE)]
        public ActionResult DeletePartial(QLQ_DMCQBHXH dmCqbhxh)
        {

            //code xóa bản ghi
            try
            {
                _iDmcqbhxhService.Delete(dmCqbhxh);
                _iDmcqbhxhService.CommitChanges();
                ViewData["EditSuccess"] = Resources.Localizing.MessageDelete;
                //log.Error("Danh mục đơn vị quản lý - Xóa: " + nguoidung.TENDANGNHAP);                    
            }
            catch (Exception)
            {
                ViewData["EditError"] = Resources.Localizing.MessageDate;
            }
            return DMCQBHXHPartial();
        }
    }
}
