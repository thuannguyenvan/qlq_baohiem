﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using FX.Core;
using log4net;
using QLQ.Core.Common;
using QLQ.Core.Security;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.DM_TOCHUCDAUTHAU)]
    [Authorize]
    public class DMToChucDauThauController : BaseController
    {
        //
        // GET: /dmtochucdauthau/
        private readonly IDM_TOCHUCDAUTHAUService _iDmToChucDauThauService;

        public DMToChucDauThauController()
        {
            _iDmToChucDauThauService = IoC.Resolve<IDM_TOCHUCDAUTHAUService>();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            //GetAllPermission();
            return View("Index");
        }

        public ActionResult FilterView()
        {
            return PartialView("FilterView");
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult DMTOCHUCDAUTHAUPartial(string timkiem)
        {
            List<QLQ_DMTOCHUCDAUTHAU> model = new List<QLQ_DMTOCHUCDAUTHAU>();
            model = _iDmToChucDauThauService.Search(timkiem);
            return PartialView("DMTOCHUCDAUTHAUPartial", model);
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Search(string timkiem)
        {
            return DMTOCHUCDAUTHAUPartial(timkiem);
        }

        //[RBACAuthorize(Permissions = "DM_TOCHUCDAUTHAU_ADD")]
        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult AddNewPartial(QLQ_DMTOCHUCDAUTHAU dmtochucdauthau)
        {
            if (ModelState.IsValid)
            {
                if (_iDmToChucDauThauService.GetByMa(dmtochucdauthau.ma_tochucdauthau) == null)//kiểm tra tồn tại ma_tochucdauthau
                {
                    //thêm mới bản ghi
                    try
                    {
                        //khai báo các trường tự tự động điền
                        //obj.CREATED_BY=;  //người tạo
                        dmtochucdauthau.creation_date = DateTime.Now.Date;  //ngày tạo
                        dmtochucdauthau.active = dmtochucdauthau.active == null ? 0 : dmtochucdauthau.active;
                        _iDmToChucDauThauService.CreateNew(dmtochucdauthau);
                        _iDmToChucDauThauService.CommitChanges();
                        ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                    }
                    catch (Exception e)
                    {
                        ViewData["EditError"] = e.Message;
                    }
                }
                else
                    ViewData["EditError"] = Resources.Localizing.MessageDuplicatekey;//in ra thông báo lỗi trùng mã
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return DMTOCHUCDAUTHAUPartial(null);
        }

        // [RBACAuthorize(Permissions = "DM_TOCHUCDAUTHAU_UPDATE")]
        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult UpdatePartial(QLQ_DMTOCHUCDAUTHAU dmtochucdauthau)
        {

            if (ModelState.IsValid)
            {
                //cập nhật bản ghi
                try
                {
                    QLQ_DMTOCHUCDAUTHAU temp = _iDmToChucDauThauService.GetById(dmtochucdauthau.tochucdauthau_id);
                    temp.ma_tochucdauthau = dmtochucdauthau.ma_tochucdauthau;
                    temp.ten_tochucdauthau = dmtochucdauthau.ten_tochucdauthau;
                    temp.dia_chi = dmtochucdauthau.dia_chi;
                    temp.daidien = dmtochucdauthau.daidien;
                    temp.chucvu = dmtochucdauthau.chucvu;
                    temp.dien_thoai = dmtochucdauthau.dien_thoai;
                    temp.fax = dmtochucdauthau.fax;
                    temp.so_taikhoanthanhtoan = dmtochucdauthau.so_taikhoanthanhtoan;
                    temp.noidk_taikhoanthanhtoan = dmtochucdauthau.noidk_taikhoanthanhtoan;
                    temp.so_cmt = dmtochucdauthau.so_cmt;
                    temp.ngaycap = dmtochucdauthau.ngaycap;
                    temp.noicap = dmtochucdauthau.noicap;
                    temp.active = dmtochucdauthau.active == null ? 0 : dmtochucdauthau.active;
                    //temp.LASTUPDATE_BY =;   //người sửa
                    temp.lastupdated_date = DateTime.Now.Date;
                    _iDmToChucDauThauService.Clear();
                    _iDmToChucDauThauService.Save(temp);
                    _iDmToChucDauThauService.CommitChanges();
                    _iDmToChucDauThauService.CommitChanges();
                    ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return DMTOCHUCDAUTHAUPartial(null);
        }

        //[RBACAuthorize(Permissions = "DM_TOCHUCDAUTHAU_DELETE")]
        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.DELETE)]
        public ActionResult DeletePartial(QLQ_DMTOCHUCDAUTHAU dmtochucdauthau)
        {

            //code xóa bản ghi
            try
            {
                _iDmToChucDauThauService.Delete(dmtochucdauthau);
                _iDmToChucDauThauService.CommitChanges();
                ViewData["EditSuccess"] = Resources.Localizing.MessageDelete;
            }
            catch (Exception)
            {
                ViewData["EditError"] = Resources.Localizing.MessageDate;
            }
            return DMTOCHUCDAUTHAUPartial(null);
        }
    }
}
