﻿using DevExpress.Web.Mvc;
using FX.Core;
using QLQ.Core.Common;
using QLQ.Core.CustomView;
using QLQ.Core.IService;
using QLQ.Core.Security;
using QLQ.FEW.Models;
using QLQ.FEW.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.PHIEU_C89)]
    [Authorize]
    public class PhieuC89Controller : BaseController
    {
        private readonly IDM_HINHTHUCDTUService _iDmHinhthucdtuService;
        private readonly IDM_DAUTUService _iDmDautuService;
        private readonly IDM_HOPDONGService _iDmHopdongService;
        private readonly IHopDongService _iHopDongService;
        private readonly IPhieuC89Service _iPhieuC89Service;
        private readonly IHS_KHACHHANGService _iHsKhachHangService;

        public PhieuC89Controller()
        {
            _iHopDongService = IoC.Resolve<IHopDongService>();
            _iDmDautuService = IoC.Resolve<IDM_DAUTUService>();
            _iDmHopdongService = IoC.Resolve<IDM_HOPDONGService>();
            _iDmHinhthucdtuService = IoC.Resolve<IDM_HINHTHUCDTUService>();
            _iPhieuC89Service = IoC.Resolve<IPhieuC89Service>();
            _iHsKhachHangService = IoC.Resolve<IHS_KHACHHANGService>();
            GetFilterData();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DanhMucDauTuComboBoxPartial(string hinhThucDauTuId)
        {
            var listDmDauTu = new List<CommonValue>();
            long hinhThucDauTu = 0;
            if (long.TryParse(hinhThucDauTuId, out hinhThucDauTu))
            {
                listDmDauTu = _iDmDautuService.GetAll()
                    .Where(i => i.hinhthuc_id == hinhThucDauTu)
                    .OrderBy(i => i.ten_dautu)
                    .Select(i => new CommonValue { Text = i.ten_dautu, Value = i.dautu_id.ToString() })
                    .ToList();
            }
            listDmDauTu.Insert(0, new CommonValue { Value = "", Text = "Chọn" });
            return PartialView("DanhMucDauTuComboBoxPartial", listDmDauTu);
        }

        public ActionResult HopDongComboBoxPartial(string danhMucHopDongId)
        {
            var listHopDong = new List<CommonValue>();
            long danhMucHopDong = 0;
            if (long.TryParse(danhMucHopDongId, out danhMucHopDong))
            {
                listHopDong = _iHopDongService.GetAll()
                    .Where(i => i.hopdong_id == danhMucHopDong)
                    .OrderBy(i => i.so_hopdong)
                    .Select(i => new CommonValue { Text = i.so_hopdong, Value = i.qlhopdong_id.ToString() })
                    .ToList();
            }
            listHopDong.Insert(0, new CommonValue { Value = "", Text = "Chọn" });
            return PartialView("HopDongComboBoxPartial", listHopDong);
        }

        public ActionResult C89GridViewPartial()
        {
            return PartialView("C89GridViewPartial", _iPhieuC89Service.Search(new PhieuC89SearchModel()));
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Search(PhieuC89SearchModel request)
        {
            return PartialView("C89GridViewPartial", _iPhieuC89Service.Search(request));
        }

        [ActionPermissions(QlqActions.PRINT)]
        public ActionResult Print(long id)
        {
            try
            {
                var phieuC89 = _iPhieuC89Service.Getbykey(id);
                if (phieuC89 == null || phieuC89.HopDong == null)
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
                var report = new BC_C89HD_PhieuTinhLaiPhaiThuPhatSinhTrongNam();
                var khachHang = _iHsKhachHangService.Getbykey(phieuC89.HopDong.khachhang_id);
                if (khachHang != null)
                {
                    report.lblTenDonViVayVon.Text = khachHang.TEN_KHACHHANG;
                }
                report.lblSoPhieu.Text = "Số: " + phieuC89.SoPhieu;
                report.lblSoHopDong.Text = phieuC89.HopDong.so_hopdong;
                if (phieuC89.HopDong.NGAY_LAP.HasValue)
                {
                    report.lblNgayVay.Text = phieuC89.HopDong.NGAY_LAP.Value.ToString(Resources.Localizing.DateFormat);
                }
                if (phieuC89.HopDong.han_hopdong.HasValue)
                {
                    report.lblHanTra.Text = phieuC89.HopDong.han_hopdong.Value.ToString(Resources.Localizing.DateFormat);
                }
                report.lblSoTienVay.Text = phieuC89.HopDong.sotien.ToString("N0");
                report.lblLaiSuatChoVay.Text = phieuC89.HopDong.laisuat.ToString("N2");
                if (phieuC89.LaiSuatDieuChinh.HasValue)
                {
                    report.lblLaiSuatDieuChinh.Text = phieuC89.LaiSuatDieuChinh.Value.ToString("N2");
                }
                report.lblTuNgay.Text = phieuC89.TuNgay.ToString(Resources.Localizing.DateFormat);
                report.lblDenNgay.Text = phieuC89.DenNgay.ToString(Resources.Localizing.DateFormat);
                report.lblTongSoLaiPhaiThu.Text = phieuC89.TongSoLaiPhaiThu.ToString("N0");
                report.lblLaiPhaiThuDenHan.Text = phieuC89.LaiPhaiThuTinhDenHan.ToString("N0");
                report.lblLaiPhaiThuChuaDenHan.Text = phieuC89.LaiPhaiThuChuaDenHan.ToString("N0");
                report.lblLaiPhaiThuChuaDenHanBangChu.Text = Common.moneyToString(phieuC89.LaiPhaiThuChuaDenHan) + ")";
                report.lblLaiPhaiThuDenHanBangChu.Text = Common.moneyToString(phieuC89.LaiPhaiThuTinhDenHan) + ")";
                report.lblSoTienVayBangChu.Text = Common.moneyToString(phieuC89.HopDong.sotien) + ")";
                report.lblTongSoLaiPhaiThuBangChu.Text = Common.moneyToString(phieuC89.TongSoLaiPhaiThu) + ")";
                report.lblDate.Text = string.Format("Ngày {0} tháng {1} năm {2}", DateTime.Now.ToString("dd"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("yyyy"));
                return PartialView("ReportPartial", report);
            }
            catch (Exception)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        private void GetFilterData()
        {
            var listHinhThucDauTu = _iDmHinhthucdtuService.GetAll()
                .OrderBy(i => i.ten_hinhthuc)
                .Select(i => new CommonValue { Text = i.ten_hinhthuc, Value = i.hinhthuc_id.ToString() })
                .ToList();
            listHinhThucDauTu.Insert(0, new CommonValue { Value = "", Text = "Chọn" });
            ViewBag.ListHinhThuc = listHinhThucDauTu;
            var listDmHopDong = _iDmHopdongService.GetAll()
                .OrderBy(i => i.ten_hopdong)
                .Select(i => new CommonValue { Text = i.ten_hopdong, Value = i.hopdong_id.ToString() })
                .ToList();
            listDmHopDong.Insert(0, new CommonValue { Value = "", Text = "Chọn" });
            ViewBag.ListTenHopDong = listDmHopDong;
        }
    }
}
