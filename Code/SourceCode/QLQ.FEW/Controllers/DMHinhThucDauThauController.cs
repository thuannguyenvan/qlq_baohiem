﻿using FX.Core;
using log4net;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using QLQ.Core.Security;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.DM_HINHTHUCDAUTHAU)]
    [Authorize]
    public class DMHinhThucDauThauController : BaseController
    {
        private readonly IDM_HINHTHUCDAUTHAUService _iDmHinhThucDauThauService;

        //
        // GET: /DMHinhThucDauThau/

        public DMHinhThucDauThauController()
        {
            _iDmHinhThucDauThauService = IoC.Resolve<IDM_HINHTHUCDAUTHAUService>();
        }

        public ActionResult Index()
        {
            return View();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult DMHinhThucDauThauPartial(string timkiem)
        {
            List<QLQ_DMHINHTHUCDAUTHAU> model = new List<QLQ_DMHINHTHUCDAUTHAU>();
            model = _iDmHinhThucDauThauService.Search(timkiem);
            return PartialView("DMHinhThucDauThauPartial", model);
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Search(string timkiem)
        {
            return DMHinhThucDauThauPartial(timkiem);
        }

        public ActionResult AddNewPartial(QLQ_DMHINHTHUCDAUTHAU obj)
        {
            if (ModelState.IsValid)
            {
                if (_iDmHinhThucDauThauService.GetByMa(obj.ma_hinhthuc) == null)//kiểm tra tồn tại MA_HINHTHUC
                {
                    //thêm mới bản ghi
                    try
                    {
                        //khai báo các trường tự tự động điền
                        //obj.CREATED_BY=;  //người tạo
                        obj.creation_date = DateTime.Now.Date;  //ngày tạo
                        obj.active = obj.active == null ? 0 : obj.active;
                        _iDmHinhThucDauThauService.CreateNew(obj);
                        _iDmHinhThucDauThauService.CommitChanges();
                        ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                        //log.Error("Danh mục đơn vị quản lý - Thêm mới: " + nguoidung.TENDANGNHAP);
                    }
                    catch (Exception e)
                    {
                        ViewData["EditError"] = e.Message;
                    }
                }
                else
                    ViewData["EditError"] = Resources.Localizing.MessageDuplicatekey;//in ra thông báo lỗi trùng mã
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return DMHinhThucDauThauPartial(null);
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult UpdatePartial(QLQ_DMHINHTHUCDAUTHAU obj)
        {
            if (ModelState.IsValid)
            {
                //cập nhật bản ghi
                try
                {
                    QLQ_DMHINHTHUCDAUTHAU temp = _iDmHinhThucDauThauService.GetById(obj.hinhthuc_id);
                    temp.ma_hinhthuc = obj.ma_hinhthuc;
                    temp.ten_hinhthuc = obj.ten_hinhthuc;
                    temp.active = obj.active == null ? 0 : obj.active;
                    //temp.LASTUPDATE_BY =;   //người sửa
                    temp.lastupdated_date = DateTime.Now.Date;
                    _iDmHinhThucDauThauService.Clear();
                    _iDmHinhThucDauThauService.Save(temp);
                    _iDmHinhThucDauThauService.CommitChanges();
                    ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                    //log.Error("Danh mục đơn vị quản lý - Sửa: " + nguoidung.TENDANGNHAP);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return DMHinhThucDauThauPartial(null);
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.DELETE)]
        public ActionResult DeletePartial(QLQ_DMHINHTHUCDAUTHAU obj)
        {
            //code xóa bản ghi
            try
            {
                _iDmHinhThucDauThauService.Delete(obj);
                _iDmHinhThucDauThauService.CommitChanges();
                ViewData["EditSuccess"] = Resources.Localizing.MessageDelete;

            }
            catch (Exception)
            {
                ViewData["EditSuccess"] = Resources.Localizing.MessageDeleteError;
            }
            return DMHinhThucDauThauPartial(null);
        }
    }
}
