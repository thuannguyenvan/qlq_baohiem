﻿using FX.Core;
using QLQ.Core.CustomView;
using QLQ.Core.IService;
using QLQ.Core.Security;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.SOCHTIETLAIDAUTUQUYBHXH)]
    [Authorize]
    public class SoChiTietTienLaiDauTuQuyBHXHController : BaseController
    {
        private readonly ILAISUATNH_DTLService _iLaiSuatNhDtlService;
        private readonly ILAISUATNH_HDRService _iLaiSuatNhHdrService;
        private readonly IHopDongService _iHopDongService;
        //
        // GET: /SoTheoDoi/
        public SoChiTietTienLaiDauTuQuyBHXHController()
        {
            _iLaiSuatNhDtlService = IoC.Resolve<ILAISUATNH_DTLService>();
            _iLaiSuatNhHdrService = IoC.Resolve<ILAISUATNH_HDRService>();
            _iHopDongService = IoC.Resolve<IHopDongService>();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult SoChiTietTienLaiDauTuQuyBHXHPartial()
        {
            ViewBag.ListHopdong = _iHopDongService.GetAll();
            return PartialView("SoChiTietTienLaiDauTuQuyBHXHPartial");
        }

        [HttpPost]
        [ActionPermissions(QlqActions.PRINT)]
        public ActionResult fn_InAn(int? sohopdong/*,string tendonvi,  int? thoihanvay, DateTime? ngayhd, decimal? laisuat*/)
        {

            List<SoChiTietTienLaiDauTuBHXHModel> lst = new List<SoChiTietTienLaiDauTuBHXHModel> {
            new SoChiTietTienLaiDauTuBHXHModel() {
                ngay_ghiso = DateTime.Now.Date,
                soct = 1, ngay_ct = DateTime.Now.Date,
                diengiai = "xxx", tienvay = 1231212312,
                laisuat = "5%", songaytinhlai = 365,
                phaithu =213123,dathu=1231,cl=434
            } };

            SoChiTietLaiDauTuQuyBHXH_Report report = new SoChiTietLaiDauTuQuyBHXH_Report();
            report.DataSource = lst;
            return PartialView("ReportSoChiTietTienLaiDauTuQuyBHXH", report);
        }

    }
}
