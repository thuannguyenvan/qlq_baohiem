﻿using FX.Core;
using QLQ.Core.CustomView;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using QLQ.Core.Security;
using QLQ.Core.StoreProcedure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.TH_SODUDAUTUMOI)]
    [Authorize]
    public class SoDauTuMoiController : BaseController
    {
        private readonly IHopDongService _iHopDongService;
        private List<QLQ_DMDAUTU> _listDmDauTu ;
        private readonly IDM_DAUTUService _iDmDautuService;
        private readonly IHS_KHACHHANGService _iHSKhachHangService;
        private List<HS_KHACHHANG> _listHsKhachhang;

        public SoDauTuMoiController()
        {
            _iHopDongService = IoC.Resolve<IHopDongService>();
            _iHSKhachHangService =IoC.Resolve<IHS_KHACHHANGService>();
            _listHsKhachhang = new List<HS_KHACHHANG>();
            _iDmDautuService = IoC.Resolve<IDM_DAUTUService>();
            _listDmDauTu = new List<QLQ_DMDAUTU>();
            GetDataFilter();
        }
        public void GetDataFilter()
        {

            _listDmDauTu =_iDmDautuService.GetAll().ToList();
          
            ViewData["ListDMDauTu"] = _listDmDauTu;
        }
       
        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            return View();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Search(long? DautuId, string FromDate, string ToDate, long? KhachHangId)
        {
            var model = _iHopDongService.Query.Where(s => s.IsDeleted == 0);
            if (DautuId.HasValue)
                model = model.Where(m => m.DAUTU_ID == DautuId.Value);
            if (!string.IsNullOrEmpty(FromDate))
            {
                var fromdate = DateTime.Parse(FromDate);
                model = model.Where(m => m.ngay_hieuluc >= fromdate);
            }

            if (!string.IsNullOrEmpty(ToDate))
            {
                var todate = DateTime.Parse(ToDate);
                model = model.Where(m => m.ngay_hieuluc <= todate);
            }

            if (KhachHangId.HasValue)
                model = model.Where(m => m.khachhang_id == KhachHangId.Value);

            return PartialView("SoDauTuMoiPartial", model.ToList());
        }

        public ActionResult ReturnKhachHangPartial()
        {
            long DmDautuid = Request.Params["dmDauTu"] == null ? 0 : long.Parse(Request.Params["dmDauTu"]);
            var listResult = _iHSKhachHangService.Query.Where(t => t.DAUTU_ID == DmDautuid);
            return PartialView("ReturnKhachHangPartial", listResult);
        }
       //public ActionResult ReturnDatetimePartial()
       // {

       //     return PartialView("ReturnDatetimePartial");
       // }
        public ActionResult SoDauTuMoiPartial()
        {
            List<QLQ_HOPDONG> model = new List<QLQ_HOPDONG>();
            var now = DateTime.Now;
            var startOfYear = new DateTime(now.Year, 1, 1);
            var endOfYear = new DateTime(now.Year, 12, 31);
            model = _iHopDongService.Query.Where(s => s.IsDeleted == 0 && s.ngay_hieuluc >= startOfYear && s.ngay_hieuluc <= endOfYear).ToList();
            return PartialView("SoDauTuMoiPartial", model);
        }
    }
}
