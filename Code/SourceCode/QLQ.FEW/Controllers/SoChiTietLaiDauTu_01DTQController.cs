﻿using DevExpress.Web.Mvc;
using FX.Core;
using QLQ.Core.CustomView;
using QLQ.Core.IService;
using QLQ.Core.Security;
using QLQ.Core.StoreProcedure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QLQ.FEW.Reports;
using System.Data;
using DevExpress.XtraReports.UI;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.SOCHITIETLAIDAUTU_01DTQ)]
    [Authorize]
    public class SoChiTietLaiDauTu_01DTQController : BaseController
    {
        //
        // GET: /SoChiTietLaiDauTu_01DTQ/
        private readonly IHopDongService _iHopDongService;
        private readonly ISoChiTietLaiDauTuService _iSoChiTietDauTu;
        private readonly QLQStoreProcedure _storeProcedure;

        private long? HopDongId
        {
            get
            {
                long? id = null;
                if (TempData["hopdong_id"] != null)
                {
                    id = long.Parse(TempData["hopdong_id"].ToString());
                }
                return id;
            }
            set
            {
                TempData["hopdong_id"] = value;
            }
        }

        private decimal TongTienLaiPhaiThu { get; set; }
        private decimal TongTienLaiDaThu { get; set; }
        private decimal TongTienLaiCLThuaThieu { get; set; }

        public SoChiTietLaiDauTu_01DTQController()
        {
            _iHopDongService = IoC.Resolve<IHopDongService>();
            _iSoChiTietDauTu = IoC.Resolve<ISoChiTietLaiDauTuService>();
            _storeProcedure = new QLQStoreProcedure();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SoDauTu(long hopdongid)
        {
            HopDongId = hopdongid;
            var sodautuLaiSuat = _iHopDongService.GetLaiSuatDauTu(hopdongid, DateTime.Now);
            return View("Detail", sodautuLaiSuat);
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult SearchByThangHopdong(DateTime date)
        {
            var model = _iHopDongService.GetLaiSuatDauTu(HopDongId.HasValue ? HopDongId.Value : 0, date);
            HopDongId = HopDongId; // assign for next request
            return PartialView("SoDauTuChiTietPartial", model);
        }


        public ActionResult SoDauTuChiTietPartial(LaiSuatDauTuModel model)
        {
            return PartialView(model);
        }

        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult BatchEditingUpdateModel(MVCxGridViewBatchUpdateValues<ThongTinLaiSuat, object> updateValues)
        {

            bool isDataChanged = false;
            foreach (var update in updateValues.Update)
            {
                var model = _iSoChiTietDauTu.Getbykey(update.SoChiTietId);
                if (model != null && model.SOCHITIET_ID > 0)
                {
                    isDataChanged = true;
                    model.NGAYCHUNGTU = update.NgayChungTu;
                    model.NGAYGHISO = update.NgayGhiSo;
                    model.SOCHUNGTU = update.SoChungTu;
                    model.SOTIENDATHU = update.SoTienDaThu;
                    _iSoChiTietDauTu.Save(model);
                }
            }
            if (isDataChanged)
            {
                _iSoChiTietDauTu.CommitChanges();
            }
            var sodautuLaiSuat = _iHopDongService.GetLaiSuatDauTu(HopDongId.HasValue ? HopDongId.Value : 0);
            return PartialView("SoDauTuChiTietPartial", sodautuLaiSuat.DanhSachLaiSuat);
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult SearchByThang(DateTime date)
        {
            var model = _storeProcedure.SP_GetLaiSuatDauTuTheoThang(date);
            return PartialView("SoDauTuChiTietHangThangPartial", model);
        }

        public ActionResult SoDauTuChiTietHangThangPartial()
        {
            var model = _storeProcedure.SP_GetLaiSuatDauTuTheoThang(DateTime.Now);
            return PartialView("SoDauTuChiTietHangThangPartial", model);
        }

        public ActionResult fn_InAn()
        {
            int month = DateTime.Now.Month;
            int year = DateTime.Now.Year;

            // lay thong tin hop dong 
            var hopdong = _iHopDongService.GetLaiSuatDauTu(HopDongId.HasValue ? HopDongId.Value : 0);

            BC_01_DTQ report = new BC_01_DTQ();
            report.lbThangNam.Text = string.Format("Tháng {0} năm {1}", month, year);

            report.lbTenDonVi.Text = string.Format("{0}", hopdong.ThongTinHopDong.TenKhachHang);
            report.lbTenHopDong.Text = string.Format("{0}", hopdong.ThongTinHopDong.TenHopDong);
            report.lbNgayKyHD.Text = string.Format("{0}", hopdong.ThongTinHopDong.NgayLap.ToString("dd/MM/yyyy"));
            report.lbThoiHanHD.Text = string.Format("{0}", hopdong.ThongTinHopDong.ThoiHan);
            report.lbLaiSuat.Text = string.Format("{0}%", hopdong.ThongTinHopDong.LaiSuat);

            // tao data source cho phan danh sach thong tin lai suat
            report.DataSource = FillDataBaoCao(hopdong.DanhSachLaiSuat.Items).Tables[0];

            // bind data
            report.ngayThangNamGhiSo.DataBindings.Add("Text", null, "ngayThangNamGhiSo");
            report.soChungTu.DataBindings.Add("Text", null, "soChungTu");
            report.ngayChungTu.DataBindings.Add("Text", null, "ngayChungTu");
            report.dienGiai.DataBindings.Add("Text", null, "dienGiai");
            report.soTienChoVay.DataBindings.Add("Text", null, "soTienChoVay", "{0:n0}");
            report.laiSuat.DataBindings.Add("Text", null, "laiSuat");
            report.soNgayTinhLai.DataBindings.Add("Text", null, "soNgayTinhLai");
            report.soTienLaiPhaiThu.DataBindings.Add("Text", null, "soTienLaiPhaiThu", "{0:n0}");
            report.soTienLaiDaThu.DataBindings.Add("Text", null, "soTienLaiDaThu", "{0:n0}");
            report.soTienLaiThuaThieu.DataBindings.Add("Text", null, "soTienLaiThuaThieu", "{0:n0}");

            report.lbTongTienLaiPhaiThu_Nam.Text = TongTienLaiPhaiThu.ToString("n0");
            report.lbTongTienLaiDaThu_Nam.Text = TongTienLaiDaThu.ToString("n0");
            report.lbTongTienLaiCLThuaThieu_Nam.Text = TongTienLaiCLThuaThieu.ToString("n0");

            return PartialView("Report01DQTPartial", report);
        }

        private DataSet FillDataBaoCao(List<ThongTinLaiSuat> danhsachThongTinLaiSuat)
        {
            TongTienLaiPhaiThu = TongTienLaiDaThu = TongTienLaiCLThuaThieu = 0;

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            ds.Tables.Add(dt);

            dt.TableName = "bc01DTQ";
            dt.Columns.Add("ngayThangNamGhiSo", typeof(string));
            dt.Columns.Add("soChungTu", typeof(string));
            dt.Columns.Add("ngayChungTu", typeof(string));
            dt.Columns.Add("dienGiai", typeof(string));
            dt.Columns.Add("soTienChoVay", typeof(long));
            dt.Columns.Add("laiSuat", typeof(float));
            dt.Columns.Add("soNgayTinhLai", typeof(int));
            dt.Columns.Add("soTienLaiPhaiThu", typeof(long));
            dt.Columns.Add("soTienLaiDaThu", typeof(long));
            dt.Columns.Add("soTienLaiThuaThieu", typeof(long));

            foreach (ThongTinLaiSuat item in danhsachThongTinLaiSuat)
            {
                TongTienLaiPhaiThu += item.SoTienPhaiThu;
                TongTienLaiDaThu += item.SoTienDaThu;
                if (item.SoTienThuaThieu != null)
                {
                    TongTienLaiCLThuaThieu += (decimal)item.SoTienThuaThieu;
                } else
                {
                    TongTienLaiCLThuaThieu += 0;
                }
                var ngayghiSo = item.NgayGhiSo.HasValue ? item.NgayGhiSo.Value.ToString("dd/MM/yyyy") : string.Empty;
                var ngaychungtu = item.NgayChungTu.HasValue ? item.NgayChungTu.Value.ToString("dd/MM/yyyy") : string.Empty;
                ds.Tables["bc01DTQ"].Rows.Add(new Object[] { ngayghiSo, item.SoChungTu, ngaychungtu, item.DienGiai, item.SoTienChoVay, item.LaiSuat, item.SoNgayTinhLai, item.SoTienPhaiThu, item.SoTienDaThu, item.SoTienThuaThieu });
            }

            return ds;
        }
    }
}
