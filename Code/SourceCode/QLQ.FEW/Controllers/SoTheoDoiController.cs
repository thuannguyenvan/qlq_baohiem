﻿using FX.Core;
using log4net;
using QLQ.Core.CustomView;
using QLQ.Core.IService;
using System.Collections.Generic;
using System.Web.Mvc;
using QLQ.Core.StoreProcedure;
using System.Linq;
using QLQ.FEW.Helper;
using QLQ.Core.Security;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.SOTHEODOI)]
    [Authorize]
    public class SoTheoDoiController : BaseController
    {
        private readonly IDM_NGANHANGService _iDmNganHangService;
        private readonly ILAISUATNH_DTLService _iLaiSuatNhDtlService;
        private readonly ILAISUATNH_HDRService _iLaiSuatNhHdrService;
        private static readonly ILog Log = LogManager.GetLogger(typeof(SoTheoDoiController));
        private readonly QLQStoreProcedure _spQlq = new QLQStoreProcedure();
        //
        // GET: /SoTheoDoi/
        public SoTheoDoiController()
        {
            _iDmNganHangService = IoC.Resolve<IDM_NGANHANGService>();
            _iLaiSuatNhDtlService = IoC.Resolve<ILAISUATNH_DTLService>();
            _iLaiSuatNhHdrService = IoC.Resolve<ILAISUATNH_HDRService>();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SoTheoDoiPartial()
        {
            ViewBag.ListNganHang = _iDmNganHangService.GetAll();
            return PartialView("SoTheoDoiPartial");
        }

        [HttpPost]
        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult fn_InAn(long? nganHang, int year, int cnTc, int type)
        {
            List<record_SoTheoDoi> records = new List<record_SoTheoDoi>();
            if (cnTc == 0 && type == 0)
                records = _spQlq.sp_SoTheoDoi(nganHang, year, (int)ELoaiLaiSuat.CaNhanKyCuoi);//cuối kì cá nhân
            else if (cnTc == 0 && type == 1)
                records = _spQlq.sp_SoTheoDoi(nganHang, year, (int)ELoaiLaiSuat.CaNhanHangThang);//hàng tháng cá nhân
            else if (cnTc == 1 && type == 0)                
                records = _spQlq.sp_SoTheoDoi(nganHang, year, (int)ELoaiLaiSuat.ToChucKyCuoi);//cuối kì tổ chức
            else
                records = _spQlq.sp_SoTheoDoi(nganHang, year, (int)ELoaiLaiSuat.ToChucHangThang);//hàng tháng tổ chức
            List<SotheoDoiModel> lst = new List<SotheoDoiModel>();
            SotheoDoiModel model = new SotheoDoiModel();
            decimal id = 0;
            if (records.Count > 0) id = records[0].LaiSuatId;//nếu có dữ liệu trả về thì gán id vào để check
            LaiSuatNHModel lsHuyDong = new LaiSuatNHModel();

            foreach (var item in records)
            {
                if (id == item.LaiSuatId)
                {
                    //gán các trường vào model
                    setSoTheoDoiDetail(ref model, item, lsHuyDong);
                }
                if (id != item.LaiSuatId)
                {
                    // add the old to list
                    lst.Add(model);
                    id = item.LaiSuatId;
                    // khoi tao cai moi
                    model = new SotheoDoiModel();
                    setSoTheoDoiDetail(ref model, item, lsHuyDong);
                }
            }

            if (id > 0)
                lst.Add(model);
            lst = lst.OrderBy(t => t.NGAY_HIEULUC).ToList();
            SoTheoDoi_Report report = new SoTheoDoi_Report();
            report.DataSource = lst;
            report.txt_nam.Text = year.ToString();
            report.txt_name.Text = _iDmNganHangService.GetById(nganHang).TEN_NGANHANG;
            return PartialView("ReportSoTheoDoi", report);
        }

        [ActionPermissions(QlqActions.VIEW)]
        private void setSoTheoDoiDetail(ref SotheoDoiModel model, record_SoTheoDoi item, LaiSuatNHModel lsHuyDong)
        {
            model.NGAY_HIEULUC = item.NGAY_HIEULUC;
            model.SO_CVDEN = item.SO_CVDEN;
            model.NGAY_CVDEN = item.NGAY_CVDEN;
            model.SO_CVNH = item.SO_CVNH;
            model.NGAY_CVNH = item.NGAY_CVNH;
            model.THONGBAO_NH = item.THONGBAO_NH;
            model.NGAY_THONGBAO = item.NGAY_THONGBAO;
            long? so_thang = lsHuyDong.getSoThang(item.KYHAN_ID);
            if (so_thang == null)
                so_thang = 0;
            if (so_thang == -1) model.KKH = item.Value;
            else if (so_thang == 0) model.Under1Month = item.Value;
            else if (so_thang == 1) model.Month1 = item.Value;
            else if (so_thang == 2) model.Month2 = item.Value;
            else if (so_thang == 3) model.Month3 = item.Value;
            else if (so_thang == 4) model.Month4 = item.Value;
            else if (so_thang == 5) model.Month5 = item.Value;
            else if (so_thang == 6) model.Month6 = item.Value;
            else if (so_thang == 7) model.Month7 = item.Value;
            else if (so_thang == 8) model.Month8 = item.Value;
            else if (so_thang == 9) model.Month9 = item.Value;
            else if (so_thang == 10) model.Month10 = item.Value;
            else if (so_thang == 11) model.Month11 = item.Value;
            else if (so_thang == 12) model.Month12 = item.Value;
            else if (so_thang == 18) model.Month18 = item.Value;
            else if (so_thang == 24) model.Month24 = item.Value;
            else if (so_thang == 36) model.Month36 = item.Value;
            else if (so_thang == 60) model.Month60 = item.Value;
        }
    }
}
