﻿using System;
using System.Linq;
using System.Web.Mvc;
using QLQ.Core.IService;
using FX.Core;
using QLQ.Core.Domain;
using log4net;
using DevExpress.Web;
using DevExpress.Web.Mvc;
using QLQ.Core.Common;
using QLQ.Core.Security;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.HOSO)]
    [Authorize]
    public class HOSOController : BaseController
    {
        //
        // GET: /HOSO/
        public const string UploadDirectory = @"/Uploads/HoSo/";

        private readonly IHOSOService _iHoSoSvc;
        private readonly IDM_NGANHANGService _iDmNganHangSvc;

        public HOSOController()
        {
            _iHoSoSvc = IoC.Resolve<IHOSOService>();
            _iDmNganHangSvc = IoC.Resolve<IDM_NGANHANGService>();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            return View();
        }

        //public ActionResult Search()
        //{
        //    var strTimKiem = Request.Params["timkiemS"] ?? "";
        //    ViewData["DM_NGANHANG"] = _iDmNganHangSvc.Query.ToList();
        //    var dmKH = _iHoSoSvc.Search(strTimKiem);
        //    return PartialView("HOSOPartial", dmKH);
        //}


        public ActionResult HOSOPartial()
        {
            ViewData["DM_NGANHANG"] = _iDmNganHangSvc.Query.ToList();
            var dmKH = _iHoSoSvc.Query.Select(x => new { x.HOSO_ID, x.NGANHANG_ID, UPLOADS_DLNH = UploadDirectory + x.UPLOADS_DLNH, UPLOADS_GPKD = UploadDirectory + x.UPLOADS_GPKD, UPLOADS_GUQ = UploadDirectory + x.UPLOADS_GUQ, UPLOADS_BCTC = UploadDirectory + x.UPLOADS_BCTC });
            return PartialView("HOSOPartial", dmKH);
        }

        public ActionResult PopupSaveData()
        {
            ViewData["DM_NGANHANG"] = _iDmNganHangSvc.Query.ToList();
            return PartialView();
        }

        public ActionResult DragAndDropImageUpload_DLNH()
        {
            UploadedFile[] files = UploadControlExtension.GetUploadedFiles("ctrUpload_DLNH", UploadControlDemosHelper.ValidationSettings, uc_FileUploadComplete);
            return null;
        }

        public ActionResult DragAndDropImageUpload_GPKD()
        {
            UploadedFile[] files = UploadControlExtension.GetUploadedFiles("ctrUpload_GPKD", UploadControlDemosHelper.ValidationSettings, uc_FileUploadComplete);
            return null;
        }

        public ActionResult DragAndDropImageUpload_GUQ()
        {
            UploadedFile[] files = UploadControlExtension.GetUploadedFiles("ctrUpload_GUQ", UploadControlDemosHelper.ValidationSettings, uc_FileUploadComplete);
            return null;
        }

        public ActionResult DragAndDropImageUpload_BCTC()
        {
            UploadedFile[] files = UploadControlExtension.GetUploadedFiles("ctrUpload_BCTC", UploadControlDemosHelper.ValidationSettings, uc_FileUploadComplete);
            return null;
        }

        public void uc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                string resultFilePath = Request.MapPath(UploadDirectory + e.UploadedFile.FileName);

                try
                {
                    bool dirExist = System.IO.Directory.Exists(Server.MapPath(UploadDirectory));
                    if (!dirExist) // neu chua co thu muc
                    {
                        System.IO.Directory.CreateDirectory(UploadDirectory);
                    }

                    //Lưu file upload vào thư mục
                    e.UploadedFile.SaveAs(resultFilePath, true);
                    System.Web.UI.IUrlResolutionService urlResolver = sender as System.Web.UI.IUrlResolutionService;
                    if (urlResolver != null)
                    {
                        //e.CallbackData = urlResolver.ResolveClientUrl(resultFilePath);
                        e.CallbackData = UploadDirectory + e.UploadedFile.FileName;
                    }
                }
                catch (Exception ex) { }
            }
        }


        [ActionPermissions(QlqActions.DELETE)]
        public ActionResult DeletePartial(HOSO hoso)
        {
            try
            {
                _iHoSoSvc.Delete(hoso);
                _iHoSoSvc.CommitChanges();
                ViewData["EditSuccess"] = Resources.Localizing.MessageDelete;
            }
            catch (Exception ex)
            {
                ViewData["EditError"] = Resources.Localizing.MessageDate;
                //Log.Error("DeletePartial Xoa: " + ex.Message);
            }
            return HOSOPartial();
        }

        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult LuuHoSo(HOSO hoso, string dlnh, string gpkd, string guq, string bctc)
        {
            hoso.UPLOADS_DLNH = dlnh;
            hoso.UPLOADS_GPKD = gpkd;
            hoso.UPLOADS_GUQ = guq;
            hoso.UPLOADS_BCTC = bctc;
            try
            {
                if (hoso.HOSO_ID == null)
                {
                    hoso.CREATION_DATE = DateTime.Now;
                    _iHoSoSvc.Clear();
                    _iHoSoSvc.CreateNew(hoso);
                }
                else
                {
                    var model = _iHoSoSvc.GetById(hoso.HOSO_ID);
                    if (model != null)
                    {
                        model.NGANHANG_ID = hoso.NGANHANG_ID;
                        model.UPLOADS_DLNH = hoso.UPLOADS_DLNH;
                        model.UPLOADS_GPKD = hoso.UPLOADS_GPKD;
                        model.UPLOADS_GUQ = hoso.UPLOADS_GUQ;
                        model.UPLOADS_BCTC = hoso.UPLOADS_BCTC;

                        model.LASTUPDATED_DATE = DateTime.Now;
                        _iHoSoSvc.Clear();
                        _iHoSoSvc.Update(model);
                    }
                    else
                    {
                        return Json("Fail", JsonRequestBehavior.AllowGet);
                    }
                }
                _iHoSoSvc.CommitChanges();
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Fail", JsonRequestBehavior.AllowGet);
                //Log.Error("LuuHoSo: " + ex.Message);
            }
            return Json("Fail");
        }

        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult SuaHoSo(long hosoId)
        {
            var hoso = _iHoSoSvc.GetById(hosoId);
            if (hoso == null)
            {
                return Json("Fail");
            }
            //set viewbag -> load image upload hidden
            ViewBag.UPLOADS_DLNH = hoso.UPLOADS_DLNH;
            ViewBag.UPLOADS_GPKD = hoso.UPLOADS_GPKD;
            ViewBag.UPLOADS_GUQ = hoso.UPLOADS_GUQ;
            ViewBag.UPLOADS_BCTC = hoso.UPLOADS_BCTC;

            hoso.UPLOADS_DLNH = UploadDirectory + hoso.UPLOADS_DLNH;
            hoso.UPLOADS_GPKD = UploadDirectory + hoso.UPLOADS_GPKD;
            hoso.UPLOADS_GUQ = UploadDirectory + hoso.UPLOADS_GUQ;
            hoso.UPLOADS_BCTC = UploadDirectory + hoso.UPLOADS_BCTC;

            ViewData["DM_NGANHANG"] = _iDmNganHangSvc.Query.ToList();
            return PartialView("PopupSaveData", hoso);
        }
    }
}
