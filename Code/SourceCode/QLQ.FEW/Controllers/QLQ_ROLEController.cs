﻿using FX.Core;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using QLQ.Core.Security;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.ROLEPERMISSION)]
    [Authorize]
    public class QLQ_ROLEController : BaseController
    {
        private readonly IQLQ_ROLEService _iQlqRoleService;
        //private static readonly ILog log = LogManager.GetLogger(typeof(DMNGANHANGaController));
        //
        // GET: /DMNGANHANG/
        public QLQ_ROLEController()
        {
            _iQlqRoleService = IoC.Resolve<IQLQ_ROLEService>();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            var a = _iQlqRoleService.GetAll();
            return View();
        }

        public ActionResult QLQ_ROLEPartial()
        {
            List<QLQ_ROLE> model = new List<QLQ_ROLE>();
            model = _iQlqRoleService.GetAll();
            return PartialView("QLQ_ROLEPartial", model);
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult AddNewPartial(QLQ_ROLE obj)
        {
            if (ModelState.IsValid)
            {
                if (_iQlqRoleService.GetById(obj.role_id) == null)
                {
                    //thêm mới bản ghi
                    try
                    {
                        //khai báo các trường tự tự động điền                                                
                        _iQlqRoleService.CreateNew(obj);
                        _iQlqRoleService.CommitChanges();
                        ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                    }
                    catch (Exception e)
                    {
                        ViewData["EditError"] = e.Message;
                    }
                }
                else
                    ViewData["EditError"] = Resources.Localizing.MessageDuplicatekey;
            }
            else
            {
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            }
            return QLQ_ROLEPartial();
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult UpdatePartial(QLQ_ROLE obj)
        {
            if (ModelState.IsValid)
            {
                //cập nhật bản ghi
                try
                {
                    QLQ_ROLE temp = _iQlqRoleService.GetById(obj.role_id);
                    temp.name = obj.name;
                    temp.description = obj.description;
                    _iQlqRoleService.Save(temp);
                    _iQlqRoleService.CommitChanges();
                    ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return QLQ_ROLEPartial();
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.DELETE)]
        public ActionResult DeletePartial(QLQ_ROLE obj)
        {
            //code xóa bản ghi
            try
            {
                _iQlqRoleService.Delete(obj);
                _iQlqRoleService.CommitChanges();
                ViewData["EditSuccess"] = Resources.Localizing.MessageDelete;

            }
            catch (Exception)
            {
                ViewData["EditError"] = Resources.Localizing.MessageDate;
            }
            return QLQ_ROLEPartial();
        }


    }
}
