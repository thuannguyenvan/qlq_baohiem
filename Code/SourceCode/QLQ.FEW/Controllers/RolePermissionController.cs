﻿using FX.Core;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using QLQ.Core.Security;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.ROLEPERMISSION)]
    [Authorize]
    public class RolePermissionController : Controller
    {
        private readonly IQLQ_ROLEService _iRoleservice;
        private readonly IQLQ_PERMISSIONService _iPermissionservice;
        private readonly IQLQ_ROLEPERMISSIONService _iRolepmsservice;
        private readonly IOBJECTService _iObjectservice;
        public static List<QLQ_PERMISSION> Pers = new List<QLQ_PERMISSION>();
        public static List<QLQ_ROLEPERMISSION> RolePers = new List<QLQ_ROLEPERMISSION>();

        public RolePermissionController()
        {
            _iRoleservice = IoC.Resolve<IQLQ_ROLEService>();
            _iPermissionservice = IoC.Resolve<IQLQ_PERMISSIONService>();
            _iRolepmsservice = IoC.Resolve<IQLQ_ROLEPERMISSIONService>();
            _iObjectservice = IoC.Resolve<IOBJECTService>();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            ViewData["Role"] = _iRoleservice.GetAll();
            ViewData["Object"] = _iObjectservice.GetAll();
            return View();
        }

        public ActionResult ContentPartial(int? objId , long? roleId)
        {
            RolePers = new List<QLQ_ROLEPERMISSION>();
            if (roleId == null)
            {
                var lstObj = _iObjectservice.GetAll();
                Pers = _iPermissionservice.GetByObjId(int.Parse(lstObj[0].object_id.ToString()));
                var lstRole = _iRoleservice.GetAll();
                var tempRolePers = _iRolepmsservice.GetByRoleId(lstRole[0].role_id);
                foreach (var item in Pers)
                {
                    foreach (var item1 in tempRolePers)
                    {
                        if (item.permission_id == item1.permission_id)
                        {
                            RolePers.Add(new QLQ_ROLEPERMISSION {role_id=item1.role_id, permission_id=item1.permission_id,rolepermission_id=item1.rolepermission_id });
                        }
                    }
                }
                ViewBag.ListPer = Pers;
                ViewBag.ListRolePer = RolePers;
            }
            else
            {
                Pers = _iPermissionservice.GetByObjId(objId);
                var tempRolePers = _iRolepmsservice.GetByRoleId(roleId);
                foreach (var item in Pers)
                {
                    foreach (var item1 in tempRolePers)
                    {
                        if (item.permission_id == item1.permission_id)
                        {
                            RolePers.Add(new QLQ_ROLEPERMISSION { role_id = item1.role_id, permission_id = item1.permission_id, rolepermission_id = item1.rolepermission_id });
                        }
                    }
                }
                ViewBag.ListPer = Pers;
                ViewBag.ListRolePer = RolePers;
            }
            return PartialView("ContentPartial");
        }

        [HttpPost]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult ActionAdd (long? roleId, int index)
        {
            RolePers = new List<QLQ_ROLEPERMISSION>();
            var idPer = Pers[index].permission_id;
            var obj = _iRolepmsservice.GetObj(roleId, idPer);
            if(obj==null)
            {
                var model = new QLQ_ROLEPERMISSION
                {
                    permission_id = idPer,
                    role_id = roleId
                };
                _iRolepmsservice.CreateNew(model);
                _iRolepmsservice.CommitChanges();
            }
            var tempRolePers = _iRolepmsservice.GetByRoleId(roleId);
            foreach (var item in Pers)
            {
                foreach (var item1 in tempRolePers)
                {
                    if (item.permission_id == item1.permission_id)
                    {
                        RolePers.Add(new QLQ_ROLEPERMISSION { role_id = item1.role_id, permission_id = item1.permission_id, rolepermission_id = item1.rolepermission_id });
                    }
                }
            }
            ViewBag.ListPer = Pers;
            ViewBag.ListRolePer = RolePers;
            return PartialView("ContentPartial");
        }

        [ActionPermissions(QlqActions.DELETE)]
        public ActionResult ActionRemoved (long? roleId, int index)
        {
            var idPer = RolePers[index].permission_id;
            var obj = _iRolepmsservice.GetObj(roleId, idPer);
            if(obj!=null)
            {
                _iRolepmsservice.Delete(obj);
                _iRolepmsservice.CommitChanges();
            }
            RolePers = new List<QLQ_ROLEPERMISSION>();
            var tempRolePers = _iRolepmsservice.GetByRoleId(roleId);
            foreach (var item in Pers)
            {
                foreach (var item1 in tempRolePers)
                {
                    if (item.permission_id == item1.permission_id)
                    {
                        RolePers.Add(new QLQ_ROLEPERMISSION { role_id = item1.role_id, permission_id = item1.permission_id, rolepermission_id = item1.rolepermission_id });
                    }
                }
            }
            ViewBag.ListPer = Pers;
            ViewBag.ListRolePer = RolePers;
            return PartialView("ContentPartial");
        }
        //public ActionResult RolePerPartial( long? RoleId)
        //{
        //    List<QLQ_PERMISSION> Pers = new List<QLQ_PERMISSION>();
        //    List<QLQ_ROLEPERMISSION> RolePers = new List<QLQ_ROLEPERMISSION>();
        //    try
        //    {
        //        ViewData["LstPer"] = _i_permissionservice.Getdata();
        //        if (RoleId == null)
        //        {
        //            var lstObj = _i_objectservice.GetAll();
        //            Pers = _i_permissionservice.getByObjId(int.Parse(lstObj[0].object_id.ToString()));
        //            var lstRole = _i_roleservice.GetAll();
        //            RolePers = _i_rolepmsservice.getByRoleId(lstRole[0].role_id);
        //            ViewData["ListRolePer"] = RolePers;
        //        }
        //        else
        //        {
        //            ViewData["ListRolePer"] = _i_rolepmsservice.getByRoleId(RoleId);
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        throw;
        //    }
        //    return PartialView("RolePerPartial");
        //}
    }
}
