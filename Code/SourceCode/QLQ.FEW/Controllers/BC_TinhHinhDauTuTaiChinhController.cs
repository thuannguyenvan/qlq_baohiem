﻿using FX.Core;
using QLQ.Core.IService;
using QLQ.Core.Security;
using QLQ.Core.StoreProcedure;
using System.Web.Mvc;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.BC_TINHHINHDAUTUTAICHINH)]
    [Authorize]
    public class BC_TinhHinhDauTuTaiChinhController : BaseController
    {
        private readonly IDM_HINHTHUCDTUService _iDmHinhThucDauTuService;
        private readonly ILAISUATNH_DTLService _iLaiSuatNhDtlService;
        private readonly ILAISUATNH_HDRService _iLaiSuatNhHdrService;
        private QLQStoreProcedure _spQlq = new QLQStoreProcedure();
        //
        // GET: /SoTheoDoi/
        public BC_TinhHinhDauTuTaiChinhController()
        {
            _iDmHinhThucDauTuService = IoC.Resolve<IDM_HINHTHUCDTUService>();
            _iLaiSuatNhDtlService = IoC.Resolve<ILAISUATNH_DTLService>();
            _iLaiSuatNhHdrService = IoC.Resolve<ILAISUATNH_HDRService>();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult BC_THDTTCPartial()
        {
            ViewBag.Listhtdautu = _iDmHinhThucDauTuService.GetAll();
            return PartialView("BC_THDTTCPartial");
        }

        [HttpPost]
        [ActionPermissions(QlqActions.PRINT)]
        public ActionResult fn_InAn(string htDautu)
        {
            BC_B10aBH report = new BC_B10aBH();
            report.DataSource = null;
            return PartialView("ReportBC_THDTTC", report);
        }
    }
}
