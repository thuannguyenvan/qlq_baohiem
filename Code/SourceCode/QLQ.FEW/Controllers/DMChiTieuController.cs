﻿using FX.Core;
using log4net;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using QLQ.Core.Security;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.DM_CHITIEU)]
    [Authorize]
    public class DMChiTieuController : BaseController
    {
        [Function(QlqFunctions.DM_CHITIEU)]
        private readonly IDM_CHITIEUService _iDmChitieuService;
        private static readonly ILog Log = LogManager.GetLogger(typeof(DMChiTieuController));
        //
        // GET: /DMChiTieu/
        public DMChiTieuController()
        {
            _iDmChitieuService = IoC.Resolve<IDM_CHITIEUService>();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            return View();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult DMCHITIEUPartial(string timkiem)
        {
            List<QLQ_DMCHITIEU> model = new List<QLQ_DMCHITIEU>();
            model = _iDmChitieuService.Search(timkiem);
            return PartialView("DMCHITIEUPartial", model);
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Search(string timkiem)
        {
            return DMCHITIEUPartial(timkiem);
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult AddNewPartial(QLQ_DMCHITIEU obj)
        {
            if (ModelState.IsValid)
            {
                if (_iDmChitieuService.GetByMa(obj.ma_chitieu) == null)//kiểm tra tồn tại MA_CHITIEU
                {
                    //thêm mới bản ghi
                    try
                    {
                        //khai báo các trường tự tự động điền
                        //obj.CREATED_BY=;  //người tạo
                        obj.creation_date = DateTime.Now.Date;  //ngày tạo
                        obj.active = obj.active == null ? 0 : obj.active;
                        _iDmChitieuService.CreateNew(obj);
                        _iDmChitieuService.CommitChanges();
                        ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                        //log.Error("Danh mục đơn vị quản lý - Thêm mới: " + nguoidung.TENDANGNHAP);
                    }
                    catch (Exception e)
                    {
                        ViewData["EditError"] = e.Message;
                    }
                }
                else
                    ViewData["EditError"] = Resources.Localizing.MessageDuplicatekey;//in ra thông báo lỗi trùng mã
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return DMCHITIEUPartial(null);
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult UpdatePartial(QLQ_DMCHITIEU obj)
        {
            if (ModelState.IsValid)
            {
                //cập nhật bản ghi
                try
                {
                    QLQ_DMCHITIEU temp = _iDmChitieuService.GetById(obj.chitieu_id);
                    temp.ma_chitieu = obj.ma_chitieu;
                    temp.ten_chitieu = obj.ten_chitieu;
                    temp.active = obj.active == null ? 0 : obj.active;
                    //temp.LASTUPDATE_BY =;   //người sửa
                    temp.lastupdated_date = DateTime.Now.Date;
                    _iDmChitieuService.Clear();
                    _iDmChitieuService.Save(temp);
                    _iDmChitieuService.CommitChanges();
                    ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                    //log.Error("Danh mục đơn vị quản lý - Sửa: " + nguoidung.TENDANGNHAP);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return DMCHITIEUPartial(null);
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.DELETE)]
        public ActionResult DeletePartial(QLQ_DMCHITIEU obj)
        {
            //code xóa bản ghi
            try
            {
                _iDmChitieuService.Delete(obj);
                _iDmChitieuService.CommitChanges();
                ViewData["EditSuccess"] = Resources.Localizing.MessageDelete;
                //log.Error("Danh mục đơn vị quản lý - Xóa: " + nguoidung.TENDANGNHAP);                    
            }
            catch (Exception)
            {
                ViewData["EditError"] = Resources.Localizing.MessageDate;
            }
            return DMCHITIEUPartial(null);
        }
    }
}
