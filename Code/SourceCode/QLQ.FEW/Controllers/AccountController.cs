﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using QLQ.Core.IService;
using FX.Core;
using IdentityManagement.Authorization;
using IdentityManagement.Domain;
using IdentityManagement.Service;
using log4net;
using QLQ.Core.Common;
using DotNetCasClient;
using QLQ.Core.Security;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.ACCOUMT)]
    [Authorize]
    public class QLQountController : BaseController
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(QLQountController));
        public IuserService UserDataService;
        private readonly IDMDVQLService _idmdvqlService;
        private readonly IACCOUNTService _iAccountService;

        public QLQountController()
        {
            //UserDataService = IoC.Resolve<IuserService>();
            //_idmdvqlService = IoC.Resolve<IDMDVQLService>();
            //_iAccountService = IoC.Resolve<IACCOUNTService>();
        }

        [AllowAnonymous]
        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Login(string returnUrl)
        {
            var user = ""; //((QLQ.Core.QLQContext)FX.Context.FXContext.Current).CurrentUser;
            if (user == null)
            {
                Session.Abandon();
                if (Request.IsAjaxRequest())
                {
                    return Json("NonAuthorize", JsonRequestBehavior.AllowGet);
                }else
                {
                    return RedirectToAction("Index");
                }
               
            }
            else
            {
                return RedirectToAction("Index", "NonAuthorize");
            }

        }

        //public ActionResult Login()
        //{
        //    return RedirectToAction("Index", "QLQount");
        //}


        public ActionResult Index()
        {
            var nguoidung = ""; //((QLQ.Core.QLQContext)FX.Context.FXContext.Current).CurrentNguoidung;
            return RedirectToAction("Index", "Home");
            if (nguoidung != null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.Title = "QLQ v" + RouteConfig.Version + " | Đăng nhập";
                Session.Clear();
                //return View(new NGUOIDUNG
                //{
                //    Time = new TimeFilter(),
                //    CapImage = "data:image/png;base64," + Convert.ToBase64String(new Utility().VerificationTextGenerator()),
                //    CapImageText = Convert.ToString(Session["Captcha"])
                //});
            }
        }

        public ActionResult TempIndex()
        {
            var nguoidung = "";// ((QLQ.Core.QLQContext)FX.Context.FXContext.Current).CurrentNguoidung;
            return RedirectToAction("Index", "Home");
            if (nguoidung != null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                //ViewBag.Title = "QLQ v" + RouteConfig.Version + " | Đăng nhập";
                //Session.Clear();
                //return View(new NGUOIDUNG
                //{
                //    Time = new TimeFilter(),
                //    CapImage = "data:image/png;base64," + Convert.ToBase64String(new Utility().VerificationTextGenerator()),
                //    CapImageText = Convert.ToString(Session["Captcha"])
                //});
            }
        }

        [HttpPost]
        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Login(string username, string password, string captcha)
        {

            //var b = FormsAuthentication.HashPasswordForStoringInConfigFile("1qaz2wsx", "MD5");
            //var firstOrDefault = UserDataService.Query.FirstOrDefault(x => x.username == username);
            //if (firstOrDefault != null)
            //{
            //    var a = firstOrDefault.password;
            //}
            string CapImaText = Convert.ToString(Session["Captcha"]);
            if (captcha != CapImaText)
            {
                return Json(new { data = 3 }, JsonRequestBehavior.AllowGet);
            }
            Session["MustChangePW"] = false;

           
            //_iLogSystemService.CreateNew(username.Trim(), "Đăng nhập hệ thống ", "Thực hiện đăng nhập hệ thống",
            //    Helper.GetIPAddress.GetVisitorIPAddress(), HttpContext.Request.Browser.Browser);
            try
            {
                user userBug = UserDataService.Query.FirstOrDefault(m => m.username.ToUpper() == username.ToUpper());
                if (userBug != null)
                {
                    username = userBug.username;
                }
                var authenticationService = IoC.Resolve<FanxiAuthenticationBase>();
                Session["password"] = password;
                if (authenticationService.Logon(username.Trim(), password.Trim()))
                {
                    //isert log system
                    //_iLogSystemService.CreateNew(username.Trim(), "Đăng nhập hệ thống ", "Đăng nhập thành công",
                    //       Helper.GetIPAddress.GetVisitorIPAddress(), HttpContext.Request.Browser.Browser);

                    CheckChangePassword cc_password = new CheckChangePassword();
                    bool isChange = cc_password.CheckChange(password);

                    var currentUser = UserDataService.Query.FirstOrDefault(x => x.username.ToUpper() == username.ToUpper()) ?? new user();
                    var nguoidungId = "";// NguoidungService.Query.FirstOrDefault(x => x.TENDANGNHAP.ToUpper() == username.Trim().ToUpper()) ?? new NGUOIDUNG();

                    //if (currentUser.ISADMIN != true && nguoidungId.ISPQ != true && nguoidungId.DF_LOAITG == null)
                    //{
                    //    if (!isChange)
                    //    {
                    //        Session["MustChangePW"] = true;
                    //    }

                    //    System.Web.HttpContext.Current.Session["DVQL_Session"] = nguoidungId.DVQLS;
                    //    return Json(new { data = 5 }, JsonRequestBehavior.AllowGet);
                    //}

                    if (!isChange)
                    {
                        Session["MustChangePW"] = true;
                        return Json(new { data = 4 }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { data = 1 }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { data = 2 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                return Json(new { data = 6, message = e.Message, stack = e.StackTrace }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult LogOff()
        {
            //_iLogSystemService.CreateNew(HttpContext.User.Identity.Name, "Thoát khỏi hệ thống ", "Thực hiện đăng nhập đăng xuất khỏi hệ thống",
            //          Helper.GetIPAddress.GetVisitorIPAddress(), HttpContext.Request.Browser.Browser);
            //Session.Clear();
            //Session.Abandon();
            //FormsAuthentication.SignOut();
            CasAuthentication.SingleSignOut();

            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        //public ActionResult DY_PhienLamViec(string madvql, int? loaiTg, int? thang, int? quy, int? nam)
        //{
        //    var nguoidung = "";//((QLQContext)FXContext.Current).CurrentNguoidung;
        //    if (nguoidung == null)
        //    {
        //        return Json("false", JsonRequestBehavior.AllowGet);
        //    }
        //    if (loaiTg == null)
        //    {
        //        return Json("TimeNull", JsonRequestBehavior.AllowGet);
        //    }

        //    if (madvql == null)
        //    {
        //        return Json("kcbnull", JsonRequestBehavior.AllowGet);
        //    }
        //    var checkExistCskcb = _IDMDVQLService.Query.FirstOrDefault(m => m.MA_DVQL == madvql);
        //    if (checkExistCskcb == null)
        //    {
        //        return Json("notExistCskcb", JsonRequestBehavior.AllowGet);
        //    }

        //    try
        //    {
        //        nguoidung.DF_MA_DVQL = madvql;
        //        nguoidung.DF_LOAITG = loaiTg;
        //        nguoidung.DF_THANG = thang;
        //        nguoidung.DF_QUY = quy;
        //        nguoidung.DF_NAM = nam;
        //        NguoidungService.Save(nguoidung);
        //        NguoidungService.CommitChanges();
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error(ex.Message);
        //        return Json("false", JsonRequestBehavior.AllowGet);
        //    }
        //    return Json("true", JsonRequestBehavior.AllowGet);
        //}

        public ActionResult DvqlComboboxPatial()
        {
            return PartialView();
        }
        //public ActionResult PartialPhienLamViec()
        //{
        //    return View(new NGUOIDUNG
        //    {
        //        Time = new TimeFilter()
        //    });
        //}
        //public ActionResult PartialImgCaptcha()
        //{
        //    Session["Captcha"] = null;
        //    NGUOIDUNG objNguoidung = new NGUOIDUNG();
        //    objNguoidung.CapImage = "data:image/png;base64," + Convert.ToBase64String(new Utility().VerificationTextGenerator());
        //    objNguoidung.CapImageText = Convert.ToString(Session["Captcha"]);
        //    return PartialView(objNguoidung);
        //}
    }
}