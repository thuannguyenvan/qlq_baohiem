﻿using FX.Core;
using QLQ.Core.IService;
using QLQ.Core.Security;
using QLQ.Core.StoreProcedure;
using System.Web.Mvc;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.BC_THULAIDAUTUTAICHINH)]
    [Authorize]
    public class BC_ThuLaiDauTuTaiChinhController : BaseController
    {
        private readonly IDM_HINHTHUCDTUService _iDmHinhThucDauTuService;
        private readonly ILAISUATNH_DTLService _iLaiSuatNhDtlService;
        private readonly ILAISUATNH_HDRService _iLaiSuatNhHdrService;
        private QLQStoreProcedure _spQlq = new QLQStoreProcedure();
        //
        // GET: /SoTheoDoi/
        public BC_ThuLaiDauTuTaiChinhController()
        {
            _iDmHinhThucDauTuService = IoC.Resolve<IDM_HINHTHUCDTUService>();
            _iLaiSuatNhDtlService = IoC.Resolve<ILAISUATNH_DTLService>();
            _iLaiSuatNhHdrService = IoC.Resolve<ILAISUATNH_HDRService>();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult BC_ThuLaiDTTCPartial()
        {
            ViewBag.Listhtdautu = _iDmHinhThucDauTuService.GetAll();
            return PartialView("BC_ThuLaiDTTCPartial");
        }

        [HttpPost]
        [ActionPermissions(QlqActions.PRINT)]
        public ActionResult fn_InAn(string htDautu)
        {
            BC_B10bBH report = new BC_B10bBH();
            report.DataSource = null;
            return PartialView("ReportBC_ThuLaiDTTC", report);
        }

    }
}
