﻿using FX.Core;
using QLQ.Core.Common;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using QLQ.Core.Security;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.DM_QUYEN)]
    [Authorize]
    public class QLQ_PERMISSIONController : BaseController
    {
        private readonly IQLQ_PERMISSIONService _iQlqPermissionService;
        //private static readonly ILog log = LogManager.GetLogger(typeof(DMNGANHANGaController));
        //
        public QLQ_PERMISSIONController()
        {
            _iQlqPermissionService = IoC.Resolve<IQLQ_PERMISSIONService>();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            return View();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult QLQ_PERMISSIONPartial()
        {
            List<QLQ_PERMISSION> model = new List<QLQ_PERMISSION>();
            model = _iQlqPermissionService.GetAll();
            return PartialView("QLQ_PERMISSIONPartial", model);
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult AddNewPartial(QLQ_PERMISSION obj)
        {
            if (ModelState.IsValid)
            {
                if (_iQlqPermissionService.GetById(obj.permission_id) == null)
                {
                    //thêm mới bản ghi
                    try
                    {
                        //khai báo các trường tự tự động điền                                                
                        _iQlqPermissionService.CreateNew(obj);
                        _iQlqPermissionService.CommitChanges();
                        ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                    }
                    catch (Exception e)
                    {
                        ViewData["EditError"] = e.Message;
                    }
                }
                else
                    ViewData["EditError"] = Resources.Localizing.MessageDuplicatekey;
            }
            else
            {
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            }
            return QLQ_PERMISSIONPartial();
        }
        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult UpdatePartial(QLQ_PERMISSION obj)
        {
            if (ModelState.IsValid)
            {
                //cập nhật bản ghi
                try
                {
                    QLQ_PERMISSION temp = _iQlqPermissionService.GetById(obj.permission_id);
                    temp.name = obj.name;
                    temp.description = obj.description;
                    _iQlqPermissionService.Save(temp);
                    _iQlqPermissionService.CommitChanges();
                    ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return QLQ_PERMISSIONPartial();
        }
        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.DELETE)]
        public ActionResult DeletePartial(QLQ_PERMISSION obj)
        {
            //code xóa bản ghi
            try
            {
                _iQlqPermissionService.Delete(obj);
                _iQlqPermissionService.CommitChanges();
                ViewData["EditSuccess"] = Resources.Localizing.MessageDelete;

            }
            catch (Exception)
            {
                ViewData["EditError"] = Resources.Localizing.MessageDate;
            }
            return QLQ_PERMISSIONPartial();
        }
    }
}
