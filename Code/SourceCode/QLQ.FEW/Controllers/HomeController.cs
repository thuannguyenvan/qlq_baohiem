﻿using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using QLQ.Core.Common;
using QLQ.Core.CustomView;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using DevExpress.DashboardWeb.Mvc;
using FX.Core;
using System.Net.Http;
using System.Net;
using log4net;
using System.Configuration;
using System.Web;

namespace QLQ.FEW.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        //private readonly IDONVIINFOService IdonviinfoService;
        private readonly IACCOUNTService _iAccountService;
        private static readonly ILog Log = LogManager.GetLogger(typeof(HomeController));
        public HomeController()
        {
            _iAccountService = IoC.Resolve<IACCOUNTService>();
        }
        public ActionResult Index(string ticket)
        {
            //if (ticket != null)
            //{
            //    HttpCookie tk = new HttpCookie("Ticket");
            //    tk.Value = ticket;
            //    tk.Expires = DateTime.Now.AddHours(1);
            //    Response.Cookies.Add(tk);
            //}

            //var httpCookie = Request.Cookies["Ticket"];
            //string tkcheck = httpCookie?.Value;

//            if (ticket != null)
//            {
//                try
//                { 
//                    //trường hợp đăng nhập mới 
//                    string value = "";//khai báo string chứa nội dung response
//                    var urlXML = ConfigurationManager.AppSettings["sso"] + "?ticket=" + ticket + "&service="+ ConfigurationManager.AppSettings["QLQ"];
//                    using (var client = new HttpClient())//khởi tạo client
//                    {
//                        //SSL
//#if DEBUG
//                        ServicePointManager.ServerCertificateValidationCallback +=
//                            (sender, cert, chain, sslPolicyErrors) => true;
//#endif
//                        // HTTP GET
//                        HttpResponseMessage response = client.GetAsync(urlXML).Result;
//                        if (response.IsSuccessStatusCode)
//                        {
//                            value = response.Content.ReadAsStringAsync().Result;
//                        }
//                    }
//                    QLQ_ACCOUNT result = new QLQ_ACCOUNT();//obj đọc ra từ value
//                    var list = GetDataService<Account>("cas:authenticationSuccess", value);
//                    var list2 = GetDataService<Account>("cas:attributes", value);
//                    //lấy giá trị từ 2 list đổ vào reuslt
//                    result.USERNAME = list[0].user;
//                    result.TYPE = list2[0].type;
//                    result.MA_CQ_BHXH = list2[0].maCqBhxh;
//                    result.TEN_CQ_BHXH = list2[0].tenCqBhxh;
//                    result.ACTIVE = list2[0].tinhTrangHoatDong == "active" ? 1 : 0;
//                    result.SYS_LDAP_ADMIN = list2[0].sysLdapAdmin == "true" ? 1 : 0;
//                    result.EMAIL = list2[0].email;
//                    result.AUTH_DATE = DateTime.Now;
//                    //kiểm tra đối tượng có trong DB chưa. Nếu chưa thì thêm mới vào DB
//                    if (_iAccountService.GetByUser(result.USERNAME) == null)
//                    {
//                        _iAccountService.CreateNew(result);
//                        _iAccountService.CommitChanges();
//                        Log.Error("Lần đầu đăng nhập: " + result.USERNAME);
//                    }
//                    //lưu lại username vào session để thực hiện các quyền về sau
//                    Nguoidung = _iAccountService.GetByUser(result.USERNAME);
//                }
//                catch (Exception ex)
//                {
//                    ViewData["EditError"] = "Có lỗi xảy ra trong quá trình đăng nhập";
//                    Log.Error(DateTime.Now + " LaiSuatHuyDongController insert_LaiSuat Error: " + ex.Message);
//                }
//                //ViewBag.Title = "PHẦN MỀM QUẢN LÝ QUỸ";
//                //return View();
//            }
            //var urlRedirect = "https://local.baohiemxahoi.gov.vn:8443/cas/login?service=" + ConfigurationManager.AppSettings["QLQ"];
            //return Redirect(urlRedirect);

            ViewBag.Title = "PHẦN MỀM QUẢN LÝ QUỸ";
            return View();
        }
        public ActionResult GridViewPartialView()
        {
            // DXCOMMENT: Pass a data model for GridView in the PartialView method's second parameter
            return PartialView("GridViewPartialView");
        }
        public ActionResult ViewAction()
        {

            //ReflectionController reflection = new ReflectionController();
            //List<Type> listController = reflection.GetControllers("QLQ.FEW.Controllers");
            //string result = "<ul>";
            //foreach (Type controller in listController)
            //{
            //    result += "<li>" + controller.Name;
            //    List<string> listAction = reflection.GetActions(controller);
            //    result += "<ul>";
            //    foreach (string action in listAction)
            //    {
            //        result += "<li>" + action + "</li>";
            //    }
            //    result += "</ul></li>";
            //    ViewBag.result = result;

            //}
            return View();
        }

        public ActionResult NotificationAuthorize()
        {
            return View();
        }
        public ActionResult ConstantsSession()
        {
            var constSession = typeof(Constant.Session)
                .GetFields()
                .ToDictionary(x => x.Name, x => x.GetValue(null));
            var json = new JavaScriptSerializer().Serialize(constSession);
            return JavaScript("var constSession = " + json + ";");
        }
        public ActionResult DashboardViewerPartial()
        {
            return PartialView("_DashboardViewerPartial", DashboardViewerSettings.Model);
        }
        public FileStreamResult DashboardViewerPartialExport()

        {
            return DevExpress.DashboardWeb.Mvc.DashboardViewerExtension.Export("DashboardViewer", DashboardViewerSettings.Model);
        }
    }
}
class DashboardViewerSettings
{
    public static DevExpress.DashboardWeb.Mvc.DashboardSourceModel Model
    {
        get
        {

            return null;// GetDashboardSourceModel();
        }



    }
    private static DashboardSourceModel GetDashboardSourceModel1()
    {
        DashboardSourceModel model = new DashboardSourceModel();
        model.DashboardId = "QLTC";

        model.DashboardLoading = (sender, e) =>
        {
            // ReSharper disable once AssignNullToNotNullAttribute
            string dashboardDefinition = File.ReadAllText(System.Web.Hosting.
                        HostingEnvironment.MapPath(@"~\App_Data\QLTC_Dashboard.xml"));
            e.DashboardXml = dashboardDefinition;
        };
        //model.DataLoading = (sender, e) =>
        //{
        //    DashboardQLTC dashboard2 = new DashboardQLTC();
        //    var _isoduService = IoC.Resolve<ISODUService>();
        //    var nguoidung = ((QLQ.Core.QLQContext)FX.Context.FXContext.Current).CurrentNguoidung;
        //    var data = _isoduService.Query.Where(
        //        m => m.MA_DVQL == nguoidung.DF_MA_DVQL).Take(10000).ToList();
        //    model.DashboardSource = dashboard2;
        //    if (e.DataSourceName == "SoDuDataSource")
        //    {
        //        e.Data = data;
        //    }

        //};
        return model;
    }
    //private static DashboardSourceModel GetDashboardSourceModel()
    //{
    //    DashboardSourceModel model = new DashboardSourceModel();
    //    model.DashboardId = "QLTC";

    //    model.DashboardLoading = (sender, e) =>
    //    {
    //        // ReSharper disable once AssignNullToNotNullAttribute
    //        string dashboardDefinition = File.ReadAllText(System.Web.Hosting.
    //                    HostingEnvironment.MapPath(@"~\App_Data\TCKT_Dashboard.xml"));
    //        var nguoidung = ((QLQ.Core.QLQContext)FX.Context.FXContext.Current).CurrentNguoidung;
    //        e.DashboardXml = string.Format(dashboardDefinition,nguoidung.MA_DVQL,nguoidung.DVQL.TEN_DVQL.ToUpper());
    //    };

    //    return model;
    //}

}
public enum HeaderViewRenderMode { Full, Title }