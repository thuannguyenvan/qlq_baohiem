﻿using QLQ.Core.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.BC_C90HD)]
    [Authorize]
    public class BC_C90HDController : BaseController
    {
        //
        // GET: /BC_C90HD/

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult BC_C90HDPartial()
        {
            return PartialView("BC_C90HDPartial");
        }

        [ActionPermissions(QlqActions.PRINT)]
        public ActionResult fn_InAn(int year)
        {
            string namthang = new DateTime(year + 1, 1, 1).AddDays(-1).ToString("dd/MM/yyyy");
            BC_C90HD rp = new BC_C90HD();
            rp.title_nam.Text = string.Format("Năm {0}", year);
            rp.head_rt.Text =
                string.Format(
                    "Bảo hiểm xã hội Việt Nam đề nghị {0} đối chiếu số dư, lãi đầu tư tài chính thời điểm ngày {1} như sau:",
                    "                             ", namthang);
            rp.DataSource = null;
            return PartialView("BC_C90HD", rp);
        }
    }
}
