﻿using FX.Core;
using log4net;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using QLQ.Core.Security;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.QUANLYNGUOIDUNG)]
    [Authorize]
    public class QuanLyNguoiDungController : BaseController
    {
        private readonly IACCOUNTService _iAccountService;
        private readonly IQLQ_ROLEService _iRoleService;
        private readonly IACCOUNT_ROLEService _iAccountRoleService;
        private static readonly ILog Log = LogManager.GetLogger(typeof(DMNGANHANGController));
        //
        // GET: /QuanLyNguoiDung/
        public QuanLyNguoiDungController()
        {
            _iAccountService = IoC.Resolve<IACCOUNTService>();
            _iRoleService = IoC.Resolve<IQLQ_ROLEService>();
            _iAccountRoleService = IoC.Resolve<IACCOUNT_ROLEService>();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            List<QLQ_ACCOUNT> model = _iAccountService.GetAll();
            return View(model);
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult DS_NguoiDungPartial(string timkiem)
        {
            List<QLQ_ACCOUNT> model = new List<QLQ_ACCOUNT>();
            model = _iAccountService.Search(timkiem);
            return PartialView("DS_NguoiDungPartial", model);
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Search(string timkiem)
        {
            return DS_NguoiDungPartial(timkiem);
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult ThongTinNguoiDungPartial(int userId)
        {
            var model = _iAccountService.GetById(userId);
            ViewBag.listRole = _iRoleService.GetAll();
            ViewBag.ListAccountRole = _iAccountRoleService.GetByAccount(long.Parse(userId.ToString()));
            return PartialView(model);
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult UpdatePartial(QLQ_ACCOUNT obj)
        {
            if (ModelState.IsValid)
            {
                //cập nhật bản ghi
                try
                {
                    QLQ_ACCOUNT temp = _iAccountService.GetByUser(obj.USERNAME);
                    temp.FULL_NAME = obj.FULL_NAME;
                    temp.ACTIVE = obj.ACTIVE;
                    temp.MA_CQ_BHXH = obj.MA_CQ_BHXH;
                    temp.TEN_CQ_BHXH = obj.TEN_CQ_BHXH;
                    _iAccountService.Save(temp);
                    _iAccountService.CommitChanges();
                    ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                    //log.Error("Danh mục đơn vị quản lý - Sửa: " + nguoidung.TENDANGNHAP);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return DS_NguoiDungPartial(null);
        }

        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult SaveUser(long? accountId, string maCoQuanBhxh, string tenCoQuanBhxh, string fullName, int active, string role)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var arr = role.Split(',');//cắt chuỗi lấy ra array role được check
                    var listRole = _iRoleService.GetAll();//lấy tổng list role
                    foreach (var item in arr)
                    {
                        foreach (var item1 in listRole)
                        {
                            if (item == item1.role_id.ToString())
                            {
                                if (_iAccountRoleService.GetId(long.Parse(accountId.ToString()), long.Parse(item), 0) == null)
                                {
                                    //trường hợp chưa có bản ghi trong DB => tạo bản ghi mới
                                    QLQ_ACCOUNTROLE accRole = new QLQ_ACCOUNTROLE
                                    {
                                        ACCOUNT_ID = Convert.ToInt64(accountId),
                                        ROLE_ID = long.Parse(item),
                                        ACTIVE = 1
                                    };
                                    _iAccountRoleService.CreateNew(accRole);
                                    _iAccountRoleService.CommitChanges();
                                }
                                else
                                {
                                    //trường hợp có bản ghi trong DB => tiến hành update
                                    QLQ_ACCOUNTROLE temp1 = _iAccountRoleService.GetId(long.Parse(accountId.ToString()), long.Parse(item), 0);
                                    temp1.ACTIVE = 1;
                                    _iAccountRoleService.Save(temp1);
                                    _iAccountRoleService.CommitChanges();
                                }
                            }
                            else
                            {
                                //bỏ active cho bản ghi
                                var obj = _iAccountRoleService.GetId(long.Parse(accountId.ToString()), long.Parse(item1.role_id.ToString()), 1);
                                if (obj != null)
                                {
                                    //có bản ghi tiến hành update              
                                    obj.ACTIVE = 0;
                                    _iAccountRoleService.Save(obj);
                                    _iAccountRoleService.CommitChanges();
                                }
                            }
                        }
                    }
                    _iAccountService.BeginTran();
                    _iAccountService.Clear();
                    //cập nhật thông tin cá nhân
                    var temp = _iAccountService.GetById(Convert.ToInt32(accountId));
                    temp.ACTIVE = active;
                    temp.MA_CQ_BHXH = maCoQuanBhxh;
                    temp.MA_CQ_BHXH = tenCoQuanBhxh;
                    temp.FULL_NAME = fullName;
                    _iAccountService.Save(temp);
                    _iAccountService.CommitTran();
                    return Json("Success");
                }
                catch (Exception e)
                {
                    Log.Error("Save User Error: " + e.Message);
                    return Json("Exception");
                }
            }
            else
            {
                ViewData["EditError"] = Resources.Localizing.MessageCommon;
                return Json("NotValid");
            }
        }
    }
}
