﻿using FX.Core;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using QLQ.Core.Security;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.OBJECT)]
    [Authorize]
    public class QLQ_ObjectController : BaseController
    {
        private readonly IOBJECTService _iobjService;
        private readonly IQLQ_PERMISSIONService _ipmsService;

        public QLQ_ObjectController()
        {
            _iobjService = IoC.Resolve<IOBJECTService>();
            _ipmsService = IoC.Resolve<IQLQ_PERMISSIONService>();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ObjectPartial()
        {
            List<QLQ_OBJECT> model = new List<QLQ_OBJECT>();
            model = _iobjService.GetAll();
            return PartialView("ObjectPartial", model);
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult AddNewPartial(QLQ_OBJECT obj)
        {
            if (ModelState.IsValid)
            {
                if (_iobjService.GetbyId(obj.object_id) == null)
                {
                    //thêm mới bản ghi
                    try
                    {
                        //khai báo các trường tự tự động điền 
                         obj.active = obj.active == null ? 0 : obj.active;
                        _iobjService.CreateNew(obj);
                        _iobjService.CommitChanges();
                        ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                    }
                    catch (Exception e)
                    {
                        ViewData["EditError"] = e.Message;
                    }
                }
                else
                    ViewData["EditError"] = Resources.Localizing.MessageDuplicatekey;
            }
            else
            {
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            }
            return ObjectPartial();
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult UpdatePartial(QLQ_OBJECT obj)
        {
            if (ModelState.IsValid)
            {
                //cập nhật bản ghi
                try
                {
                    QLQ_OBJECT temp = _iobjService.GetbyId(obj.object_id);
                    temp.object_name = obj.object_name;
                    temp.active = obj.active == null ? 0 : obj.active;
                    _iobjService.Save(temp);
                    _iobjService.CommitChanges();
                    ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return ObjectPartial();
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.DELETE)]
        public ActionResult DeletePartial(QLQ_OBJECT obj)
        {
            //code xóa bản ghi
            try
            {
                _iobjService.Delete(obj);
                _iobjService.CommitChanges();
                ViewData["EditSuccess"] = Resources.Localizing.MessageDelete;

            }
            catch (Exception)
            {
                ViewData["EditError"] = Resources.Localizing.MessageDate;
            }
            return ObjectPartial();
        }

    }
}
