﻿using FX.Core;
using QLQ.Core.CustomView;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using QLQ.Core.StoreProcedure;
using QLQ.FEW.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace QLQ.FEW.Controllers
{
    public class TongHopSoDuDauTuController : BaseController
    {
        private readonly IDM_HINHTHUCDTUService _HINHTHUCDTUService;
        private readonly QLQStoreProcedure _procedure;
        private string NgayXem
        {
            get
            {
                if (TempData["ngayxem"] == null)
                    return string.Empty;
                return TempData["ngayxem"].ToString();
            }
            set
            {
                TempData["ngayxem"] = value;
            }
        }
        public ActionResult Index()
        {
            return View();
        }
        public TongHopSoDuDauTuController() {
            _HINHTHUCDTUService = IoC.Resolve<IDM_HINHTHUCDTUService>();
            _procedure = new QLQStoreProcedure();
            ViewData["DSHinhThucDTU1"] = _HINHTHUCDTUService.GetAll();
          
        }
        
        public ActionResult TongHopSoDuDauTuPartial()
        {
            return Search(NgayXem);
        }
        public ActionResult Search(string thoigian)
        {
            NgayXem = thoigian;
            DateTime ngayxem = DateTime.Now;
            if (!string.IsNullOrEmpty(NgayXem))
            {
                ngayxem = DateTime.Parse(thoigian);
            }
             
            var model = _procedure.SP_TongHopSoDuDauTu(ngayxem);
            NgayXem = NgayXem;
            return PartialView("TongHopSoDuDauTuPartial", model);
        }  
    }
}
