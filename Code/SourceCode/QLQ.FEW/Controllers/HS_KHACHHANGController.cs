﻿using System;
using System.Linq;
using System.Web.Mvc;
using QLQ.Core.IService;
using FX.Core;
using QLQ.Core.Domain;
using log4net;
using QLQ.Core.Security;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.HS_KHACHHANG)]
    [Authorize]
    public class HS_KHACHHANGController : BaseController
    {
        //
        // GET: /HS_KHACHHANG/
        private readonly IHS_KHACHHANGService _iHsKhachHangSvc;
        private readonly IDM_DAUTUService _iDmDauTuSvc;
        private readonly IDM_HINHTHUCDTUService _iDmHinhThucDauTuSvc;
        private readonly IHopDongService _iHopDongService;
        private static readonly ILog Log = LogManager.GetLogger(typeof(HS_KHACHHANGController));
        public HS_KHACHHANGController() {
            _iHsKhachHangSvc = IoC.Resolve<IHS_KHACHHANGService>();
            _iDmDauTuSvc = IoC.Resolve<IDM_DAUTUService>();
            _iDmHinhThucDauTuSvc = IoC.Resolve<IDM_HINHTHUCDTUService>();
            _iHopDongService = IoC.Resolve<IHopDongService>();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Search()
        {
            var strTimKiem = Request.Params["timkiemS"] ?? "";
            ViewData["DM_DAUTU"] = _iDmDauTuSvc.Query.ToList();
            var dmKH = _iHsKhachHangSvc.Search(strTimKiem);
            return PartialView("HS_KHACHHANGPartial", dmKH);
        }

        public ActionResult HS_KHACHHANGPartial()
        {
            ViewData["DM_DAUTU"] = _iDmDauTuSvc.Query.ToList();
            ViewData["DM_HINHTHUC"] = _iDmHinhThucDauTuSvc.Query.ToList();
            var dmKH = _iHsKhachHangSvc.Query;
            return PartialView("HS_KHACHHANGPartial", dmKH);
        }

        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult AddNewPartial(HS_KHACHHANG model)
        {
            if (ModelState.IsValid)
            {
                model.CREATION_DATE = DateTime.Now;
                try
                {
                    _iHsKhachHangSvc.Clear();
                    _iHsKhachHangSvc.CreateNew(model);
                    _iHsKhachHangSvc.CommitChanges();
                    ViewData["EditSuccess"] = "Thêm mới thành công";
                }
                catch (Exception ex)
                {
                    ViewData["EditError"] = "Có lỗi xảy ra trong quá trình cập nhật dữ liệu";
                    Log.Error("AddNewPartial Them Moi: " + ex.Message);
                }
            }
            else
            {
                ViewData["EditError"] = Resources.Localizing.MessageCommon;
            }
            return HS_KHACHHANGPartial();
        }

        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult UpdatePartial(HS_KHACHHANG model)
        {
            if (ModelState.IsValid)
            {
                model.LASTUPDATED_DATE = DateTime.Now;
                try
                {
                    _iHsKhachHangSvc.Clear();
                    _iHsKhachHangSvc.Update(model);
                    _iHsKhachHangSvc.CommitChanges();
                    ViewData["EditSuccess"] = "Cập nhật thành công";
                }
                catch (Exception ex)
                {
                    ViewData["EditError"] = "Có lỗi xảy ra trong quá trình cập nhật dữ liệu";
                    Log.Error("UpdatePartial Sua: " + ex.Message);
                }
            }
            else
            {
                ViewData["EditError"] = Resources.Localizing.MessageCommon;
            }
            return HS_KHACHHANGPartial();
        }

        [ActionPermissions(QlqActions.DELETE)]
        public ActionResult DeletePartial(HS_KHACHHANG hsKhachHang)
        {
            try
            {
                if (!_iHopDongService.CheckDmKhachHangId(hsKhachHang.KHACHHANG_ID))
                {
                    _iHsKhachHangSvc.Delete(hsKhachHang.KHACHHANG_ID);
                    _iHsKhachHangSvc.CommitChanges();
                    ViewData["EditSuccess"] = "Xóa thành công";
                }
                else
                {
                    ViewData["EditSuccess"] = Resources.Localizing.MessageDeleteExist;
                }
            }
            catch (Exception ex)
            {
                ViewData["EditError"] = "Có lỗi xảy ra trong quá trình xóa dữ liệu";
                Log.Error("DeletePartial Xoa: " + ex.Message);
            }
            return HS_KHACHHANGPartial();
        }

        public ActionResult DMDauTuPartial()
        {
            long hinhthuc_id = (Request.Params["HINHTHUC_ID"] != null) ? long.Parse((Request.Params["HINHTHUC_ID"])) : 0;
            var listdautu = _iDmDauTuSvc.GetAll().Where(t => t.hinhthuc_id == Convert.ToInt64(hinhthuc_id)).ToList();
            return PartialView("DMDauTuPartial", listdautu);
        }
        
    }
}
