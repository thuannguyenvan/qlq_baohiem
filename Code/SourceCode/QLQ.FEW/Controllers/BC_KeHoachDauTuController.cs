﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QLQ.FEW.Reports;
using QLQ.Core.Security;
using QLQ.Core.IService;
using FX.Core;
using System.Globalization;
using QLQ.Core.Domain;
using log4net;
using QLQ.Core.CustomView;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.BC_KEHOACHDAUTU)]
    [Authorize]
    public class BC_KeHoachDauTuController : BaseController
    {
        private readonly IKeHoachDauTuService _keHoachDauTuService;
        private static readonly ILog Log = LogManager.GetLogger(typeof(BC_KeHoachDauTuController));

        public BC_KeHoachDauTuController()
        {
            _keHoachDauTuService = IoC.Resolve<IKeHoachDauTuService>();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            return View();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult BC_KeHoachDauTuPartial()
        {
            return PartialView("BC_KeHoachDauTuPartial", _keHoachDauTuService.Query
                .OrderByDescending(i => i.Nam)
                .ThenByDescending(i => i.Thang).ToList());
        }

        public ActionResult Search(string thoiGian)
        {
            var query = _keHoachDauTuService.Query;
            DateTime date;
            if (DateTime.TryParseExact(thoiGian, "MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            {
                query = query.Where(i => i.Nam == date.Year && i.Thang == date.Month);
            }
            return PartialView("BC_KeHoachDauTuPartial", query.OrderByDescending(i => i.Nam).ThenByDescending(i => i.Thang).ToList());
        }


        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult Update(KeHoachDauTu model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _keHoachDauTuService.Clear();
                    _keHoachDauTuService.Update(model);
                    _keHoachDauTuService.CommitChanges();
                    EditSuccessMessage = "Cập nhật thành công";
                }
                catch (Exception ex)
                {
                    EditErrorMessage = "Có lỗi xảy ra trong quá trình cập nhật dữ liệu";
                    Log.Error("BC_KeHoachDauTuController Update: " + ex.Message);
                }
            }
            else
            {
                EditErrorMessage = Resources.Localizing.MessageCommon;
            }
            return BC_KeHoachDauTuPartial();
        }

        [ActionPermissions(QlqActions.PRINT)]
        public ActionResult fn_InAn(long id)
        {
            var keHoachDauTu = _keHoachDauTuService.Getbykey(id);
            if (keHoachDauTu == null)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
            var currentMonth = new DateTime(keHoachDauTu.Nam, keHoachDauTu.Thang, 1);
            var lastMonth = currentMonth.AddMonths(-1);
            var currentMonthText = currentMonth.ToString("MM/yyyy");
            var lastMonthText = lastMonth.ToString("MM/yyyy");
            BC_KeHoachDauTu report = new BC_KeHoachDauTu();
            report.xrLabel7.Text = GetDecimalString(keHoachDauTu.SoDuTaiKhoan);
            report.xrLabel13.Text = GetDecimalString(keHoachDauTu.ThuBHYTBHXH);
            report.xrLabel16.Text = GetDecimalString(keHoachDauTu.ThuNoGocDenHan);
            report.xrLabel19.Text = GetDecimalString(keHoachDauTu.ThuLaiDauTu);
            report.xrLabel22.Text = GetDecimalString(keHoachDauTu.DuKienKinhPhiDuocSuDung);
            report.xrLabel28.Text = GetDecimalString(keHoachDauTu.CapKinhPhiBHYTBHXH);
            report.xrLabel31.Text = GetDecimalString(keHoachDauTu.ChiPhiQuanLy);
            report.xrLabel34.Text = GetDecimalString(keHoachDauTu.ChiKTPL);
            report.xrLabel37.Text = GetDecimalString(keHoachDauTu.ChuyenTuTKThanhToan);
            report.xrLabel40.Text = GetDecimalString(keHoachDauTu.CapKinhPhiThangSau);
            report.xrLabel43.Text = GetDecimalString(keHoachDauTu.ChenhLechThuChi);


            report.II_rt.Text = string.Format("Trong tháng {0}, BHXH Việt Nam đã thực hiện đầu tư {1} tỷ đồng trong đó:",
                lastMonthText, GetDecimalString(keHoachDauTu.KetQuaDauTu));

            report.xrLabel133.Text = string.Format("- {0} (để b/c).", keHoachDauTu.NoiNhan);
            report.xrLabel135.Text = keHoachDauTu.ChucVuNguoiKy.ToUpper();
            report.xrLabel136.Text = keHoachDauTu.NguoiKy;
            report.title_NamThang.Text = string.Format("Kế hoạch đầu tư tháng {0}", currentMonthText);
            report.rtHead.Text =
                string.Format(
                    "Căn cứ số liệu do Vụ Tài chính - Kế toán cung cấp về số dự kiến chi BHXH, BHYT, BHTN trong tháng {0}(Có công văn kèm theo), trong khi chờ số liệu thu BHXH, BHYT, BHTN tháng {1} do Ban Thu cung cấp, dự kiến số thu trong tháng {2} khoảng {3} tỷ đồng. Vụ Quản lý đầu tư quỹ báo cáo Tổng Giám đốc báo cáo số tiền có thể đâu tư được trong tháng và đề xuát kế hoạch đầu tư tháng, cụ thể như sau:",
                    currentMonthText, currentMonthText, currentMonthText, GetDecimalString(keHoachDauTu.DuKienThu));
            report.I_Title.Text = string.Format("I. Cấn đối thu - chi tháng {0}", currentMonthText);
            report.I_1lab.Text = string.Format("Số dư trên các tài khoản của BHXH VN đầu ngày {0}", currentMonth.ToString("dd/MM/yyyy"));
            report.I_2lab.Text = string.Format("Dự kiến tổng số thu trong tháng {0} gồm", currentMonthText);
            report.I_4đ_lab.Text = string.Format("Cấp trước kinh phí chi BHXH, BHTN tháng {0}", currentMonthText);
            report.II_NamThangTl.Text = string.Format("II. Kết quả đầu tư tháng {0}", lastMonthText);
            report.II_rt2.Text = string.Format("Cơ cấu số dư đầu tư tính đến ngày {0} như sau: ", currentMonth.ToString("dd/MM/yyyy"));
            report.II_head_3.Text = string.Format("Số dư đến ngày {0}", lastMonth.ToString("dd/MM/yyyy"));
            report.II_head_4.Text = string.Format("Số PT tăng trong tháng {0}", lastMonthText);
            report.II_head_5.Text = string.Format("Số PT giảm trong tháng {0}", lastMonthText);
            report.II_head_6.Text = string.Format("Số dư đến {0}", currentMonth.AddDays(-1).ToString("dd/MM/yyyy"));
            report.III_NamThangTl.Text = string.Format("III. Kế hoạch đầu tư tháng {0}", currentMonthText);
            report.III_rt.Text =
                string.Format(
                    "Trên cơ sở cân đối số tiền có thể đầu tư được trong tháng, Vụ quản lý đầu tư quỹ báo cáo và đề xuất Tổng giám đốc xem xét, phê duyệt kế hoạch đầu tư trong tháng {0} như sau:",
                    currentMonthText);
            report.I_4a_lab.Text = string.Format("Chuyển kinh phí trong tháng {0}(BHXH, BHYT, BHTN)", currentMonthText);
            report.DataSource = GetListKeHoach_KhachHang(lastMonth,currentMonth);
            return PartialView("BC_KeHoachDauTu", report);
            report.objectDataSource2.DataSource = GetListKeHoach_KhachHang(lastMonth, currentMonth);
        } 
        
        private string GetDecimalString(decimal? data)
        {
            return data.HasValue ? string.Format("{0:N0}",data.Value/1000000000) : string.Empty;
        }

        private List<KeHoachDauTu_KhachHang> GetListKeHoach_KhachHang(DateTime from, DateTime to)
        {
            var data = new List<KeHoachDauTu_KhachHang> {
                new KeHoachDauTu_KhachHang
                {
                    STT = "I",
                    Ten = "Trái phiếu chính phủ",
                    SoDuDauKy = 45000,
                    SoTangTrongThang = 0,
                    SoGiamTrongThang = 1000,
                    SoDuCuoiKy = 44000,
                    TyTrong = 10
                },
                new KeHoachDauTu_KhachHang
                {
                    STT = "II",
                    Ten = "Ngân hàng thương mại"
                },
                new KeHoachDauTu_KhachHang
                {
                    STT = "1",
                    Ten = "NH TMCP Ngoại thương",
                    SoDuDauKy = 5000,
                    SoTangTrongThang = 0,
                    SoGiamTrongThang = 1000,
                    SoDuCuoiKy = 4000,
                    TyTrong = 10
                }
            };
            return data;
        }
    }
}
