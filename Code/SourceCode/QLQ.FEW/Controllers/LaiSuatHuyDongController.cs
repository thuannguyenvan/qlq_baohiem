﻿using DevExpress.Web.Mvc;
using FX.Core;
using log4net;
using QLQ.Core.CustomView;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using QLQ.Core.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.LAISUATHUYDONG)]
    [Authorize]
    public class LaiSuatHuyDongController : BaseController
    {
        private readonly IDM_NGANHANGService _iDmNganHangService;
        private readonly ILAISUATNH_DTLService _iLaiSuatNhDtlService;
        private readonly ILAISUATNH_HDRService _iLaiSuatNhHdrService;
        private readonly IDM_KYHANService _iKyHanService;
        private static readonly ILog Log = LogManager.GetLogger(typeof(LaiSuatHuyDongController));
        //public static long IdLaiSuat;
        //public static List<QLQ_LAISUATNH_DTL> LstLaiSuat;
        //
        // GET: /LaiSuatHuyDong/
        public LaiSuatHuyDongController()
        {
            _iDmNganHangService = IoC.Resolve<IDM_NGANHANGService>();
            _iLaiSuatNhDtlService = IoC.Resolve<ILAISUATNH_DTLService>();
            _iLaiSuatNhHdrService = IoC.Resolve<ILAISUATNH_HDRService>();
            _iKyHanService = IoC.Resolve<IDM_KYHANService>();

            GetFilters();
        }

        private void GetFilters()
        {
            ViewBag.ListNganHang = _iDmNganHangService.GetAll().ToList(); // lay danh sach ngan hang active
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            return View("Index");
        }

        public ActionResult ChiTiet(long id)
        {
            //var dsKyhan = _iKyHanService.GetActiveRecords().ToList(); // lay danh sach ngan hang active
            var model = _iLaiSuatNhHdrService.GetById(id);
            LaiSuatNHModel lst = new LaiSuatNHModel();
            TempData["LaiSuatNHID"] = id;
            if (model != null)
            {
                lst.laiSuat = model;

                lst.lst_LaiSuat = _iLaiSuatNhDtlService.GetByIdLaiSuatNh(id);
            }
            return PartialView("Detail", lst);

        }


        public ActionResult NhapLaiSuatPartial(LaiSuatNHModel model)
        {
            return PartialView(model);
        }
        
        //[HttpPost]
        //public ActionResult insert_LaiSuat(long idNganhang, DateTime? dateHieuLuc, string txtCvDen, DateTime? dateCvDen, string txtCv, DateTime? dateCv, string txtNh, DateTime? dateNh)
        //{
        //    try
        //    {
        //        var temp = _iLaiSuatNhHdrService.GetId(idNganhang, dateHieuLuc, txtCvDen, dateCvDen, txtCv, dateCv, txtNh, dateNh);
        //        if (temp == null)
        //        {
        //            QLQ_LAISUATNH_HDR obj = new QLQ_LAISUATNH_HDR
        //            {
        //                NGANHANG_ID = idNganhang,
        //                CREATION_DATE = DateTime.Now.Date,
        //                //CREATED_BY=nguoidung.ACCOUNT_ID,
        //                NGAY_HIEULUC = dateHieuLuc,
        //                SO_CVDEN = txtCvDen,
        //                NGAY_CVDEN = dateCvDen,
        //                SO_CVNH = txtCv,
        //                NGAY_CVNH = dateCv,
        //                THONGBAO_NH = txtNh,
        //                NGAY_THONGBAO = dateNh
        //            };
        //            _iLaiSuatNhHdrService.CreateNew(obj);
        //            _iLaiSuatNhHdrService.CommitChanges();

        //        }
        //        var temp1 = _iLaiSuatNhHdrService.GetId(idNganhang, dateHieuLuc, txtCvDen, dateCvDen, txtCv, dateCv, txtNh, dateNh);
        //        ViewBag.LaiSuatNHID = temp1.LAISUATNH_ID;
        //    }
        //    catch (Exception ex)
        //    {
        //        ViewData["EditError"] = "Có lỗi xảy ra trong quá trình xóa dữ liệu";
        //        Log.Error(DateTime.Now + " LaiSuatHuyDongController insert_LaiSuat Error: " + ex.Message);
        //    }
        //    return PartialView("Index");
        //}

        public ActionResult BatchEditingUpdateModel(MVCxGridViewBatchUpdateValues<QLQ_LAISUATNH_DTL, object> updateValues)
        {
            if (TempData["LaiSuatNHID"] == null)
                throw new Exception("Bạn truy cập từ đường link không được phép");
            long id = long.Parse(TempData["LaiSuatNHID"].ToString());
            LaiSuatNHModel lst_LaiSuatNHModel = new LaiSuatNHModel();
            try
            {
                foreach (var model in updateValues.Update)
                {
                    if (updateValues.IsValid(model))
                    {
                        if (model.LAISUATNH_DTL_ID != null)
                        {
                            //LstLaiSuat[0].LAISUATNH_DTL_ID = model.LAISUATNH_DTL_ID;
                            //trường hợp có dữ liệu từ tìm kiếm
                            _iLaiSuatNhDtlService.Save(model);
                            //_iLaiSuatNhDtlService.CommitChanges();
                        }
                        else
                        {
                            //trường hợp thêm mới dữ liệu
                            model.LAISUATNH_ID = id;
                            model.LAISUATNH_DTL_ID = _iLaiSuatNhDtlService.CreateNew(model).LAISUATNH_DTL_ID; // create new one
                        }
                    }
                }

                _iLaiSuatNhDtlService.CommitChanges(); // commit change to server
                ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
            }
            catch (Exception ex)
            {
                ViewData["EditError"] = "Có lỗi xảy ra trong quá trình sửa dữ liệu";
                Log.Error(DateTime.Now + " LaiSuatHuyDongController BatchEditingUpdateModel Error: " + ex.Message);
            }
            lst_LaiSuatNHModel.lst_LaiSuat = _iLaiSuatNhDtlService.GetByIdLaiSuatNh(id);
            TempData["LaiSuatNHID"] = id;
            return PartialView("NhapLaiSuatPartial", lst_LaiSuatNHModel);
        }

        [HttpPost]
        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult SearchFullField(long idNganhang, DateTime? dateHieuLuc, string txtCvDen, DateTime? dateCvDen, string txtCv, DateTime? dateCv, string txtNh, DateTime? dateNh)
        {
            try
            {
                var temp = _iLaiSuatNhHdrService.GetId(idNganhang, dateHieuLuc, txtCvDen, dateCvDen, txtCv, dateCv, txtNh, dateNh);
                if (temp != null)
                {
                    var model = _iLaiSuatNhDtlService.GetByIdLaiSuatNh(temp.LAISUATNH_ID);
                    return PartialView("NhapLaiSuatPartial", model);
                }
            }
            catch (Exception ex)
            {
                ViewData["EditError"] = "Có lỗi xảy ra trong quá trình tìm kiếm";
                Log.Error(DateTime.Now + " LaiSuatHuyDongController Search Error: " + ex.Message);
            }
            return PartialView("NhapLaiSuatPartial");
        }

        public ActionResult LaiSuatHuyDongPartial()
        {
            var list = _iLaiSuatNhHdrService.GetAll();
            return PartialView("LaiSuatHuyDongPartial", list);
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Search(string timkiem)
        {
            var model = _iLaiSuatNhHdrService.Search(timkiem);
            return PartialView("LaiSuatHuyDongPartial", model);
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult AddNewPartial(QLQ_LAISUATNH_HDR obj)
        {
            if (ModelState.IsValid)
            {
                //if (_iLaiSuatNhHdrService.G(obj.ma_hopdong) == null)//kiểm tra tồn tại ma_hopdong
                //{
                //thêm mới bản ghi
                try
                {
                    obj.CREATION_DATE = DateTime.Now.Date;  //ngày tạo
                    obj.CREATED_BY = CurrentUser?.AccountId;
                    _iLaiSuatNhHdrService.CreateNew(obj);
                    _iLaiSuatNhHdrService.CommitChanges();
                    ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                    //log.Error("Danh mục đơn vị quản lý - Thêm mới: " + nguoidung.TENDANGNHAP);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
                //else
                //    ViewData["EditError"] = Resources.Localizing.MessageDuplicatekey;//in ra thông báo lỗi trùng mã
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;

            return LaiSuatHuyDongPartial();
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult UpdatePartial(QLQ_LAISUATNH_HDR obj)
        {
            if (ModelState.IsValid)
            {
                //cập nhật bản ghi
                try
                {
                    QLQ_LAISUATNH_HDR temp = _iLaiSuatNhHdrService.GetById(obj.LAISUATNH_ID);
                    temp.NGANHANG_ID = obj.NGANHANG_ID;
                    temp.NGAY_CVDEN = obj.NGAY_CVDEN;
                    temp.NGAY_CVNH = obj.NGAY_CVNH;
                    temp.NGAY_HIEULUC = obj.NGAY_HIEULUC;
                    temp.NGAY_THONGBAO = obj.NGAY_THONGBAO;
                    temp.SO_CVDEN = obj.SO_CVDEN;
                    temp.SO_CVNH = obj.SO_CVNH;
                    temp.THONGBAO_NH = obj.THONGBAO_NH;

                    temp.LASTUPDATE_BY = CurrentUser?.AccountId;
                    temp.LASTUPDATED_DATE = DateTime.Now.Date;
                    _iLaiSuatNhHdrService.Clear();
                    _iLaiSuatNhHdrService.Save(temp);
                    _iLaiSuatNhHdrService.CommitChanges();
                    ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                    //log.Error("Danh mục đơn vị quản lý - Sửa: " + nguoidung.TENDANGNHAP);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return LaiSuatHuyDongPartial();
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.DELETE)]
        public ActionResult DeletePartial(QLQ_LAISUATNH_HDR obj)
        {
            //code xóa bản ghi
            try
            {
                if (_iLaiSuatNhHdrService.GetById(obj.LAISUATNH_ID) != null) // kiem tra neu ton tai
                {
                    _iLaiSuatNhHdrService.Delete(obj);
                    _iLaiSuatNhHdrService.CommitChanges();
                    ViewData["EditSuccess"] = Resources.Localizing.MessageDelete;
                    //log.Error("Danh mục đơn vị quản lý - Xóa: " + nguoidung.TENDANGNHAP);  
                }
                else
                {
                    ViewData["EditSuccess"] = Resources.Localizing.MessageDeleteExist;
                }

            }
            catch (Exception)
            {
                ViewData["EditSuccess"] = Resources.Localizing.MessageDeleteError;
            }
            return LaiSuatHuyDongPartial();
        }
    }
}
