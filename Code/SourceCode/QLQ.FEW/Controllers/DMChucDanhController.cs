﻿using FX.Core;
using log4net;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using QLQ.Core.Security;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.DM_CHUCDANH)]
    [Authorize]
    public class DMChucDanhController : BaseController
    {
        private readonly IDM_CHUCDANHService _iDmChucdanhService;
        private static readonly ILog Log = LogManager.GetLogger(typeof(DMChiTieuController));
        //
        // GET: /DMChucDanh/
        public DMChucDanhController()
        {
            _iDmChucdanhService = IoC.Resolve<IDM_CHUCDANHService>();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            return View();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult DMCHUCDANHPartial(string timkiem)
        {
            List<QLQ_DMCHUCDANH> model = new List<QLQ_DMCHUCDANH>();
            model = _iDmChucdanhService.Search(timkiem);
            return PartialView("DMCHUCDANHPartial", model);
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Search(string timkiem)
        {
            return DMCHUCDANHPartial(timkiem);
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult AddNewPartial(QLQ_DMCHUCDANH obj)
        {
            if (ModelState.IsValid)
            {
                if (_iDmChucdanhService.GetByMa(obj.ma_chucdanh) == null)//kiểm tra tồn tại MA_CHITIEU
                {
                    //thêm mới bản ghi
                    try
                    {
                        //khai báo các trường tự tự động điền
                        //obj.CREATED_BY=;  //người tạo
                        obj.creation_date = DateTime.Now.Date;  //ngày tạo
                        obj.active = obj.active == null ? 0 : obj.active;
                        _iDmChucdanhService.CreateNew(obj);
                        _iDmChucdanhService.CommitChanges();
                        ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                        //log.Error("Danh mục đơn vị quản lý - Thêm mới: " + nguoidung.TENDANGNHAP);
                    }
                    catch (Exception e)
                    {
                        ViewData["EditError"] = e.Message;
                    }
                }
                else
                    ViewData["EditError"] = Resources.Localizing.MessageDuplicatekey;//in ra thông báo lỗi trùng mã
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return DMCHUCDANHPartial(null);
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult UpdatePartial(QLQ_DMCHUCDANH obj)
        {
            if (ModelState.IsValid)
            {
                //cập nhật bản ghi
                try
                {
                    QLQ_DMCHUCDANH temp = _iDmChucdanhService.GetById(obj.chucdanh_id);
                    temp.ma_chucdanh = obj.ma_chucdanh;
                    temp.ten_chucdanh = obj.ten_chucdanh;
                    temp.active = obj.active == null ? 0 : obj.active;
                    temp.nguoidaidien = obj.nguoidaidien;
                    //temp.LASTUPDATE_BY =;   //người sửa
                    temp.lastupdated_date = DateTime.Now.Date;
                    _iDmChucdanhService.Clear();
                    _iDmChucdanhService.Save(temp);
                    _iDmChucdanhService.CommitChanges();
                    ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                    //log.Error("Danh mục đơn vị quản lý - Sửa: " + nguoidung.TENDANGNHAP);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return DMCHUCDANHPartial(null);
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.DELETE)]
        public ActionResult DeletePartial(QLQ_DMCHUCDANH obj)
        {
            //code xóa bản ghi
            try
            {
                _iDmChucdanhService.Delete(obj);
                _iDmChucdanhService.CommitChanges();
                ViewData["EditSuccess"] = Resources.Localizing.MessageDelete;
                //log.Error("Danh mục đơn vị quản lý - Xóa: " + nguoidung.TENDANGNHAP);                    
            }
            catch (Exception)
            {
                ViewData["EditError"] = Resources.Localizing.MessageDate;
            }
            return DMCHUCDANHPartial(null);
        }

    }
}
