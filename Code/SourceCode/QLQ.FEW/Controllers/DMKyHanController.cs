﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using FX.Core;
using log4net;
using QLQ.Core.Common;
using QLQ.Core.Security;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.DM_KIHAN)]
    [Authorize]
    public class DMKyHanController : BaseController
    {
        //
        // GET: /dmkyhan/
        private readonly IDM_KYHANService _iDmKyHanService;
        private static readonly ILog Log = LogManager.GetLogger(typeof(DMDauTuController));
        private GridViewCustomBindingHandlers _gridViewHanders = new GridViewCustomBindingHandlers();


        public DMKyHanController()
        {
            _iDmKyHanService = IoC.Resolve<IDM_KYHANService>();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            //GetAllPermission();
            return View("Index");
        }
        //private void GetAllPermission()
        //{
        //    string username = nguoidung.TENDANGNHAP;
        //    ViewBag.DM_DAUTU_ADD = Common.DoCheckPermission(username, "DM_DAUTU_ADD");
        //    ViewBag.DM_DAUTU_UPDATE = Common.DoCheckPermission(username, "DM_DAUTU_UPDATE");
        //    ViewBag.DM_DAUTU_DELETE = Common.DoCheckPermission(username, "DM_DAUTU_DELETE");
        //}
        public ActionResult FilterView()
        {
            return PartialView("FilterView");
        }

        public ActionResult DMKYHANPartial()
        {
            var dmKyHan = _iDmKyHanService.Query.ToList();
            return PartialView("DMKYHANPartial", dmKyHan);
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Search(string timkiem)
        {
            var model = _iDmKyHanService.Search(timkiem).ToList();
            return PartialView("DMKYHANPartial", model);
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult AddNewPartial(QLQ_DMKYHAN model)
        {
            if (ModelState.IsValid)
            {
                if (!_iDmKyHanService.CheckExistDmKyHan(model.ten_kyhan))//kiểm tra tồn tại ten_kyhan
                {
                    //thêm mới bản ghi
                    try
                    {
                        //khai báo các trường tự tự động điền
                        //obj.CREATED_BY=;  //người tạo
                        model.creation_date = DateTime.Now.Date;  //ngày tạo
                        //model.created_by = Nguoidung?.ACCOUNT_ID ?? 0;
                        model.active = model.active == null ? 0 : model.active;
                        _iDmKyHanService.CreateNew(model);
                        _iDmKyHanService.CommitChanges();
                        ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                    }
                    catch (Exception e)
                    {
                        ViewData["EditError"] = e.Message;
                    }
                }
                else
                    ViewData["EditError"] = Resources.Localizing.MessageDuplicatekey;//in ra thông báo lỗi trùng mã
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return DMKYHANPartial();
        }

        // [RBACAuthorize(Permissions = "DM_DAUTU_UPDATE")]
        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult UpdatePartial(QLQ_DMKYHAN model)
        {

            if (ModelState.IsValid)
            {
                //cập nhật bản ghi
                try
                {
                    QLQ_DMKYHAN temp = _iDmKyHanService.GetById(model.kyhan_id);
                    temp.ten_kyhan = model.ten_kyhan;
                    temp.active = model.active == null ? 0 : model.active;
                    temp.so_thang = model.so_thang;
                    temp.lastupdated_date = DateTime.Now.Date;
                    temp.lastupdate_by = CurrentUser?.AccountId ?? 0;

                    _iDmKyHanService.Clear();
                    _iDmKyHanService.Save(temp);
                    _iDmKyHanService.CommitChanges();
                    ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                    //log.Error("Danh mục đơn vị quản lý - Sửa: " + nguoidung.TENDANGNHAP);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return DMKYHANPartial();
        }

        //[RBACAuthorize(Permissions = "DM_DAUTU_DELETE")]
        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.DELETE)]
        public ActionResult DeletePartial(QLQ_DMKYHAN model)
        {
            //code xóa bản ghi
            try
            {
                _iDmKyHanService.Delete(model);
                _iDmKyHanService.CommitChanges();
                ViewData["EditSuccess"] = Resources.Localizing.MessageDelete;
            }
            catch (Exception)
            {
                ViewData["EditError"] = Resources.Localizing.MessageDate;
            }
            return DMKYHANPartial();
        }
    }
}
