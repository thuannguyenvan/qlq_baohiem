﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using FX.Core;
using log4net;
using QLQ.Core.Common;
using QLQ.Core.Security;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.DM_DAU_TU)]
    [Authorize]
    public class DMDauTuController : BaseController
    {
        //
        // GET: /dmdautu/

        //public NGUOIDUNG nguoidung;
        //public string _HT_DT;
        private readonly IDM_DAUTUService _iDmDautuService;
        private readonly IHopDongService _iDmHopDongService;
        private readonly IHS_KHACHHANGService _iDmHSKhachHangService;
        private readonly IDM_HINHTHUCDTUService _iDmHinhthucdtuService;
        private static readonly ILog Log = LogManager.GetLogger(typeof(DMDauTuController));
        private GridViewCustomBindingHandlers _gridViewHanders = new GridViewCustomBindingHandlers();


        public DMDauTuController()
        {
            _iDmDautuService = IoC.Resolve<IDM_DAUTUService>();
            _iDmHopDongService = IoC.Resolve<IHopDongService>();
            _iDmHSKhachHangService = IoC.Resolve<IHS_KHACHHANGService>();
            _iDmHinhthucdtuService = IoC.Resolve<IDM_HINHTHUCDTUService>();
            ViewData["DM_HINHTHUCDTU"] = _iDmHinhthucdtuService.Query.ToList();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            //GetAllPermission();
            return View("Index");
        }
        //private void GetAllPermission()
        //{
        //    string username = nguoidung.TENDANGNHAP;
        //    ViewBag.DM_DAUTU_ADD = Common.DoCheckPermission(username, "DM_DAUTU_ADD");
        //    ViewBag.DM_DAUTU_UPDATE = Common.DoCheckPermission(username, "DM_DAUTU_UPDATE");
        //    ViewBag.DM_DAUTU_DELETE = Common.DoCheckPermission(username, "DM_DAUTU_DELETE");
        //}
        public ActionResult FilterView()
        {
            return PartialView("FilterView");
        }

        public ActionResult DMDAUTUPartial()
        {
            var a = _iDmHinhthucdtuService.GetAll();
            var dmdautu = _iDmDautuService.Query;
            return PartialView("DMDAUTUPartial", dmdautu);

            //List<QLQ_DMDAUTU> model = new List<QLQ_DMDAUTU>();
            //model = _iDM_DAUTUService.Search(timkiem);
            //return PartialView("DMDAUTUPartial", model);
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Search(string timkiem)
        {
            List<QLQ_DMDAUTU> model = new List<QLQ_DMDAUTU>();
            model = _iDmDautuService.Search(timkiem);
            return PartialView("DMDAUTUPartial", model);
        }

        //[RBACAuthorize(Permissions = "DM_DAUTU_ADD")]
        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult AddNewPartial(QLQ_DMDAUTU dmdautu)
        {
            if (ModelState.IsValid)
            {
                if (_iDmDautuService.GetByMa(dmdautu.ma_dautu) == null)//kiểm tra tồn tại ma_dautu
                {
                    //thêm mới bản ghi
                    try
                    {
                        //khai báo các trường tự tự động điền
                        //obj.CREATED_BY=;  //người tạo
                        dmdautu.creation_date = DateTime.Now.Date;  //ngày tạo
                        dmdautu.active = dmdautu.active == null ? 0 : dmdautu.active;
                        _iDmDautuService.CreateNew(dmdautu);
                        _iDmDautuService.CommitChanges();
                        ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                        //log.Error("Danh mục đơn vị quản lý - Thêm mới: " + nguoidung.TENDANGNHAP);
                    }
                    catch (Exception e)
                    {
                        ViewData["EditError"] = e.Message;
                    }
                }
                else
                    ViewData["EditError"] = Resources.Localizing.MessageDuplicatekey;//in ra thông báo lỗi trùng mã
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return DMDAUTUPartial();
        }

        // [RBACAuthorize(Permissions = "DM_DAUTU_UPDATE")]
        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.EDIT)]
        public ActionResult UpdatePartial(QLQ_DMDAUTU dmdautu)
        {

            if (ModelState.IsValid)
            {
                //cập nhật bản ghi
                try
                {
                    QLQ_DMDAUTU temp = _iDmDautuService.GetById(dmdautu.dautu_id);
                    temp.ma_dautu = dmdautu.ma_dautu;
                    temp.ten_dautu = dmdautu.ten_dautu;
                    temp.active = dmdautu.active == null ? 0 : dmdautu.active;
                    //temp.LASTUPDATE_BY =;   //người sửa
                    temp.lastupdated_date = DateTime.Now.Date;
                    _iDmDautuService.Clear();
                    _iDmDautuService.Save(temp);
                    _iDmDautuService.CommitChanges();
                    _iDmHinhthucdtuService.CommitChanges();
                    ViewData["EditSuccess"] = Resources.Localizing.MessageSuccess;
                    //log.Error("Danh mục đơn vị quản lý - Sửa: " + nguoidung.TENDANGNHAP);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = Resources.Localizing.MessageInsert;
            return DMDAUTUPartial();
        }

        //[RBACAuthorize(Permissions = "DM_DAUTU_DELETE")]
        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.DELETE)]
        public ActionResult DeletePartial(QLQ_DMDAUTU dmdautu)
        {

            //code xóa bản ghi
            try
            {
                //long l = (long)Convert.ToInt64(dmdautu.dautu_id);
                var dauTuId = dmdautu.dautu_id ?? 0;
                if (!_iDmHopDongService.CheckExistDM_Dautu(dauTuId) && !_iDmHSKhachHangService.CheckExistDM_Dautu(dauTuId))
                {
                    _iDmDautuService.Delete(dmdautu);
                    _iDmDautuService.CommitChanges();
                    ViewData["EditSuccess"] = Resources.Localizing.MessageDelete;
                    //log.Error("Danh mục đơn vị quản lý - Xóa: " + nguoidung.TENDANGNHAP);        
                }
                else
                {
                    ViewData["EditSuccess"] = Resources.Localizing.MessageDeleteExist;
                }

            }
            catch (Exception)
            {
                ViewData["EditError"] = Resources.Localizing.MessageDate;
            }
            return DMDAUTUPartial();
        }
    }
}
