﻿using FX.Core;
using QLQ.Core.CustomView;
using QLQ.Core.IService;
using QLQ.Core.Security;
using QLQ.Core.StoreProcedure;
using QLQ.FEW.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.TH_SODUDAUTUDENHAN)]
    [Authorize]
    public class SoDuDauTuDenHanController : BaseController
    {
        private readonly IDM_DAUTUService _iDmDautuService;
        private readonly IHS_KHACHHANGService _iHsKhachhangService;
        private readonly IHopDongService _iHopDongService;

        public SoDuDauTuDenHanController()
        {
            _iHopDongService = IoC.Resolve<IHopDongService>();
            _iHsKhachhangService = IoC.Resolve<IHS_KHACHHANGService>();
            _iDmDautuService = IoC.Resolve<IDM_DAUTUService>();
            GetFilterData();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SoDuDauTuDenHanPartial()
        {
            var now = DateTime.Now;
            var startOfMonth = new DateTime(now.Year, now.Month, 1);
            var month = now.Month + 1;
            var year = now.Year;
            if (now.Month == 12)
            {
                month = 1;
                year++;
            }
            var endOfMonth = new DateTime(year, month, 1).AddDays(-1);

            var request = new GetSoDuDauTuRequest() { ToDateStr = endOfMonth.ToString("dd/MM/yyyy"), FromDateStr = startOfMonth.ToString("dd/MM/yyyy") };
            return PartialView("SoDuDauTuDenHanPartial", _iHopDongService.GetSoDuDauTuDenHan(request));
        }

        public ActionResult ComboBoxKhachHangPartial()
        {
            long dauTuId = 0;
            long.TryParse(Request.Params["cbDauTuId"], out dauTuId);
            var listKhachHang = _iHsKhachhangService.Query
                .Where(i => i.DAUTU_ID == dauTuId)
                .Select(i => new CommonValue { Text = i.TEN_KHACHHANG, Value = i.KHACHHANG_ID.ToString() })
                .OrderBy(i => i.Text)
                .ToList();
            listKhachHang.Insert(0, new CommonValue { Value = "0", Text = "Chọn" });
            return PartialView("ComboBoxKhachHangPartial", listKhachHang);
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Search(GetSoDuDauTuRequest request)
        {
            return PartialView("SoDuDauTuDenHanPartial", _iHopDongService.GetSoDuDauTuDenHan(request));
        }

        private void GetFilterData()
        {
            var listDauTu = _iDmDautuService.GetAll()
                .OrderBy(i => i.ten_dautu)
                .Select(i => new CommonValue { Text = i.ten_dautu, Value = i.dautu_id.ToString() })
                .ToList();
            listDauTu.Insert(0, new CommonValue { Value = "0", Text = "Chọn" });
            ViewBag.ListDauTu = listDauTu;
        }
    }
}
