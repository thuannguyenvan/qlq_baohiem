﻿using FX.Core;
using QLQ.Core.CustomView;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using QLQ.Core.Security;
using QLQ.Core.StoreProcedure;
using QLQ.FEW.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.TH_SODUDAUTUCHITIET)]
    [Authorize]
    public class SoDuDauTuChiTietController : BaseController
    {
        private readonly IDM_DAUTUService _iDmDautuService;
        private readonly IHS_KHACHHANGService _iHsKhachhangService;
        private readonly IHopDongService _iHopDongService;

        public SoDuDauTuChiTietController()
        {
            _iHopDongService = IoC.Resolve<IHopDongService>();
            _iHsKhachhangService = IoC.Resolve<IHS_KHACHHANGService>();
            _iDmDautuService = IoC.Resolve<IDM_DAUTUService>();
            GetFilterData();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SoDuDauTuChiTietPartial()
        {
            return PartialView("SoDuDauTuChiTietPartial", _iHopDongService.GetSoDuDauTuChiTiet(new GetSoDuDauTuRequest()));
        }

        public ActionResult ComboBoxKhachHangPartial()
        {
            long dauTuId = 0;
            long.TryParse(Request.Params["cbDauTuId"], out dauTuId);
            var listKhachHang = _iHsKhachhangService.Query
                .Where(i => i.DAUTU_ID == dauTuId)
                .Select(i => new CommonValue { Text = i.TEN_KHACHHANG, Value = i.KHACHHANG_ID.ToString() })
                .OrderBy(i => i.Text)
                .ToList();
            listKhachHang.Insert(0, new CommonValue { Value = "0", Text = "Chọn" });
            return PartialView("ComboBoxKhachHangPartial", listKhachHang);
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Search(GetSoDuDauTuRequest request)
        {
            return PartialView("SoDuDauTuChiTietPartial", _iHopDongService.GetSoDuDauTuChiTiet(request));
        }

        private void GetFilterData()
        {
            var listDauTu = _iDmDautuService.GetAll()
                .OrderBy(i => i.ten_dautu)
                .Select(i => new CommonValue { Text = i.ten_dautu, Value = i.dautu_id.ToString() })
                .ToList();
            listDauTu.Insert(0, new CommonValue { Value = "0", Text = "Chọn" });
            ViewBag.ListDauTu = listDauTu;
        }
    }
}
