﻿
using FX.Core;
using log4net;
using QLQ.Core.Domain;
using QLQ.Core.IService;
using QLQ.Core.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.HOPDONGQUAHAN)]
    [Authorize]
    public class HDQuaHanController : BaseController
    {
        public readonly IHopDongService _iHopDongService;
        public readonly IHS_KHACHHANGService _iKHACHHANGService;
        public readonly IDM_HINHTHUCDTUService _iDM_HINHTHUCDTUService;
        public readonly IDM_DAUTUService _iDM_DAUTUService;
        private readonly IHS_KHACHHANGService _iHS_KHACHHANGService;
        private static readonly ILog log = LogManager.GetLogger(typeof(DMNGANHANGController));
        private readonly IDM_HOPDONGService _iDM_HOPDONGService;
        private List<QLQ_DMDAUTU> listDMDauTu = new List<QLQ_DMDAUTU>();
        private List<QLQ_DMHINHTHUCDTU> listDMHinhThuCDT = new List<QLQ_DMHINHTHUCDTU>();
        private List<QLQ_DMHOPDONG> listDMHopDong = new List<QLQ_DMHOPDONG>();
        private List<HS_KHACHHANG> listHSKhachhang = new List<HS_KHACHHANG>();

        enum GocLaiValue
        {
            TatCa = 0,
            Goc = 1,
            Lai = 2
        }
        //TODO tobe refactor
        class DropdownValue
        {
            public string Name { get; set; }
            public GocLaiValue Value { get; set; }
        }
        public HDQuaHanController()
        {
            _iHopDongService = IoC.Resolve<IHopDongService>();
            _iDM_DAUTUService = IoC.Resolve<IDM_DAUTUService>();

            _iDM_HOPDONGService = IoC.Resolve<IDM_HOPDONGService>();
            _iHS_KHACHHANGService = IoC.Resolve<IHS_KHACHHANGService>();
            _iDM_HINHTHUCDTUService = IoC.Resolve<IDM_HINHTHUCDTUService>();
            _iKHACHHANGService = IoC.Resolve<IHS_KHACHHANGService>();
            GetCBFilter();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            return View();
        }

        public void GetCBFilter()
        {
            string chon = "Chọn";
            ViewData["GocLai"] = new List<DropdownValue>
            {
                new DropdownValue { Name = "Tất cả", Value = GocLaiValue.TatCa },
                new DropdownValue { Name = "Gốc", Value = GocLaiValue.Goc },
                new DropdownValue { Name = "Lãi", Value = GocLaiValue.Lai },
            };
            var _listDmDauTu = _iDM_DAUTUService.GetAll();
            _listDmDauTu.Insert(0, new QLQ_DMDAUTU { dautu_id = 0, ten_dautu = chon });
            ViewData["ListDauTu"] = _listDmDauTu;
            var _listDmHopDong = _iDM_HOPDONGService.GetAll();
            _listDmHopDong.Insert(0, new QLQ_DMHOPDONG { hopdong_id = 0, ten_hopdong = chon });
            ViewData["ListHopDong"] = _listDmHopDong;
            ViewData["ListHopDong1"] = _iDM_HOPDONGService.GetAll();
        }

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult HDQuaHanPartial(byte? gocLai, long? dauTuId, long? hopDongId, DateTime? denNgay)
        {
            var query = _iHopDongService.Query;
            gocLai = gocLai ?? (byte)GocLaiValue.TatCa;

            if (dauTuId.HasValue && dauTuId.Value > 0)
            {
                query = query.Where(s => s.DAUTU_ID == dauTuId);
            }
            if (hopDongId.HasValue && hopDongId.Value > 0)
            {
                query = query.Where(s => s.hopdong_id == hopDongId);
            }
            query = query.Where(s => s.IsDeleted == 0);
            denNgay = denNgay ?? DateTime.Today;
            var model = query.ToList();
            foreach (var hd in model)
            {
                hd.DenNgay = denNgay.Value;
            }
            return PartialView("HDQuaHanPartial", model);
        }

        [HttpPost, ValidateInput(false)]
        [ActionPermissions(QlqActions.DELETE)]
        public ActionResult DeletePartial(QLQ_HOPDONG obj)
        {
            try
            {
                _iHopDongService.SoftDelete(obj.qlhopdong_id.Value);
                _iHopDongService.CommitChanges();
                ViewData["EditSuccess"] = Resources.Localizing.MessageDelete;
            }
            catch (Exception)
            {
                ViewData["EditError"] = Resources.Localizing.MessageDate;
            }
            return HDQuaHanPartial(null, null, null ,null);
        }
    }
}
