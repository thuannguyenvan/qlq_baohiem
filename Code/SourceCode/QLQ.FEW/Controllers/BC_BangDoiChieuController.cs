﻿using QLQ.Core.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QLQ.FEW.Controllers
{
    [Function(QlqFunctions.BC_BANGDOICHIEU)]
    [Authorize]
    public class BC_BangDoiChieuController : BaseController
    {
        //
        // GET: /BC_BangDoiChieu/

        [ActionPermissions(QlqActions.VIEW)]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult BC_BangDoiChieuPartial()
        {
            return PartialView("BC_BangDoiChieuPartial");
        }

        [ActionPermissions(QlqActions.PRINT)]
        public ActionResult fn_InAn(int year, int month)
        {
            string namthang = new DateTime(year, month, 1).ToString("MM/yyyy");
            BC_BangDoiChieu rp = new BC_BangDoiChieu();
            rp.title_nam.Text = namthang;
            rp.DataSource = null;
            return PartialView("BC_BangDoiChieu", rp);
        }
    }
}
